﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=9.85
@EndOfDesignText@
#Region Shared Files
#CustomBuildAction: folders ready, %WINDIR%\System32\Robocopy.exe,"..\..\Shared Files" "..\Files"
'Ctrl + click to sync files: ide://run?file=%WINDIR%\System32\Robocopy.exe&args=..\..\Shared+Files&args=..\Files&FilesSync=True
#End Region

'Ctrl + click to export as zip: ide://run?File=%B4X%\Zipper.jar&Args=SampleHttpPages.zip

Sub Class_Globals
	Private Root As B4XView
	Private xui As XUI
	
	Private Label1 As Label
	Private LabelUpload As Label
	Private ImageView1 As B4XView
	Private Svr As httpServer
	Private CounterConenction As Int = 0
	Private WebView1 As WebView
	Private Button1 As Button
End Sub

Public Sub Initialize
	
End Sub

'This event will be called once, before the page becomes visible.
Private Sub B4XPage_Created (Root1 As B4XView)
	Root = Root1
	Root.LoadLayout("MainPage")

	Svr.Initialize(Me,"Svr")
	Svr.Start(51051)
	Label1.Text=$"Server ip: ${Svr.GetMyWifiIp} ${51051}"$
	
	Dim Unzip As Archiver
	File.Copy(File.DirAssets,"www.zip",Svr.TempPath,"www.zip")
	Unzip.Unzip(Svr.TempPath,"www.zip",Svr.TempPath,"")
End Sub

'You can see the list of page related events in the B4XPagesManager object. The event name is B4XPage.

Private Sub Button1_Click
	WebView1.LoadUrl("http://127.0.0.1:51051/index.html")
End Sub


Private Sub Svr_NewConection(req As ServletRequest)
	CounterConenction=CounterConenction+1
	Log("New connection: " & req.RemoteAddress & " Counter: " & CounterConenction)
	req.ID=CounterConenction
End Sub

Private Sub Svr_Handle(req As ServletRequest, resp As ServletResponse)
	Dim fn As String = req.GetRequestURI '.ToLowerCase
	Log("Request URI: " & fn)
	
	Select req.GetMethod
		Case "GET"
			If fn = "/" Then fn = "/index.html"
			If File.Exists(Svr.TempPath,fn ) Then
				' other file
				resp.SendFile(Svr.TempPath,fn )
			Else if req.GetRequestURI="/hello" Then
				DinamicPage(req ,resp)
			Else
				'not found
				resp.SendNotFound(fn)
			End If
		Case "POST"
			If req.ContentType="multipart/form-data" Then
				' Upload
				If req.MultipartFilename.Size>0 Then
					For Each filename As String In req.MultipartFilename.Keys
						Log("Start uploading: " & filename)
					Next
				End If
			Else
				' Post form
				Dim Field As String = ""
				For Each Param As String In req.ParameterMap.Keys
				Field=$"${Field}${Param}=${req.ParameterMap.Get(Param)}<BR>"$
				Next
				resp.SendString(Field & $"<B>Recorded data</B><BR><a href="/index.html">Back</a>"$)
			End If
	End Select
End Sub

Private Sub Svr_UploadProgress (resp As ServletResponse, Progress As Float)
	LabelUpload.Text=$"Download $1.0{Progress*100}%"$
End Sub

Private Sub Svr_UploadedFile (req As ServletRequest, resp As ServletResponse)
	Dim tempfilename As String
	Dim OriginaleFileName As String
	If req.MultipartFilename.Size>0 Then
		For Each Filename As String In req.MultipartFilename.Keys
			tempfilename=req.MultipartFilename.Get(Filename)
			OriginaleFileName=Filename
		Next
	End If
	
	
	If OriginaleFileName.ToLowerCase.EndsWith("png") Or OriginaleFileName.ToLowerCase.EndsWith("jpg")Then
		'file image
		'File.copy(File.DirTemp,tempfilename,File.DirDocuments,OriginaleFileName)
		ImageView1.SetBitmap(xui.LoadBitmap(Svr.TempPath,tempfilename))
		resp.Status=200
		resp.SendString($"<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>UPload</title>
</head>

<body id="body">
<B>Download: ${Filename}</B><BR>
<img src="${tempfilename}"><BR>
<a href="/fileupload.html">Back To Upload</a><BR>
<a href="/index.html">Back To Home</a>
</body>
</html>"$)
	Else
		' generic file
		resp.Status=200
		resp.SendString($"<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>UPload</title>
</head>

<body id="body">
<B>Download: ${Filename}</B><BR>
<a href="/fileupload.html">Back to Upload</a><BR>
<a href="/index.html">Back to Home</a>
</body>
</html>"$)
	End If
	
	Log("Download: " & Filename)
End Sub

Private Sub DinamicPage(req As ServletRequest,resp As ServletResponse)
	resp.ContentType = "text/html"
	resp.SendString($"<img src='logo.png'/ width=100 height=100><br/>
<b>Hello world!!!</b><br/>
Your ip address is: ${req.RemoteAddress}<br/>
The time here is: ${DateTime.Date(DateTime.Now)} ${DateTime.Time(DateTime.Now)}<br/>
<a href='/'>Back</a>"$)
End Sub
