object Form1: TForm1
  Width = 640
  Height = 480
  object WebLabel1: TWebLabel
    Left = 152
    Top = 144
    Width = 58
    Height = 15
    Caption = 'WebLabel1'
    HeightPercent = 100.000000000000000000
    WidthPercent = 100.000000000000000000
  end
  object WebButton1: TWebButton
    Left = 136
    Top = 40
    Width = 96
    Height = 25
    Caption = 'WebButton1'
    HeightPercent = 100.000000000000000000
    WidthPercent = 100.000000000000000000
    OnClick = WebButton1Click
  end
  object WebButton2: TWebButton
    Left = 136
    Top = 71
    Width = 96
    Height = 25
    Caption = 'Socket'
    ChildOrder = 1
    HeightPercent = 100.000000000000000000
    WidthPercent = 100.000000000000000000
    OnClick = WebButton2Click
  end
  object WebHttpRequest1: TWebHttpRequest
    Headers.Strings = (
      'Cache-Control=no-cache, no-store, must-revalidate')
    URL = 'http://localhost:51051/test'
    Left = 304
    Top = 224
  end
  object WebSocketClient1: TWebSocketClient
    Port = 51051
    HostName = 'localhost'
    PathName = '/foo'
    OnConnect = WebSocketClient1Connect
    OnDataReceived = WebSocketClient1DataReceived
    Left = 296
    Top = 304
  end
end
