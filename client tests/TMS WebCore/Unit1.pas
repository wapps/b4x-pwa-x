unit Unit1;

interface

uses
  System.SysUtils, System.Classes, JS, Web, WEBLib.Graphics, WEBLib.Controls,
  WEBLib.Forms, WEBLib.Dialogs, WEBLib.REST, Vcl.Controls, Vcl.StdCtrls,
  WEBLib.StdCtrls, WEBLib.WebSocketClient;

type
  TForm1 = class(TWebForm)
    WebButton1: TWebButton;
    WebHttpRequest1: TWebHttpRequest;
    WebSocketClient1: TWebSocketClient;
    WebButton2: TWebButton;
    WebLabel1: TWebLabel;
    procedure WebButton1Click(Sender: TObject);
    procedure WebButton2Click(Sender: TObject);
    procedure WebSocketClient1Connect(Sender: TObject);
    procedure WebSocketClient1DataReceived(Sender: TObject; Origin: string;
      SocketData: TJSObjectRecord);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.WebButton1Click(Sender: TObject);
  procedure Response (AResponse: string; ARequest: TJSXMLHttpRequest);
  begin
    Showmessage (AResponse);
  end;

begin
  WebHttpRequest1.Execute(@Response);
end;

procedure TForm1.WebButton2Click(Sender: TObject);
begin
  WebSocketClient1.Connect;
end;

procedure TForm1.WebSocketClient1Connect(Sender: TObject);
begin
  WebSocketClient1.Send('Hello world');
end;

procedure TForm1.WebSocketClient1DataReceived(Sender: TObject; Origin: string;
  SocketData: TJSObjectRecord);
begin
  WebLabel1.Caption := SocketData.jsobject.toLocaleString;
end;

end.