﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=8.9
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private LastEvent As String = ""
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	
	iQuery.AutomaticEvents=iQuery.NoEvent
	iQuery.RunMethodWithResult("width","#cvs",Null)
	LastEvent="width"
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	If LastEvent="width" Then
		resp.Query.RunMethodWithResult("height","#cvs",Null)
		LastEvent="height"
	Else
		LastEvent=""
		resp.Query.SetCSS("body",Array As String("background-color","#9eebf8"))
		For i=20 To 300 Step 10
			resp.Query.RunFunction("ClearRect","", Array As String(i,i,90,90))
			resp.Query.RunFunction("DrawImage","", Array As String(i+10,i+20))
			Sleep(300)
		Next
	End If
End Sub