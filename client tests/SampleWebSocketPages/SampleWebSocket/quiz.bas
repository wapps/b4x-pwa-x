﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=8.9
@EndOfDesignText@
'Static code module
Sub Process_Globals

End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	
	iQuery.AutomaticEvents=iQuery.CreateEvent("btnnext",iQuery.Event_click,iQuery.CreateEvent("btnprev",iQuery.Event_click,iQuery.CreateEvent("questions_div",iQuery.Event_change,Null)))
	UpdateQuiz(resp)
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Dim M As Map=req.GetWebSocketMapData
	
	Select M.Get("type")
		Case "event"
			Dim Params As Map = M.Get("params")
			#if B4A
				'callsub(Me,....) don't work for b4A in CodeModule
				Select M.Get("event")
					Case "questions_div_change"
						questions_div_change(resp,Params)
					Case "btnnext_click"
						btnnext_click(resp,Params)
				End Select
			#else
				CallSub3(Me,M.Get("event"),resp,Params)
			#End If
		Case "data"
			Log(M.Get("data"))
	End Select
End Sub

#Region Function

Private Sub UpdateQuiz(resp As ServletResponse)
	Dim Questions As Map = CreateMap("<i>Question <B>#1</B>:</i> 1 * 12 ?":Array As String("16","24","12","0"),"<i>Question #2:</i> 8 *3 ?":Array As String("16","24","12","0"),"<i>Question #3:</i> 6 *4 ?":Array As String("16","24","12","0"))
	Dim QuizN As Int = Rnd(1,Questions.Size+1)
	
	
	For Each Key As String In Questions.Keys
		QuizN=QuizN-1
		If QuizN=0 Then
			Dim Question As String= Key
			Dim Solution() As String = Questions.Get(Key)
		End If
	Next
	
	
	For i=0 To 3
		resp.Query.SetHtml($"label[for=a${i}]"$,Array As String($"${Solution(i)}"$))
		resp.Query.SetPropriety("#a" & I,Array As Map(CreateMap("checked":False)))
	Next

	resp.Query.SetPropriety("#btnprev",Array As Map(CreateMap("disabled":True)))
	resp.Query.SetPropriety("#btnnext",Array As Map(CreateMap("disabled":True)))
	resp.Query.SetHtml("#question", Array As String(Question))
End Sub

#End Region

#region Event

' -------------------------------------------- QUIZ -------------------------------------------

Private Sub questions_div_change (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
	Resp.Query.SetPropriety("#btnnext",Array As Object("disabled",False))
End Sub

Private Sub btnnext_click (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
	UpdateQuiz(Resp)
End Sub


#End Region