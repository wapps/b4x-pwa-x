﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=9
@EndOfDesignText@
 'Static code module
Sub Process_Globals

End Sub

Public Sub Svr_Handle (req As ServletRequest, resp As ServletResponse)
	' if Post has data content
	Dim parser As JSONParser
	Dim code As StringBuilder
	code.Initialize
	
	resp.ContentType = "application/json"
	code.Append("Dim parser As JSONParser").Append(" <br>")
	code.Append("parser.Initialize(&lt;text&gt;)").Append(" <br>")
	code.Append("Dim root As Map = parser.NextObject").Append(" <br>")
	code.Append("Dim json As String = root.Get(""json"") <br>")
	For Each DataRow As String In req.RequestPostDataRow
		Log(DataRow)
		Try
			parser.Initialize(DataRow)
			Dim JsonText As StringBuilder
			JsonText.Initialize
			JsonText.Append("<ul>")
			For Each k As String In parser.NextObject.Values
				JsonText.Append($"<li>json: ${k}</li>"$)
			Next
			JsonText.Append("</ul>")
			Log(JsonText.ToString)
			
			Dim JsonResp As JSONGenerator
			JsonResp.Initialize(CreateMap("code":code,"success":True,"tree":JsonText.ToString))
		Catch
			Log(LastException.Message)
			Dim JsonResp As JSONGenerator
			JsonResp.Initialize(CreateMap("code":code,"success":False,"error": "Error parsing string:" & CRLF & LastException.Message))
		End Try
	Next
	resp.SendString(JsonResp.ToString)
End Sub