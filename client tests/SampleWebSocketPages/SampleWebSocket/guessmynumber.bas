﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=8.9
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private LastEvent As String = ""
	Private SecreNumber As Int = 5
	Public ListTimer As List
	
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	
	iQuery.AutomaticEvents=iQuery.CreateEvent("btnguess",iQuery.Event_click,iQuery.CreateEvent("txtnumber","keyup",iQuery.CreateEvent("btnreset",iQuery.Event_click,Null)))
	
	ListTimer.Add(resp)
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Dim M As Map=req.GetWebSocketMapData
	
	Select M.Get("type")
		Case "event"
			LastEvent=M.Get("event")
			Dim Params As Map = M.Get("params")
			#if b4a
				'callsub(Me,....) don't work for b4A in CodeModule
				Select M.Get("event")
					Case "btnguess_click"
						btnguess_Click(resp,Params)
					Case "btnreset_click"
						btnreset_Click(resp,Params)
					Case "txtnumber_keyup"
						txtnumber_keyup(resp,Params)
				End Select
			#else
				CallSub3(Me,M.Get("event"),resp,Params)
			#End If
		Case "data"
			If LastEvent="txtnumber_keyup" Then resp.Query.SetHtml("#result",Array As String ("You are writing " & M.Get("data")))
			If LastEvent="btnguess_click" Then
				If SecreNumber>M.Get("data") Then
					resp.Query.SetHtml("#result",Array As String ("My number is <b>larger</b> of " & M.Get("data")))
				else if SecreNumber<M.Get("data") Then
					resp.Query.SetHtml("#result",Array As String ("My number is <b>smaller</b> of " & M.Get("data")))
				Else
					resp.Query.SetHtml("#result",Array As String ("You guessed " & M.Get("data")))
				End If
				
			End If
	End Select
End Sub


#Region Query Event 

Private Sub btnguess_Click(Resp As ServletResponse,Params As Map)
	Resp.Query.RunMethodWithResult("val","#txtnumber",Null)
End Sub

Private Sub btnreset_Click(Resp As ServletResponse,Params As Map)
	SecreNumber=Rnd(1,21)
End Sub

Private Sub txtnumber_keyup(Resp As ServletResponse,Params As Map)
	Resp.Query.RunMethodWithResult("val","#txtnumber",Null)
End Sub

#End Region
