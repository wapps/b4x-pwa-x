﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=9
@EndOfDesignText@
Sub Process_Globals
	Private Student As List
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	acquireStudentList

	iQuery.AutomaticEvents=iQuery.CreateEvent("lststudentsid",iQuery.Event_change,iQuery.CreateEvent("lsttests",iQuery.Event_change,iQuery.CreateEvent("btnsetgrade",iQuery.Event_click,Null)))
	iQuery.SetHtml("#lststudentsid",Array As String(StudentList))
	iQuery.RunMethod("dataTable","#tblfailedtests",Array As Map(CreateMap("bPaginate":False,"bFilter":False,"bSort":False,"sScrollY":"200px")))
	iQuery.RunMethod("dataTable","#tblstudents",Array As Map(readDataStudent))
	
	iQuery.RunFunction("addSelectionToTable","",Array As String("#tblstudents","TableView1_SelectedRowChanged"))
	iQuery.RunMethodWithResult("val","#lststudentsid",Null)
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Private outQuery As QueryElement = resp.Query
	Select req.GetWebSocketMapData.Get("type")
		Case "data"
			Dim Count As Int = 0
			Dim id As String = req.GetWebSocketMapData.Get("data") ' id student
			Dim row As Int = 0
			'find sudent
			For Each std As Map In Student
				If id=std.Get("id") Then 
					outQuery.SetText("#lblstudentname",Array As String(std.Get("Name") & " " & std.Get("Surname")))
					outQuery.SetText("#lblbirthday",Array As String(std.Get("bir")))
					row=Count
				End If
				Count=Count+1
			Next
			
			outQuery.SetHtml("#lsttests",Array As String("<option value='Test #01'>Test #01</option><option value='Test #02'>Test #02</option><option value='Test #03'>Test #03</option><option value='Test #04'>Test #04</option><option value='Test #05'>Test #05</option><option value='Test #06'>Test #06</option><option value='Test #07'>Test #07</option><option value='Test #08'>Test #08</option><option value='Test #09'>Test #09</option><option value='Test #10'>Test #10</option><option value='Test #11'>Test #11</option><option value='Test #12'>Test #12</option><option value='Test #13'>Test #13</option><option value='Test #14'>Test #14</option><option value='Test #15'>Test #15</option><option value='Test #16'>Test #16</option><option value='Test #17'>Test #17</option><option value='Test #18'>Test #18</option><option value='Test #19'>Test #19</option><option value='Test #20'>Test #20</option>"))
			outQuery.RunMethod("val","#txtgrade",Array As String("21"))
			outQuery.Eval("$(arguments[0]).dataTable().fnClearTable()",Array As String("#tblfailedtests"))
	
			' this can load from DB 
			' public test result
			outQuery.Eval("$(arguments[0]).dataTable().fnAddData(arguments[1])", Array As Object("#tblfailedtests", _
			Array As Object(Array As String("Test #01",Rnd(20,51)),Array As String("Test #04",Rnd(20,51)),Array As String("Test #05",Rnd(20,51)),Array As String("Test #09",Rnd(20,51)))))
	
			outQuery.RunFunction("setSelectedRow","",Array As String("#tblstudents", row))
		Case "event"
			' if click event request data
			If req.GetWebSocketMapData.Get("event")="lststudentsid_change" Then
				'click on list
				outQuery.RunMethodWithResult("val","#lststudentsid",Null)
			else If req.GetWebSocketMapData.Get("event")="tableview1_selectedrowchanged" Then
				' click on table
				Dim param As Map = req.GetWebSocketMapData.Get("params")
				Dim row As Int = 0
				'find sudent
				For Each std As Map In Student
					If row=param.Get("row") Then
						'{"method":"val","etype":"runmethod","id":"#lststudentsid","params":["020169"]}
						outQuery.RunMethod("val","#lststudentsid", Array As String(std.Get("id")))
						outQuery.SetText("#lblstudentname",Array As String(std.Get("Name") & " " & std.Get("Surname")))
						outQuery.SetText("#lblbirthday",Array As String(std.Get("bir")))
					End If
					row=row+1
				Next
				
				outQuery.SetHtml("#lsttests",Array As String("<option value='Test #01'>Test #01</option><option value='Test #02'>Test #02</option><option value='Test #03'>Test #03</option><option value='Test #04'>Test #04</option><option value='Test #05'>Test #05</option><option value='Test #06'>Test #06</option><option value='Test #07'>Test #07</option><option value='Test #08'>Test #08</option><option value='Test #09'>Test #09</option><option value='Test #10'>Test #10</option><option value='Test #11'>Test #11</option><option value='Test #12'>Test #12</option><option value='Test #13'>Test #13</option><option value='Test #14'>Test #14</option><option value='Test #15'>Test #15</option><option value='Test #16'>Test #16</option><option value='Test #17'>Test #17</option><option value='Test #18'>Test #18</option><option value='Test #19'>Test #19</option><option value='Test #20'>Test #20</option>"))
				outQuery.RunMethod("val","#txtgrade",Array As String("21"))
				outQuery.Eval("$(arguments[0]).dataTable().fnClearTable()",Array As String("#tblfailedtests"))
	
				' this can load from DB
				' public test result
				outQuery.Eval("$(arguments[0]).dataTable().fnAddData(arguments[1])", Array As Object("#tblfailedtests", _
			Array As Object(Array As String("Test #01",Rnd(20,51)),Array As String("Test #04",Rnd(20,51)),Array As String("Test #05",Rnd(20,51)),Array As String("Test #09",Rnd(20,51)))))
	
				outQuery.RunFunction("setSelectedRow","",Array As String("#tblstudents", row))
			End If
			
			
	End Select
End Sub

Private Sub acquireStudentList
	' Or read from DB
	Student.Initialize
	For i=1 To 20
		Student.Add(CreateMap("id":$"Std_${Rnd(100,900)}"$,"Name":"Name " & i,"Surname":"Surn" & I,"bir":$"$2.0{Rnd(1,29)}/$2.0{Rnd(1,13)}/${Rnd(1970,2011)}"$))
	Next
End Sub

Private Sub StudentList As String
	Private functionQuery As QueryElement

	functionQuery.Initialize(Null) ' only for EscpateHtm method
	Dim sb As StringBuilder
	sb.Initialize
	For Each std As Map In Student
		Dim val As String = functionQuery.EscapeHtml(std.Get("id"))
		sb.Append($"<option value='${val}'>${val}</option>"$)
	Next
	Return sb.ToString
End Sub

Private Sub readDataStudent As Map
	' Or read from DB
	Dim M As Map = CreateMap("bPaginate":True)
	Dim L(Student.Size) As Object
	Dim Count As Int = 0
	
	For Each std As Map In Student
		Dim Fields(4) As String
		Fields(0)=std.Get("id")
		Fields(1)=std.Get("Name")
		Fields(2)=std.Get("Surname")
		Fields(3)=std.Get("bir")
		L(Count)=Fields
		Count=Count+1
	Next
	
	M.Put("aaData",L)
	
	Return M
End Sub

