﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=9.85
@EndOfDesignText@
#Region Shared Files
#CustomBuildAction: folders ready, %WINDIR%\System32\Robocopy.exe,"..\..\Shared Files" "..\Files"
'Ctrl + click to sync files: ide://run?file=%WINDIR%\System32\Robocopy.exe&args=..\..\Shared+Files&args=..\Files&FilesSync=True
#End Region

'Ctrl + click to export as zip: ide://run?File=%B4X%\Zipper.jar&Args=SampleWebSocketPages.zip

Sub Class_Globals
	Private Root As B4XView
	Private xui As XUI
	
	Private Label1 As Label
	Private LabelUpload As Label
	Private ImageView1 As B4XView
	Private Svr As httpServer
	Private CounterConenction As Int = 0
	Private Timer1 As Timer
	Private WebView1 As WebView
End Sub

Public Sub Initialize
	
End Sub

'This event will be called once, before the page becomes visible.
Private Sub B4XPage_Created (Root1 As B4XView)
	Root = Root1
	Root.LoadLayout("MainPage") 
	
	Svr.Initialize(Me,"Svr")
	Svr.Start(51051)
	Label1.Text=$"Server ip: ${Svr.GetMyWifiIp} ${51051}"$
	
	Dim Unzip As Archiver
	Log(Svr.TempPath)
	File.Copy(File.DirAssets,"www.zip",Svr.TempPath,"www.zip")
	Unzip.Unzip(Svr.TempPath,"www.zip",Svr.TempPath,"")
	
	' Timer for Guess my Number
	Timer1.Initialize("Timer1", 1000)
	Timer1.Enabled = True
	guessmynumber.ListTimer.Initialize
End Sub

'You can see the list of page related events in the B4XPagesManager object. The event name is B4XPage.

Private Sub Button1_Click
	WebView1.LoadUrl("http://localhost:51051")
	'xui.MsgboxAsync("Hello httpServer!", "B4X")
End Sub


Private Sub Svr_NewConection(req As ServletRequest)
	CounterConenction=CounterConenction+1
	Log("New connection: " & req.RemoteAddress & " Counter: " & CounterConenction)
	req.ID=CounterConenction
End Sub

' Handle web page request
Private Sub Svr_Handle(req As ServletRequest, resp As ServletResponse)
	Dim URI As String = req.GetRequestURI
	Log($"Request (${req.ID}) URI: "$ & req.GetRequestURI)
	
	If req.GetRequestURI="/" Then URI="/index.html" ' main page
	If req.RequestCookies.ContainsKey("JSESSIONID")=False Then 	 ' save cookies
		resp.SetCookies("JSESSIONID",req.RemoteAddress.Replace(".","") & req.RemotePort)
	End If
	
	'Handle request page
	Select req.GetRequestURI
		Case "/json/jsonHelper"
			jsonhelper.Svr_Handle(req,resp)
		Case "/reports/chartsHelper"
			gcharts.Svr_Handle(req,resp)
		Case Else 'other page
			resp.SendFile(Svr.TempPath, File.Combine("www" , URI)) ' responde win pahe htm request
	End Select
End Sub

' Handle websocket request
Private Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Select req.GetRequestURI
		Case "/push/ws"
			pushws.Svr_SwitchToWebSocket(req,resp)
		Case "/guessmynumber_ws/ws"
			guessmynumber.Svr_SwitchToWebSocket(req,resp)
		Case "/chat/login"
			chat.Svr_SwitchToWebSocket(req,resp)
		Case "/chat/main"
			chat.Svr_SwitchToWebSocket(req,resp)
		Case "/quiz/ws"
			quiz.Svr_SwitchToWebSocket(req,resp)
		Case "/smiley/ws"
			smiley.Svr_SwitchToWebSocket(req,resp)
		Case "/dynamic/ws"
			Dymamic.Svr_SwitchToWebSocket(req,resp)
		Case "/datepicker/ws"
			date_picker.Svr_SwitchToWebSocket(req,resp)
		Case "/regex_ws/ws"
			WSRegex.Svr_SwitchToWebSocket(req,resp)
		Case "/dbutils/ws"
			wsDBUtils.Svr_SwitchToWebSocket(req,resp)
		Case "/reports/chartsHelper"
	End Select
End Sub

Private Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Select req.GetRequestURI
		Case "/push/ws"
			pushws.Svr_HandleWebSocket(req,resp)
		Case "/guessmynumber_ws/ws"
			guessmynumber.Svr_HandleWebSocket(req,resp)
		Case "/chat/login"
			chat.Svr_HandleWebSocket(req,resp)
		Case "/chat/main"
			chat.Svr_HandleWebSocket(req,resp)
		Case "/quiz/ws"
			quiz.Svr_HandleWebSocket(req,resp)
		Case "/smiley/ws"
			smiley.Svr_HandleWebSocket(req,resp)
		Case "/dynamic/ws"
			Dymamic.Svr_HandleWebSocket(req,resp)
		Case "/datepicker/ws"
			date_picker.Svr_HandleWebSocket(req,resp)		
		Case "/regex_ws/ws"
			WSRegex.Svr_HandleWebSocket(req,resp)
		Case "/dbutils/ws"
			wsDBUtils.Svr_HandleWebSocket(req,resp)
	End Select
End Sub

Private Sub Svr_WebSocketClose(CloseCode As Int, CloseMessage As String)
	Log($"Closed: ${CloseCode} -  Error: ${CloseMessage}"$)
End Sub

#Region Guess my number Timer

'-------------------------------- Guess my number --------------------
private Sub Timer1_Tick
	If guessmynumber.ListTimer.Size>0 Then
		For Each resp As ServletResponse In guessmynumber.ListTimer
			If resp.Connected Then
				resp.Query.SetHtml("#servertime",Array As String("Server time: " & DateTime.Time(DateTime.Now)))
			Else if guessmynumber.ListTimer.IndexOf(resp)>-1 Then
				Try
					guessmynumber.ListTimer.RemoveAt(guessmynumber.ListTimer.IndexOf(resp))
				Catch
					Log(LastException.Message)
				End Try
			End If
		Next
	End If
End Sub

#End Region