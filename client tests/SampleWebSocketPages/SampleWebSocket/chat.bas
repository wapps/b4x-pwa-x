﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=8.9
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private ListChat As List
	Private NickNameSession As Map
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	
	If NickNameSession.IsInitialized=False Then  NickNameSession.Initialize
	If ListChat.IsInitialized=False Then ListChat.Initialize
	
	If req.GetRequestURI="/chat/login" Then  ' ------------------------------ login ----------------------
		iQuery.AutomaticEvents=iQuery.CreateEvent("enter",iQuery.Event_click,iQuery.CreateEvent("username",iQuery.Event_keyup,Null))
	Else ' ----------------------------------------chat ----------------------
		'add responde to list if absent
		If ListChat.IndexOf(resp)=-1 Then ListChat.Add(resp)
			
		iQuery.AutomaticEvents=iQuery.CreateEvent("btnsend",iQuery.Event_click,iQuery.CreateEvent("btnlogout",iQuery.Event_click,Null))
		iQuery.SelectElement("#txt")
		
		' prepare list user
		Dim Us As String = ""
		For Each user As String In NickNameSession.Keys
			Us=Us & $"<li>${NickNameSession.Get(user)}</li>"$
		Next
		
		' send user list to all user
		For Each chatresp As ServletResponse In ListChat
			If chatresp.Connected Then
				chatresp.Query.SetHtml("#users",Array As String(Us))
			Else if ListChat.IndexOf(chatresp)>-1 Then ' remove if user not connect
				Try
					ListChat.RemoveAt(ListChat.IndexOf(chatresp))
				Catch
					Log(LastException.Message)
				End Try
			End If
		Next
			
		iQuery.RunMethod("append","#chattxt",Array As String())
	End If
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	If req.GetRequestURI="/chat/login" Then  ' ------------------------------ login ----------------------
		Dim M As Map=req.GetWebSocketMapData
	
		Select M.Get("type")
			Case "event"
				Dim Params As Map = M.Get("params")
				#if B4A
					'callsub(Me,....) don't work for b4A in CodeModule
					Select M.Get("event")
						Case "username_keyup"
							username_keyup(resp,Params)
						Case "enter_click"
							enter_Click(resp,Params)
					End Select
				#else
					CallSub3(Me,M.Get("event"),resp,Params)
				#End If
			Case "data"
				' Save nickname
				NickNameSession.Put(req.RequestCookies.Get("JSESSIONID"),M.Get("data"))
				resp.Query.Eval("window.location = arguments[0]",Array As String("chat.html"))
		End Select
	Else ' ------------------------------------- Chat-----------------------------
		If req.GetWebSocketMapData.Get("type")="event" Then ' event  button
			If req.GetWebSocketMapData.Get("event")="btnsend_click" Then 
				resp.Query.RunMethodWithResult("val","#txt",Null)
			End If
			
		Else If req.GetWebSocketMapData.Get("type")="data" Then ' send message
			resp.Query.SelectElement("#txt")
			Dim Message As String = $"<b>${NickNameSession.Get(req.RequestCookies.Get("JSESSIONID"))}</b>: ${req.GetWebSocketMapData.Get("data")}<BR>"$
			resp.Query.RunMethod("append","#chattxt",Array As String(Message))
			resp.Query.RunFunction("scrollDown","",Null)
			
			' send message to all users
			For Each chatresp As ServletResponse In ListChat
				If chatresp.Connected Then
					If chatresp<>resp Then chatresp.Query.RunMethod("append","#chattxt",Array As String(Message))
				Else if ListChat.IndexOf(chatresp)>-1 Then ' remove if user not connect
					Try
						ListChat.RemoveAt(ListChat.IndexOf(chatresp))
					Catch
						Log(LastException.Message)
					End Try
				End If
			Next
		End If
	End If
End Sub

#Region Query Event

Private Sub username_keyup (Resp As ServletResponse,Params As Map) '
	
End Sub

Private Sub enter_Click (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
	Resp.Query.RunMethodWithResult("val","#username",Null)
End Sub

#End Region