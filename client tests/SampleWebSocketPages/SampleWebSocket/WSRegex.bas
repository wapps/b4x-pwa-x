﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=9
@EndOfDesignText@
 'Static code module
Sub Process_Globals
	Private Count As Int = 0
	Private fCaseSensitive As Boolean = False
	Private fMultiline As Boolean = False
	Private ftext As String  = ""
	Private fpattern As String  = ""
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query

	iQuery.AutomaticEvents=iQuery.CreateEvent("btnparse",iQuery.Event_click,Null)
	iQuery.RunMethod("autosize","#textarea",Null)
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Select req.GetWebSocketMapData.Get("type")
		Case "event"
			' event btnparse_click
				resp.Query.GetPropriety("#casesensitive",Array As String("checked")) '0
				resp.Query.GetPropriety("#multiline",Array As String("checked")) '1
				resp.Query.GetVal("#textarea",Null) '2
				resp.Query.GetVal("#pattern",Null) '3
		Case "data"
			' data trasmit
			Select Count
				Case 0
					fCaseSensitive=(req.GetWebSocketMapData.Get("data")="false")
				Case 1
					fMultiline=(req.GetWebSocketMapData.Get("data")="false")
				Case 2
					ftext=req.GetWebSocketMapData.Get("data")
				Case 3
					fpattern=req.GetWebSocketMapData.Get("data")
			End Select
			Count=Count+1
			
			If Count=4 Then ' all parameters
				Dim options As Int = 0
				If fCaseSensitive = False Then options = Regex.CASE_INSENSITIVE
				If fMultiline = True Then options = Bit.Or(options, Regex.Multiline)
			
				Try
					Dim m As Matcher = Regex.Matcher2(fpattern, options, ftext)
					Dim res As StringBuilder
					res.Initialize
					Do While m.Find
						res.Append($"Match (length=${resp.Query.EscapeHtml(m.Match)}): ${m.Match}<br>"$)
						For g = 1 To m.GroupCount
							res.Append($"Group #${g}: ${resp.Query.EscapeHtml(m.Group(g))}<br>"$)
						Next
					Loop
					If res.Length = 0 Then res.Append("No matches were found.")
					resp.Query.SetHtml("#logsdiv",Array As String(res.ToString))
				Catch
					'ws.Alert("Error: " & LastException.Message)
					Log("Error: " & LastException.Message)
				End Try
			End If
	End Select
	
End Sub