﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=9
@EndOfDesignText@
'Static code module
Sub Process_Globals
	
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	
	iQuery.AutomaticEvents=iQuery.NoEvent
	iQuery.SetVal("#datepicker",Array As String(DateTime.Date(DateTime.Now)))
	iQuery.SetDialog("#dialog",Array As Map(CreateMap("autoOpen":False,"modal":True)))
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Select req.GetWebSocketMapData.Get("type")
		Case "event"
			' event type
			Select req.GetWebSocketMapData.Get("event")
				Case "datepicker_select"
					resp.Query.RunMethodWithResult("val","#datepicker",Null)
				Case "tabs_tabchange"
					Dim Params As Map = req.GetWebSocketMapData.Get("params")
					resp.Query.SetText("#tabstitle",Array As String($"Currently selected tab is: ${Params.Get("tab")}"$))
			End Select
		Case "data"
			' data trasmit
			resp.Query.SetHtml("#dialog",Array As String($"<b>The day selected is <b>: <br>${req.GetWebSocketMapData.Get("data")}"$))
			resp.Query.SetDialog("#dialog",Array As String("option","title","day"))
			resp.Query.SetDialog("#dialog",Array As String("open"))
	End Select
	
End Sub