﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=8.9
@EndOfDesignText@
'Static code module
Sub Process_Globals
	Private ListMessage As List
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	If ListMessage.IsInitialized=False Then ListMessage.Initialize
	
	iQuery.AutomaticEvents=iQuery.CreateEvent("btnsend",iQuery.Event_click,Null)
	' set datatable
	iQuery.RunMethod("dataTable","#tblmessages", Array As Map( CreateMap("bFilter": False, "bPaginate": False, "bSort": False, _
			"bAutoWidth": False, "aoColumns": Array As Map(CreateMap("sWidth": "20%"), CreateMap("sWidth": "80%")))))
	UpdateTable(resp)
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Dim M As Map=req.GetWebSocketMapData
	
	Select M.Get("type")
		Case "event"
			Dim Params As Map = M.Get("params")
			#if B4A
				'callsub(Me,.... don't work for b4A in CodeModule
				btnSend_Click(resp,Params)
			#Else
				CallSub3(Me,M.Get("event"),resp,Params)
			#End If
		Case "data"
			resp.Query.SelectElement("#ttxt")
			ListMessage.Add(M.Get("data"))
			UpdateTable(resp)
	End Select
End Sub

#Region Function

Private Sub UpdateTable(resp As ServletResponse)
	resp.Query.Eval("$(arguments[0]).dataTable().fnClearTable()", Array As Object("#tblmessages"))
	resp.Query.Eval("$(arguments[0]).dataTable().fnAddData(arguments[1])", Array As Object("#tblmessages", ListToArray))
	resp.Query.SetHtml("#users",Array As String("<li>Total number of users: 0</li>"))
End Sub

Private Sub ListToArray As Object()
	Dim D(ListMessage.Size) As Object
	
	For i=0 To ListMessage.Size-1
		Dim Fields(2) As String
		Fields(0)=i
		Fields(1)=ListMessage.Get(i)
		D(i)=Fields
	Next
	
	Return D
End Sub
#End Region

#Region Query Event

' websocket event
Private Sub btnSend_Click (Resp As ServletResponse,Params As Map)
	'required val
	Resp.Query.RunMethodWithResult("val","#txt",Null)
End Sub


#End Region