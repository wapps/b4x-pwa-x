﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=9
@EndOfDesignText@
Sub Process_Globals
	
End Sub

Public Sub Svr_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	Private iQuery As QueryElement = resp.Query
	
	iQuery.AutomaticEvents=iQuery.CreateEvent("maindiv",iQuery.Event_click,Null)
			
	Dim sb As StringBuilder
	sb.Initialize
	sb.Append($"""$)
	For y = 0 To 9
		For x = 0 To 9
			sb.Append($"<button id="btn${(y * 10 + x)}" style="position: absolute; left: ${(x*50)}px; top: ${(y*50)}px; width: 45px; height: 45px;">0</button>"$)
		Next
	Next
	sb.Append($"""$)
	iQuery.SetHtml("#maindiv",Array As String(sb.ToString))
End Sub

Public Sub Svr_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	Dim MapParams As Map = req.GetWebSocketMapData.Get("params")
	Dim target As String = MapParams.Get("target")
	target="#" & target.Replace($"""$,"")

	resp.Query.SetHtml("#plog",Array As String($"Button <i>${target}</i> was clicked"$))
	'resp.Query.RunMethod("text",target,Array As String($"${Rnd(2,11)}"$)) 
	resp.Query.SetText(target,Array As String($"${Rnd(2,11)}"$))' change value of text button
	resp.Query.SetCSS(target,Array As String("background-color",$"rgb(${Rnd(0,256)}, ${Rnd(0,256)}, ${Rnd(0,256)})"$)) ' change color
End Sub