package net.wapps.httpserver;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class b4i_queryelement_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 14;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 15;BA.debugLine="Public Const Event_change As String = \"change\"";
b4i_queryelement._event_change = BA.ObjectToString("change");__ref.setField("_event_change",b4i_queryelement._event_change);
 //BA.debugLineNum = 16;BA.debugLine="Public Const Event_click As String = \"click\"";
b4i_queryelement._event_click = BA.ObjectToString("click");__ref.setField("_event_click",b4i_queryelement._event_click);
 //BA.debugLineNum = 17;BA.debugLine="Public Const Event_dblclick As String = \"dblclick";
b4i_queryelement._event_dblclick = BA.ObjectToString("dblclick");__ref.setField("_event_dblclick",b4i_queryelement._event_dblclick);
 //BA.debugLineNum = 18;BA.debugLine="Public Const Event_focus As String = \"focus\"";
b4i_queryelement._event_focus = BA.ObjectToString("focus");__ref.setField("_event_focus",b4i_queryelement._event_focus);
 //BA.debugLineNum = 19;BA.debugLine="Public Const Event_focusin As String = \"focusin\"";
b4i_queryelement._event_focusin = BA.ObjectToString("focusin");__ref.setField("_event_focusin",b4i_queryelement._event_focusin);
 //BA.debugLineNum = 20;BA.debugLine="Public Const Event_focusout As String = \"focusout";
b4i_queryelement._event_focusout = BA.ObjectToString("focusout");__ref.setField("_event_focusout",b4i_queryelement._event_focusout);
 //BA.debugLineNum = 21;BA.debugLine="Public Const Event_keyup As String = \"keyup\"";
b4i_queryelement._event_keyup = BA.ObjectToString("keyup");__ref.setField("_event_keyup",b4i_queryelement._event_keyup);
 //BA.debugLineNum = 22;BA.debugLine="Public Const Event_mousedown As String = \"mousedo";
b4i_queryelement._event_mousedown = BA.ObjectToString("mousedown");__ref.setField("_event_mousedown",b4i_queryelement._event_mousedown);
 //BA.debugLineNum = 23;BA.debugLine="Public Const Event_mouseenter As String = \"mousee";
b4i_queryelement._event_mouseenter = BA.ObjectToString("mouseenter");__ref.setField("_event_mouseenter",b4i_queryelement._event_mouseenter);
 //BA.debugLineNum = 24;BA.debugLine="Public Const Event_mouseleave As String = \"mousel";
b4i_queryelement._event_mouseleave = BA.ObjectToString("mouseleave");__ref.setField("_event_mouseleave",b4i_queryelement._event_mouseleave);
 //BA.debugLineNum = 25;BA.debugLine="Public Const Event_mousemove As String = \"mousemo";
b4i_queryelement._event_mousemove = BA.ObjectToString("mousemove");__ref.setField("_event_mousemove",b4i_queryelement._event_mousemove);
 //BA.debugLineNum = 26;BA.debugLine="Public Const Event_mouseup As String = \"mouseup\"";
b4i_queryelement._event_mouseup = BA.ObjectToString("mouseup");__ref.setField("_event_mouseup",b4i_queryelement._event_mouseup);
 //BA.debugLineNum = 28;BA.debugLine="Public const NoEvent() As Map=Array As Map()";
b4i_queryelement._noevent = RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {});__ref.setField("_noevent",b4i_queryelement._noevent);
 //BA.debugLineNum = 30;BA.debugLine="Private Resp As ServletResponse";
b4i_queryelement._resp = RemoteObject.createNew ("b4i_servletresponse");__ref.setField("_resp",b4i_queryelement._resp);
 //BA.debugLineNum = 31;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _createevent(RemoteObject __ref,RemoteObject _objectname,RemoteObject _event,RemoteObject _otherevent) throws Exception{
try {
		Debug.PushSubsStack("CreateEvent (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,91);
if (RapidSub.canDelegate("createevent")) { return __ref.runUserSub(false, "queryelement","createevent", __ref, _objectname, _event, _otherevent);}
RemoteObject _ev = null;
int _i = 0;
Debug.locals.put("ObjectName", _objectname);
Debug.locals.put("Event", _event);
Debug.locals.put("OtherEvent", _otherevent);
 BA.debugLineNum = 91;BA.debugLine="Public Sub CreateEvent(ObjectName As String,Event";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 92;BA.debugLine="If isnull(OtherEvent)=False Then";
Debug.ShouldStop(134217728);
if (RemoteObject.solveBoolean("=",__ref.runClassMethod (b4i_queryelement.class, "_isnull:" /*RemoteObject*/ ,(Object)((_otherevent))),b4i_main.__c.runMethod(true,"False"))) { 
 BA.debugLineNum = 93;BA.debugLine="Dim Ev(OtherEvent.Length+1) As Map";
Debug.ShouldStop(268435456);
_ev = RemoteObject.createNew("B4IArray").runMethod(false, "initObjects:::", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_otherevent.getField(true,"Length"),RemoteObject.createImmutable(1)}, "+",1, 1)}, null, "B4IMap");Debug.locals.put("Ev", _ev);
 BA.debugLineNum = 94;BA.debugLine="For i=0 To OtherEvent.Length-1";
Debug.ShouldStop(536870912);
{
final int step3 = 1;
final int limit3 = RemoteObject.solve(new RemoteObject[] {_otherevent.getField(true,"Length"),RemoteObject.createImmutable(1)}, "-",1, 1).<Number>get().intValue();
_i = 0 ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 95;BA.debugLine="Ev(i)=OtherEvent(i)";
Debug.ShouldStop(1073741824);
_ev.runVoidMethod ("setObjectFast::", BA.numberCast(int.class, _i),_otherevent.runMethod(false,"getObjectFast:", BA.numberCast(int.class, _i)));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 97;BA.debugLine="Ev(OtherEvent.Length).Initialize";
Debug.ShouldStop(1);
_ev.runMethod(false,"getObjectFast:", _otherevent.getField(true,"Length")).runVoidMethod ("Initialize");
 BA.debugLineNum = 98;BA.debugLine="Ev(OtherEvent.Length).Put(\"id\",ObjectName)";
Debug.ShouldStop(2);
_ev.runMethod(false,"getObjectFast:", _otherevent.getField(true,"Length")).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("id"))),(Object)((_objectname)));
 BA.debugLineNum = 99;BA.debugLine="Ev(OtherEvent.Length).Put(\"event\",Event)";
Debug.ShouldStop(4);
_ev.runMethod(false,"getObjectFast:", _otherevent.getField(true,"Length")).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("event"))),(Object)((_event)));
 }else {
 BA.debugLineNum = 101;BA.debugLine="Dim Ev(1) As Map";
Debug.ShouldStop(16);
_ev = RemoteObject.createNew("B4IArray").runMethod(false, "initObjects:::", (Object)new RemoteObject[] {BA.numberCast(int.class, 1)}, null, "B4IMap");Debug.locals.put("Ev", _ev);
 BA.debugLineNum = 102;BA.debugLine="Ev(0).Initialize";
Debug.ShouldStop(32);
_ev.runMethod(false,"getObjectFast:", BA.numberCast(int.class, 0)).runVoidMethod ("Initialize");
 BA.debugLineNum = 103;BA.debugLine="Ev(0).Put(\"id\",ObjectName)";
Debug.ShouldStop(64);
_ev.runMethod(false,"getObjectFast:", BA.numberCast(int.class, 0)).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("id"))),(Object)((_objectname)));
 BA.debugLineNum = 104;BA.debugLine="Ev(0).Put(\"event\",Event)";
Debug.ShouldStop(128);
_ev.runMethod(false,"getObjectFast:", BA.numberCast(int.class, 0)).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("event"))),(Object)((_event)));
 };
 BA.debugLineNum = 107;BA.debugLine="Return Ev";
Debug.ShouldStop(1024);
if (true) return _ev;
 BA.debugLineNum = 108;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _escapehtml(RemoteObject __ref,RemoteObject _raw) throws Exception{
try {
		Debug.PushSubsStack("EscapeHtml (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,188);
if (RapidSub.canDelegate("escapehtml")) { return __ref.runUserSub(false, "queryelement","escapehtml", __ref, _raw);}
Debug.locals.put("Raw", _raw);
 BA.debugLineNum = 188;BA.debugLine="Public Sub EscapeHtml(Raw As String) As String";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 189;BA.debugLine="Return Raw.Replace(QUOTE,\"&quot;\").Replace(\"'\",\"&";
Debug.ShouldStop(268435456);
if (true) return _raw.runMethod(true,"Replace::",(Object)(b4i_main.__c.runMethod(true,"QUOTE")),(Object)(RemoteObject.createImmutable("&quot;"))).runMethod(true,"Replace::",(Object)(BA.ObjectToString("'")),(Object)(RemoteObject.createImmutable("&#39;"))).runMethod(true,"Replace::",(Object)(BA.ObjectToString("<")),(Object)(RemoteObject.createImmutable("&lt;"))).runMethod(true,"Replace::",(Object)(BA.ObjectToString("<")),(Object)(RemoteObject.createImmutable("&gt;"))).runMethod(true,"Replace::",(Object)(BA.ObjectToString("&")),(Object)(RemoteObject.createImmutable("&amp;")));
 BA.debugLineNum = 190;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _eval(RemoteObject __ref,RemoteObject _script,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("Eval (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,83);
if (RapidSub.canDelegate("eval")) { return __ref.runUserSub(false, "queryelement","eval", __ref, _script, _params);}
Debug.locals.put("Script", _script);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 83;BA.debugLine="Public Sub Eval(Script As String, Params As List)";
Debug.ShouldStop(262144);
 BA.debugLineNum = 84;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"eval\",\"\",Scr";
Debug.ShouldStop(524288);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("eval")),(Object)(BA.ObjectToString("")),(Object)(_script),(Object)(BA.ObjectToString("")),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")),(Object)(_params))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 85;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _evalwithresult(RemoteObject __ref,RemoteObject _script,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("EvalWithResult (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,87);
if (RapidSub.canDelegate("evalwithresult")) { return __ref.runUserSub(false, "queryelement","evalwithresult", __ref, _script, _params);}
Debug.locals.put("Script", _script);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 87;BA.debugLine="Public Sub EvalWithResult(Script As String, Params";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 88;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"evalWithResu";
Debug.ShouldStop(8388608);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("evalWithResult")),(Object)(BA.ObjectToString("")),(Object)(_script),(Object)(BA.ObjectToString("")),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")),(Object)(_params))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 89;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getpropriety(RemoteObject __ref,RemoteObject _property,RemoteObject _value) throws Exception{
try {
		Debug.PushSubsStack("GetPropriety (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,168);
if (RapidSub.canDelegate("getpropriety")) { return __ref.runUserSub(false, "queryelement","getpropriety", __ref, _property, _value);}
Debug.locals.put("Property", _property);
Debug.locals.put("Value", _value);
 BA.debugLineNum = 168;BA.debugLine="Public Sub GetPropriety (Property As String, Value";
Debug.ShouldStop(128);
 BA.debugLineNum = 169;BA.debugLine="RunMethodWithResult(\"prop\",Property,Value)";
Debug.ShouldStop(256);
__ref.runClassMethod (b4i_queryelement.class, "_runmethodwithresult:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("prop")),(Object)(_property),(Object)(_value));
 BA.debugLineNum = 170;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getval(RemoteObject __ref,RemoteObject _id,RemoteObject _valuelist) throws Exception{
try {
		Debug.PushSubsStack("GetVal (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,139);
if (RapidSub.canDelegate("getval")) { return __ref.runUserSub(false, "queryelement","getval", __ref, _id, _valuelist);}
Debug.locals.put("ID", _id);
Debug.locals.put("ValueList", _valuelist);
 BA.debugLineNum = 139;BA.debugLine="Public Sub GetVal (ID As String, ValueList As List";
Debug.ShouldStop(1024);
 BA.debugLineNum = 140;BA.debugLine="RunMethodWithResult(\"val\",ID,ValueList)";
Debug.ShouldStop(2048);
__ref.runClassMethod (b4i_queryelement.class, "_runmethodwithresult:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("val")),(Object)(_id),(Object)(_valuelist));
 BA.debugLineNum = 141;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _response) throws Exception{
try {
		Debug.PushSubsStack("Initialize (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,34);
if (RapidSub.canDelegate("initialize")) { return __ref.runUserSub(false, "queryelement","initialize", __ref, _ba, _response);}
__ref.runVoidMethodAndSync("initializeClassModule");
Debug.locals.put("ba", _ba);
Debug.locals.put("Response", _response);
 BA.debugLineNum = 34;BA.debugLine="Public Sub Initialize(Response As ServletResponse)";
Debug.ShouldStop(2);
 BA.debugLineNum = 35;BA.debugLine="Resp=Response";
Debug.ShouldStop(4);
__ref.setField ("_resp" /*RemoteObject*/ ,_response);
 BA.debugLineNum = 36;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _isnull(RemoteObject __ref,RemoteObject _o) throws Exception{
try {
		Debug.PushSubsStack("isnull (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,110);
if (RapidSub.canDelegate("isnull")) { return __ref.runUserSub(false, "queryelement","isnull", __ref, _o);}
Debug.locals.put("O", _o);
 BA.debugLineNum = 110;BA.debugLine="Private Sub isnull(O As Object) As Boolean";
Debug.ShouldStop(8192);
 BA.debugLineNum = 111;BA.debugLine="Return (o = Null)";
Debug.ShouldStop(16384);
if (true) return BA.ObjectToBoolean((RemoteObject.solveBoolean("n",_o)));
 BA.debugLineNum = 112;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _runfunction(RemoteObject __ref,RemoteObject _function,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("RunFunction (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,65);
if (RapidSub.canDelegate("runfunction")) { return __ref.runUserSub(false, "queryelement","runfunction", __ref, _function, _id, _params);}
Debug.locals.put("function", _function);
Debug.locals.put("ID", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 65;BA.debugLine="Public Sub RunFunction (function As String, ID As";
Debug.ShouldStop(1);
 BA.debugLineNum = 66;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runFunction\"";
Debug.ShouldStop(2);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("runFunction")),(Object)(BA.ObjectToString("")),(Object)(_function),(Object)(_id),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")),(Object)(_params))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 67;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _runfunctionwithresult(RemoteObject __ref,RemoteObject _function,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("RunFunctionWithResult (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,69);
if (RapidSub.canDelegate("runfunctionwithresult")) { return __ref.runUserSub(false, "queryelement","runfunctionwithresult", __ref, _function, _id, _params);}
Debug.locals.put("function", _function);
Debug.locals.put("ID", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 69;BA.debugLine="Public Sub RunFunctionWithResult (function As Stri";
Debug.ShouldStop(16);
 BA.debugLineNum = 70;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runFunctionW";
Debug.ShouldStop(32);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("runFunctionWithResult")),(Object)(BA.ObjectToString("")),(Object)(_function),(Object)(_id),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")),(Object)(_params))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 71;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _runmethod(RemoteObject __ref,RemoteObject _method,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("RunMethod (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,74);
if (RapidSub.canDelegate("runmethod")) { return __ref.runUserSub(false, "queryelement","runmethod", __ref, _method, _id, _params);}
Debug.locals.put("Method", _method);
Debug.locals.put("ID", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 74;BA.debugLine="Public Sub RunMethod (Method As String, ID As Stri";
Debug.ShouldStop(512);
 BA.debugLineNum = 75;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runmethod\",M";
Debug.ShouldStop(1024);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("runmethod")),(Object)(_method),(Object)(BA.ObjectToString("")),(Object)(_id),(Object)(_params),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 76;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _runmethodwithresult(RemoteObject __ref,RemoteObject _method,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("RunMethodWithResult (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,79);
if (RapidSub.canDelegate("runmethodwithresult")) { return __ref.runUserSub(false, "queryelement","runmethodwithresult", __ref, _method, _id, _params);}
Debug.locals.put("Method", _method);
Debug.locals.put("ID", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 79;BA.debugLine="Public Sub RunMethodWithResult (Method As String,";
Debug.ShouldStop(16384);
 BA.debugLineNum = 80;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runmethodWit";
Debug.ShouldStop(32768);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("runmethodWithResult")),(Object)(_method),(Object)(BA.ObjectToString("")),(Object)(_id),(Object)(_params),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 81;BA.debugLine="End Sub";
Debug.ShouldStop(65536);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _selectelement(RemoteObject __ref,RemoteObject _id) throws Exception{
try {
		Debug.PushSubsStack("SelectElement (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,184);
if (RapidSub.canDelegate("selectelement")) { return __ref.runUserSub(false, "queryelement","selectelement", __ref, _id);}
Debug.locals.put("ID", _id);
 BA.debugLineNum = 184;BA.debugLine="Public Sub SelectElement (ID As String)";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 185;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runmethod\",\"";
Debug.ShouldStop(16777216);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_queryelement.class, "_setcommand::::::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("runmethod")),(Object)(BA.ObjectToString("select")),(Object)(BA.ObjectToString("")),(Object)(_id),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")),BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4IList"), b4i_main.__c.runMethod(false,"Null")))),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 186;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setautomaticevents(RemoteObject __ref,RemoteObject _arg) throws Exception{
try {
		Debug.PushSubsStack("setAutomaticEvents (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,53);
if (RapidSub.canDelegate("setautomaticevents")) { return __ref.runUserSub(false, "queryelement","setautomaticevents", __ref, _arg);}
RemoteObject _j = RemoteObject.declareNull("B4IJSONGenerator");
RemoteObject _m = RemoteObject.declareNull("B4IMap");
Debug.locals.put("Arg", _arg);
 BA.debugLineNum = 53;BA.debugLine="Public Sub setAutomaticEvents(Arg() As Map)";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 54;BA.debugLine="Dim J As JSONGenerator";
Debug.ShouldStop(2097152);
_j = RemoteObject.createNew ("B4IJSONGenerator");Debug.locals.put("J", _j);
 BA.debugLineNum = 55;BA.debugLine="Dim M As Map";
Debug.ShouldStop(4194304);
_m = RemoteObject.createNew ("B4IMap");Debug.locals.put("M", _m);
 BA.debugLineNum = 56;BA.debugLine="M.Initialize";
Debug.ShouldStop(8388608);
_m.runVoidMethod ("Initialize");
 BA.debugLineNum = 57;BA.debugLine="M.Put(\"etype\",\"setAutomaticEvents\")";
Debug.ShouldStop(16777216);
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("etype"))),(Object)((RemoteObject.createImmutable("setAutomaticEvents"))));
 BA.debugLineNum = 58;BA.debugLine="M.Put(\"data\",Arg)";
Debug.ShouldStop(33554432);
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("data"))),(Object)((_arg)));
 BA.debugLineNum = 59;BA.debugLine="J.Initialize(M)";
Debug.ShouldStop(67108864);
_j.runVoidMethod ("Initialize:",(Object)(_m));
 BA.debugLineNum = 61;BA.debugLine="Resp.SendWebSocketString(J.ToString,False,\"\")";
Debug.ShouldStop(268435456);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketstring:::" /*RemoteObject*/ ,(Object)(_j.runMethod(true,"ToString")),(Object)(b4i_main.__c.runMethod(true,"False")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 62;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setautomaticeventsfrompage(RemoteObject __ref,RemoteObject _html) throws Exception{
try {
		Debug.PushSubsStack("setAutomaticEventsFromPage (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,115);
if (RapidSub.canDelegate("setautomaticeventsfrompage")) { return __ref.runUserSub(false, "queryelement","setautomaticeventsfrompage", __ref, _html);}
Debug.locals.put("Html", _html);
 BA.debugLineNum = 115;BA.debugLine="Private Sub setAutomaticEventsFromPage(Html As Str";
Debug.ShouldStop(262144);
 BA.debugLineNum = 116;BA.debugLine="Log(Html)";
Debug.ShouldStop(524288);
b4i_main.__c.runVoidMethod ("LogImpl:::","37602177",_html,0);
 BA.debugLineNum = 117;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setcommand(RemoteObject __ref,RemoteObject _etype,RemoteObject _method,RemoteObject _property,RemoteObject _id,RemoteObject _params,RemoteObject _arg) throws Exception{
try {
		Debug.PushSubsStack("SetCommand (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,38);
if (RapidSub.canDelegate("setcommand")) { return __ref.runUserSub(false, "queryelement","setcommand", __ref, _etype, _method, _property, _id, _params, _arg);}
RemoteObject _j = RemoteObject.declareNull("B4IJSONGenerator");
RemoteObject _m = RemoteObject.declareNull("B4IMap");
Debug.locals.put("etype", _etype);
Debug.locals.put("Method", _method);
Debug.locals.put("property", _property);
Debug.locals.put("ID", _id);
Debug.locals.put("Params", _params);
Debug.locals.put("Arg", _arg);
 BA.debugLineNum = 38;BA.debugLine="Public Sub SetCommand (etype As String,Method As S";
Debug.ShouldStop(32);
 BA.debugLineNum = 39;BA.debugLine="Dim J As JSONGenerator";
Debug.ShouldStop(64);
_j = RemoteObject.createNew ("B4IJSONGenerator");Debug.locals.put("J", _j);
 BA.debugLineNum = 40;BA.debugLine="Dim M As Map";
Debug.ShouldStop(128);
_m = RemoteObject.createNew ("B4IMap");Debug.locals.put("M", _m);
 BA.debugLineNum = 41;BA.debugLine="M.Initialize";
Debug.ShouldStop(256);
_m.runVoidMethod ("Initialize");
 BA.debugLineNum = 42;BA.debugLine="M.Put(\"etype\",etype)";
Debug.ShouldStop(512);
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("etype"))),(Object)((_etype)));
 BA.debugLineNum = 43;BA.debugLine="If Method<>\"\" Then M.Put(\"method\",Method)";
Debug.ShouldStop(1024);
if (RemoteObject.solveBoolean("!",_method,BA.ObjectToString(""))) { 
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("method"))),(Object)((_method)));};
 BA.debugLineNum = 44;BA.debugLine="If ID<>\"\" Then M.Put(\"id\",ID)";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("!",_id,BA.ObjectToString(""))) { 
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("id"))),(Object)((_id)));};
 BA.debugLineNum = 45;BA.debugLine="If property<>\"\" Then M.Put(\"prop\",property)";
Debug.ShouldStop(4096);
if (RemoteObject.solveBoolean("!",_property,BA.ObjectToString(""))) { 
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("prop"))),(Object)((_property)));};
 BA.debugLineNum = 46;BA.debugLine="If Params.IsInitialized Then M.Put(\"params\",Param";
Debug.ShouldStop(8192);
if (_params.runMethod(true,"IsInitialized").getBoolean()) { 
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("params"))),(Object)(((_params).getObject())));};
 BA.debugLineNum = 47;BA.debugLine="If Arg.IsInitialized Then M.Put(\"value\",Arg)";
Debug.ShouldStop(16384);
if (_arg.runMethod(true,"IsInitialized").getBoolean()) { 
_m.runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("value"))),(Object)(((_arg).getObject())));};
 BA.debugLineNum = 48;BA.debugLine="J.Initialize(M)";
Debug.ShouldStop(32768);
_j.runVoidMethod ("Initialize:",(Object)(_m));
 BA.debugLineNum = 50;BA.debugLine="Return J.ToString";
Debug.ShouldStop(131072);
if (true) return _j.runMethod(true,"ToString");
 BA.debugLineNum = 51;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setcss(RemoteObject __ref,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("SetCSS (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,151);
if (RapidSub.canDelegate("setcss")) { return __ref.runUserSub(false, "queryelement","setcss", __ref, _id, _params);}
Debug.locals.put("id", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 151;BA.debugLine="Public Sub SetCSS (id As String, Params As List)";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 152;BA.debugLine="RunMethod(\"css\",id,Params)";
Debug.ShouldStop(8388608);
__ref.runClassMethod (b4i_queryelement.class, "_runmethod:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("css")),(Object)(_id),(Object)(_params));
 BA.debugLineNum = 153;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setdialog(RemoteObject __ref,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("SetDialog (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,147);
if (RapidSub.canDelegate("setdialog")) { return __ref.runUserSub(false, "queryelement","setdialog", __ref, _id, _params);}
Debug.locals.put("id", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 147;BA.debugLine="Public Sub SetDialog (id As String, Params As List";
Debug.ShouldStop(262144);
 BA.debugLineNum = 148;BA.debugLine="Resp.Query.RunMethod(\"dialog\",id,Params)";
Debug.ShouldStop(524288);
__ref.getField(false,"_resp" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_getquery" /*RemoteObject*/ ).runClassMethod (b4i_queryelement.class, "_runmethod:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("dialog")),(Object)(_id),(Object)(_params));
 BA.debugLineNum = 149;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sethtml(RemoteObject __ref,RemoteObject _id,RemoteObject _params) throws Exception{
try {
		Debug.PushSubsStack("SetHtml (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,159);
if (RapidSub.canDelegate("sethtml")) { return __ref.runUserSub(false, "queryelement","sethtml", __ref, _id, _params);}
Debug.locals.put("id", _id);
Debug.locals.put("Params", _params);
 BA.debugLineNum = 159;BA.debugLine="Public Sub SetHtml (id As String,  Params As List)";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 160;BA.debugLine="RunMethod(\"html\",id,Params)";
Debug.ShouldStop(-2147483648);
__ref.runClassMethod (b4i_queryelement.class, "_runmethod:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("html")),(Object)(_id),(Object)(_params));
 BA.debugLineNum = 161;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setpropriety(RemoteObject __ref,RemoteObject _property,RemoteObject _value) throws Exception{
try {
		Debug.PushSubsStack("SetPropriety (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,164);
if (RapidSub.canDelegate("setpropriety")) { return __ref.runUserSub(false, "queryelement","setpropriety", __ref, _property, _value);}
Debug.locals.put("Property", _property);
Debug.locals.put("Value", _value);
 BA.debugLineNum = 164;BA.debugLine="Public Sub SetPropriety (Property As String, Value";
Debug.ShouldStop(8);
 BA.debugLineNum = 165;BA.debugLine="RunMethod(\"prop\",Property,Value)";
Debug.ShouldStop(16);
__ref.runClassMethod (b4i_queryelement.class, "_runmethod:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("prop")),(Object)(_property),(Object)(_value));
 BA.debugLineNum = 166;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _settext(RemoteObject __ref,RemoteObject _id,RemoteObject _textlist) throws Exception{
try {
		Debug.PushSubsStack("SetText (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,172);
if (RapidSub.canDelegate("settext")) { return __ref.runUserSub(false, "queryelement","settext", __ref, _id, _textlist);}
Debug.locals.put("ID", _id);
Debug.locals.put("TextList", _textlist);
 BA.debugLineNum = 172;BA.debugLine="Public Sub SetText (ID As String, TextList As List";
Debug.ShouldStop(2048);
 BA.debugLineNum = 173;BA.debugLine="RunMethod(\"text\",ID,TextList)";
Debug.ShouldStop(4096);
__ref.runClassMethod (b4i_queryelement.class, "_runmethod:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("text")),(Object)(_id),(Object)(_textlist));
 BA.debugLineNum = 174;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setval(RemoteObject __ref,RemoteObject _id,RemoteObject _valuelist) throws Exception{
try {
		Debug.PushSubsStack("SetVal (queryelement) ","queryelement",4,__ref.getField(false, "bi"),__ref,176);
if (RapidSub.canDelegate("setval")) { return __ref.runUserSub(false, "queryelement","setval", __ref, _id, _valuelist);}
Debug.locals.put("ID", _id);
Debug.locals.put("ValueList", _valuelist);
 BA.debugLineNum = 176;BA.debugLine="Public Sub SetVal (ID As String, ValueList As List";
Debug.ShouldStop(32768);
 BA.debugLineNum = 177;BA.debugLine="RunMethod(\"val\",ID,ValueList)";
Debug.ShouldStop(65536);
__ref.runClassMethod (b4i_queryelement.class, "_runmethod:::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("val")),(Object)(_id),(Object)(_valuelist));
 BA.debugLineNum = 178;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}