package net.wapps.httpserver;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class b4i_main_subs_0 {


public static RemoteObject  _application_start(RemoteObject _nav) throws Exception{
try {
		Debug.PushSubsStack("Application_Start (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,41);
if (RapidSub.canDelegate("application_start")) { return b4i_main.remoteMe.runUserSub(false, "main","application_start", _nav);}
RemoteObject _rl = RemoteObject.declareNull("B4IReleaseLogger");
RemoteObject _unzip = RemoteObject.declareNull("B4IArchiver");
Debug.locals.put("Nav", _nav);
 BA.debugLineNum = 41;BA.debugLine="Private Sub Application_Start (Nav As NavigationCo";
Debug.ShouldStop(256);
 BA.debugLineNum = 42;BA.debugLine="NavControl = Nav";
Debug.ShouldStop(512);
b4i_main._navcontrol = _nav;
 BA.debugLineNum = 43;BA.debugLine="Page1.Initialize(\"Page1\")";
Debug.ShouldStop(1024);
b4i_main._page1.runVoidMethod ("Initialize::",b4i_main.ba,(Object)(RemoteObject.createImmutable("Page1")));
 BA.debugLineNum = 44;BA.debugLine="Page1.RootPanel.LoadLayout(\"Page1\")";
Debug.ShouldStop(2048);
b4i_main._page1.runMethod(false,"RootPanel").runMethodAndSync(false,"LoadLayout::",(Object)(RemoteObject.createImmutable("Page1")),b4i_main.ba);
 BA.debugLineNum = 45;BA.debugLine="NavControl.ShowPage(Page1)";
Debug.ShouldStop(4096);
b4i_main._navcontrol.runVoidMethod ("ShowPage:",(Object)(((b4i_main._page1).getObject())));
 BA.debugLineNum = 47;BA.debugLine="Dim rl As ReleaseLogger";
Debug.ShouldStop(16384);
_rl = RemoteObject.createNew ("B4IReleaseLogger");Debug.locals.put("rl", _rl);
 BA.debugLineNum = 48;BA.debugLine="rl.Initialize(\"192.168.1.105\", 54323)";
Debug.ShouldStop(32768);
_rl.runVoidMethod ("Initialize:::",b4i_main.ba,(Object)(BA.ObjectToString("192.168.1.105")),(Object)(BA.numberCast(int.class, 54323)));
 BA.debugLineNum = 50;BA.debugLine="Svr.Initialize(Me,\"Svr\")";
Debug.ShouldStop(131072);
b4i_main._svr.runClassMethod (b4i_httpserver.class, "_initialize:::" /*RemoteObject*/ ,b4i_main.ba,(Object)(b4i_main.getObject()),(Object)(RemoteObject.createImmutable("Svr")));
 BA.debugLineNum = 51;BA.debugLine="File.Copy(File.DirAssets,\"file.htdigest\",File.Dir";
Debug.ShouldStop(262144);
b4i_main.__c.runMethod(false,"File").runVoidMethod ("Copy::::",(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirAssets")),(Object)(BA.ObjectToString("file.htdigest")),(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(RemoteObject.createImmutable("file.htdigest")));
 BA.debugLineNum = 52;BA.debugLine="Svr.htdigest=File.ReadList(File.DirTemp,\"file.htd";
Debug.ShouldStop(524288);
b4i_main._svr.setField ("_htdigest" /*RemoteObject*/ ,b4i_main.__c.runMethod(false,"File").runMethod(false,"ReadList::",(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(RemoteObject.createImmutable("file.htdigest"))));
 BA.debugLineNum = 53;BA.debugLine="Svr.Start(51051)";
Debug.ShouldStop(1048576);
b4i_main._svr.runClassMethod (b4i_httpserver.class, "_start:" /*void*/ ,(Object)(BA.numberCast(int.class, 51051)));
 BA.debugLineNum = 54;BA.debugLine="Svr.DigestPath=\"/dir/\"";
Debug.ShouldStop(2097152);
b4i_main._svr.setField ("_digestpath" /*RemoteObject*/ ,BA.ObjectToString("/dir/"));
 BA.debugLineNum = 55;BA.debugLine="Svr.realm=\"testrealm@host.com\"";
Debug.ShouldStop(4194304);
b4i_main._svr.setField ("_realm" /*RemoteObject*/ ,BA.ObjectToString("testrealm@host.com"));
 BA.debugLineNum = 56;BA.debugLine="Label1.Text=$\"Server ip: ${Svr.GetMyWifiIp} ${510";
Debug.ShouldStop(8388608);
b4i_main._label1.runMethod(true,"setText:",(RemoteObject.concat(RemoteObject.createImmutable("Server ip: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main._svr.runClassMethod (b4i_httpserver.class, "_getmywifiip" /*RemoteObject*/ )))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(RemoteObject.createImmutable((51051)))),RemoteObject.createImmutable(""))));
 BA.debugLineNum = 58;BA.debugLine="Dim unzip As Archiver";
Debug.ShouldStop(33554432);
_unzip = RemoteObject.createNew ("B4IArchiver");Debug.locals.put("unzip", _unzip);
 BA.debugLineNum = 59;BA.debugLine="unzip.Unzip(File.DirAssets,\"www.zip\",File.DirTemp";
Debug.ShouldStop(67108864);
_unzip.runVoidMethod ("Unzip::::",(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirAssets")),(Object)(BA.ObjectToString("www.zip")),(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(RemoteObject.createImmutable("")));
 BA.debugLineNum = 60;BA.debugLine="End Sub";
Debug.ShouldStop(134217728);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _button1_click() throws Exception{
try {
		Debug.PushSubsStack("Button1_Click (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,62);
if (RapidSub.canDelegate("button1_click")) { return b4i_main.remoteMe.runUserSub(false, "main","button1_click");}
 BA.debugLineNum = 62;BA.debugLine="Sub Button1_Click";
Debug.ShouldStop(536870912);
 BA.debugLineNum = 63;BA.debugLine="xui.MsgboxAsync(\"Hello world!\", \"B4X\")";
Debug.ShouldStop(1073741824);
b4i_main._xui.runVoidMethod ("MsgboxAsync:::",b4i_main.ba,(Object)(BA.ObjectToString("Hello world!")),(Object)(RemoteObject.createImmutable("B4X")));
 BA.debugLineNum = 64;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _dinamicpage(RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("DinamicPage (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,111);
if (RapidSub.canDelegate("dinamicpage")) { return b4i_main.remoteMe.runUserSub(false, "main","dinamicpage", _req, _resp);}
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 111;BA.debugLine="Private Sub DinamicPage(req As ServletRequest,resp";
Debug.ShouldStop(16384);
 BA.debugLineNum = 112;BA.debugLine="resp.ContentType = \"text/html\"";
Debug.ShouldStop(32768);
_resp.setField ("_contenttype" /*RemoteObject*/ ,BA.ObjectToString("text/html"));
 BA.debugLineNum = 113;BA.debugLine="resp.SendString($\"<img src='logo.png'/ width=100";
Debug.ShouldStop(65536);
_resp.runClassMethod (b4i_servletresponse.class, "_sendstring:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("<img src='logo.png'/ width=100 height=100><br/>\n"),RemoteObject.createImmutable("<b>Hello world!!!</b><br/>\n"),RemoteObject.createImmutable("Your ip address is: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_req.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ )))),RemoteObject.createImmutable("<br/>\n"),RemoteObject.createImmutable("The time here is: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Date:",(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Time:",(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("<br/>\n"),RemoteObject.createImmutable("<a href='/'>Back</a>")))));
 BA.debugLineNum = 118;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _page1_resize(RemoteObject _width,RemoteObject _height) throws Exception{
try {
		Debug.PushSubsStack("Page1_Resize (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,66);
if (RapidSub.canDelegate("page1_resize")) { return b4i_main.remoteMe.runUserSub(false, "main","page1_resize", _width, _height);}
Debug.locals.put("Width", _width);
Debug.locals.put("Height", _height);
 BA.debugLineNum = 66;BA.debugLine="Private Sub Page1_Resize(Width As Int, Height As I";
Debug.ShouldStop(2);
 BA.debugLineNum = 68;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 26;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 29;BA.debugLine="Public App As Application";
b4i_main._app = RemoteObject.createNew ("B4IApplicationWrapper");
 //BA.debugLineNum = 30;BA.debugLine="Public NavControl As NavigationController";
b4i_main._navcontrol = RemoteObject.createNew ("B4INavigationControllerWrapper");
 //BA.debugLineNum = 31;BA.debugLine="Private Page1 As Page";
b4i_main._page1 = RemoteObject.createNew ("B4IPage");
 //BA.debugLineNum = 32;BA.debugLine="Private xui As XUI";
b4i_main._xui = RemoteObject.createNew ("B4IXUI");
 //BA.debugLineNum = 34;BA.debugLine="Private Label1 As Label";
b4i_main._label1 = RemoteObject.createNew ("B4ILabelWrapper");
 //BA.debugLineNum = 35;BA.debugLine="Private LabelUpload As Label";
b4i_main._labelupload = RemoteObject.createNew ("B4ILabelWrapper");
 //BA.debugLineNum = 36;BA.debugLine="Private ImageView1 As ImageView";
b4i_main._imageview1 = RemoteObject.createNew ("B4IImageViewWrapper");
 //BA.debugLineNum = 37;BA.debugLine="Private Svr As httpServer";
b4i_main._svr = RemoteObject.createNew ("b4i_httpserver");
 //BA.debugLineNum = 38;BA.debugLine="Private CounterConenction As Int = 0";
b4i_main._counterconenction = BA.numberCast(int.class, 0);
 //BA.debugLineNum = 39;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _svr_handle(RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Svr_Handle (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,76);
if (RapidSub.canDelegate("svr_handle")) { return b4i_main.remoteMe.runUserSub(false, "main","svr_handle", _req, _resp);}
RemoteObject _fn = RemoteObject.createImmutable("");
RemoteObject _filename = RemoteObject.createImmutable("");
RemoteObject _field = RemoteObject.createImmutable("");
RemoteObject _param = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 76;BA.debugLine="Private Sub Svr_Handle(req As ServletRequest, resp";
Debug.ShouldStop(2048);
 BA.debugLineNum = 77;BA.debugLine="Dim fn As String = req.GetRequestURI.ToLowerCase";
Debug.ShouldStop(4096);
_fn = _req.runClassMethod (b4i_servletrequest.class, "_getrequesturi" /*RemoteObject*/ ).runMethod(true,"ToLowerCase");Debug.locals.put("fn", _fn);Debug.locals.put("fn", _fn);
 BA.debugLineNum = 78;BA.debugLine="Log(\"Request URI: \" & fn)";
Debug.ShouldStop(8192);
b4i_main.__c.runVoidMethod ("LogImpl:::","3327682",RemoteObject.concat(RemoteObject.createImmutable("Request URI: "),_fn),0);
 BA.debugLineNum = 80;BA.debugLine="Select req.GetMethod";
Debug.ShouldStop(32768);
switch (BA.switchObjectToInt(_req.runClassMethod (b4i_servletrequest.class, "_getmethod" /*RemoteObject*/ ),BA.ObjectToString("GET"),BA.ObjectToString("POST"))) {
case 0: {
 BA.debugLineNum = 82;BA.debugLine="If fn = \"/\" Then fn = \"/index.html\"";
Debug.ShouldStop(131072);
if (RemoteObject.solveBoolean("=",_fn,BA.ObjectToString("/"))) { 
_fn = BA.ObjectToString("/index.html");Debug.locals.put("fn", _fn);};
 BA.debugLineNum = 83;BA.debugLine="If File.Exists(File.DirTemp,fn ) Then";
Debug.ShouldStop(262144);
if (b4i_main.__c.runMethod(false,"File").runMethod(true,"Exists::",(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(_fn)).getBoolean()) { 
 BA.debugLineNum = 85;BA.debugLine="resp.SendFile(File.DirTemp,fn )";
Debug.ShouldStop(1048576);
_resp.runClassMethod (b4i_servletresponse.class, "_sendfile::" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(_fn));
 }else 
{ BA.debugLineNum = 86;BA.debugLine="Else if req.GetRequestURI=\"/hello\" Then";
Debug.ShouldStop(2097152);
if (RemoteObject.solveBoolean("=",_req.runClassMethod (b4i_servletrequest.class, "_getrequesturi" /*RemoteObject*/ ),BA.ObjectToString("/hello"))) { 
 BA.debugLineNum = 87;BA.debugLine="DinamicPage(req ,resp)";
Debug.ShouldStop(4194304);
_dinamicpage(_req,_resp);
 }else {
 BA.debugLineNum = 90;BA.debugLine="resp.SendNotFound(fn)";
Debug.ShouldStop(33554432);
_resp.runClassMethod (b4i_servletresponse.class, "_sendnotfound:" /*RemoteObject*/ ,(Object)(_fn));
 }}
;
 break; }
case 1: {
 BA.debugLineNum = 93;BA.debugLine="If req.ContentType=\"multipart/form-data\" Then";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean("=",_req.getField(true,"_contenttype" /*RemoteObject*/ ),BA.ObjectToString("multipart/form-data"))) { 
 BA.debugLineNum = 95;BA.debugLine="If req.MultipartFilename.Size>0 Then";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean(">",_req.getField(false,"_multipartfilename" /*RemoteObject*/ ).runMethod(true,"Size"),BA.numberCast(double.class, 0))) { 
 BA.debugLineNum = 96;BA.debugLine="For Each filename As String In req.MultipartF";
Debug.ShouldStop(-2147483648);
{
final RemoteObject group16 = _req.getField(false,"_multipartfilename" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen16 = group16.runMethod(true,"Size").<Integer>get()
;int index16 = 0;
;
for (; index16 < groupLen16;index16++){
_filename = BA.ObjectToString(group16.runMethod(false,"Get:",index16));Debug.locals.put("filename", _filename);
Debug.locals.put("filename", _filename);
 BA.debugLineNum = 97;BA.debugLine="Log(\"Start uploading: \" & filename)";
Debug.ShouldStop(1);
b4i_main.__c.runVoidMethod ("LogImpl:::","3327701",RemoteObject.concat(RemoteObject.createImmutable("Start uploading: "),_filename),0);
 }
}Debug.locals.put("filename", _filename);
;
 };
 }else {
 BA.debugLineNum = 102;BA.debugLine="Dim Field As String = \"\"";
Debug.ShouldStop(32);
_field = BA.ObjectToString("");Debug.locals.put("Field", _field);Debug.locals.put("Field", _field);
 BA.debugLineNum = 103;BA.debugLine="For Each Param As String In req.ParameterMap.K";
Debug.ShouldStop(64);
{
final RemoteObject group22 = _req.runClassMethod (b4i_servletrequest.class, "_parametermap" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen22 = group22.runMethod(true,"Size").<Integer>get()
;int index22 = 0;
;
for (; index22 < groupLen22;index22++){
_param = BA.ObjectToString(group22.runMethod(false,"Get:",index22));Debug.locals.put("Param", _param);
Debug.locals.put("Param", _param);
 BA.debugLineNum = 104;BA.debugLine="Field=$\"${Field}${Param}=${req.ParameterMap.G";
Debug.ShouldStop(128);
_field = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_field))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_param))),RemoteObject.createImmutable("="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_req.runClassMethod (b4i_servletrequest.class, "_parametermap" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_param))))),RemoteObject.createImmutable("<BR>")));Debug.locals.put("Field", _field);
 }
}Debug.locals.put("Param", _param);
;
 BA.debugLineNum = 106;BA.debugLine="resp.SendString(Field & $\"<B>Recorded data</B>";
Debug.ShouldStop(512);
_resp.runClassMethod (b4i_servletresponse.class, "_sendstring:" /*RemoteObject*/ ,(Object)(RemoteObject.concat(_field,(RemoteObject.createImmutable("<B>Recorded data</B><BR><a href=\"/index.html\">Back</a>")))));
 };
 break; }
}
;
 BA.debugLineNum = 109;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _svr_newconection(RemoteObject _req) throws Exception{
try {
		Debug.PushSubsStack("Svr_NewConection (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,70);
if (RapidSub.canDelegate("svr_newconection")) { return b4i_main.remoteMe.runUserSub(false, "main","svr_newconection", _req);}
Debug.locals.put("req", _req);
 BA.debugLineNum = 70;BA.debugLine="Private Sub Svr_NewConection(req As ServletRequest";
Debug.ShouldStop(32);
 BA.debugLineNum = 71;BA.debugLine="CounterConenction=CounterConenction+1";
Debug.ShouldStop(64);
b4i_main._counterconenction = RemoteObject.solve(new RemoteObject[] {b4i_main._counterconenction,RemoteObject.createImmutable(1)}, "+",1, 1);
 BA.debugLineNum = 72;BA.debugLine="Log(\"New connection: \" & req.RemoteAddress & \" Co";
Debug.ShouldStop(128);
b4i_main.__c.runVoidMethod ("LogImpl:::","3262146",RemoteObject.concat(RemoteObject.createImmutable("New connection: "),_req.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ ),RemoteObject.createImmutable(" Counter: "),b4i_main._counterconenction),0);
 BA.debugLineNum = 73;BA.debugLine="req.ID=CounterConenction";
Debug.ShouldStop(256);
_req.setField ("_id" /*RemoteObject*/ ,BA.NumberToString(b4i_main._counterconenction));
 BA.debugLineNum = 74;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _svr_uploadedfile(RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Svr_UploadedFile (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,124);
if (RapidSub.canDelegate("svr_uploadedfile")) { return b4i_main.remoteMe.runUserSub(false, "main","svr_uploadedfile", _req, _resp);}
RemoteObject _tempfilename = RemoteObject.createImmutable("");
RemoteObject _originalefilename = RemoteObject.createImmutable("");
RemoteObject _filename = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 124;BA.debugLine="Private Sub Svr_UploadedFile (req As ServletReques";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 125;BA.debugLine="Dim tempfilename As String";
Debug.ShouldStop(268435456);
_tempfilename = RemoteObject.createImmutable("");Debug.locals.put("tempfilename", _tempfilename);
 BA.debugLineNum = 126;BA.debugLine="Dim OriginaleFileName As String";
Debug.ShouldStop(536870912);
_originalefilename = RemoteObject.createImmutable("");Debug.locals.put("OriginaleFileName", _originalefilename);
 BA.debugLineNum = 127;BA.debugLine="If req.MultipartFilename.Size>0 Then";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean(">",_req.getField(false,"_multipartfilename" /*RemoteObject*/ ).runMethod(true,"Size"),BA.numberCast(double.class, 0))) { 
 BA.debugLineNum = 128;BA.debugLine="For Each Filename As String In req.MultipartFile";
Debug.ShouldStop(-2147483648);
{
final RemoteObject group4 = _req.getField(false,"_multipartfilename" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen4 = group4.runMethod(true,"Size").<Integer>get()
;int index4 = 0;
;
for (; index4 < groupLen4;index4++){
_filename = BA.ObjectToString(group4.runMethod(false,"Get:",index4));Debug.locals.put("Filename", _filename);
Debug.locals.put("Filename", _filename);
 BA.debugLineNum = 129;BA.debugLine="tempfilename=req.MultipartFilename.Get(Filename";
Debug.ShouldStop(1);
_tempfilename = BA.ObjectToString(_req.getField(false,"_multipartfilename" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_filename))));Debug.locals.put("tempfilename", _tempfilename);
 BA.debugLineNum = 130;BA.debugLine="OriginaleFileName=Filename";
Debug.ShouldStop(2);
_originalefilename = _filename;Debug.locals.put("OriginaleFileName", _originalefilename);
 }
}Debug.locals.put("Filename", _filename);
;
 };
 BA.debugLineNum = 135;BA.debugLine="If OriginaleFileName.ToLowerCase.EndsWith(\"png\")";
Debug.ShouldStop(64);
if (RemoteObject.solveBoolean(".",_originalefilename.runMethod(true,"ToLowerCase").runMethod(true,"EndsWith:",(Object)(RemoteObject.createImmutable("png")))) || RemoteObject.solveBoolean(".",_originalefilename.runMethod(true,"ToLowerCase").runMethod(true,"EndsWith:",(Object)(RemoteObject.createImmutable("jpg"))))) { 
 BA.debugLineNum = 138;BA.debugLine="ImageView1.Bitmap=LoadBitmap(File.DirTemp,tempfi";
Debug.ShouldStop(512);
b4i_main._imageview1.runMethod(false,"setBitmap:",b4i_main.__c.runMethod(false,"LoadBitmap::",(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(_tempfilename)));
 BA.debugLineNum = 139;BA.debugLine="resp.Status=200";
Debug.ShouldStop(1024);
_resp.setField ("_status" /*RemoteObject*/ ,BA.numberCast(int.class, 200));
 BA.debugLineNum = 140;BA.debugLine="resp.SendString($\"<!DOCTYPE html> <html lang=\"en";
Debug.ShouldStop(2048);
_resp.runClassMethod (b4i_servletresponse.class, "_sendstring:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("<!DOCTYPE html>\n"),RemoteObject.createImmutable("<html lang=\"en\" dir=\"ltr\">\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("<head>\n"),RemoteObject.createImmutable("  <meta charset=\"utf-8\">\n"),RemoteObject.createImmutable("  <title>UPload</title>\n"),RemoteObject.createImmutable("</head>\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("<body id=\"body\">\n"),RemoteObject.createImmutable("<B>Download: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_filename))),RemoteObject.createImmutable("</B><BR>\n"),RemoteObject.createImmutable("<img src=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_tempfilename))),RemoteObject.createImmutable("\"><BR>\n"),RemoteObject.createImmutable("<a href=\"/fileupload.html\">Back To Upload</a><BR>\n"),RemoteObject.createImmutable("<a href=\"/index.html\">Back To Home</a>\n"),RemoteObject.createImmutable("</body>\n"),RemoteObject.createImmutable("</html>")))));
 }else {
 BA.debugLineNum = 157;BA.debugLine="resp.Status=200";
Debug.ShouldStop(268435456);
_resp.setField ("_status" /*RemoteObject*/ ,BA.numberCast(int.class, 200));
 BA.debugLineNum = 158;BA.debugLine="resp.SendString($\"<!DOCTYPE html> <html lang=\"en";
Debug.ShouldStop(536870912);
_resp.runClassMethod (b4i_servletresponse.class, "_sendstring:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("<!DOCTYPE html>\n"),RemoteObject.createImmutable("<html lang=\"en\" dir=\"ltr\">\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("<head>\n"),RemoteObject.createImmutable("  <meta charset=\"utf-8\">\n"),RemoteObject.createImmutable("  <title>UPload</title>\n"),RemoteObject.createImmutable("</head>\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("<body id=\"body\">\n"),RemoteObject.createImmutable("<B>Download: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_filename))),RemoteObject.createImmutable("</B><BR>\n"),RemoteObject.createImmutable("<a href=\"/fileupload.html\">Back to Upload</a><BR>\n"),RemoteObject.createImmutable("<a href=\"/index.html\">Back to Home</a>\n"),RemoteObject.createImmutable("</body>\n"),RemoteObject.createImmutable("</html>")))));
 };
 BA.debugLineNum = 174;BA.debugLine="Log(\"Download: \" & Filename)";
Debug.ShouldStop(8192);
b4i_main.__c.runVoidMethod ("LogImpl:::","3524338",RemoteObject.concat(RemoteObject.createImmutable("Download: "),_filename),0);
 BA.debugLineNum = 175;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _svr_uploadprogress(RemoteObject _resp,RemoteObject _progress) throws Exception{
try {
		Debug.PushSubsStack("Svr_UploadProgress (main) ","main",0,b4i_main.ba,b4i_main.mostCurrent,120);
if (RapidSub.canDelegate("svr_uploadprogress")) { return b4i_main.remoteMe.runUserSub(false, "main","svr_uploadprogress", _resp, _progress);}
Debug.locals.put("resp", _resp);
Debug.locals.put("Progress", _progress);
 BA.debugLineNum = 120;BA.debugLine="Private Sub Svr_UploadProgress (resp As ServletRes";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 121;BA.debugLine="LabelUpload.Text=$\"Download $1.0{Progress*100}%\"$";
Debug.ShouldStop(16777216);
b4i_main._labelupload.runMethod(true,"setText:",(RemoteObject.concat(RemoteObject.createImmutable("Download "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("1.0")),(Object)((RemoteObject.solve(new RemoteObject[] {_progress,RemoteObject.createImmutable(100)}, "*",0, 0)))),RemoteObject.createImmutable("%"))));
 BA.debugLineNum = 122;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}