package net.wapps.httpserver;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class b4i_httpserver_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 9;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 10;BA.debugLine="Private Users As Map";
b4i_httpserver._users = RemoteObject.createNew ("B4IMap");__ref.setField("_users",b4i_httpserver._users);
 //BA.debugLineNum = 11;BA.debugLine="Private Serv As ServerSocket";
b4i_httpserver._serv = RemoteObject.createNew ("B4IServerSocketWrapper");__ref.setField("_serv",b4i_httpserver._serv);
 //BA.debugLineNum = 13;BA.debugLine="Private Work As Boolean = False";
b4i_httpserver._work = b4i_main.__c.runMethod(true,"False");__ref.setField("_work",b4i_httpserver._work);
 //BA.debugLineNum = 14;BA.debugLine="Private mCallBack As Object";
b4i_httpserver._mcallback = RemoteObject.createNew ("NSObject");__ref.setField("_mcallback",b4i_httpserver._mcallback);
 //BA.debugLineNum = 15;BA.debugLine="Private mEventName As String";
b4i_httpserver._meventname = RemoteObject.createImmutable("");__ref.setField("_meventname",b4i_httpserver._meventname);
 //BA.debugLineNum = 18;BA.debugLine="Public Const DigestAuthentication As Boolean = Fa";
b4i_httpserver._digestauthentication = b4i_main.__c.runMethod(true,"False");__ref.setField("_digestauthentication",b4i_httpserver._digestauthentication);
 //BA.debugLineNum = 19;BA.debugLine="Public DigestPath As String = \"/\"";
b4i_httpserver._digestpath = BA.ObjectToString("/");__ref.setField("_digestpath",b4i_httpserver._digestpath);
 //BA.debugLineNum = 20;BA.debugLine="Public realm As String = \"\"";
b4i_httpserver._realm = BA.ObjectToString("");__ref.setField("_realm",b4i_httpserver._realm);
 //BA.debugLineNum = 21;BA.debugLine="Public htdigest As List";
b4i_httpserver._htdigest = RemoteObject.createNew ("B4IList");__ref.setField("_htdigest",b4i_httpserver._htdigest);
 //BA.debugLineNum = 22;BA.debugLine="Public IgnoreNC As Boolean = False";
b4i_httpserver._ignorenc = b4i_main.__c.runMethod(true,"False");__ref.setField("_ignorenc",b4i_httpserver._ignorenc);
 //BA.debugLineNum = 23;BA.debugLine="Public Timeout As Int = 5000";
b4i_httpserver._timeout = BA.numberCast(int.class, 5000);__ref.setField("_timeout",b4i_httpserver._timeout);
 //BA.debugLineNum = 24;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _data_handle(RemoteObject __ref,RemoteObject _req,RemoteObject _res) throws Exception{
try {
		Debug.PushSubsStack("Data_Handle (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,81);
if (RapidSub.canDelegate("data_handle")) { return __ref.runUserSub(false, "httpserver","data_handle", __ref, _req, _res);}
Debug.locals.put("Req", _req);
Debug.locals.put("Res", _res);
 BA.debugLineNum = 81;BA.debugLine="Private Sub Data_Handle(Req As ServletRequest, Res";
Debug.ShouldStop(65536);
 BA.debugLineNum = 82;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handle\",2)";
Debug.ShouldStop(131072);
if (__ref.runClassMethod (b4i_httpserver.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_Handle"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_Handle"))),(Object)((_req)),(Object)((_res)));};
 BA.debugLineNum = 83;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _data_handlewebsocket(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Data_HandleWebSocket (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,97);
if (RapidSub.canDelegate("data_handlewebsocket")) { return __ref.runUserSub(false, "httpserver","data_handlewebsocket", __ref, _req, _resp);}
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 97;BA.debugLine="Private Sub Data_HandleWebSocket (req As ServletRe";
Debug.ShouldStop(1);
 BA.debugLineNum = 98;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_HandleWebS";
Debug.ShouldStop(2);
if (__ref.runClassMethod (b4i_httpserver.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_HandleWebSocket"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_HandleWebSocket"))),(Object)((_req)),(Object)((_resp)));};
 BA.debugLineNum = 99;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _data_switchtowebsocket(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Data_SwitchToWebSocket (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,93);
if (RapidSub.canDelegate("data_switchtowebsocket")) { return __ref.runUserSub(false, "httpserver","data_switchtowebsocket", __ref, _req, _resp);}
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 93;BA.debugLine="Private Sub Data_SwitchToWebSocket (req As Servlet";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 94;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_SwitchToWe";
Debug.ShouldStop(536870912);
if (__ref.runClassMethod (b4i_httpserver.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_SwitchToWebSocket"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_SwitchToWebSocket"))),(Object)((_req)),(Object)((_resp)));};
 BA.debugLineNum = 95;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _data_uploadedfile(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Data_UploadedFile (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,89);
if (RapidSub.canDelegate("data_uploadedfile")) { return __ref.runUserSub(false, "httpserver","data_uploadedfile", __ref, _req, _resp);}
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 89;BA.debugLine="Private Sub Data_UploadedFile(req As ServletReques";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 90;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_UploadedFi";
Debug.ShouldStop(33554432);
if (__ref.runClassMethod (b4i_httpserver.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_UploadedFile"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_UploadedFile"))),(Object)((_req)),(Object)((_resp)));};
 BA.debugLineNum = 91;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _data_websocketclose(RemoteObject __ref,RemoteObject _closecode,RemoteObject _closemessage) throws Exception{
try {
		Debug.PushSubsStack("Data_WebSocketClose (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,101);
if (RapidSub.canDelegate("data_websocketclose")) { return __ref.runUserSub(false, "httpserver","data_websocketclose", __ref, _closecode, _closemessage);}
Debug.locals.put("CloseCode", _closecode);
Debug.locals.put("CloseMessage", _closemessage);
 BA.debugLineNum = 101;BA.debugLine="Private Sub Data_WebSocketClose(CloseCode As Int,";
Debug.ShouldStop(16);
 BA.debugLineNum = 102;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_WebSocketC";
Debug.ShouldStop(32);
if (__ref.runClassMethod (b4i_httpserver.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_WebSocketClose"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_WebSocketClose"))),(Object)((_closecode)),(Object)((_closemessage)));};
 BA.debugLineNum = 103;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getmyip(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetMyIP (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,61);
if (RapidSub.canDelegate("getmyip")) { return __ref.runUserSub(false, "httpserver","getmyip", __ref);}
 BA.debugLineNum = 61;BA.debugLine="Public Sub GetMyIP As String";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 62;BA.debugLine="Return Serv.GetMyIP";
Debug.ShouldStop(536870912);
if (true) return __ref.getField(false,"_serv" /*RemoteObject*/ ).runMethod(true,"GetMyIP");
 BA.debugLineNum = 63;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getmywifiip(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetMyWifiIp (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,65);
if (RapidSub.canDelegate("getmywifiip")) { return __ref.runUserSub(false, "httpserver","getmywifiip", __ref);}
 BA.debugLineNum = 65;BA.debugLine="Public Sub GetMyWifiIp As String";
Debug.ShouldStop(1);
 BA.debugLineNum = 69;BA.debugLine="Return Serv.GetMyWifiIp";
Debug.ShouldStop(16);
if (true) return __ref.getField(false,"_serv" /*RemoteObject*/ ).runMethod(true,"GetMyWifiIp");
 BA.debugLineNum = 71;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _gettemppath(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("getTempPath (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,73);
if (RapidSub.canDelegate("gettemppath")) { return __ref.runUserSub(false, "httpserver","gettemppath", __ref);}
 BA.debugLineNum = 73;BA.debugLine="Public Sub getTempPath As String";
Debug.ShouldStop(256);
 BA.debugLineNum = 77;BA.debugLine="Return File.DirTemp";
Debug.ShouldStop(4096);
if (true) return b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp");
 BA.debugLineNum = 79;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _callback,RemoteObject _eventname) throws Exception{
try {
		Debug.PushSubsStack("Initialize (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,27);
if (RapidSub.canDelegate("initialize")) { return __ref.runUserSub(false, "httpserver","initialize", __ref, _ba, _callback, _eventname);}
__ref.runVoidMethodAndSync("initializeClassModule");
Debug.locals.put("ba", _ba);
Debug.locals.put("CallBack", _callback);
Debug.locals.put("EventName", _eventname);
 BA.debugLineNum = 27;BA.debugLine="Public Sub Initialize(CallBack As Object, EventNam";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 28;BA.debugLine="Users.Initialize";
Debug.ShouldStop(134217728);
__ref.getField(false,"_users" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 29;BA.debugLine="mCallBack=CallBack";
Debug.ShouldStop(268435456);
__ref.setField ("_mcallback" /*RemoteObject*/ ,_callback);
 BA.debugLineNum = 30;BA.debugLine="mEventName=EventName";
Debug.ShouldStop(536870912);
__ref.setField ("_meventname" /*RemoteObject*/ ,_eventname);
 BA.debugLineNum = 32;BA.debugLine="htdigest.Initialize";
Debug.ShouldStop(-2147483648);
__ref.getField(false,"_htdigest" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 33;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static void  _start(RemoteObject __ref,RemoteObject _port) throws Exception{
try {
		Debug.PushSubsStack("Start (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,36);
if (RapidSub.canDelegate("start")) { __ref.runUserSub(false, "httpserver","start", __ref, _port); return;}
ResumableSub_Start rsub = new ResumableSub_Start(null,__ref,_port);
rsub.resume(null, null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static class ResumableSub_Start extends BA.ResumableSub {
public ResumableSub_Start(b4i_httpserver parent,RemoteObject __ref,RemoteObject _port) {
this.parent = parent;
this.__ref = __ref;
this._port = _port;
}
java.util.LinkedHashMap<String, Object> rsLocals = new java.util.LinkedHashMap<String, Object>();
RemoteObject __ref;
b4i_httpserver parent;
RemoteObject _port;
RemoteObject _successful = RemoteObject.createImmutable(false);
RemoteObject _newsocket = RemoteObject.declareNull("B4ISocketWrapper");
RemoteObject _sr = RemoteObject.declareNull("b4i_servletrequest");

@Override
public void resume(BA ba, RemoteObject result) throws Exception{
try {
		Debug.PushSubsStack("Start (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,36);
Debug.locals = rsLocals;Debug.currentSubFrame.locals = rsLocals;

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
Debug.locals.put("_ref", __ref);
Debug.locals.put("Port", _port);
 BA.debugLineNum = 37;BA.debugLine="Serv.Initialize(Port,\"Serv\")";
Debug.ShouldStop(16);
__ref.getField(false,"_serv" /*RemoteObject*/ ).runVoidMethod ("Initialize:::",__ref.getField(false, "bi"),(Object)(_port),(Object)(RemoteObject.createImmutable("Serv")));
 BA.debugLineNum = 39;BA.debugLine="Work=True";
Debug.ShouldStop(64);
__ref.setField ("_work" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));
 BA.debugLineNum = 40;BA.debugLine="Do While Work";
Debug.ShouldStop(128);
if (true) break;

case 1:
//do while
this.state = 14;
while (__ref.getField(true,"_work" /*RemoteObject*/ ).getBoolean()) {
this.state = 3;
if (true) break;
}
if (true) break;

case 3:
//C
this.state = 4;
 BA.debugLineNum = 41;BA.debugLine="Serv.Listen";
Debug.ShouldStop(256);
__ref.getField(false,"_serv" /*RemoteObject*/ ).runVoidMethod ("Listen");
 BA.debugLineNum = 42;BA.debugLine="Wait For Serv_NewConnection (Successful As Boole";
Debug.ShouldStop(512);
b4i_main.__c.runVoidMethod ("WaitFor::::","serv_newconnection::", __ref.getField(false, "bi"), anywheresoftware.b4a.pc.PCResumableSub.createDebugResumeSub(this, "httpserver", "start"), null);
this.state = 15;
return;
case 15:
//C
this.state = 4;
_successful = (RemoteObject) result.runMethod(true,"objectAtIndex:", RemoteObject.createImmutable(1));Debug.locals.put("Successful", _successful);
_newsocket = (RemoteObject) result.runMethod(false,"objectAtIndex:", RemoteObject.createImmutable(2));Debug.locals.put("NewSocket", _newsocket);
;
 BA.debugLineNum = 44;BA.debugLine="If Successful Then";
Debug.ShouldStop(2048);
if (true) break;

case 4:
//if
this.state = 13;
if (_successful.getBoolean()) { 
this.state = 6;
}if (true) break;

case 6:
//C
this.state = 7;
 BA.debugLineNum = 45;BA.debugLine="Dim SR As ServletRequest";
Debug.ShouldStop(4096);
_sr = RemoteObject.createNew ("b4i_servletrequest");Debug.locals.put("SR", _sr);
 BA.debugLineNum = 46;BA.debugLine="SR.Timeout=Timeout";
Debug.ShouldStop(8192);
_sr.setField ("_timeout" /*RemoteObject*/ ,BA.numberCast(long.class, __ref.getField(true,"_timeout" /*RemoteObject*/ )));
 BA.debugLineNum = 47;BA.debugLine="SR.Initialize(Me,\"Data\",NewSocket)";
Debug.ShouldStop(16384);
_sr.runClassMethod (b4i_servletrequest.class, "_initialize::::" /*RemoteObject*/ ,__ref.getField(false, "bi"),(Object)(__ref),(Object)(BA.ObjectToString("Data")),(Object)(_newsocket));
 BA.debugLineNum = 49;BA.debugLine="If SubExists(mCallBack,mEventName & \"_NewConect";
Debug.ShouldStop(65536);
if (true) break;

case 7:
//if
this.state = 12;
if (b4i_main.__c.runMethod(true,"SubExists:::",(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_NewConection"))),(Object)(BA.numberCast(int.class, 1))).getBoolean()) { 
this.state = 9;
;}if (true) break;

case 9:
//C
this.state = 12;
b4i_main.__c.runMethodAndSync(false,"CallSub2::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_NewConection"))),(Object)((_sr)));
if (true) break;

case 12:
//C
this.state = 13;
;
 if (true) break;

case 13:
//C
this.state = 1;
;
 if (true) break;

case 14:
//C
this.state = -1;
;
 BA.debugLineNum = 55;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
if (true) break;

            }
        }
    }
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}
public static void  _serv_newconnection(RemoteObject __ref,RemoteObject _successful,RemoteObject _newsocket) throws Exception{
}
public static RemoteObject  _stop(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("Stop (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,57);
if (RapidSub.canDelegate("stop")) { return __ref.runUserSub(false, "httpserver","stop", __ref);}
 BA.debugLineNum = 57;BA.debugLine="Public Sub Stop";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 58;BA.debugLine="Work=False";
Debug.ShouldStop(33554432);
__ref.setField ("_work" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"False"));
 BA.debugLineNum = 59;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _subexists2(RemoteObject __ref,RemoteObject _callobject,RemoteObject _eventname,RemoteObject _param) throws Exception{
try {
		Debug.PushSubsStack("SubExists2 (httpserver) ","httpserver",1,__ref.getField(false, "bi"),__ref,105);
if (RapidSub.canDelegate("subexists2")) { return __ref.runUserSub(false, "httpserver","subexists2", __ref, _callobject, _eventname, _param);}
Debug.locals.put("CallObject", _callobject);
Debug.locals.put("EventName", _eventname);
Debug.locals.put("Param", _param);
 BA.debugLineNum = 105;BA.debugLine="Private Sub SubExists2(CallObject As Object, Event";
Debug.ShouldStop(256);
 BA.debugLineNum = 107;BA.debugLine="Return SubExists(CallObject,EventName, param)";
Debug.ShouldStop(1024);
if (true) return b4i_main.__c.runMethod(true,"SubExists:::",(Object)(_callobject),(Object)(_eventname),(Object)(_param));
 BA.debugLineNum = 111;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}