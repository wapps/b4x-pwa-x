package net.wapps.httpserver;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class b4i_servletresponse_subs_0 {


public static RemoteObject  _appenfinal(RemoteObject __ref,RemoteObject _b) throws Exception{
try {
		Debug.PushSubsStack("AppenFinal (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,425);
if (RapidSub.canDelegate("appenfinal")) { return __ref.runUserSub(false, "servletresponse","appenfinal", __ref, _b);}
RemoteObject _fn = null;
Debug.locals.put("B", _b);
 BA.debugLineNum = 425;BA.debugLine="Private Sub AppenFinal(B() As Byte)";
Debug.ShouldStop(256);
 BA.debugLineNum = 426;BA.debugLine="Dim Fn(Final.Length+b.Length) As Byte";
Debug.ShouldStop(512);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {__ref.getField(false,"_final" /*RemoteObject*/ ).getField(true,"Length"),_b.getField(true,"Length")}, "+",1, 1)});Debug.locals.put("Fn", _fn);
 BA.debugLineNum = 427;BA.debugLine="Bit.ArrayCopy(Final,0,Fn,0,Final.Length)";
Debug.ShouldStop(1024);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(__ref.getField(false,"_final" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(__ref.getField(false,"_final" /*RemoteObject*/ ).getField(true,"Length")));
 BA.debugLineNum = 428;BA.debugLine="Bit.ArrayCopy(B,0,Fn,Final.Length,B.Length)";
Debug.ShouldStop(2048);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_b),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(__ref.getField(false,"_final" /*RemoteObject*/ ).getField(true,"Length")),(Object)(_b.getField(true,"Length")));
 BA.debugLineNum = 429;BA.debugLine="Final=Fn";
Debug.ShouldStop(4096);
__ref.setField ("_final" /*RemoteObject*/ ,_fn);
 BA.debugLineNum = 430;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _astream_write(RemoteObject __ref,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("astream_write (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,272);
if (RapidSub.canDelegate("astream_write")) { return __ref.runUserSub(false, "servletresponse","astream_write", __ref, _data);}
Debug.locals.put("Data", _data);
 BA.debugLineNum = 272;BA.debugLine="Private Sub astream_write(Data() As Byte)";
Debug.ShouldStop(32768);
 BA.debugLineNum = 273;BA.debugLine="Try";
Debug.ShouldStop(65536);
try { BA.debugLineNum = 274;BA.debugLine="astream.Write(Data)";
Debug.ShouldStop(131072);
__ref.getField(false,"_astream" /*RemoteObject*/ ).runVoidMethod ("Write:",(Object)(_data));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e4) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e4.toString()); BA.debugLineNum = 276;BA.debugLine="Log(LastException)";
Debug.ShouldStop(524288);
b4i_main.__c.runVoidMethod ("LogImpl:::","36225924",BA.ObjectToString(b4i_main.__c.runMethod(false,"LastException")),0);
 };
 BA.debugLineNum = 278;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 1;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 2;BA.debugLine="Private Request As ServletRequest";
b4i_servletresponse._request = RemoteObject.createNew ("b4i_servletrequest");__ref.setField("_request",b4i_servletresponse._request);
 //BA.debugLineNum = 3;BA.debugLine="Private astream As AsyncStreams";
b4i_servletresponse._astream = RemoteObject.createNew ("B4IAsyncStreams");__ref.setField("_astream",b4i_servletresponse._astream);
 //BA.debugLineNum = 5;BA.debugLine="Private ResponseHeader As Map";
b4i_servletresponse._responseheader = RemoteObject.createNew ("B4IMap");__ref.setField("_responseheader",b4i_servletresponse._responseheader);
 //BA.debugLineNum = 6;BA.debugLine="Private ResponseParameter As Map";
b4i_servletresponse._responseparameter = RemoteObject.createNew ("B4IMap");__ref.setField("_responseparameter",b4i_servletresponse._responseparameter);
 //BA.debugLineNum = 7;BA.debugLine="Private ResponseCookies As Map";
b4i_servletresponse._responsecookies = RemoteObject.createNew ("B4IMap");__ref.setField("_responsecookies",b4i_servletresponse._responsecookies);
 //BA.debugLineNum = 8;BA.debugLine="Private Code As Map";
b4i_servletresponse._code = RemoteObject.createNew ("B4IMap");__ref.setField("_code",b4i_servletresponse._code);
 //BA.debugLineNum = 9;BA.debugLine="Private MIME As Map";
b4i_servletresponse._mime = RemoteObject.createNew ("B4IMap");__ref.setField("_mime",b4i_servletresponse._mime);
 //BA.debugLineNum = 10;BA.debugLine="Private Client As Socket";
b4i_servletresponse._client = RemoteObject.createNew ("B4ISocketWrapper");__ref.setField("_client",b4i_servletresponse._client);
 //BA.debugLineNum = 12;BA.debugLine="Public Status As Int = 200";
b4i_servletresponse._status = BA.numberCast(int.class, 200);__ref.setField("_status",b4i_servletresponse._status);
 //BA.debugLineNum = 13;BA.debugLine="Public ContentType As String = \"\"";
b4i_servletresponse._contenttype = BA.ObjectToString("");__ref.setField("_contenttype",b4i_servletresponse._contenttype);
 //BA.debugLineNum = 14;BA.debugLine="Public CharacterEncoding As String = \"UTF-8\" '\"IS";
b4i_servletresponse._characterencoding = BA.ObjectToString("UTF-8");__ref.setField("_characterencoding",b4i_servletresponse._characterencoding);
 //BA.debugLineNum = 15;BA.debugLine="Public ContentLenght As Int = 0";
b4i_servletresponse._contentlenght = BA.numberCast(int.class, 0);__ref.setField("_contentlenght",b4i_servletresponse._contentlenght);
 //BA.debugLineNum = 17;BA.debugLine="Private DDay() As String = Array As String(\"\",\"Su";
b4i_servletresponse._dday = RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {BA.ObjectToString(""),BA.ObjectToString("Sun"),BA.ObjectToString("Mon"),BA.ObjectToString("Tue"),BA.ObjectToString("Wed"),BA.ObjectToString("Thu"),BA.ObjectToString("Fri"),RemoteObject.createImmutable("Sat")});__ref.setField("_dday",b4i_servletresponse._dday);
 //BA.debugLineNum = 18;BA.debugLine="Private MMmonth() As String = Array As String(\"\",";
b4i_servletresponse._mmmonth = RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {BA.ObjectToString(""),BA.ObjectToString("January"),BA.ObjectToString("February"),BA.ObjectToString("March"),BA.ObjectToString("April"),BA.ObjectToString("May"),BA.ObjectToString("June"),BA.ObjectToString("July"),BA.ObjectToString("August"),BA.ObjectToString("September"),BA.ObjectToString("October"),BA.ObjectToString("November"),RemoteObject.createImmutable("December")});__ref.setField("_mmmonth",b4i_servletresponse._mmmonth);
 //BA.debugLineNum = 20;BA.debugLine="Private BC As ByteConverter";
b4i_servletresponse._bc = RemoteObject.createNew ("B4IByteConverter");__ref.setField("_bc",b4i_servletresponse._bc);
 //BA.debugLineNum = 21;BA.debugLine="Private myQuery As QueryElement";
b4i_servletresponse._myquery = RemoteObject.createNew ("b4i_queryelement");__ref.setField("_myquery",b4i_servletresponse._myquery);
 //BA.debugLineNum = 22;BA.debugLine="Private Final() As Byte";
b4i_servletresponse._final = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 0)});__ref.setField("_final",b4i_servletresponse._final);
 //BA.debugLineNum = 28;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _close(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("Close (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,83);
if (RapidSub.canDelegate("close")) { return __ref.runUserSub(false, "servletresponse","close", __ref);}
 BA.debugLineNum = 83;BA.debugLine="Public Sub Close";
Debug.ShouldStop(262144);
 BA.debugLineNum = 84;BA.debugLine="Try";
Debug.ShouldStop(524288);
try { BA.debugLineNum = 85;BA.debugLine="Request.Close";
Debug.ShouldStop(1048576);
__ref.getField(false,"_request" /*RemoteObject*/ ).runClassMethod (b4i_servletrequest.class, "_close" /*RemoteObject*/ );
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e4) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e4.toString()); BA.debugLineNum = 87;BA.debugLine="Log(\"Server error:\" & LastException.Message)";
Debug.ShouldStop(4194304);
b4i_main.__c.runVoidMethod ("LogImpl:::","35177348",RemoteObject.concat(RemoteObject.createImmutable("Server error:"),b4i_main.__c.runMethod(false,"LastException").runMethod(true,"Message")),0);
 };
 BA.debugLineNum = 89;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _completedate(RemoteObject __ref,RemoteObject _dt) throws Exception{
try {
		Debug.PushSubsStack("CompleteDate (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,197);
if (RapidSub.canDelegate("completedate")) { return __ref.runUserSub(false, "servletresponse","completedate", __ref, _dt);}
Debug.locals.put("DT", _dt);
 BA.debugLineNum = 197;BA.debugLine="Private Sub CompleteDate(DT As Long) As String";
Debug.ShouldStop(16);
 BA.debugLineNum = 198;BA.debugLine="Return $\"${DDay(DateTime.GetDayOfWeek(DT))}, $2.0";
Debug.ShouldStop(32);
if (true) return (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(false,"_dday" /*RemoteObject*/ ).runMethod(true,"getObjectFast:", b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetDayOfWeek:",(Object)(_dt)))))),RemoteObject.createImmutable(", "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("2.0")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetDayOfMonth:",(Object)(_dt))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(false,"_mmmonth" /*RemoteObject*/ ).runMethod(true,"getObjectFast:", b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetMonth:",(Object)(_dt))).runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(BA.numberCast(int.class, 3)))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetYear:",(Object)(_dt))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Time:",(Object)(_dt))))),RemoteObject.createImmutable(" GMT")));
 BA.debugLineNum = 199;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _connected(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("Connected (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,95);
if (RapidSub.canDelegate("connected")) { return __ref.runUserSub(false, "servletresponse","connected", __ref);}
 BA.debugLineNum = 95;BA.debugLine="Public Sub Connected As Boolean";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 96;BA.debugLine="Return Client.Connected";
Debug.ShouldStop(-2147483648);
if (true) return __ref.getField(false,"_client" /*RemoteObject*/ ).runMethod(true,"Connected");
 BA.debugLineNum = 97;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _datamask(RemoteObject __ref,RemoteObject _data,RemoteObject _maskingkey) throws Exception{
try {
		Debug.PushSubsStack("DataMask (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,385);
if (RapidSub.canDelegate("datamask")) { return __ref.runUserSub(false, "servletresponse","datamask", __ref, _data, _maskingkey);}
RemoteObject _datafree = null;
int _i = 0;
RemoteObject _smask = RemoteObject.createImmutable(0);
Debug.locals.put("Data", _data);
Debug.locals.put("Maskingkey", _maskingkey);
 BA.debugLineNum = 385;BA.debugLine="Private Sub DataMask(Data() As Byte, Maskingkey()";
Debug.ShouldStop(1);
 BA.debugLineNum = 388;BA.debugLine="Dim DataFree(Data.Length) As Byte";
Debug.ShouldStop(8);
_datafree = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {_data.getField(true,"Length")});Debug.locals.put("DataFree", _datafree);
 BA.debugLineNum = 389;BA.debugLine="For i=0 To Data.Length-1";
Debug.ShouldStop(16);
{
final int step2 = 1;
final int limit2 = RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),RemoteObject.createImmutable(1)}, "-",1, 1).<Number>get().intValue();
_i = 0 ;
for (;(step2 > 0 && _i <= limit2) || (step2 < 0 && _i >= limit2) ;_i = ((int)(0 + _i + step2))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 390;BA.debugLine="Dim Smask As Int = Bit.And(0xFF, Data(i))";
Debug.ShouldStop(32);
_smask = b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, 0xff)),(Object)(BA.numberCast(int.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, _i)))));Debug.locals.put("Smask", _smask);Debug.locals.put("Smask", _smask);
 BA.debugLineNum = 391;BA.debugLine="DataFree(i)=Bit.Xor(Smask,Bit.And(0xFF,Maskingke";
Debug.ShouldStop(64);
_datafree.runVoidMethod ("setByteFast::", BA.numberCast(int.class, _i),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Xor::",(Object)(_smask),(Object)(b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, 0xff)),(Object)(BA.numberCast(int.class, _maskingkey.runMethod(true,"getByteFast:", RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),RemoteObject.createImmutable(4)}, "%",0, 1)))))))));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 394;BA.debugLine="Return DataFree";
Debug.ShouldStop(512);
if (true) return _datafree;
 BA.debugLineNum = 395;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _deflatedate(RemoteObject __ref,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("DeflateDate (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,397);
if (RapidSub.canDelegate("deflatedate")) { return __ref.runUserSub(false, "servletresponse","deflatedate", __ref, _data);}
RemoteObject _nativeme = RemoteObject.declareNull("B4INativeObject");
RemoteObject _infobject = RemoteObject.declareNull("NSObject");
RemoteObject _raw = null;
RemoteObject _inft = null;
Debug.locals.put("data", _data);
 BA.debugLineNum = 397;BA.debugLine="Private Sub DeflateDate(data() As Byte) As Byte()";
Debug.ShouldStop(4096);
 BA.debugLineNum = 401;BA.debugLine="Dim NativeMe As NativeObject = Me";
Debug.ShouldStop(65536);
_nativeme = RemoteObject.createNew ("B4INativeObject");
_nativeme = BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4INativeObject"), __ref);Debug.locals.put("NativeMe", _nativeme);
 BA.debugLineNum = 402;BA.debugLine="Dim infObject As Object = NativeMe.RunMethod(\"gz";
Debug.ShouldStop(131072);
_infobject = ((_nativeme.runMethod(false,"RunMethod::",(Object)(BA.ObjectToString("gzipInflate:")),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_nativeme.runMethod(false,"ArrayToNSData:",(Object)(_data))})))).getObject());Debug.locals.put("infObject", _infobject);Debug.locals.put("infObject", _infobject);
 BA.debugLineNum = 403;BA.debugLine="Dim raw() As Byte= NativeMe.NSDataToArray(infObj";
Debug.ShouldStop(262144);
_raw = _nativeme.runMethod(false,"NSDataToArray:",(Object)(_infobject));Debug.locals.put("raw", _raw);Debug.locals.put("raw", _raw);
 BA.debugLineNum = 404;BA.debugLine="Dim Inft(raw.Length-2) As Byte";
Debug.ShouldStop(524288);
_inft = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_raw.getField(true,"Length"),RemoteObject.createImmutable(2)}, "-",1, 1)});Debug.locals.put("Inft", _inft);
 BA.debugLineNum = 405;BA.debugLine="Bit.ArrayCopy(raw,2,Inft,0,raw.Length-2)";
Debug.ShouldStop(1048576);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_raw),(Object)(BA.numberCast(int.class, 2)),(Object)(_inft),(Object)(BA.numberCast(int.class, 0)),(Object)(RemoteObject.solve(new RemoteObject[] {_raw.getField(true,"Length"),RemoteObject.createImmutable(2)}, "-",1, 1)));
 BA.debugLineNum = 406;BA.debugLine="Return Inft";
Debug.ShouldStop(2097152);
if (true) return _inft;
 BA.debugLineNum = 423;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getoutputstream(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("getOutputStream (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,99);
if (RapidSub.canDelegate("getoutputstream")) { return __ref.runUserSub(false, "servletresponse","getoutputstream", __ref);}
 BA.debugLineNum = 99;BA.debugLine="Public Sub getOutputStream As OutputStream";
Debug.ShouldStop(4);
 BA.debugLineNum = 100;BA.debugLine="Return Client.OutputStream";
Debug.ShouldStop(8);
if (true) return __ref.getField(false,"_client" /*RemoteObject*/ ).runMethod(false,"OutputStream");
 BA.debugLineNum = 101;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getquery(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("getQuery (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,91);
if (RapidSub.canDelegate("getquery")) { return __ref.runUserSub(false, "servletresponse","getquery", __ref);}
 BA.debugLineNum = 91;BA.debugLine="Public Sub getQuery As QueryElement";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 92;BA.debugLine="Return myQuery";
Debug.ShouldStop(134217728);
if (true) return __ref.getField(false,"_myquery" /*RemoteObject*/ );
 BA.debugLineNum = 93;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _handshakefile(RemoteObject __ref,RemoteObject _lastmodified,RemoteObject _filesize) throws Exception{
try {
		Debug.PushSubsStack("HandShakeFile (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,235);
if (RapidSub.canDelegate("handshakefile")) { return __ref.runUserSub(false, "servletresponse","handshakefile", __ref, _lastmodified, _filesize);}
RemoteObject _handshake = RemoteObject.createImmutable("");
RemoteObject _k = RemoteObject.createImmutable("");
RemoteObject _scks = RemoteObject.createImmutable("");
Debug.locals.put("lastModified", _lastmodified);
Debug.locals.put("FileSize", _filesize);
 BA.debugLineNum = 235;BA.debugLine="Private Sub HandShakeFile(lastModified As String,";
Debug.ShouldStop(1024);
 BA.debugLineNum = 236;BA.debugLine="Dim HandShake As String = \"\"";
Debug.ShouldStop(2048);
_handshake = BA.ObjectToString("");Debug.locals.put("HandShake", _handshake);Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 239;BA.debugLine="For Each K As String In ResponseHeader.Keys";
Debug.ShouldStop(16384);
{
final RemoteObject group2 = __ref.getField(false,"_responseheader" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen2 = group2.runMethod(true,"Size").<Integer>get()
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_k = BA.ObjectToString(group2.runMethod(false,"Get:",index2));Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 240;BA.debugLine="HandShake=$\"${HandShake}${k}: ${ResponseHeader.G";
Debug.ShouldStop(32768);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_k))),RemoteObject.createImmutable(": "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_k))))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 243;BA.debugLine="Dim Scks As String = \"\"";
Debug.ShouldStop(262144);
_scks = BA.ObjectToString("");Debug.locals.put("Scks", _scks);Debug.locals.put("Scks", _scks);
 BA.debugLineNum = 244;BA.debugLine="For Each K As String In ResponseCookies.Keys";
Debug.ShouldStop(524288);
{
final RemoteObject group6 = __ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen6 = group6.runMethod(true,"Size").<Integer>get()
;int index6 = 0;
;
for (; index6 < groupLen6;index6++){
_k = BA.ObjectToString(group6.runMethod(false,"Get:",index6));Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 245;BA.debugLine="Scks=$\"${Scks}${k}=${ResponseCookies.Get(K)}; \"$";
Debug.ShouldStop(1048576);
_scks = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_k))),RemoteObject.createImmutable("="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(__ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_k))))),RemoteObject.createImmutable("; ")));Debug.locals.put("Scks", _scks);
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 247;BA.debugLine="If Scks<>\"\" Then Scks=$\"Set-Cookie: ${Scks}${CRLF";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean("!",_scks,BA.ObjectToString(""))) { 
_scks = (RemoteObject.concat(RemoteObject.createImmutable("Set-Cookie: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("Expires="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now"),(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"TicksPerDay"),RemoteObject.createImmutable(365)}, "*",0, 2))}, "+",1, 2)))))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("")));Debug.locals.put("Scks", _scks);};
 BA.debugLineNum = 249;BA.debugLine="HandShake=$\"HTTP/1.1 200 OK Date: ${CompleteDate(";
Debug.ShouldStop(16777216);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.1 200 OK\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Last-Modified: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_lastmodified))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Content-Type: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_contenttype" /*RemoteObject*/ )))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Accept-Ranges: bytes\n"),RemoteObject.createImmutable("Content-Length: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_filesize))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Server: HttpServer (B4X)\n"),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 258;BA.debugLine="Return HandShake.Replace(CRLF,Chr(13) & Chr(10))";
Debug.ShouldStop(2);
if (true) return _handshake.runMethod(true,"Replace::",(Object)(b4i_main.__c.runMethod(true,"CRLF")),(Object)(RemoteObject.concat(BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(BA.numberCast(int.class, 13)))),BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(BA.numberCast(int.class, 10)))))));
 BA.debugLineNum = 259;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _handshakestring(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("HandShakeString (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,209);
if (RapidSub.canDelegate("handshakestring")) { return __ref.runUserSub(false, "servletresponse","handshakestring", __ref);}
RemoteObject _handshake = RemoteObject.createImmutable("");
RemoteObject _k = RemoteObject.createImmutable("");
RemoteObject _scks = RemoteObject.createImmutable("");
 BA.debugLineNum = 209;BA.debugLine="Private Sub HandShakeString As String";
Debug.ShouldStop(65536);
 BA.debugLineNum = 210;BA.debugLine="Dim HandShake As String = \"\"";
Debug.ShouldStop(131072);
_handshake = BA.ObjectToString("");Debug.locals.put("HandShake", _handshake);Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 213;BA.debugLine="For Each K As String In ResponseHeader.Keys";
Debug.ShouldStop(1048576);
{
final RemoteObject group2 = __ref.getField(false,"_responseheader" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen2 = group2.runMethod(true,"Size").<Integer>get()
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_k = BA.ObjectToString(group2.runMethod(false,"Get:",index2));Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 214;BA.debugLine="HandShake=$\"${HandShake}${k}: ${ResponseHeader.G";
Debug.ShouldStop(2097152);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_k))),RemoteObject.createImmutable(": "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_k))))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 217;BA.debugLine="Dim Scks As String = \"\"";
Debug.ShouldStop(16777216);
_scks = BA.ObjectToString("");Debug.locals.put("Scks", _scks);Debug.locals.put("Scks", _scks);
 BA.debugLineNum = 218;BA.debugLine="For Each K As String In ResponseCookies.Keys";
Debug.ShouldStop(33554432);
{
final RemoteObject group6 = __ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen6 = group6.runMethod(true,"Size").<Integer>get()
;int index6 = 0;
;
for (; index6 < groupLen6;index6++){
_k = BA.ObjectToString(group6.runMethod(false,"Get:",index6));Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 219;BA.debugLine="Scks=$\"${Scks}${k}=${ResponseCookies.Get(K)}; \"$";
Debug.ShouldStop(67108864);
_scks = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_k))),RemoteObject.createImmutable("="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(__ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_k))))),RemoteObject.createImmutable("; ")));Debug.locals.put("Scks", _scks);
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 221;BA.debugLine="If Scks<>\"\" Then Scks=$\"Set-Cookie: ${Scks}${CRLF";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean("!",_scks,BA.ObjectToString(""))) { 
_scks = (RemoteObject.concat(RemoteObject.createImmutable("Set-Cookie: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("Expires="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now"),(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"TicksPerDay"),RemoteObject.createImmutable(365)}, "*",0, 2))}, "+",1, 2)))))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("")));Debug.locals.put("Scks", _scks);};
 BA.debugLineNum = 224;BA.debugLine="HandShake=$\"HTTP/1.1 ${Status} ${StatusCode(Statu";
Debug.ShouldStop(-2147483648);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.1 "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_status" /*RemoteObject*/ )))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_statuscode:" /*RemoteObject*/ ,(Object)(__ref.getField(true,"_status" /*RemoteObject*/ )))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Server: HttpServer (B4X)\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Last-Modified: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Content-Type: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_contenttype" /*RemoteObject*/ )))),RemoteObject.createImmutable("; charset="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable("Content-Length: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_contentlenght" /*RemoteObject*/ )))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 232;BA.debugLine="Return HandShake";
Debug.ShouldStop(128);
if (true) return _handshake;
 BA.debugLineNum = 233;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _req,RemoteObject _ast,RemoteObject _sck) throws Exception{
try {
		Debug.PushSubsStack("Initialize (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,31);
if (RapidSub.canDelegate("initialize")) { return __ref.runUserSub(false, "servletresponse","initialize", __ref, _ba, _req, _ast, _sck);}
__ref.runVoidMethodAndSync("initializeClassModule");
Debug.locals.put("ba", _ba);
Debug.locals.put("Req", _req);
Debug.locals.put("ast", _ast);
Debug.locals.put("Sck", _sck);
 BA.debugLineNum = 31;BA.debugLine="Public Sub Initialize(Req As ServletRequest, ast A";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 32;BA.debugLine="Request=Req";
Debug.ShouldStop(-2147483648);
__ref.setField ("_request" /*RemoteObject*/ ,_req);
 BA.debugLineNum = 33;BA.debugLine="astream=ast";
Debug.ShouldStop(1);
__ref.setField ("_astream" /*RemoteObject*/ ,_ast);
 BA.debugLineNum = 34;BA.debugLine="Client=Sck";
Debug.ShouldStop(2);
__ref.setField ("_client" /*RemoteObject*/ ,_sck);
 BA.debugLineNum = 36;BA.debugLine="ResponseHeader.Initialize";
Debug.ShouldStop(8);
__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 37;BA.debugLine="ResponseParameter.Initialize";
Debug.ShouldStop(16);
__ref.getField(false,"_responseparameter" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 38;BA.debugLine="ResponseCookies.Initialize";
Debug.ShouldStop(32);
__ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 40;BA.debugLine="ResponseHeader.Put(\"Accept-Ranges\",\"bytes\")";
Debug.ShouldStop(128);
__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("Accept-Ranges"))),(Object)((RemoteObject.createImmutable("bytes"))));
 BA.debugLineNum = 41;BA.debugLine="ResponseHeader.Put(\"Server\",\"b4x (0.0.1)\")";
Debug.ShouldStop(256);
__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("Server"))),(Object)((RemoteObject.createImmutable("b4x (0.0.1)"))));
 BA.debugLineNum = 42;BA.debugLine="ResponseHeader.Put(\"Accept-Charset\",\"utf-8\")";
Debug.ShouldStop(512);
__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("Accept-Charset"))),(Object)((RemoteObject.createImmutable("utf-8"))));
 BA.debugLineNum = 43;BA.debugLine="ResponseHeader.Put(\"Cache-control\",\"no-cache\")";
Debug.ShouldStop(1024);
__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)(RemoteObject.createImmutable(("Cache-control"))),(Object)((RemoteObject.createImmutable("no-cache"))));
 BA.debugLineNum = 46;BA.debugLine="myQuery.Initialize(Me)";
Debug.ShouldStop(8192);
__ref.getField(false,"_myquery" /*RemoteObject*/ ).runClassMethod (b4i_queryelement.class, "_initialize::" /*RemoteObject*/ ,__ref.getField(false, "bi"),(Object)((__ref)));
 BA.debugLineNum = 49;BA.debugLine="Code=CreateMap(100:\"Continue\", 101:\"Switching Pro";
Debug.ShouldStop(65536);
__ref.setField ("_code" /*RemoteObject*/ ,b4i_main.__c.runMethod(false, "createMap:", (Object)(new RemoteObject[] {RemoteObject.createImmutable((100)),RemoteObject.createImmutable(("Continue")),RemoteObject.createImmutable((101)),RemoteObject.createImmutable(("Switching Protocols")),RemoteObject.createImmutable((200)),RemoteObject.createImmutable(("OK")),RemoteObject.createImmutable((201)),RemoteObject.createImmutable(("Created")),RemoteObject.createImmutable((202)),RemoteObject.createImmutable(("Accepted")),RemoteObject.createImmutable((203)),RemoteObject.createImmutable(("Non-Authoritative Informatio")),RemoteObject.createImmutable((204)),RemoteObject.createImmutable(("No Content")),RemoteObject.createImmutable((205)),RemoteObject.createImmutable(("Reset Content")),RemoteObject.createImmutable((206)),RemoteObject.createImmutable(("Partial Content")),RemoteObject.createImmutable((300)),RemoteObject.createImmutable(("Multiple Choices")),RemoteObject.createImmutable((301)),RemoteObject.createImmutable(("Moved Permanently")),RemoteObject.createImmutable((302)),RemoteObject.createImmutable(("Found")),RemoteObject.createImmutable((303)),RemoteObject.createImmutable(("See Other")),RemoteObject.createImmutable((304)),RemoteObject.createImmutable(("Not Modified")),RemoteObject.createImmutable((305)),RemoteObject.createImmutable(("Use Proxy")),RemoteObject.createImmutable((307)),RemoteObject.createImmutable(("Temporary Redirect")),RemoteObject.createImmutable((400)),RemoteObject.createImmutable(("Bad Request")),RemoteObject.createImmutable((401)),RemoteObject.createImmutable(("Unauthorized")),RemoteObject.createImmutable((402)),RemoteObject.createImmutable(("Payment Required")),RemoteObject.createImmutable((403)),RemoteObject.createImmutable(("Forbidden")),RemoteObject.createImmutable((404)),RemoteObject.createImmutable(("Not Found")),RemoteObject.createImmutable((405)),RemoteObject.createImmutable(("	Method Not Allowed")),RemoteObject.createImmutable((406)),RemoteObject.createImmutable(("Not Acceptable")),RemoteObject.createImmutable((407)),RemoteObject.createImmutable(("Proxy Authentication Required")),RemoteObject.createImmutable((408)),RemoteObject.createImmutable(("Request Time-out")),RemoteObject.createImmutable((409)),RemoteObject.createImmutable(("Conflict")),RemoteObject.createImmutable((410)),RemoteObject.createImmutable(("Gone")),RemoteObject.createImmutable((411)),RemoteObject.createImmutable(("Length Required")),RemoteObject.createImmutable((412)),RemoteObject.createImmutable(("Precondition Failed")),RemoteObject.createImmutable((413)),RemoteObject.createImmutable(("Request Entity Too Large")),RemoteObject.createImmutable((414)),RemoteObject.createImmutable(("Request-URI Too Large")),RemoteObject.createImmutable((415)),RemoteObject.createImmutable(("Unsupported Media Type")),RemoteObject.createImmutable((416)),RemoteObject.createImmutable(("Requested range not satisfiable")),RemoteObject.createImmutable((417)),RemoteObject.createImmutable(("Expectation Failed")),RemoteObject.createImmutable((500)),RemoteObject.createImmutable(("Internal Server Error")),RemoteObject.createImmutable((502)),RemoteObject.createImmutable(("Bad Gateway")),RemoteObject.createImmutable((503)),RemoteObject.createImmutable(("Service Unavailable")),RemoteObject.createImmutable((504)),RemoteObject.createImmutable(("Gateway Time-out")),RemoteObject.createImmutable((505)),(RemoteObject.createImmutable("HTTP Version Not Supported."))})));
 BA.debugLineNum = 58;BA.debugLine="MIME=CreateMap(\"htm\":\"text/html\",\"html\":\"text/htm";
Debug.ShouldStop(33554432);
__ref.setField ("_mime" /*RemoteObject*/ ,b4i_main.__c.runMethod(false, "createMap:", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("htm")),RemoteObject.createImmutable(("text/html")),RemoteObject.createImmutable(("html")),RemoteObject.createImmutable(("text/html")),RemoteObject.createImmutable(("xml")),RemoteObject.createImmutable(("text/xml")),RemoteObject.createImmutable(("txt")),RemoteObject.createImmutable(("text/plain")),RemoteObject.createImmutable(("pdf")),RemoteObject.createImmutable(("application/pdf")),RemoteObject.createImmutable(("css")),RemoteObject.createImmutable(("text/css")),RemoteObject.createImmutable(("csv")),RemoteObject.createImmutable(("text/csv")),RemoteObject.createImmutable(("js")),RemoteObject.createImmutable(("text/javascript")),RemoteObject.createImmutable(("json")),RemoteObject.createImmutable(("application/json")),RemoteObject.createImmutable(("doc")),RemoteObject.createImmutable(("application/msword")),RemoteObject.createImmutable(("docx")),RemoteObject.createImmutable(("application/vnd.openxmlformats-officedocument.wordprocessingml.document")),RemoteObject.createImmutable(("xls")),RemoteObject.createImmutable(("application/vnd.ms-excel")),RemoteObject.createImmutable(("xlsx")),RemoteObject.createImmutable(("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")),RemoteObject.createImmutable(("odt")),RemoteObject.createImmutable(("application/vnd.oasis.opendocument.text")),RemoteObject.createImmutable(("ods")),RemoteObject.createImmutable(("application/vnd.oasis.opendocument.spreadsheet")),RemoteObject.createImmutable(("epub")),RemoteObject.createImmutable(("application/epub+zip")),RemoteObject.createImmutable(("jar")),RemoteObject.createImmutable(("application/java-archive")),RemoteObject.createImmutable(("rar")),RemoteObject.createImmutable(("application/vnd.rar")),RemoteObject.createImmutable(("zip")),RemoteObject.createImmutable(("application/zip")),RemoteObject.createImmutable(("jpeg")),RemoteObject.createImmutable(("image/jpeg")),RemoteObject.createImmutable(("jpg")),RemoteObject.createImmutable(("image/jpeg")),RemoteObject.createImmutable(("bmp")),RemoteObject.createImmutable(("image/bmp")),RemoteObject.createImmutable(("png")),RemoteObject.createImmutable(("image/png")),RemoteObject.createImmutable(("gif")),RemoteObject.createImmutable(("image/gif")),RemoteObject.createImmutable(("ico")),RemoteObject.createImmutable(("image/vnd.microsoft.icon")),RemoteObject.createImmutable(("mp3")),RemoteObject.createImmutable(("audio/mpeg")),RemoteObject.createImmutable(("avi")),RemoteObject.createImmutable(("video/x-msvideo")),RemoteObject.createImmutable(("mpeg")),(RemoteObject.createImmutable("video/mpeg"))})));
 BA.debugLineNum = 68;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _resetcookies(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("ResetCookies (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,79);
if (RapidSub.canDelegate("resetcookies")) { return __ref.runUserSub(false, "servletresponse","resetcookies", __ref);}
 BA.debugLineNum = 79;BA.debugLine="Public Sub ResetCookies";
Debug.ShouldStop(16384);
 BA.debugLineNum = 80;BA.debugLine="ResponseCookies.Clear";
Debug.ShouldStop(32768);
__ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runVoidMethod ("Clear");
 BA.debugLineNum = 81;BA.debugLine="End Sub";
Debug.ShouldStop(65536);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _searchmime(RemoteObject __ref,RemoteObject _nm) throws Exception{
try {
		Debug.PushSubsStack("SearchMime (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,261);
if (RapidSub.canDelegate("searchmime")) { return __ref.runUserSub(false, "servletresponse","searchmime", __ref, _nm);}
Debug.locals.put("Nm", _nm);
 BA.debugLineNum = 261;BA.debugLine="private Sub SearchMime(Nm As String) As String";
Debug.ShouldStop(16);
 BA.debugLineNum = 262;BA.debugLine="If Nm.IndexOf(\".\")>-1 Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean(">",_nm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("."))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 263;BA.debugLine="Nm=Nm.ToLowerCase.SubString(Nm.IndexOf(\".\")+1)";
Debug.ShouldStop(64);
_nm = _nm.runMethod(true,"ToLowerCase").runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_nm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("."))),RemoteObject.createImmutable(1)}, "+",1, 1)));Debug.locals.put("Nm", _nm);
 BA.debugLineNum = 264;BA.debugLine="If MIME.ContainsKey(Nm) Then";
Debug.ShouldStop(128);
if (__ref.getField(false,"_mime" /*RemoteObject*/ ).runMethod(true,"ContainsKey:",(Object)((_nm))).getBoolean()) { 
 BA.debugLineNum = 265;BA.debugLine="Return MIME.Get(Nm)";
Debug.ShouldStop(256);
if (true) return BA.ObjectToString(__ref.getField(false,"_mime" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_nm))));
 };
 };
 BA.debugLineNum = 269;BA.debugLine="Return \"application/*.*\"";
Debug.ShouldStop(4096);
if (true) return BA.ObjectToString("application/*.*");
 BA.debugLineNum = 270;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendfile(RemoteObject __ref,RemoteObject _dir,RemoteObject _filename) throws Exception{
try {
		Debug.PushSubsStack("SendFile (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,122);
if (RapidSub.canDelegate("sendfile")) { return __ref.runUserSub(false, "servletresponse","sendfile", __ref, _dir, _filename);}
RemoteObject _filesize = RemoteObject.createImmutable(0L);
RemoteObject _lastmodified = RemoteObject.createImmutable("");
Debug.locals.put("Dir", _dir);
Debug.locals.put("fileName", _filename);
 BA.debugLineNum = 122;BA.debugLine="Public Sub SendFile(Dir As String, fileName As Str";
Debug.ShouldStop(33554432);
 BA.debugLineNum = 123;BA.debugLine="If File.Exists(Dir, fileName) Then";
Debug.ShouldStop(67108864);
if (b4i_main.__c.runMethod(false,"File").runMethod(true,"Exists::",(Object)(_dir),(Object)(_filename)).getBoolean()) { 
 BA.debugLineNum = 124;BA.debugLine="Dim FileSize As Long = File.Size(Dir, fileName)";
Debug.ShouldStop(134217728);
_filesize = b4i_main.__c.runMethod(false,"File").runMethod(true,"Size::",(Object)(_dir),(Object)(_filename));Debug.locals.put("FileSize", _filesize);Debug.locals.put("FileSize", _filesize);
 BA.debugLineNum = 125;BA.debugLine="Dim lastModified As String = CompleteDate(File.L";
Debug.ShouldStop(268435456);
_lastmodified = __ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"LastModified::",(Object)(_dir),(Object)(_filename))));Debug.locals.put("lastModified", _lastmodified);Debug.locals.put("lastModified", _lastmodified);
 BA.debugLineNum = 127;BA.debugLine="ContentType=SearchMime(fileName)";
Debug.ShouldStop(1073741824);
__ref.setField ("_contenttype" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletresponse.class, "_searchmime:" /*RemoteObject*/ ,(Object)(_filename)));
 BA.debugLineNum = 128;BA.debugLine="astream_write(HandShakeFile(lastModified,FileSiz";
Debug.ShouldStop(-2147483648);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_servletresponse.class, "_handshakefile::" /*RemoteObject*/ ,(Object)(_lastmodified),(Object)(_filesize)).runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));
 BA.debugLineNum = 129;BA.debugLine="astream_write(File.ReadBytes(Dir, fileName))";
Debug.ShouldStop(1);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"File").runMethod(false,"ReadBytes::",(Object)(_dir),(Object)(_filename))));
 }else {
 BA.debugLineNum = 131;BA.debugLine="SendNotFound(fileName)";
Debug.ShouldStop(4);
__ref.runClassMethod (b4i_servletresponse.class, "_sendnotfound:" /*RemoteObject*/ ,(Object)(_filename));
 };
 BA.debugLineNum = 133;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendfile2(RemoteObject __ref,RemoteObject _dir,RemoteObject _filename,RemoteObject _content_type) throws Exception{
try {
		Debug.PushSubsStack("SendFile2 (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,135);
if (RapidSub.canDelegate("sendfile2")) { return __ref.runUserSub(false, "servletresponse","sendfile2", __ref, _dir, _filename, _content_type);}
RemoteObject _filesize = RemoteObject.createImmutable(0L);
RemoteObject _lastmodified = RemoteObject.createImmutable("");
Debug.locals.put("Dir", _dir);
Debug.locals.put("fileName", _filename);
Debug.locals.put("Content_Type", _content_type);
 BA.debugLineNum = 135;BA.debugLine="Public Sub SendFile2(Dir As String, fileName As St";
Debug.ShouldStop(64);
 BA.debugLineNum = 136;BA.debugLine="If File.Exists(Dir, fileName) Then";
Debug.ShouldStop(128);
if (b4i_main.__c.runMethod(false,"File").runMethod(true,"Exists::",(Object)(_dir),(Object)(_filename)).getBoolean()) { 
 BA.debugLineNum = 137;BA.debugLine="Dim FileSize As Long = File.Size(Dir, fileName)";
Debug.ShouldStop(256);
_filesize = b4i_main.__c.runMethod(false,"File").runMethod(true,"Size::",(Object)(_dir),(Object)(_filename));Debug.locals.put("FileSize", _filesize);Debug.locals.put("FileSize", _filesize);
 BA.debugLineNum = 138;BA.debugLine="Dim lastModified As String = CompleteDate(File.L";
Debug.ShouldStop(512);
_lastmodified = __ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"LastModified::",(Object)(_dir),(Object)(_filename))));Debug.locals.put("lastModified", _lastmodified);Debug.locals.put("lastModified", _lastmodified);
 BA.debugLineNum = 140;BA.debugLine="ContentType=Content_Type";
Debug.ShouldStop(2048);
__ref.setField ("_contenttype" /*RemoteObject*/ ,_content_type);
 BA.debugLineNum = 141;BA.debugLine="astream_write(HandShakeFile(lastModified,FileSiz";
Debug.ShouldStop(4096);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_servletresponse.class, "_handshakefile::" /*RemoteObject*/ ,(Object)(_lastmodified),(Object)(_filesize)).runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));
 BA.debugLineNum = 143;BA.debugLine="astream_write(File.ReadBytes(Dir, fileName))";
Debug.ShouldStop(16384);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"File").runMethod(false,"ReadBytes::",(Object)(_dir),(Object)(_filename))));
 }else {
 BA.debugLineNum = 145;BA.debugLine="SendNotFound(fileName)";
Debug.ShouldStop(65536);
__ref.runClassMethod (b4i_servletresponse.class, "_sendnotfound:" /*RemoteObject*/ ,(Object)(_filename));
 };
 BA.debugLineNum = 147;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendnotfound(RemoteObject __ref,RemoteObject _filenamenotfound) throws Exception{
try {
		Debug.PushSubsStack("SendNotFound (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,174);
if (RapidSub.canDelegate("sendnotfound")) { return __ref.runUserSub(false, "servletresponse","sendnotfound", __ref, _filenamenotfound);}
RemoteObject _s = RemoteObject.createImmutable("");
Debug.locals.put("filenameNotFound", _filenamenotfound);
 BA.debugLineNum = 174;BA.debugLine="Public Sub SendNotFound(filenameNotFound As String";
Debug.ShouldStop(8192);
 BA.debugLineNum = 175;BA.debugLine="Dim S As String = $\"HTTP/1.1 404 Not Found Date:";
Debug.ShouldStop(16384);
_s = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.1 404 Not Found\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Cache-Control: must-revalidate,no-cache,no-store\n"),RemoteObject.createImmutable("Content-Type: text/html; charset="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))),RemoteObject.createImmutable(";\n"),RemoteObject.createImmutable("Content-Length: 320\n"),RemoteObject.createImmutable("Server: HttpServer (B4X)\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("<html>\n"),RemoteObject.createImmutable("<head>\n"),RemoteObject.createImmutable("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"/>\n"),RemoteObject.createImmutable("<title>Error 404 Not Found</title>\n"),RemoteObject.createImmutable("</head>\n"),RemoteObject.createImmutable("<body><h2>HTTP ERROR 404</h2>\n"),RemoteObject.createImmutable("<p>Problem accessing "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_filenamenotfound))),RemoteObject.createImmutable(". Reason:\n"),RemoteObject.createImmutable("<pre>    Not Found</pre></p>\n"),RemoteObject.createImmutable("</body>\n"),RemoteObject.createImmutable("</html>")));Debug.locals.put("S", _s);Debug.locals.put("S", _s);
 BA.debugLineNum = 192;BA.debugLine="astream_write(S.GetBytes(CharacterEncoding))";
Debug.ShouldStop(-2147483648);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_s.runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));
 BA.debugLineNum = 193;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendraw(RemoteObject __ref,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("SendRaw (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,117);
if (RapidSub.canDelegate("sendraw")) { return __ref.runUserSub(false, "servletresponse","sendraw", __ref, _data);}
Debug.locals.put("Data", _data);
 BA.debugLineNum = 117;BA.debugLine="Public Sub SendRaw(Data() As Byte)";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 118;BA.debugLine="astream_write(Data)";
Debug.ShouldStop(2097152);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_data));
 BA.debugLineNum = 119;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendredirect(RemoteObject __ref,RemoteObject _address) throws Exception{
try {
		Debug.PushSubsStack("SendRedirect (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,149);
if (RapidSub.canDelegate("sendredirect")) { return __ref.runUserSub(false, "servletresponse","sendredirect", __ref, _address);}
RemoteObject _handshake = RemoteObject.createImmutable("");
RemoteObject _k = RemoteObject.createImmutable("");
RemoteObject _scks = RemoteObject.createImmutable("");
RemoteObject _s = RemoteObject.createImmutable("");
Debug.locals.put("Address", _address);
 BA.debugLineNum = 149;BA.debugLine="Public Sub SendRedirect(Address As String)";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 150;BA.debugLine="Dim HandShake As String = \"\"";
Debug.ShouldStop(2097152);
_handshake = BA.ObjectToString("");Debug.locals.put("HandShake", _handshake);Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 153;BA.debugLine="For Each K As String In ResponseHeader.Keys";
Debug.ShouldStop(16777216);
{
final RemoteObject group2 = __ref.getField(false,"_responseheader" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen2 = group2.runMethod(true,"Size").<Integer>get()
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_k = BA.ObjectToString(group2.runMethod(false,"Get:",index2));Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 154;BA.debugLine="HandShake=$\"${HandShake}${k}: ${ResponseHeader.G";
Debug.ShouldStop(33554432);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_k))),RemoteObject.createImmutable(": "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_k))))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 157;BA.debugLine="Dim Scks As String = \"\"";
Debug.ShouldStop(268435456);
_scks = BA.ObjectToString("");Debug.locals.put("Scks", _scks);Debug.locals.put("Scks", _scks);
 BA.debugLineNum = 158;BA.debugLine="For Each K As String In ResponseCookies.Keys";
Debug.ShouldStop(536870912);
{
final RemoteObject group6 = __ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen6 = group6.runMethod(true,"Size").<Integer>get()
;int index6 = 0;
;
for (; index6 < groupLen6;index6++){
_k = BA.ObjectToString(group6.runMethod(false,"Get:",index6));Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 159;BA.debugLine="Scks=$\"${Scks}${k}=${ResponseCookies.Get(K)}; \"$";
Debug.ShouldStop(1073741824);
_scks = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_k))),RemoteObject.createImmutable("="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(__ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_k))))),RemoteObject.createImmutable("; ")));Debug.locals.put("Scks", _scks);
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 161;BA.debugLine="If Scks<>\"\" Then Scks=$\"Set-Cookie: ${Scks}${CRLF";
Debug.ShouldStop(1);
if (RemoteObject.solveBoolean("!",_scks,BA.ObjectToString(""))) { 
_scks = (RemoteObject.concat(RemoteObject.createImmutable("Set-Cookie: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("Expires="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now"),(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"TicksPerDay"),RemoteObject.createImmutable(365)}, "*",0, 2))}, "+",1, 2)))))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(true,"CRLF")))),RemoteObject.createImmutable("")));Debug.locals.put("Scks", _scks);};
 BA.debugLineNum = 164;BA.debugLine="Dim S As String = $\"HTTP/1.1 302 Found Date: ${Co";
Debug.ShouldStop(8);
_s = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.1 302 Found\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletresponse.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Location: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_address))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Content-Length: 0\n"),RemoteObject.createImmutable("Server: HttpServer (B4X)\n"),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_scks))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("")));Debug.locals.put("S", _s);Debug.locals.put("S", _s);
 BA.debugLineNum = 171;BA.debugLine="astream_write(S.GetBytes(CharacterEncoding))";
Debug.ShouldStop(1024);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_s.runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));
 BA.debugLineNum = 172;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendstring(RemoteObject __ref,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("SendString (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,109);
if (RapidSub.canDelegate("sendstring")) { return __ref.runUserSub(false, "servletresponse","sendstring", __ref, _text);}
RemoteObject _b = null;
RemoteObject _s = RemoteObject.createImmutable("");
Debug.locals.put("Text", _text);
 BA.debugLineNum = 109;BA.debugLine="Public Sub SendString(Text As String)";
Debug.ShouldStop(4096);
 BA.debugLineNum = 110;BA.debugLine="If ContentType=\"\" Then ContentType=$\"text/html; c";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("=",__ref.getField(true,"_contenttype" /*RemoteObject*/ ),BA.ObjectToString(""))) { 
__ref.setField ("_contenttype" /*RemoteObject*/ ,(RemoteObject.concat(RemoteObject.createImmutable("text/html; charset="),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))),RemoteObject.createImmutable(";"))));};
 BA.debugLineNum = 111;BA.debugLine="Dim B() As Byte = Text.GetBytes(CharacterEncoding";
Debug.ShouldStop(16384);
_b = _text.runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )));Debug.locals.put("B", _b);Debug.locals.put("B", _b);
 BA.debugLineNum = 112;BA.debugLine="ContentLenght=B.Length";
Debug.ShouldStop(32768);
__ref.setField ("_contentlenght" /*RemoteObject*/ ,_b.getField(true,"Length"));
 BA.debugLineNum = 113;BA.debugLine="Dim S As String = HandShakeString & Text";
Debug.ShouldStop(65536);
_s = RemoteObject.concat(__ref.runClassMethod (b4i_servletresponse.class, "_handshakestring" /*RemoteObject*/ ),_text);Debug.locals.put("S", _s);Debug.locals.put("S", _s);
 BA.debugLineNum = 114;BA.debugLine="astream_write(s.GetBytes(CharacterEncoding))";
Debug.ShouldStop(131072);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_s.runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));
 BA.debugLineNum = 115;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendwebsocketbinary(RemoteObject __ref,RemoteObject _data,RemoteObject _masked) throws Exception{
try {
		Debug.PushSubsStack("SendWebSocketBinary (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,357);
if (RapidSub.canDelegate("sendwebsocketbinary")) { return __ref.runUserSub(false, "servletresponse","sendwebsocketbinary", __ref, _data, _masked);}
Debug.locals.put("Data", _data);
Debug.locals.put("Masked", _masked);
 BA.debugLineNum = 357;BA.debugLine="Public Sub SendWebSocketBinary(Data() As Byte, Mas";
Debug.ShouldStop(16);
 BA.debugLineNum = 359;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendwebsocketclose(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("SendWebSocketClose (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,361);
if (RapidSub.canDelegate("sendwebsocketclose")) { return __ref.runUserSub(false, "servletresponse","sendwebsocketclose", __ref);}
RemoteObject _b = null;
 BA.debugLineNum = 361;BA.debugLine="Public Sub SendWebSocketClose";
Debug.ShouldStop(256);
 BA.debugLineNum = 362;BA.debugLine="Dim B(2) As Byte";
Debug.ShouldStop(512);
_b = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 2)});Debug.locals.put("B", _b);
 BA.debugLineNum = 363;BA.debugLine="B(0)=Bit.Or(128,8)";
Debug.ShouldStop(1024);
_b.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Or::",(Object)(BA.numberCast(int.class, 128)),(Object)(BA.numberCast(int.class, 8)))));
 BA.debugLineNum = 364;BA.debugLine="B(1)=0";
Debug.ShouldStop(2048);
_b.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, 0));
 BA.debugLineNum = 366;BA.debugLine="astream_write(B)";
Debug.ShouldStop(8192);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_b));
 BA.debugLineNum = 367;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendwebsocketping(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("SendWebSocketPing (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,369);
if (RapidSub.canDelegate("sendwebsocketping")) { return __ref.runUserSub(false, "servletresponse","sendwebsocketping", __ref);}
RemoteObject _b = null;
 BA.debugLineNum = 369;BA.debugLine="Public Sub SendWebSocketPing";
Debug.ShouldStop(65536);
 BA.debugLineNum = 370;BA.debugLine="Dim B(2) As Byte";
Debug.ShouldStop(131072);
_b = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 2)});Debug.locals.put("B", _b);
 BA.debugLineNum = 371;BA.debugLine="B(0)=Bit.Or(128,9)";
Debug.ShouldStop(262144);
_b.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Or::",(Object)(BA.numberCast(int.class, 128)),(Object)(BA.numberCast(int.class, 9)))));
 BA.debugLineNum = 372;BA.debugLine="B(1)=0";
Debug.ShouldStop(524288);
_b.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, 0));
 BA.debugLineNum = 374;BA.debugLine="astream_write(B)";
Debug.ShouldStop(2097152);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_b));
 BA.debugLineNum = 375;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendwebsocketpong(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("SendWebSocketPong (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,377);
if (RapidSub.canDelegate("sendwebsocketpong")) { return __ref.runUserSub(false, "servletresponse","sendwebsocketpong", __ref);}
RemoteObject _b = null;
 BA.debugLineNum = 377;BA.debugLine="Public Sub SendWebSocketPong";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 378;BA.debugLine="Dim B(2) As Byte";
Debug.ShouldStop(33554432);
_b = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 2)});Debug.locals.put("B", _b);
 BA.debugLineNum = 379;BA.debugLine="B(0)=Bit.Or(128,10)";
Debug.ShouldStop(67108864);
_b.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Or::",(Object)(BA.numberCast(int.class, 128)),(Object)(BA.numberCast(int.class, 10)))));
 BA.debugLineNum = 380;BA.debugLine="B(1)=0";
Debug.ShouldStop(134217728);
_b.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, 0));
 BA.debugLineNum = 382;BA.debugLine="astream_write(B)";
Debug.ShouldStop(536870912);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_b));
 BA.debugLineNum = 383;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendwebsocketstring(RemoteObject __ref,RemoteObject _text,RemoteObject _masked,RemoteObject _compressed) throws Exception{
try {
		Debug.PushSubsStack("SendWebSocketString (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,304);
if (RapidSub.canDelegate("sendwebsocketstring")) { return __ref.runUserSub(false, "servletresponse","sendwebsocketstring", __ref, _text, _masked, _compressed);}
RemoteObject _payload = null;
RemoteObject _head = null;
RemoteObject _maskkey = null;
int _i = 0;
Debug.locals.put("Text", _text);
Debug.locals.put("Masked", _masked);
Debug.locals.put("Compressed", _compressed);
 BA.debugLineNum = 304;BA.debugLine="Public Sub SendWebSocketString(Text As String, Mas";
Debug.ShouldStop(32768);
 BA.debugLineNum = 305;BA.debugLine="Compressed=\"\"";
Debug.ShouldStop(65536);
_compressed = BA.ObjectToString("");Debug.locals.put("Compressed", _compressed);
 BA.debugLineNum = 306;BA.debugLine="Dim PayLoad() As Byte";
Debug.ShouldStop(131072);
_payload = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 0)});Debug.locals.put("PayLoad", _payload);
 BA.debugLineNum = 307;BA.debugLine="If Compressed<>\"none\" And Compressed<>\"\" Then";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean("!",_compressed,BA.ObjectToString("none")) && RemoteObject.solveBoolean("!",_compressed,BA.ObjectToString(""))) { 
 BA.debugLineNum = 308;BA.debugLine="PayLoad=DeflateDate(Text.GetBytes(\"UTF8\"))";
Debug.ShouldStop(524288);
_payload = __ref.runClassMethod (b4i_servletresponse.class, "_deflatedate:" /*RemoteObject*/ ,(Object)(_text.runMethod(false,"GetBytes:",(Object)(RemoteObject.createImmutable("UTF8")))));Debug.locals.put("PayLoad", _payload);
 }else {
 BA.debugLineNum = 310;BA.debugLine="PayLoad= Text.GetBytes(\"UTF8\")";
Debug.ShouldStop(2097152);
_payload = _text.runMethod(false,"GetBytes:",(Object)(RemoteObject.createImmutable("UTF8")));Debug.locals.put("PayLoad", _payload);
 };
 BA.debugLineNum = 313;BA.debugLine="Final= Array As Byte()";
Debug.ShouldStop(16777216);
__ref.setField ("_final" /*RemoteObject*/ ,RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {}));
 BA.debugLineNum = 314;BA.debugLine="If PayLoad.Length<126 Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean("<",_payload.getField(true,"Length"),BA.numberCast(double.class, 126))) { 
 BA.debugLineNum = 315;BA.debugLine="Dim Head(2) As Byte";
Debug.ShouldStop(67108864);
_head = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 2)});Debug.locals.put("Head", _head);
 BA.debugLineNum = 316;BA.debugLine="Head(0)=129 ' Final+ Compressed (64-100) + Text";
Debug.ShouldStop(134217728);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, 129));
 BA.debugLineNum = 317;BA.debugLine="Head(1)=PayLoad.Length";
Debug.ShouldStop(268435456);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, _payload.getField(true,"Length")));
 }else 
{ BA.debugLineNum = 318;BA.debugLine="else if PayLoad.Length<65536 Then";
Debug.ShouldStop(536870912);
if (RemoteObject.solveBoolean("<",_payload.getField(true,"Length"),BA.numberCast(double.class, 65536))) { 
 BA.debugLineNum = 319;BA.debugLine="Dim Head(4) As Byte";
Debug.ShouldStop(1073741824);
_head = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 4)});Debug.locals.put("Head", _head);
 BA.debugLineNum = 320;BA.debugLine="Head(0)=129 ' Final+ reserved (64-100) + Text";
Debug.ShouldStop(-2147483648);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, 129));
 BA.debugLineNum = 321;BA.debugLine="Head(1)=126 ' 2 Byte";
Debug.ShouldStop(1);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, 126));
 BA.debugLineNum = 322;BA.debugLine="Head(2)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
Debug.ShouldStop(2);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 2),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_payload.getField(true,"Length")}))).runMethod(true,"getByteFast:", BA.numberCast(int.class, 2)));
 BA.debugLineNum = 323;BA.debugLine="Head(3)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
Debug.ShouldStop(4);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 3),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_payload.getField(true,"Length")}))).runMethod(true,"getByteFast:", BA.numberCast(int.class, 3)));
 }else {
 BA.debugLineNum = 325;BA.debugLine="Dim Head(10) As Byte";
Debug.ShouldStop(16);
_head = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 10)});Debug.locals.put("Head", _head);
 BA.debugLineNum = 326;BA.debugLine="Head(0)=129 ' Final+ reserved (64-100) + Text";
Debug.ShouldStop(32);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, 129));
 BA.debugLineNum = 327;BA.debugLine="Head(1)=127 ' 4 byte";
Debug.ShouldStop(64);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, 127));
 BA.debugLineNum = 328;BA.debugLine="Head(2)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
Debug.ShouldStop(128);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 2),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_payload.getField(true,"Length")}))).runMethod(true,"getByteFast:", BA.numberCast(int.class, 0)));
 BA.debugLineNum = 329;BA.debugLine="Head(3)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
Debug.ShouldStop(256);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 3),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_payload.getField(true,"Length")}))).runMethod(true,"getByteFast:", BA.numberCast(int.class, 1)));
 BA.debugLineNum = 330;BA.debugLine="Head(4)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
Debug.ShouldStop(512);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 4),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_payload.getField(true,"Length")}))).runMethod(true,"getByteFast:", BA.numberCast(int.class, 2)));
 BA.debugLineNum = 331;BA.debugLine="Head(5)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
Debug.ShouldStop(1024);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 5),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_payload.getField(true,"Length")}))).runMethod(true,"getByteFast:", BA.numberCast(int.class, 3)));
 BA.debugLineNum = 332;BA.debugLine="Head(6)=0";
Debug.ShouldStop(2048);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 6),BA.numberCast(byte.class, 0));
 BA.debugLineNum = 333;BA.debugLine="Head(7)=0";
Debug.ShouldStop(4096);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 7),BA.numberCast(byte.class, 0));
 BA.debugLineNum = 334;BA.debugLine="Head(8)=0";
Debug.ShouldStop(8192);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 8),BA.numberCast(byte.class, 0));
 BA.debugLineNum = 335;BA.debugLine="Head(9)=0";
Debug.ShouldStop(16384);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 9),BA.numberCast(byte.class, 0));
 }}
;
 BA.debugLineNum = 337;BA.debugLine="If Compressed<>\"\" And Compressed<>\"none\" Then Hea";
Debug.ShouldStop(65536);
if (RemoteObject.solveBoolean("!",_compressed,BA.ObjectToString("")) && RemoteObject.solveBoolean("!",_compressed,BA.ObjectToString("none"))) { 
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Or::",(Object)(BA.numberCast(int.class, 129)),(Object)(BA.numberCast(int.class, 64)))));};
 BA.debugLineNum = 339;BA.debugLine="If Masked Then";
Debug.ShouldStop(262144);
if (_masked.getBoolean()) { 
 BA.debugLineNum = 340;BA.debugLine="Head(1)=Bit.Or(128,Head(1))";
Debug.ShouldStop(524288);
_head.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 1),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Or::",(Object)(BA.numberCast(int.class, 128)),(Object)(BA.numberCast(int.class, _head.runMethod(true,"getByteFast:", BA.numberCast(int.class, 1)))))));
 BA.debugLineNum = 341;BA.debugLine="Dim MaskKey(4) As Byte";
Debug.ShouldStop(1048576);
_maskkey = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 4)});Debug.locals.put("MaskKey", _maskkey);
 BA.debugLineNum = 342;BA.debugLine="For i=0 To 3";
Debug.ShouldStop(2097152);
{
final int step36 = 1;
final int limit36 = 3;
_i = 0 ;
for (;(step36 > 0 && _i <= limit36) || (step36 < 0 && _i >= limit36) ;_i = ((int)(0 + _i + step36))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 343;BA.debugLine="MaskKey(0)=Rnd(0,256)";
Debug.ShouldStop(4194304);
_maskkey.runVoidMethod ("setByteFast::", BA.numberCast(int.class, 0),BA.numberCast(byte.class, b4i_main.__c.runMethod(true,"Rnd::",(Object)(BA.numberCast(int.class, 0)),(Object)(BA.numberCast(int.class, 256)))));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 345;BA.debugLine="AppenFinal(MaskKey)";
Debug.ShouldStop(16777216);
__ref.runClassMethod (b4i_servletresponse.class, "_appenfinal:" /*RemoteObject*/ ,(Object)(_maskkey));
 BA.debugLineNum = 346;BA.debugLine="AppenFinal(DataMask(PayLoad,MaskKey))";
Debug.ShouldStop(33554432);
__ref.runClassMethod (b4i_servletresponse.class, "_appenfinal:" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_servletresponse.class, "_datamask::" /*RemoteObject*/ ,(Object)(_payload),(Object)(_maskkey))));
 }else {
 BA.debugLineNum = 350;BA.debugLine="AppenFinal(Head)";
Debug.ShouldStop(536870912);
__ref.runClassMethod (b4i_servletresponse.class, "_appenfinal:" /*RemoteObject*/ ,(Object)(_head));
 BA.debugLineNum = 351;BA.debugLine="AppenFinal(PayLoad)";
Debug.ShouldStop(1073741824);
__ref.runClassMethod (b4i_servletresponse.class, "_appenfinal:" /*RemoteObject*/ ,(Object)(_payload));
 };
 BA.debugLineNum = 354;BA.debugLine="astream_write(Final)";
Debug.ShouldStop(2);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_final" /*RemoteObject*/ )));
 BA.debugLineNum = 355;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setcookies(RemoteObject __ref,RemoteObject _name,RemoteObject _value) throws Exception{
try {
		Debug.PushSubsStack("SetCookies (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,75);
if (RapidSub.canDelegate("setcookies")) { return __ref.runUserSub(false, "servletresponse","setcookies", __ref, _name, _value);}
Debug.locals.put("Name", _name);
Debug.locals.put("Value", _value);
 BA.debugLineNum = 75;BA.debugLine="Public Sub SetCookies(Name As String,Value As Stri";
Debug.ShouldStop(1024);
 BA.debugLineNum = 76;BA.debugLine="ResponseCookies.Put(Name,Value)";
Debug.ShouldStop(2048);
__ref.getField(false,"_responsecookies" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_name)),(Object)((_value)));
 BA.debugLineNum = 77;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _setheader(RemoteObject __ref,RemoteObject _name,RemoteObject _value) throws Exception{
try {
		Debug.PushSubsStack("SetHeader (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,70);
if (RapidSub.canDelegate("setheader")) { return __ref.runUserSub(false, "servletresponse","setheader", __ref, _name, _value);}
Debug.locals.put("Name", _name);
Debug.locals.put("Value", _value);
 BA.debugLineNum = 70;BA.debugLine="Public Sub SetHeader(Name As String, Value As Stri";
Debug.ShouldStop(32);
 BA.debugLineNum = 71;BA.debugLine="ResponseHeader.Put(Name,Value)";
Debug.ShouldStop(64);
__ref.getField(false,"_responseheader" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_name)),(Object)((_value)));
 BA.debugLineNum = 72;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _statuscode(RemoteObject __ref,RemoteObject _sts) throws Exception{
try {
		Debug.PushSubsStack("StatusCode (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,201);
if (RapidSub.canDelegate("statuscode")) { return __ref.runUserSub(false, "servletresponse","statuscode", __ref, _sts);}
Debug.locals.put("Sts", _sts);
 BA.debugLineNum = 201;BA.debugLine="Private Sub StatusCode(Sts As Int) As String";
Debug.ShouldStop(256);
 BA.debugLineNum = 202;BA.debugLine="If Code.ContainsKey(Sts) Then";
Debug.ShouldStop(512);
if (__ref.getField(false,"_code" /*RemoteObject*/ ).runMethod(true,"ContainsKey:",(Object)((_sts))).getBoolean()) { 
 BA.debugLineNum = 203;BA.debugLine="Return Code.Get(Sts)";
Debug.ShouldStop(1024);
if (true) return BA.ObjectToString(__ref.getField(false,"_code" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_sts))));
 }else {
 BA.debugLineNum = 205;BA.debugLine="Return \"\"";
Debug.ShouldStop(4096);
if (true) return BA.ObjectToString("");
 };
 BA.debugLineNum = 207;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _write(RemoteObject __ref,RemoteObject _text) throws Exception{
try {
		Debug.PushSubsStack("Write (servletresponse) ","servletresponse",3,__ref.getField(false, "bi"),__ref,104);
if (RapidSub.canDelegate("write")) { return __ref.runUserSub(false, "servletresponse","write", __ref, _text);}
Debug.locals.put("Text", _text);
 BA.debugLineNum = 104;BA.debugLine="Public Sub Write(Text As String)";
Debug.ShouldStop(128);
 BA.debugLineNum = 105;BA.debugLine="astream_write(Text.GetBytes(CharacterEncoding))";
Debug.ShouldStop(256);
__ref.runClassMethod (b4i_servletresponse.class, "_astream_write:" /*RemoteObject*/ ,(Object)(_text.runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));
 BA.debugLineNum = 106;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}