
package net.wapps.httpserver;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class b4i_servletresponse {
    public static RemoteObject myClass;
	public b4i_servletresponse() {
	}
    public static PCBA staticBA = new PCBA(null, b4i_servletresponse.class);

public static RemoteObject __c = RemoteObject.declareNull("B4ICommon");
public static RemoteObject _request = RemoteObject.declareNull("b4i_servletrequest");
public static RemoteObject _astream = RemoteObject.declareNull("B4IAsyncStreams");
public static RemoteObject _responseheader = RemoteObject.declareNull("B4IMap");
public static RemoteObject _responseparameter = RemoteObject.declareNull("B4IMap");
public static RemoteObject _responsecookies = RemoteObject.declareNull("B4IMap");
public static RemoteObject _code = RemoteObject.declareNull("B4IMap");
public static RemoteObject _mime = RemoteObject.declareNull("B4IMap");
public static RemoteObject _client = RemoteObject.declareNull("B4ISocketWrapper");
public static RemoteObject _status = RemoteObject.createImmutable(0);
public static RemoteObject _contenttype = RemoteObject.createImmutable("");
public static RemoteObject _characterencoding = RemoteObject.createImmutable("");
public static RemoteObject _contentlenght = RemoteObject.createImmutable(0);
public static RemoteObject _dday = null;
public static RemoteObject _mmmonth = null;
public static RemoteObject _bc = RemoteObject.declareNull("B4IByteConverter");
public static RemoteObject _myquery = RemoteObject.declareNull("b4i_queryelement");
public static RemoteObject _final = null;
public static b4i_main _main = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"astream",_ref.getField(false, "_astream"),"BC",_ref.getField(false, "_bc"),"CharacterEncoding",_ref.getField(false, "_characterencoding"),"Client",_ref.getField(false, "_client"),"Code",_ref.getField(false, "_code"),"ContentLenght",_ref.getField(false, "_contentlenght"),"ContentType",_ref.getField(false, "_contenttype"),"DDay",_ref.getField(false, "_dday"),"Final",_ref.getField(false, "_final"),"MIME",_ref.getField(false, "_mime"),"MMmonth",_ref.getField(false, "_mmmonth"),"myQuery",_ref.getField(false, "_myquery"),"Request",_ref.getField(false, "_request"),"ResponseCookies",_ref.getField(false, "_responsecookies"),"ResponseHeader",_ref.getField(false, "_responseheader"),"ResponseParameter",_ref.getField(false, "_responseparameter"),"Status",_ref.getField(false, "_status")};
}
}