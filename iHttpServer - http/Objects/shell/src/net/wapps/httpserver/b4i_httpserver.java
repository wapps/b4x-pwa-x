
package net.wapps.httpserver;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class b4i_httpserver {
    public static RemoteObject myClass;
	public b4i_httpserver() {
	}
    public static PCBA staticBA = new PCBA(null, b4i_httpserver.class);

public static RemoteObject __c = RemoteObject.declareNull("B4ICommon");
public static RemoteObject _users = RemoteObject.declareNull("B4IMap");
public static RemoteObject _serv = RemoteObject.declareNull("B4IServerSocketWrapper");
public static RemoteObject _work = RemoteObject.createImmutable(false);
public static RemoteObject _mcallback = RemoteObject.declareNull("NSObject");
public static RemoteObject _meventname = RemoteObject.createImmutable("");
public static RemoteObject _digestauthentication = RemoteObject.createImmutable(false);
public static RemoteObject _digestpath = RemoteObject.createImmutable("");
public static RemoteObject _realm = RemoteObject.createImmutable("");
public static RemoteObject _htdigest = RemoteObject.declareNull("B4IList");
public static RemoteObject _ignorenc = RemoteObject.createImmutable(false);
public static RemoteObject _timeout = RemoteObject.createImmutable(0);
public static b4i_main _main = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"DigestAuthentication",_ref.getField(false, "_digestauthentication"),"DigestPath",_ref.getField(false, "_digestpath"),"htdigest",_ref.getField(false, "_htdigest"),"IgnoreNC",_ref.getField(false, "_ignorenc"),"mCallBack",_ref.getField(false, "_mcallback"),"mEventName",_ref.getField(false, "_meventname"),"realm",_ref.getField(false, "_realm"),"Serv",_ref.getField(false, "_serv"),"Timeout",_ref.getField(false, "_timeout"),"Users",_ref.getField(false, "_users"),"Work",_ref.getField(false, "_work")};
}
}