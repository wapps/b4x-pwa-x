
package net.wapps.httpserver;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class b4i_queryelement {
    public static RemoteObject myClass;
	public b4i_queryelement() {
	}
    public static PCBA staticBA = new PCBA(null, b4i_queryelement.class);

public static RemoteObject __c = RemoteObject.declareNull("B4ICommon");
public static RemoteObject _event_change = RemoteObject.createImmutable("");
public static RemoteObject _event_click = RemoteObject.createImmutable("");
public static RemoteObject _event_dblclick = RemoteObject.createImmutable("");
public static RemoteObject _event_focus = RemoteObject.createImmutable("");
public static RemoteObject _event_focusin = RemoteObject.createImmutable("");
public static RemoteObject _event_focusout = RemoteObject.createImmutable("");
public static RemoteObject _event_keyup = RemoteObject.createImmutable("");
public static RemoteObject _event_mousedown = RemoteObject.createImmutable("");
public static RemoteObject _event_mouseenter = RemoteObject.createImmutable("");
public static RemoteObject _event_mouseleave = RemoteObject.createImmutable("");
public static RemoteObject _event_mousemove = RemoteObject.createImmutable("");
public static RemoteObject _event_mouseup = RemoteObject.createImmutable("");
public static RemoteObject _noevent = null;
public static RemoteObject _resp = RemoteObject.declareNull("b4i_servletresponse");
public static b4i_main _main = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"Event_change",_ref.getField(false, "_event_change"),"Event_click",_ref.getField(false, "_event_click"),"Event_dblclick",_ref.getField(false, "_event_dblclick"),"Event_focus",_ref.getField(false, "_event_focus"),"Event_focusin",_ref.getField(false, "_event_focusin"),"Event_focusout",_ref.getField(false, "_event_focusout"),"Event_keyup",_ref.getField(false, "_event_keyup"),"Event_mousedown",_ref.getField(false, "_event_mousedown"),"Event_mouseenter",_ref.getField(false, "_event_mouseenter"),"Event_mouseleave",_ref.getField(false, "_event_mouseleave"),"Event_mousemove",_ref.getField(false, "_event_mousemove"),"Event_mouseup",_ref.getField(false, "_event_mouseup"),"NoEvent",_ref.getField(false, "_noevent"),"Resp",_ref.getField(false, "_resp")};
}
}