package net.wapps.httpserver;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class b4i_servletrequest_subs_0 {


public static RemoteObject  _arrayappend(RemoteObject __ref,RemoteObject _data1,RemoteObject _data2) throws Exception{
try {
		Debug.PushSubsStack("ArrayAppend (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,889);
if (RapidSub.canDelegate("arrayappend")) { return __ref.runUserSub(false, "servletrequest","arrayappend", __ref, _data1, _data2);}
RemoteObject _fn = null;
Debug.locals.put("Data1", _data1);
Debug.locals.put("Data2", _data2);
 BA.debugLineNum = 889;BA.debugLine="Private Sub ArrayAppend(Data1() As Byte,Data2() As";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 890;BA.debugLine="Dim Fn(Data1.Length+Data2.Length) As Byte";
Debug.ShouldStop(33554432);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_data1.getField(true,"Length"),_data2.getField(true,"Length")}, "+",1, 1)});Debug.locals.put("Fn", _fn);
 BA.debugLineNum = 891;BA.debugLine="Bit.ArrayCopy(Data1,0,Fn,0,Data1.Length)";
Debug.ShouldStop(67108864);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data1),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(_data1.getField(true,"Length")));
 BA.debugLineNum = 892;BA.debugLine="Bit.ArrayCopy(Data2,0,Fn,Data1.Length,Data2.Lengt";
Debug.ShouldStop(134217728);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data2),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(_data1.getField(true,"Length")),(Object)(_data2.getField(true,"Length")));
 BA.debugLineNum = 893;BA.debugLine="Return Fn";
Debug.ShouldStop(268435456);
if (true) return _fn;
 BA.debugLineNum = 894;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _arraycopy(RemoteObject __ref,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("ArrayCopy (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,896);
if (RapidSub.canDelegate("arraycopy")) { return __ref.runUserSub(false, "servletrequest","arraycopy", __ref, _data);}
RemoteObject _fn = null;
Debug.locals.put("Data", _data);
 BA.debugLineNum = 896;BA.debugLine="Private Sub ArrayCopy(Data() As Byte) As Byte()";
Debug.ShouldStop(-2147483648);
 BA.debugLineNum = 897;BA.debugLine="Dim Fn(Data.Length) As Byte";
Debug.ShouldStop(1);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {_data.getField(true,"Length")});Debug.locals.put("Fn", _fn);
 BA.debugLineNum = 898;BA.debugLine="Bit.ArrayCopy(Data,0,Fn,0,Data.Length)";
Debug.ShouldStop(2);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(_data.getField(true,"Length")));
 BA.debugLineNum = 899;BA.debugLine="Return Fn";
Debug.ShouldStop(4);
if (true) return _fn;
 BA.debugLineNum = 900;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _arrayindexof(RemoteObject __ref,RemoteObject _data,RemoteObject _searchdata) throws Exception{
try {
		Debug.PushSubsStack("ArrayIndexOf (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,902);
if (RapidSub.canDelegate("arrayindexof")) { return __ref.runUserSub(false, "servletrequest","arrayindexof", __ref, _data, _searchdata);}
int _i = 0;
int _sindex = 0;
Debug.locals.put("Data", _data);
Debug.locals.put("SearchData", _searchdata);
 BA.debugLineNum = 902;BA.debugLine="Private Sub ArrayIndexOf(Data() As Byte,SearchData";
Debug.ShouldStop(32);
 BA.debugLineNum = 903;BA.debugLine="For i = 0 To Data.Length - SearchData.Length";
Debug.ShouldStop(64);
{
final int step1 = 1;
final int limit1 = RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),_searchdata.getField(true,"Length")}, "-",1, 1).<Number>get().intValue();
_i = 0 ;
for (;(step1 > 0 && _i <= limit1) || (step1 < 0 && _i >= limit1) ;_i = ((int)(0 + _i + step1))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 904;BA.debugLine="If SearchData(0) = Data(i) Then";
Debug.ShouldStop(128);
if (RemoteObject.solveBoolean("=",_searchdata.runMethod(true,"getByteFast:", BA.numberCast(int.class, 0)),BA.numberCast(double.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, _i))))) { 
 BA.debugLineNum = 905;BA.debugLine="For sindex = 0 To SearchData.Length - 1";
Debug.ShouldStop(256);
{
final int step3 = 1;
final int limit3 = RemoteObject.solve(new RemoteObject[] {_searchdata.getField(true,"Length"),RemoteObject.createImmutable(1)}, "-",1, 1).<Number>get().intValue();
_sindex = 0 ;
for (;(step3 > 0 && _sindex <= limit3) || (step3 < 0 && _sindex >= limit3) ;_sindex = ((int)(0 + _sindex + step3))  ) {
Debug.locals.put("sindex", _sindex);
 BA.debugLineNum = 906;BA.debugLine="If SearchData(sindex) <> Data(i + sindex) Then";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean("!",_searchdata.runMethod(true,"getByteFast:", BA.numberCast(int.class, _sindex)),BA.numberCast(double.class, _data.runMethod(true,"getByteFast:", RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),RemoteObject.createImmutable(_sindex)}, "+",1, 1))))) { 
 BA.debugLineNum = 907;BA.debugLine="Exit";
Debug.ShouldStop(1024);
if (true) break;
 };
 }
}Debug.locals.put("sindex", _sindex);
;
 BA.debugLineNum = 910;BA.debugLine="If sindex = SearchData.Length Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_sindex),BA.numberCast(double.class, _searchdata.getField(true,"Length")))) { 
 BA.debugLineNum = 911;BA.debugLine="Return i";
Debug.ShouldStop(16384);
if (true) return BA.numberCast(int.class, _i);
 };
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 915;BA.debugLine="Return -1";
Debug.ShouldStop(262144);
if (true) return BA.numberCast(int.class, -(double) (0 + 1));
 BA.debugLineNum = 916;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _arrayinitialize(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("ArrayInitialize (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,885);
if (RapidSub.canDelegate("arrayinitialize")) { return __ref.runUserSub(false, "servletrequest","arrayinitialize", __ref);}
 BA.debugLineNum = 885;BA.debugLine="Private Sub ArrayInitialize() As Byte()";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 886;BA.debugLine="Return Array As Byte()";
Debug.ShouldStop(2097152);
if (true) return RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {});
 BA.debugLineNum = 887;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _arrayinsert(RemoteObject __ref,RemoteObject _datasource,RemoteObject _index,RemoteObject _datainsert) throws Exception{
try {
		Debug.PushSubsStack("ArrayInsert (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,918);
if (RapidSub.canDelegate("arrayinsert")) { return __ref.runUserSub(false, "servletrequest","arrayinsert", __ref, _datasource, _index, _datainsert);}
RemoteObject _fn = null;
Debug.locals.put("DataSource", _datasource);
Debug.locals.put("Index", _index);
Debug.locals.put("DataInsert", _datainsert);
 BA.debugLineNum = 918;BA.debugLine="Public Sub ArrayInsert(DataSource() As Byte,Index";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 919;BA.debugLine="Dim Fn(DataSource.Length+DataInsert.Length) As By";
Debug.ShouldStop(4194304);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_datasource.getField(true,"Length"),_datainsert.getField(true,"Length")}, "+",1, 1)});Debug.locals.put("Fn", _fn);
 BA.debugLineNum = 921;BA.debugLine="If Index>0 Then Bit.ArrayCopy(DataSource,0,Fn,0,I";
Debug.ShouldStop(16777216);
if (RemoteObject.solveBoolean(">",_index,BA.numberCast(double.class, 0))) { 
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_datasource),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(RemoteObject.solve(new RemoteObject[] {_index,RemoteObject.createImmutable(1)}, "-",1, 1)));};
 BA.debugLineNum = 922;BA.debugLine="Bit.ArrayCopy(DataInsert,0,Fn,Index,DataInsert.Le";
Debug.ShouldStop(33554432);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_datainsert),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(_index),(Object)(_datainsert.getField(true,"Length")));
 BA.debugLineNum = 923;BA.debugLine="If DataSource.Length-Index>0 Then Bit.ArrayCopy(D";
Debug.ShouldStop(67108864);
if (RemoteObject.solveBoolean(">",RemoteObject.solve(new RemoteObject[] {_datasource.getField(true,"Length"),_index}, "-",1, 1),BA.numberCast(double.class, 0))) { 
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_datasource),(Object)(_index),(Object)(_fn),(Object)(RemoteObject.solve(new RemoteObject[] {_datainsert.getField(true,"Length"),_index}, "+",1, 1)),(Object)(RemoteObject.solve(new RemoteObject[] {_datasource.getField(true,"Length"),_index}, "-",1, 1)));};
 BA.debugLineNum = 925;BA.debugLine="Return Fn";
Debug.ShouldStop(268435456);
if (true) return _fn;
 BA.debugLineNum = 926;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _arrayremove(RemoteObject __ref,RemoteObject _data,RemoteObject _start,RemoteObject _last) throws Exception{
try {
		Debug.PushSubsStack("ArrayRemove (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,928);
if (RapidSub.canDelegate("arrayremove")) { return __ref.runUserSub(false, "servletrequest","arrayremove", __ref, _data, _start, _last);}
RemoteObject _eraseq = RemoteObject.createImmutable(0);
RemoteObject _fn = null;
Debug.locals.put("Data", _data);
Debug.locals.put("Start", _start);
Debug.locals.put("Last", _last);
 BA.debugLineNum = 928;BA.debugLine="Public Sub ArrayRemove(Data() As Byte,Start As Int";
Debug.ShouldStop(-2147483648);
 BA.debugLineNum = 929;BA.debugLine="Dim EraseQ As Int = Last-Start";
Debug.ShouldStop(1);
_eraseq = RemoteObject.solve(new RemoteObject[] {_last,_start}, "-",1, 1);Debug.locals.put("EraseQ", _eraseq);Debug.locals.put("EraseQ", _eraseq);
 BA.debugLineNum = 930;BA.debugLine="Dim Fn(Data.Length-EraseQ) As Byte";
Debug.ShouldStop(2);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),_eraseq}, "-",1, 1)});Debug.locals.put("Fn", _fn);
 BA.debugLineNum = 932;BA.debugLine="If Data.Length-EraseQ=0 Then Return Array As Byte";
Debug.ShouldStop(8);
if (RemoteObject.solveBoolean("=",RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),_eraseq}, "-",1, 1),BA.numberCast(double.class, 0))) { 
if (true) return RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {});};
 BA.debugLineNum = 934;BA.debugLine="If Start>0 Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean(">",_start,BA.numberCast(double.class, 0))) { 
 BA.debugLineNum = 935;BA.debugLine="Bit.ArrayCopy(Data,0,Fn,0,Start)";
Debug.ShouldStop(64);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(_start));
 };
 BA.debugLineNum = 937;BA.debugLine="If Last<Data.Length Then";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean("<",_last,BA.numberCast(double.class, _data.getField(true,"Length")))) { 
 BA.debugLineNum = 938;BA.debugLine="Bit.ArrayCopy(Data,Last,Fn,Start,Data.Length-Las";
Debug.ShouldStop(512);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data),(Object)(_last),(Object)(_fn),(Object)(_start),(Object)(RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),_last}, "-",1, 1)));
 };
 BA.debugLineNum = 941;BA.debugLine="Return Fn";
Debug.ShouldStop(4096);
if (true) return _fn;
 BA.debugLineNum = 942;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _astream_error(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("astream_Error (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,198);
if (RapidSub.canDelegate("astream_error")) { return __ref.runUserSub(false, "servletrequest","astream_error", __ref);}
 BA.debugLineNum = 198;BA.debugLine="Private Sub astream_Error";
Debug.ShouldStop(32);
 BA.debugLineNum = 199;BA.debugLine="Log($\"Error (${ID})\"$)";
Debug.ShouldStop(64);
b4i_main.__c.runVoidMethod ("LogImpl:::","32686977",(RemoteObject.concat(RemoteObject.createImmutable("Error ("),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_id" /*RemoteObject*/ )))),RemoteObject.createImmutable(")"))),0);
 BA.debugLineNum = 200;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _astream_newdata(RemoteObject __ref,RemoteObject _buffer) throws Exception{
try {
		Debug.PushSubsStack("astream_NewData (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,188);
if (RapidSub.canDelegate("astream_newdata")) { return __ref.runUserSub(false, "servletrequest","astream_newdata", __ref, _buffer);}
Debug.locals.put("Buffer", _buffer);
 BA.debugLineNum = 188;BA.debugLine="private Sub astream_NewData (Buffer() As Byte)";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 189;BA.debugLine="Cache=ArrayAppend(Cache,Buffer)";
Debug.ShouldStop(268435456);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayappend::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(_buffer)));
 BA.debugLineNum = 190;BA.debugLine="LastBrowserComunicate=DateTime.Now";
Debug.ShouldStop(536870912);
__ref.setField ("_lastbrowsercomunicate" /*RemoteObject*/ ,b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now"));
 BA.debugLineNum = 191;BA.debugLine="ExtractHandShake";
Debug.ShouldStop(1073741824);
__ref.runClassMethod (b4i_servletrequest.class, "_extracthandshake" /*RemoteObject*/ );
 BA.debugLineNum = 192;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _astream_terminated(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("astream_Terminated (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,194);
if (RapidSub.canDelegate("astream_terminated")) { return __ref.runUserSub(false, "servletrequest","astream_terminated", __ref);}
 BA.debugLineNum = 194;BA.debugLine="Private Sub astream_Terminated";
Debug.ShouldStop(2);
 BA.debugLineNum = 195;BA.debugLine="Log($\"Client has terminated (${ID})\"$ )";
Debug.ShouldStop(4);
b4i_main.__c.runVoidMethod ("LogImpl:::","32621441",(RemoteObject.concat(RemoteObject.createImmutable("Client has terminated ("),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_id" /*RemoteObject*/ )))),RemoteObject.createImmutable(")"))),0);
 BA.debugLineNum = 196;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static void  _browsertimeout(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("BrowserTimeOut (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,620);
if (RapidSub.canDelegate("browsertimeout")) { __ref.runUserSub(false, "servletrequest","browsertimeout", __ref); return;}
ResumableSub_BrowserTimeOut rsub = new ResumableSub_BrowserTimeOut(null,__ref);
rsub.resume(null, null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static class ResumableSub_BrowserTimeOut extends BA.ResumableSub {
public ResumableSub_BrowserTimeOut(b4i_servletrequest parent,RemoteObject __ref) {
this.parent = parent;
this.__ref = __ref;
}
java.util.LinkedHashMap<String, Object> rsLocals = new java.util.LinkedHashMap<String, Object>();
RemoteObject __ref;
b4i_servletrequest parent;
RemoteObject _body = RemoteObject.createImmutable("");
RemoteObject _handshake = RemoteObject.createImmutable("");

@Override
public void resume(BA ba, RemoteObject result) throws Exception{
try {
		Debug.PushSubsStack("BrowserTimeOut (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,620);
Debug.locals = rsLocals;Debug.currentSubFrame.locals = rsLocals;

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
Debug.locals.put("_ref", __ref);
 BA.debugLineNum = 621;BA.debugLine="Sleep(Timeout)";
Debug.ShouldStop(4096);
b4i_main.__c.runVoidMethod ("Sleep:::",__ref.getField(false, "bi"),anywheresoftware.b4a.pc.PCResumableSub.createDebugResumeSub(this, "servletrequest", "browsertimeout"),BA.numberCast(int.class, __ref.getField(true,"_timeout" /*RemoteObject*/ )));
this.state = 5;
return;
case 5:
//C
this.state = 1;
;
 BA.debugLineNum = 622;BA.debugLine="If LastBrowserComunicate+Timeout=0 Then";
Debug.ShouldStop(8192);
if (true) break;

case 1:
//if
this.state = 4;
if (RemoteObject.solveBoolean("=",RemoteObject.solve(new RemoteObject[] {__ref.getField(true,"_lastbrowsercomunicate" /*RemoteObject*/ ),__ref.getField(true,"_timeout" /*RemoteObject*/ )}, "+",1, 2),BA.numberCast(long.class, 0))) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 BA.debugLineNum = 623;BA.debugLine="Log($\"TimeOut: ${ID}\"$)";
Debug.ShouldStop(16384);
b4i_main.__c.runVoidMethod ("LogImpl:::","33538947",(RemoteObject.concat(RemoteObject.createImmutable("TimeOut: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_id" /*RemoteObject*/ )))),RemoteObject.createImmutable(""))),0);
 BA.debugLineNum = 624;BA.debugLine="Dim Body As String = $\"<!DOCTYPE html> <html>";
Debug.ShouldStop(32768);
_body = (RemoteObject.concat(RemoteObject.createImmutable("<!DOCTYPE html>\n"),RemoteObject.createImmutable("<html>\n"),RemoteObject.createImmutable("  <head>\n"),RemoteObject.createImmutable("    <meta charset=\"UTF-8\" />\n"),RemoteObject.createImmutable("    <title>Error</title>\n"),RemoteObject.createImmutable("  </head>\n"),RemoteObject.createImmutable("  <body>\n"),RemoteObject.createImmutable("    <h1>401 Unauthorized.</h1>\n"),RemoteObject.createImmutable("  </body>\n"),RemoteObject.createImmutable("</html>")));Debug.locals.put("Body", _body);Debug.locals.put("Body", _body);
 BA.debugLineNum = 634;BA.debugLine="Dim HandShake As String = $\"HTTP/1.0 408 Request";
Debug.ShouldStop(33554432);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.0 408 Request Time-out\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletrequest.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Content-Type: text/html\n"),RemoteObject.createImmutable("Content-Length: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_body.runMethod(true,"Length")))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_body))),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 640;BA.debugLine="Response.Write(HandShake)";
Debug.ShouldStop(-2147483648);
__ref.getField(false,"_response" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_write:" /*RemoteObject*/ ,(Object)(_handshake));
 BA.debugLineNum = 641;BA.debugLine="Close";
Debug.ShouldStop(1);
__ref.runClassMethod (b4i_servletrequest.class, "_close" /*RemoteObject*/ );
 if (true) break;

case 4:
//C
this.state = -1;
;
 BA.debugLineNum = 643;BA.debugLine="End Sub";
Debug.ShouldStop(4);
if (true) break;

            }
        }
    }
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}
public static RemoteObject  _callevent(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("CallEvent (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,645);
if (RapidSub.canDelegate("callevent")) { return __ref.runUserSub(false, "servletrequest","callevent", __ref);}
 BA.debugLineNum = 645;BA.debugLine="Private Sub CallEvent";
Debug.ShouldStop(16);
 BA.debugLineNum = 646;BA.debugLine="If CallServer.DigestAuthentication And RequestURI";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean(".",__ref.getField(false,"_callserver" /*RemoteObject*/ ).getField(true,"_digestauthentication" /*RemoteObject*/ )) && RemoteObject.solveBoolean(".",__ref.getField(true,"_requesturi" /*RemoteObject*/ ).runMethod(true,"StartsWith:",(Object)(__ref.getField(false,"_callserver" /*RemoteObject*/ ).getField(true,"_digestpath" /*RemoteObject*/ ))))) { 
 BA.debugLineNum = 647;BA.debugLine="ElaborateHandSkakeDigestMessage(Me,Response)";
Debug.ShouldStop(64);
__ref.runClassMethod (b4i_servletrequest.class, "_elaboratehandskakedigestmessage::" /*RemoteObject*/ ,(Object)((__ref)),(Object)(__ref.getField(false,"_response" /*RemoteObject*/ )));
 }else {
 BA.debugLineNum = 649;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handle\",2";
Debug.ShouldStop(256);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_Handle"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
 BA.debugLineNum = 650;BA.debugLine="CallSub3(mCallBack,mEventName & \"_Handle\",Me,Re";
Debug.ShouldStop(512);
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_Handle"))),(Object)(__ref),(Object)((__ref.getField(false,"_response" /*RemoteObject*/ ))));
 };
 BA.debugLineNum = 652;BA.debugLine="If ConnectionAlive=False Then Close";
Debug.ShouldStop(2048);
if (RemoteObject.solveBoolean("=",__ref.getField(true,"_connectionalive" /*RemoteObject*/ ),b4i_main.__c.runMethod(true,"False"))) { 
__ref.runClassMethod (b4i_servletrequest.class, "_close" /*RemoteObject*/ );};
 };
 BA.debugLineNum = 654;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 1;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 2;BA.debugLine="Type tUser (Address As String, nc As Int, nonce A";
;
 //BA.debugLineNum = 3;BA.debugLine="Private DDay() As String = Array As String(\"\",\"Su";
b4i_servletrequest._dday = RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {BA.ObjectToString(""),BA.ObjectToString("Sun"),BA.ObjectToString("Mon"),BA.ObjectToString("Tue"),BA.ObjectToString("Wed"),BA.ObjectToString("Thu"),BA.ObjectToString("Fri"),RemoteObject.createImmutable("Sat")});__ref.setField("_dday",b4i_servletrequest._dday);
 //BA.debugLineNum = 4;BA.debugLine="Private MMmonth() As String = Array As String(\"\",";
b4i_servletrequest._mmmonth = RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {BA.ObjectToString(""),BA.ObjectToString("January"),BA.ObjectToString("February"),BA.ObjectToString("March"),BA.ObjectToString("April"),BA.ObjectToString("May"),BA.ObjectToString("June"),BA.ObjectToString("July"),BA.ObjectToString("August"),BA.ObjectToString("September"),BA.ObjectToString("October"),BA.ObjectToString("November"),RemoteObject.createImmutable("December")});__ref.setField("_mmmonth",b4i_servletrequest._mmmonth);
 //BA.debugLineNum = 6;BA.debugLine="Private client As Socket";
b4i_servletrequest._client = RemoteObject.createNew ("B4ISocketWrapper");__ref.setField("_client",b4i_servletrequest._client);
 //BA.debugLineNum = 7;BA.debugLine="Private astream As AsyncStreams";
b4i_servletrequest._astream = RemoteObject.createNew ("B4IAsyncStreams");__ref.setField("_astream",b4i_servletrequest._astream);
 //BA.debugLineNum = 8;BA.debugLine="Private mCallBack As Object";
b4i_servletrequest._mcallback = RemoteObject.createNew ("NSObject");__ref.setField("_mcallback",b4i_servletrequest._mcallback);
 //BA.debugLineNum = 9;BA.debugLine="Private mEventName As String";
b4i_servletrequest._meventname = RemoteObject.createImmutable("");__ref.setField("_meventname",b4i_servletrequest._meventname);
 //BA.debugLineNum = 10;BA.debugLine="Private bc As ByteConverter";
b4i_servletrequest._bc = RemoteObject.createNew ("B4IByteConverter");__ref.setField("_bc",b4i_servletrequest._bc);
 //BA.debugLineNum = 12;BA.debugLine="Private method As String = \"\"";
b4i_servletrequest._method = BA.ObjectToString("");__ref.setField("_method",b4i_servletrequest._method);
 //BA.debugLineNum = 13;BA.debugLine="Private RequestURI As String = \"\"";
b4i_servletrequest._requesturi = BA.ObjectToString("");__ref.setField("_requesturi",b4i_servletrequest._requesturi);
 //BA.debugLineNum = 14;BA.debugLine="Private RequestHOST As String = \"\"";
b4i_servletrequest._requesthost = BA.ObjectToString("");__ref.setField("_requesthost",b4i_servletrequest._requesthost);
 //BA.debugLineNum = 15;BA.debugLine="Private Address As String =\"\"";
b4i_servletrequest._address = BA.ObjectToString("");__ref.setField("_address",b4i_servletrequest._address);
 //BA.debugLineNum = 16;BA.debugLine="Private ConnPort As String = \"\"";
b4i_servletrequest._connport = BA.ObjectToString("");__ref.setField("_connport",b4i_servletrequest._connport);
 //BA.debugLineNum = 17;BA.debugLine="Public ID As String";
b4i_servletrequest._id = RemoteObject.createImmutable("");__ref.setField("_id",b4i_servletrequest._id);
 //BA.debugLineNum = 19;BA.debugLine="Public RequestHeader As Map";
b4i_servletrequest._requestheader = RemoteObject.createNew ("B4IMap");__ref.setField("_requestheader",b4i_servletrequest._requestheader);
 //BA.debugLineNum = 20;BA.debugLine="Public RequestParameter As Map";
b4i_servletrequest._requestparameter = RemoteObject.createNew ("B4IMap");__ref.setField("_requestparameter",b4i_servletrequest._requestparameter);
 //BA.debugLineNum = 21;BA.debugLine="Public RequestCookies As Map";
b4i_servletrequest._requestcookies = RemoteObject.createNew ("B4IMap");__ref.setField("_requestcookies",b4i_servletrequest._requestcookies);
 //BA.debugLineNum = 22;BA.debugLine="Public RequestPostDataRow As List";
b4i_servletrequest._requestpostdatarow = RemoteObject.createNew ("B4IList");__ref.setField("_requestpostdatarow",b4i_servletrequest._requestpostdatarow);
 //BA.debugLineNum = 23;BA.debugLine="Public ConnectionAlive As Boolean = True";
b4i_servletrequest._connectionalive = b4i_main.__c.runMethod(true,"True");__ref.setField("_connectionalive",b4i_servletrequest._connectionalive);
 //BA.debugLineNum = 24;BA.debugLine="Public CharacterEncoding As String = \"UTF-8\" '\"IS";
b4i_servletrequest._characterencoding = BA.ObjectToString("UTF-8");__ref.setField("_characterencoding",b4i_servletrequest._characterencoding);
 //BA.debugLineNum = 30;BA.debugLine="Private CallServer As httpServer";
b4i_servletrequest._callserver = RemoteObject.createNew ("b4i_httpserver");__ref.setField("_callserver",b4i_servletrequest._callserver);
 //BA.debugLineNum = 31;BA.debugLine="Private Response As ServletResponse";
b4i_servletrequest._response = RemoteObject.createNew ("b4i_servletresponse");__ref.setField("_response",b4i_servletrequest._response);
 //BA.debugLineNum = 32;BA.debugLine="Private Users As Map";
b4i_servletrequest._users = RemoteObject.createNew ("B4IMap");__ref.setField("_users",b4i_servletrequest._users);
 //BA.debugLineNum = 33;BA.debugLine="Private UserName As String = \"\"";
b4i_servletrequest._username = BA.ObjectToString("");__ref.setField("_username",b4i_servletrequest._username);
 //BA.debugLineNum = 34;BA.debugLine="Private Password As String = \"\"";
b4i_servletrequest._password = BA.ObjectToString("");__ref.setField("_password",b4i_servletrequest._password);
 //BA.debugLineNum = 35;BA.debugLine="Private OtherData As Boolean = False";
b4i_servletrequest._otherdata = b4i_main.__c.runMethod(true,"False");__ref.setField("_otherdata",b4i_servletrequest._otherdata);
 //BA.debugLineNum = 36;BA.debugLine="Private Cache() As Byte";
b4i_servletrequest._cache = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 0)});__ref.setField("_cache",b4i_servletrequest._cache);
 //BA.debugLineNum = 37;BA.debugLine="Private BBB() As Byte";
b4i_servletrequest._bbb = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 0)});__ref.setField("_bbb",b4i_servletrequest._bbb);
 //BA.debugLineNum = 38;BA.debugLine="Private su As StringUtils";
b4i_servletrequest._su = RemoteObject.createNew ("iStringUtils");__ref.setField("_su",b4i_servletrequest._su);
 //BA.debugLineNum = 40;BA.debugLine="Private LogPrivate As Boolean = False";
b4i_servletrequest._logprivate = b4i_main.__c.runMethod(true,"False");__ref.setField("_logprivate",b4i_servletrequest._logprivate);
 //BA.debugLineNum = 41;BA.debugLine="Public LogActive As Boolean = False";
b4i_servletrequest._logactive = b4i_main.__c.runMethod(true,"False");__ref.setField("_logactive",b4i_servletrequest._logactive);
 //BA.debugLineNum = 42;BA.debugLine="Public ContentType As String = \"\"";
b4i_servletrequest._contenttype = BA.ObjectToString("");__ref.setField("_contenttype",b4i_servletrequest._contenttype);
 //BA.debugLineNum = 43;BA.debugLine="Public ContentLength As Long = 0";
b4i_servletrequest._contentlength = BA.numberCast(long.class, 0);__ref.setField("_contentlength",b4i_servletrequest._contentlength);
 //BA.debugLineNum = 44;BA.debugLine="Public MultipartFilename As Map";
b4i_servletrequest._multipartfilename = RemoteObject.createNew ("B4IMap");__ref.setField("_multipartfilename",b4i_servletrequest._multipartfilename);
 //BA.debugLineNum = 45;BA.debugLine="Public LogFirstRefuse As Boolean = False";
b4i_servletrequest._logfirstrefuse = b4i_main.__c.runMethod(true,"False");__ref.setField("_logfirstrefuse",b4i_servletrequest._logfirstrefuse);
 //BA.debugLineNum = 46;BA.debugLine="Public Timeout As Long = 10000";
b4i_servletrequest._timeout = BA.numberCast(long.class, 10000);__ref.setField("_timeout",b4i_servletrequest._timeout);
 //BA.debugLineNum = 47;BA.debugLine="Private LastBrowserComunicate As Long = 0";
b4i_servletrequest._lastbrowsercomunicate = BA.numberCast(long.class, 0);__ref.setField("_lastbrowsercomunicate",b4i_servletrequest._lastbrowsercomunicate);
 //BA.debugLineNum = 48;BA.debugLine="Private gzip=False, deflate=False As Boolean";
b4i_servletrequest._gzip = b4i_main.__c.runMethod(true,"False");__ref.setField("_gzip",b4i_servletrequest._gzip);
b4i_servletrequest._deflate = b4i_main.__c.runMethod(true,"False");__ref.setField("_deflate",b4i_servletrequest._deflate);
 //BA.debugLineNum = 50;BA.debugLine="Private WebSocket As Boolean = False";
b4i_servletrequest._websocket = b4i_main.__c.runMethod(true,"False");__ref.setField("_websocket",b4i_servletrequest._websocket);
 //BA.debugLineNum = 51;BA.debugLine="Private keyWebSocket As String = \"\"";
b4i_servletrequest._keywebsocket = BA.ObjectToString("");__ref.setField("_keywebsocket",b4i_servletrequest._keywebsocket);
 //BA.debugLineNum = 52;BA.debugLine="Private WebSocketString As String";
b4i_servletrequest._websocketstring = RemoteObject.createImmutable("");__ref.setField("_websocketstring",b4i_servletrequest._websocketstring);
 //BA.debugLineNum = 53;BA.debugLine="Private LastOpCode As Byte = 0";
b4i_servletrequest._lastopcode = BA.numberCast(byte.class, 0);__ref.setField("_lastopcode",b4i_servletrequest._lastopcode);
 //BA.debugLineNum = 54;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _close(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("Close (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,105);
if (RapidSub.canDelegate("close")) { return __ref.runUserSub(false, "servletrequest","close", __ref);}
 BA.debugLineNum = 105;BA.debugLine="Public Sub Close";
Debug.ShouldStop(256);
 BA.debugLineNum = 106;BA.debugLine="Try";
Debug.ShouldStop(512);
try { BA.debugLineNum = 107;BA.debugLine="astream.SendAllAndClose";
Debug.ShouldStop(1024);
__ref.getField(false,"_astream" /*RemoteObject*/ ).runVoidMethod ("SendAllAndClose");
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e4) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e4.toString()); BA.debugLineNum = 110;BA.debugLine="Log($\"Server error(${ID}): ${LastException.Messa";
Debug.ShouldStop(8192);
b4i_main.__c.runVoidMethod ("LogImpl:::","31638405",(RemoteObject.concat(RemoteObject.createImmutable("Server error("),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_id" /*RemoteObject*/ )))),RemoteObject.createImmutable("): "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"LastException").runMethod(true,"Message")))),RemoteObject.createImmutable(""))),0);
 };
 BA.debugLineNum = 112;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _completedate(RemoteObject __ref,RemoteObject _dt) throws Exception{
try {
		Debug.PushSubsStack("CompleteDate (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,615);
if (RapidSub.canDelegate("completedate")) { return __ref.runUserSub(false, "servletrequest","completedate", __ref, _dt);}
Debug.locals.put("DT", _dt);
 BA.debugLineNum = 615;BA.debugLine="Private Sub CompleteDate(DT As Long) As String";
Debug.ShouldStop(64);
 BA.debugLineNum = 616;BA.debugLine="Return $\"${DDay(DateTime.GetDayOfWeek(DT))}, $2.0";
Debug.ShouldStop(128);
if (true) return (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(false,"_dday" /*RemoteObject*/ ).runMethod(true,"getObjectFast:", b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetDayOfWeek:",(Object)(_dt)))))),RemoteObject.createImmutable(", "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("2.0")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetDayOfMonth:",(Object)(_dt))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(false,"_mmmonth" /*RemoteObject*/ ).runMethod(true,"getObjectFast:", b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetMonth:",(Object)(_dt))).runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(BA.numberCast(int.class, 3)))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"GetYear:",(Object)(_dt))))),RemoteObject.createImmutable(" "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Time:",(Object)(_dt))))),RemoteObject.createImmutable(" GMT")));
 BA.debugLineNum = 618;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _connected(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("Connected (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,114);
if (RapidSub.canDelegate("connected")) { return __ref.runUserSub(false, "servletrequest","connected", __ref);}
 BA.debugLineNum = 114;BA.debugLine="Public Sub Connected As Boolean";
Debug.ShouldStop(131072);
 BA.debugLineNum = 115;BA.debugLine="Return client.Connected";
Debug.ShouldStop(262144);
if (true) return __ref.getField(false,"_client" /*RemoteObject*/ ).runMethod(true,"Connected");
 BA.debugLineNum = 116;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _decode(RemoteObject __ref,RemoteObject _s) throws Exception{
try {
		Debug.PushSubsStack("decode (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,670);
if (RapidSub.canDelegate("decode")) { return __ref.runUserSub(false, "servletrequest","decode", __ref, _s);}
RemoteObject _dec = RemoteObject.createImmutable("");
int _i = 0;
RemoteObject _ascii = RemoteObject.createImmutable(0);
Debug.locals.put("S", _s);
 BA.debugLineNum = 670;BA.debugLine="Private Sub decode(S As String) As String";
Debug.ShouldStop(536870912);
 BA.debugLineNum = 671;BA.debugLine="Dim dec As String = \"\"";
Debug.ShouldStop(1073741824);
_dec = BA.ObjectToString("");Debug.locals.put("dec", _dec);Debug.locals.put("dec", _dec);
 BA.debugLineNum = 672;BA.debugLine="For i=0 To S.Length-1";
Debug.ShouldStop(-2147483648);
{
final int step2 = 1;
final int limit2 = RemoteObject.solve(new RemoteObject[] {_s.runMethod(true,"Length"),RemoteObject.createImmutable(1)}, "-",1, 1).<Number>get().intValue();
_i = 0 ;
for (;(step2 > 0 && _i <= limit2) || (step2 < 0 && _i >= limit2) ;_i = ((int)(0 + _i + step2))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 674;BA.debugLine="If Asc(S.CharAt(i))=37 Then";
Debug.ShouldStop(2);
if (RemoteObject.solveBoolean("=",b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i))))),BA.numberCast(double.class, 37))) { 
 BA.debugLineNum = 675;BA.debugLine="Dim Ascii As Int = ((Asc(S.CharAt(i+1))-48)*16)";
Debug.ShouldStop(4);
_ascii = RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),RemoteObject.createImmutable(1)}, "+",1, 1))))),RemoteObject.createImmutable(48)}, "-",1, 1)),RemoteObject.createImmutable(16)}, "*",0, 1)),(RemoteObject.solve(new RemoteObject[] {b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),RemoteObject.createImmutable(2)}, "+",1, 1))))),RemoteObject.createImmutable(48)}, "-",1, 1))}, "+",1, 1);Debug.locals.put("Ascii", _ascii);Debug.locals.put("Ascii", _ascii);
 BA.debugLineNum = 676;BA.debugLine="dec= dec & Chr(Ascii)";
Debug.ShouldStop(8);
_dec = RemoteObject.concat(_dec,BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(_ascii))));Debug.locals.put("dec", _dec);
 BA.debugLineNum = 677;BA.debugLine="i=i+3";
Debug.ShouldStop(16);
_i = RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),RemoteObject.createImmutable(3)}, "+",1, 1).<Number>get().intValue();Debug.locals.put("i", _i);
 }else {
 BA.debugLineNum = 679;BA.debugLine="dec=dec & S.charat(i)";
Debug.ShouldStop(64);
_dec = RemoteObject.concat(_dec,BA.CharToString(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i)))));Debug.locals.put("dec", _dec);
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 683;BA.debugLine="Return dec";
Debug.ShouldStop(1024);
if (true) return _dec;
 BA.debugLineNum = 684;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _dectohex(RemoteObject __ref,RemoteObject _dec) throws Exception{
try {
		Debug.PushSubsStack("DecToHex (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,526);
if (RapidSub.canDelegate("dectohex")) { return __ref.runUserSub(false, "servletrequest","dectohex", __ref, _dec);}
Debug.locals.put("Dec", _dec);
 BA.debugLineNum = 526;BA.debugLine="Private Sub DecToHex(Dec As Int) As String 'ignore";
Debug.ShouldStop(8192);
 BA.debugLineNum = 527;BA.debugLine="Return bc.HexFromBytes(bc.IntsToBytes(Array As In";
Debug.ShouldStop(16384);
if (true) return __ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(true,"HexFromBytes:",(Object)(__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_dec})))));
 BA.debugLineNum = 528;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _demasked(RemoteObject __ref,RemoteObject _maskdata,RemoteObject _maskingkey,RemoteObject _length,RemoteObject _spacement,RemoteObject _masked) throws Exception{
try {
		Debug.PushSubsStack("DeMasked (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,823);
if (RapidSub.canDelegate("demasked")) { return __ref.runUserSub(false, "servletrequest","demasked", __ref, _maskdata, _maskingkey, _length, _spacement, _masked);}
RemoteObject _datafree = null;
int _i = 0;
RemoteObject _smask = RemoteObject.createImmutable(0);
Debug.locals.put("MaskData", _maskdata);
Debug.locals.put("Maskingkey", _maskingkey);
Debug.locals.put("Length", _length);
Debug.locals.put("Spacement", _spacement);
Debug.locals.put("Masked", _masked);
 BA.debugLineNum = 823;BA.debugLine="Private Sub DeMasked(MaskData() As Byte, Maskingke";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 824;BA.debugLine="Dim DataFree(Length) As Byte";
Debug.ShouldStop(8388608);
_datafree = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, _length)});Debug.locals.put("DataFree", _datafree);
 BA.debugLineNum = 825;BA.debugLine="If Masked Then";
Debug.ShouldStop(16777216);
if (_masked.getBoolean()) { 
 BA.debugLineNum = 828;BA.debugLine="For i=0 To Length-1";
Debug.ShouldStop(134217728);
{
final int step3 = 1;
final int limit3 = (int) (0 + RemoteObject.solve(new RemoteObject[] {_length,RemoteObject.createImmutable(1)}, "-",1, 2).<Number>get().longValue());
_i = 0 ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 829;BA.debugLine="Dim Smask As Int = Bit.And(0xFF, MaskData(i+Spa";
Debug.ShouldStop(268435456);
_smask = b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, 0xff)),(Object)(BA.numberCast(int.class, _maskdata.runMethod(true,"getByteFast:", BA.numberCast(int.class, RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),_spacement}, "+",1, 2))))));Debug.locals.put("Smask", _smask);Debug.locals.put("Smask", _smask);
 BA.debugLineNum = 830;BA.debugLine="DataFree(i)=Bit.Xor(Smask,Bit.And(0xFF,Maskingk";
Debug.ShouldStop(536870912);
_datafree.runVoidMethod ("setByteFast::", BA.numberCast(int.class, _i),BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"Xor::",(Object)(_smask),(Object)(b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, 0xff)),(Object)(BA.numberCast(int.class, _maskingkey.runMethod(true,"getByteFast:", RemoteObject.solve(new RemoteObject[] {RemoteObject.createImmutable(_i),RemoteObject.createImmutable(4)}, "%",0, 1)))))))));
 }
}Debug.locals.put("i", _i);
;
 }else {
 BA.debugLineNum = 833;BA.debugLine="Bit.ArrayCopy(MaskData,Spacement,DataFree,0,Leng";
Debug.ShouldStop(1);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_maskdata),(Object)(BA.numberCast(int.class, _spacement)),(Object)(_datafree),(Object)(BA.numberCast(int.class, 0)),(Object)(BA.numberCast(int.class, _length)));
 };
 BA.debugLineNum = 835;BA.debugLine="Return DataFree";
Debug.ShouldStop(4);
if (true) return _datafree;
 BA.debugLineNum = 836;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _elaboratehandshake(RemoteObject __ref,RemoteObject _handshake,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("ElaborateHandShake (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,252);
if (RapidSub.canDelegate("elaboratehandshake")) { return __ref.runUserSub(false, "servletrequest","elaboratehandshake", __ref, _handshake, _data);}
RemoteObject _row = RemoteObject.createImmutable("");
RemoteObject _p = null;
RemoteObject _pm = RemoteObject.createImmutable("");
RemoteObject _pms = null;
RemoteObject _parm = RemoteObject.createImmutable("");
RemoteObject _ck = null;
RemoteObject _s = RemoteObject.createImmutable("");
int _i = 0;
Debug.locals.put("HandShake", _handshake);
Debug.locals.put("Data", _data);
 BA.debugLineNum = 252;BA.debugLine="Private Sub ElaborateHandShake(HandShake As String";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 254;BA.debugLine="BBB = ArrayInitialize";
Debug.ShouldStop(536870912);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 BA.debugLineNum = 255;BA.debugLine="BBB = ArrayAppend(BBB,Data)";
Debug.ShouldStop(1073741824);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayappend::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(_data)));
 BA.debugLineNum = 258;BA.debugLine="RequestHeader.Clear";
Debug.ShouldStop(2);
__ref.getField(false,"_requestheader" /*RemoteObject*/ ).runVoidMethod ("Clear");
 BA.debugLineNum = 259;BA.debugLine="RequestParameter.Clear";
Debug.ShouldStop(4);
__ref.getField(false,"_requestparameter" /*RemoteObject*/ ).runVoidMethod ("Clear");
 BA.debugLineNum = 260;BA.debugLine="RequestCookies.Clear";
Debug.ShouldStop(8);
__ref.getField(false,"_requestcookies" /*RemoteObject*/ ).runVoidMethod ("Clear");
 BA.debugLineNum = 261;BA.debugLine="RequestPostDataRow.Clear";
Debug.ShouldStop(16);
__ref.getField(false,"_requestpostdatarow" /*RemoteObject*/ ).runVoidMethod ("Clear");
 BA.debugLineNum = 263;BA.debugLine="For Each Row As String  In Regex.Split(CRLF,HandS";
Debug.ShouldStop(64);
{
final RemoteObject group7 = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(b4i_main.__c.runMethod(true,"CRLF")),(Object)(_handshake));
final int groupLen7 = group7.runMethod(true,"Size").<Integer>get()
;int index7 = 0;
;
for (; index7 < groupLen7;index7++){
_row = group7.runMethod(true,"Get:",index7);Debug.locals.put("Row", _row);
Debug.locals.put("Row", _row);
 BA.debugLineNum = 264;BA.debugLine="Row=Row.Replace(Chr(13),\"\").Replace(Chr(10),\"\")";
Debug.ShouldStop(128);
_row = _row.runMethod(true,"Replace::",(Object)(BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(BA.numberCast(int.class, 13))))),(Object)(RemoteObject.createImmutable(""))).runMethod(true,"Replace::",(Object)(BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(BA.numberCast(int.class, 10))))),(Object)(RemoteObject.createImmutable("")));Debug.locals.put("Row", _row);
 BA.debugLineNum = 265;BA.debugLine="Dim p() As String = Regex.Split(\" \",Row)";
Debug.ShouldStop(256);
_p = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(BA.ObjectToString(" ")),(Object)(_row));Debug.locals.put("p", _p);Debug.locals.put("p", _p);
 BA.debugLineNum = 267;BA.debugLine="Select p(0).ToUpperCase";
Debug.ShouldStop(1024);
switch (BA.switchObjectToInt(_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 0)).runMethod(true,"ToUpperCase"),BA.ObjectToString("GET"),BA.ObjectToString("POST"),BA.ObjectToString("CONTENT-LENGTH:"),BA.ObjectToString("CONTENT-TYPE:"),BA.ObjectToString("HOST:"),BA.ObjectToString("COOKIE:"),BA.ObjectToString("CONNECTION:"),BA.ObjectToString("UPGRADE:"),BA.ObjectToString("SEC-WEBSOCKET-KEY:"),RemoteObject.createImmutable("Accept-Encoding:").runMethod(true,"ToUpperCase"))) {
case 0: 
case 1: {
 BA.debugLineNum = 269;BA.debugLine="method=p(0).ToUpperCase";
Debug.ShouldStop(4096);
__ref.setField ("_method" /*RemoteObject*/ ,_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 0)).runMethod(true,"ToUpperCase"));
 BA.debugLineNum = 270;BA.debugLine="RequestURI=p(1)";
Debug.ShouldStop(8192);
__ref.setField ("_requesturi" /*RemoteObject*/ ,_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)));
 BA.debugLineNum = 271;BA.debugLine="If RequestURI.IndexOf(\"?\")>-1 Then";
Debug.ShouldStop(16384);
if (RemoteObject.solveBoolean(">",__ref.getField(true,"_requesturi" /*RemoteObject*/ ).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("?"))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 272;BA.debugLine="Dim pm As String = RequestURI.SubString(Reque";
Debug.ShouldStop(32768);
_pm = __ref.getField(true,"_requesturi" /*RemoteObject*/ ).runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {__ref.getField(true,"_requesturi" /*RemoteObject*/ ).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("?"))),RemoteObject.createImmutable(1)}, "+",1, 1)));Debug.locals.put("pm", _pm);Debug.locals.put("pm", _pm);
 BA.debugLineNum = 273;BA.debugLine="RequestURI=RequestURI.SubString2(0,RequestURI";
Debug.ShouldStop(65536);
__ref.setField ("_requesturi" /*RemoteObject*/ ,__ref.getField(true,"_requesturi" /*RemoteObject*/ ).runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(__ref.getField(true,"_requesturi" /*RemoteObject*/ ).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("?"))))));
 BA.debugLineNum = 275;BA.debugLine="Dim pms() As String = Regex.Split(\"&\",pm)";
Debug.ShouldStop(262144);
_pms = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(BA.ObjectToString("&")),(Object)(_pm));Debug.locals.put("pms", _pms);Debug.locals.put("pms", _pms);
 BA.debugLineNum = 277;BA.debugLine="For Each parm As String In pms";
Debug.ShouldStop(1048576);
{
final RemoteObject group18 = _pms;
final int groupLen18 = group18.runMethod(true,"Size").<Integer>get()
;int index18 = 0;
;
for (; index18 < groupLen18;index18++){
_parm = group18.runMethod(true,"Get:",index18);Debug.locals.put("parm", _parm);
Debug.locals.put("parm", _parm);
 BA.debugLineNum = 278;BA.debugLine="If parm.IndexOf(\"=\")>-1 Then";
Debug.ShouldStop(2097152);
if (RemoteObject.solveBoolean(">",_parm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 279;BA.debugLine="RequestParameter.Put(parm.SubString2(0,parm";
Debug.ShouldStop(4194304);
__ref.getField(false,"_requestparameter" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_parm.runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(_parm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))))))),(Object)((_parm.runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_parm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),RemoteObject.createImmutable(1)}, "+",1, 1))))));
 };
 }
}Debug.locals.put("parm", _parm);
;
 };
 BA.debugLineNum = 283;BA.debugLine="If p.Length>2 Then";
Debug.ShouldStop(67108864);
if (RemoteObject.solveBoolean(">",_p.getField(true,"Length"),BA.numberCast(double.class, 2))) { 
 BA.debugLineNum = 285;BA.debugLine="If p(2).ToLowerCase.StartsWith(\"http/1\")=Fals";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean("=",_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 2)).runMethod(true,"ToLowerCase").runMethod(true,"StartsWith:",(Object)(RemoteObject.createImmutable("http/1"))),b4i_main.__c.runMethod(true,"False"))) { 
 BA.debugLineNum = 286;BA.debugLine="Close";
Debug.ShouldStop(536870912);
__ref.runClassMethod (b4i_servletrequest.class, "_close" /*RemoteObject*/ );
 };
 }else {
 BA.debugLineNum = 290;BA.debugLine="Close";
Debug.ShouldStop(2);
__ref.runClassMethod (b4i_servletrequest.class, "_close" /*RemoteObject*/ );
 };
 break; }
case 2: {
 BA.debugLineNum = 293;BA.debugLine="ContentLength=p(1)";
Debug.ShouldStop(16);
__ref.setField ("_contentlength" /*RemoteObject*/ ,BA.numberCast(long.class, _p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1))));
 BA.debugLineNum = 294;BA.debugLine="If ContentLength>BBB.Length Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean(">",__ref.getField(true,"_contentlength" /*RemoteObject*/ ),BA.numberCast(long.class, __ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length")))) { 
 BA.debugLineNum = 295;BA.debugLine="OtherData=True";
Debug.ShouldStop(64);
__ref.setField ("_otherdata" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));
 }else {
 BA.debugLineNum = 301;BA.debugLine="OtherData=False";
Debug.ShouldStop(4096);
__ref.setField ("_otherdata" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"False"));
 BA.debugLineNum = 302;BA.debugLine="If ContentLength<BBB.Length Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("<",__ref.getField(true,"_contentlength" /*RemoteObject*/ ),BA.numberCast(long.class, __ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length")))) { 
 BA.debugLineNum = 304;BA.debugLine="Cache=ArrayAppend(Cache,SubArray(BBB,Content";
Debug.ShouldStop(32768);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayappend::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(__ref.runClassMethod (b4i_servletrequest.class, "_subarray::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, __ref.getField(true,"_contentlength" /*RemoteObject*/ )))))));
 BA.debugLineNum = 306;BA.debugLine="BBB=ArrayRemove(BBB,ContentLength,BBB.Length";
Debug.ShouldStop(131072);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayremove:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, __ref.getField(true,"_contentlength" /*RemoteObject*/ ))),(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length"))));
 };
 };
 break; }
case 3: {
 BA.debugLineNum = 310;BA.debugLine="ContentType=p(1).Replace(\";\",\"\").Trim";
Debug.ShouldStop(2097152);
__ref.setField ("_contenttype" /*RemoteObject*/ ,_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)).runMethod(true,"Replace::",(Object)(BA.ObjectToString(";")),(Object)(RemoteObject.createImmutable(""))).runMethod(true,"Trim"));
 BA.debugLineNum = 311;BA.debugLine="If p.Length>2 Then";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean(">",_p.getField(true,"Length"),BA.numberCast(double.class, 2))) { 
 BA.debugLineNum = 312;BA.debugLine="If P(2).indexof(\"charset=\")>-1 Then Character";
Debug.ShouldStop(8388608);
if (RemoteObject.solveBoolean(">",_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 2)).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("charset="))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
__ref.setField ("_characterencoding" /*RemoteObject*/ ,_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 2)).runMethod(true,"Replace::",(Object)(BA.ObjectToString("charset=")),(Object)(RemoteObject.createImmutable(""))).runMethod(true,"Trim"));};
 };
 break; }
case 4: {
 BA.debugLineNum = 315;BA.debugLine="If p.Length>1 Then";
Debug.ShouldStop(67108864);
if (RemoteObject.solveBoolean(">",_p.getField(true,"Length"),BA.numberCast(double.class, 1))) { 
 BA.debugLineNum = 316;BA.debugLine="RequestHOST=p(1)";
Debug.ShouldStop(134217728);
__ref.setField ("_requesthost" /*RemoteObject*/ ,_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)));
 BA.debugLineNum = 317;BA.debugLine="If RequestHOST.IndexOf(\":\")>-1 Then RequestHO";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean(">",__ref.getField(true,"_requesthost" /*RemoteObject*/ ).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable(":"))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
__ref.setField ("_requesthost" /*RemoteObject*/ ,__ref.getField(true,"_requesthost" /*RemoteObject*/ ).runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(__ref.getField(true,"_requesthost" /*RemoteObject*/ ).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable(":"))))));};
 }else {
 BA.debugLineNum = 319;BA.debugLine="RequestHOST=\"\"";
Debug.ShouldStop(1073741824);
__ref.setField ("_requesthost" /*RemoteObject*/ ,BA.ObjectToString(""));
 };
 break; }
case 5: {
 BA.debugLineNum = 322;BA.debugLine="Dim Ck() As String = Regex.Split(\";\",Row.Repla";
Debug.ShouldStop(2);
_ck = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(BA.ObjectToString(";")),(Object)(_row.runMethod(true,"Replace::",(Object)(BA.ObjectToString("Cookie: ")),(Object)(RemoteObject.createImmutable("")))));Debug.locals.put("Ck", _ck);Debug.locals.put("Ck", _ck);
 BA.debugLineNum = 323;BA.debugLine="For Each s As String In Ck";
Debug.ShouldStop(4);
{
final RemoteObject group56 = _ck;
final int groupLen56 = group56.runMethod(true,"Size").<Integer>get()
;int index56 = 0;
;
for (; index56 < groupLen56;index56++){
_s = group56.runMethod(true,"Get:",index56);Debug.locals.put("s", _s);
Debug.locals.put("s", _s);
 BA.debugLineNum = 324;BA.debugLine="If s.IndexOf(\"=\")>-1 Then";
Debug.ShouldStop(8);
if (RemoteObject.solveBoolean(">",_s.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 325;BA.debugLine="RequestCookies.Put(s.SubString2(0, s.IndexOf";
Debug.ShouldStop(16);
__ref.getField(false,"_requestcookies" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_s.runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(_s.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))))).runMethod(true,"Trim"))),(Object)((_s.runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_s.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),RemoteObject.createImmutable(1)}, "+",1, 1))))));
 };
 }
}Debug.locals.put("s", _s);
;
 break; }
case 6: {
 BA.debugLineNum = 329;BA.debugLine="If p(1).ToLowerCase.IndexOf(\"keep-alive\")>-1 T";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean(">",_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)).runMethod(true,"ToLowerCase").runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("keep-alive"))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 330;BA.debugLine="ConnectionAlive=True";
Debug.ShouldStop(512);
__ref.setField ("_connectionalive" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));
 }else 
{ BA.debugLineNum = 331;BA.debugLine="else if p(1).ToLowerCase.IndexOf(\"close\")>-1 T";
Debug.ShouldStop(1024);
if (RemoteObject.solveBoolean(">",_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)).runMethod(true,"ToLowerCase").runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("close"))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 332;BA.debugLine="ConnectionAlive=False";
Debug.ShouldStop(2048);
__ref.setField ("_connectionalive" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"False"));
 }}
;
 break; }
case 7: {
 BA.debugLineNum = 335;BA.debugLine="If p(1).ToLowerCase=\"websocket\" Then WebSocket";
Debug.ShouldStop(16384);
if (RemoteObject.solveBoolean("=",_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)).runMethod(true,"ToLowerCase"),BA.ObjectToString("websocket"))) { 
__ref.setField ("_websocket" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));};
 break; }
case 8: {
 BA.debugLineNum = 337;BA.debugLine="WebSocket=True";
Debug.ShouldStop(65536);
__ref.setField ("_websocket" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));
 BA.debugLineNum = 338;BA.debugLine="keyWebSocket=p(1)";
Debug.ShouldStop(131072);
__ref.setField ("_keywebsocket" /*RemoteObject*/ ,_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)));
 break; }
case 9: {
 BA.debugLineNum = 340;BA.debugLine="For i=1 To p.Length-1";
Debug.ShouldStop(524288);
{
final int step73 = 1;
final int limit73 = RemoteObject.solve(new RemoteObject[] {_p.getField(true,"Length"),RemoteObject.createImmutable(1)}, "-",1, 1).<Number>get().intValue();
_i = 1 ;
for (;(step73 > 0 && _i <= limit73) || (step73 < 0 && _i >= limit73) ;_i = ((int)(0 + _i + step73))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 341;BA.debugLine="If p(I).Contains(\"gzip\") Then gzip=True";
Debug.ShouldStop(1048576);
if (_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, _i)).runMethod(true,"Contains:",(Object)(RemoteObject.createImmutable("gzip"))).getBoolean()) { 
__ref.setField ("_gzip" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));};
 BA.debugLineNum = 342;BA.debugLine="If p(I).Contains(\"deflate\") Then deflate=True";
Debug.ShouldStop(2097152);
if (_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, _i)).runMethod(true,"Contains:",(Object)(RemoteObject.createImmutable("deflate"))).getBoolean()) { 
__ref.setField ("_deflate" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"True"));};
 }
}Debug.locals.put("i", _i);
;
 break; }
default: {
 BA.debugLineNum = 345;BA.debugLine="If p(0).IndexOf(\":\")>-1 Then";
Debug.ShouldStop(16777216);
if (RemoteObject.solveBoolean(">",_p.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 0)).runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable(":"))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 346;BA.debugLine="RequestHeader.Put(Row.SubString2(0, Row.Index";
Debug.ShouldStop(33554432);
__ref.getField(false,"_requestheader" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_row.runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(_row.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable(":"))))))),(Object)((_row.runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_row.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable(":"))),RemoteObject.createImmutable(1)}, "+",1, 1))))));
 };
 break; }
}
;
 }
}Debug.locals.put("Row", _row);
;
 BA.debugLineNum = 351;BA.debugLine="If RequestHOST=\"\" Then";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean("=",__ref.getField(true,"_requesthost" /*RemoteObject*/ ),BA.ObjectToString(""))) { 
 BA.debugLineNum = 353;BA.debugLine="Close";
Debug.ShouldStop(1);
__ref.runClassMethod (b4i_servletrequest.class, "_close" /*RemoteObject*/ );
 }else 
{ BA.debugLineNum = 354;BA.debugLine="else if WebSocket Then";
Debug.ShouldStop(2);
if (__ref.getField(true,"_websocket" /*RemoteObject*/ ).getBoolean()) { 
 BA.debugLineNum = 355;BA.debugLine="LgC(HandShake.Replace(Chr(13),\"\"),0xFF0000FF)";
Debug.ShouldStop(4);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(_handshake.runMethod(true,"Replace::",(Object)(BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(BA.numberCast(int.class, 13))))),(Object)(RemoteObject.createImmutable("")))),(Object)(BA.numberCast(int.class, 0xff0000ff)));
 BA.debugLineNum = 356;BA.debugLine="SendAcceptKeyWs";
Debug.ShouldStop(8);
__ref.runClassMethod (b4i_servletrequest.class, "_sendacceptkeyws" /*RemoteObject*/ );
 BA.debugLineNum = 357;BA.debugLine="BBB = ArrayInitialize";
Debug.ShouldStop(16);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 }else 
{ BA.debugLineNum = 358;BA.debugLine="Else if OtherData=False Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean("=",__ref.getField(true,"_otherdata" /*RemoteObject*/ ),b4i_main.__c.runMethod(true,"False"))) { 
 BA.debugLineNum = 359;BA.debugLine="extractParameterFromData";
Debug.ShouldStop(64);
__ref.runClassMethod (b4i_servletrequest.class, "_extractparameterfromdata" /*RemoteObject*/ );
 BA.debugLineNum = 360;BA.debugLine="CallEvent";
Debug.ShouldStop(128);
__ref.runClassMethod (b4i_servletrequest.class, "_callevent" /*RemoteObject*/ );
 }}}
;
 BA.debugLineNum = 362;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _elaboratehandskakedigestmessage(RemoteObject __ref,RemoteObject _request,RemoteObject _sresponse) throws Exception{
try {
		Debug.PushSubsStack("ElaborateHandSkakeDigestMessage (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,364);
if (RapidSub.canDelegate("elaboratehandskakedigestmessage")) { return __ref.runUserSub(false, "servletrequest","elaboratehandskakedigestmessage", __ref, _request, _sresponse);}
RemoteObject _headerdigest = RemoteObject.createImmutable("");
RemoteObject _headermap = RemoteObject.declareNull("B4IMap");
RemoteObject _paramteremap = RemoteObject.declareNull("B4IMap");
RemoteObject _fields = RemoteObject.declareNull("B4IMap");
RemoteObject _fl = RemoteObject.createImmutable("");
RemoteObject _key = RemoteObject.createImmutable("");
RemoteObject _value = RemoteObject.createImmutable("");
RemoteObject _user = RemoteObject.declareNull("_tuser");
RemoteObject _ha1 = RemoteObject.createImmutable("");
RemoteObject _ha2 = RemoteObject.createImmutable("");
RemoteObject _ss = RemoteObject.createImmutable("");
RemoteObject _md5response = RemoteObject.createImmutable("");
RemoteObject _l = RemoteObject.createImmutable(0);
Debug.locals.put("Request", _request);
Debug.locals.put("SResponse", _sresponse);
 BA.debugLineNum = 364;BA.debugLine="Private Sub ElaborateHandSkakeDigestMessage(Reques";
Debug.ShouldStop(2048);
 BA.debugLineNum = 365;BA.debugLine="Dim HeaderDigest As String = \"Authorization\"";
Debug.ShouldStop(4096);
_headerdigest = BA.ObjectToString("Authorization");Debug.locals.put("HeaderDigest", _headerdigest);Debug.locals.put("HeaderDigest", _headerdigest);
 BA.debugLineNum = 366;BA.debugLine="Dim HeaderMap, ParamtereMap As Map";
Debug.ShouldStop(8192);
_headermap = RemoteObject.createNew ("B4IMap");Debug.locals.put("HeaderMap", _headermap);
_paramteremap = RemoteObject.createNew ("B4IMap");Debug.locals.put("ParamtereMap", _paramteremap);
 BA.debugLineNum = 368;BA.debugLine="HeaderMap.Initialize";
Debug.ShouldStop(32768);
_headermap.runVoidMethod ("Initialize");
 BA.debugLineNum = 369;BA.debugLine="ParamtereMap.Initialize";
Debug.ShouldStop(65536);
_paramteremap.runVoidMethod ("Initialize");
 BA.debugLineNum = 370;BA.debugLine="Try";
Debug.ShouldStop(131072);
try { BA.debugLineNum = 371;BA.debugLine="LgP(\"Client: \" & Request.RemoteAddress)";
Debug.ShouldStop(262144);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Client: "),_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ ))));
 BA.debugLineNum = 372;BA.debugLine="LgP(Request.RequestURI)";
Debug.ShouldStop(524288);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)(_request.getField(true,"_requesturi" /*RemoteObject*/ )));
 BA.debugLineNum = 392;BA.debugLine="If  Request.GetHeader(HeaderDigest)=\"\" Then";
Debug.ShouldStop(128);
if (RemoteObject.solveBoolean("=",_request.runClassMethod (b4i_servletrequest.class, "_getheader:" /*RemoteObject*/ ,(Object)(_headerdigest)),BA.ObjectToString(""))) { 
 BA.debugLineNum = 394;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_RefusedN";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean(".",__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_RefusedNoCredential"))),(Object)(BA.numberCast(int.class, 1)))) && RemoteObject.solveBoolean(".",__ref.getField(true,"_logfirstrefuse" /*RemoteObject*/ ))) { 
b4i_main.__c.runMethodAndSync(false,"CallSub2::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_RefusedNoCredential"))),(Object)((_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ ))));};
 BA.debugLineNum = 395;BA.debugLine="SendRefuse(SResponse,NewUser(Request.RemoteAddr";
Debug.ShouldStop(1024);
__ref.runClassMethod (b4i_servletrequest.class, "_sendrefuse::" /*RemoteObject*/ ,(Object)(_sresponse),(Object)(__ref.runClassMethod (b4i_servletrequest.class, "_newuser:" /*RemoteObject*/ ,(Object)(_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ )))));
 }else {
 BA.debugLineNum = 398;BA.debugLine="Dim Fields As Map";
Debug.ShouldStop(8192);
_fields = RemoteObject.createNew ("B4IMap");Debug.locals.put("Fields", _fields);
 BA.debugLineNum = 399;BA.debugLine="Fields.Initialize";
Debug.ShouldStop(16384);
_fields.runVoidMethod ("Initialize");
 BA.debugLineNum = 401;BA.debugLine="For Each fl As String In Regex.Split(\",\",Reques";
Debug.ShouldStop(65536);
{
final RemoteObject group14 = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(BA.ObjectToString(",")),(Object)(_request.runClassMethod (b4i_servletrequest.class, "_getheader:" /*RemoteObject*/ ,(Object)(_headerdigest))));
final int groupLen14 = group14.runMethod(true,"Size").<Integer>get()
;int index14 = 0;
;
for (; index14 < groupLen14;index14++){
_fl = group14.runMethod(true,"Get:",index14);Debug.locals.put("fl", _fl);
Debug.locals.put("fl", _fl);
 BA.debugLineNum = 402;BA.debugLine="If fl.IndexOf(\"=\")>-1 Then";
Debug.ShouldStop(131072);
if (RemoteObject.solveBoolean(">",_fl.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 403;BA.debugLine="Dim Key As String = fl.SubString2(0,fl.IndexO";
Debug.ShouldStop(262144);
_key = _fl.runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(_fl.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))))).runMethod(true,"Trim");Debug.locals.put("Key", _key);Debug.locals.put("Key", _key);
 BA.debugLineNum = 404;BA.debugLine="Dim Value As String = fl.SubString(fl.IndexOf";
Debug.ShouldStop(524288);
_value = _fl.runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_fl.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),RemoteObject.createImmutable(1)}, "+",1, 1))).runMethod(true,"Replace::",(Object)(BA.ObjectToString("\"")),(Object)(RemoteObject.createImmutable(""))).runMethod(true,"Trim");Debug.locals.put("Value", _value);Debug.locals.put("Value", _value);
 BA.debugLineNum = 405;BA.debugLine="Fields.Put(Key,Value)";
Debug.ShouldStop(1048576);
_fields.runVoidMethod ("Put::",(Object)((_key)),(Object)((_value)));
 BA.debugLineNum = 406;BA.debugLine="LgP($\"** -${Key}- -${Value}-\"$)";
Debug.ShouldStop(2097152);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("** -"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_key))),RemoteObject.createImmutable("- -"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_value))),RemoteObject.createImmutable("-")))));
 }else {
 BA.debugLineNum = 408;BA.debugLine="Log(\"Error on Header: \" & fl)";
Debug.ShouldStop(8388608);
b4i_main.__c.runVoidMethod ("LogImpl:::","32883628",RemoteObject.concat(RemoteObject.createImmutable("Error on Header: "),_fl),0);
 };
 }
}Debug.locals.put("fl", _fl);
;
 BA.debugLineNum = 413;BA.debugLine="Dim User As tUser = FindUser(Request.RemoteAddr";
Debug.ShouldStop(268435456);
_user = __ref.runClassMethod (b4i_servletrequest.class, "_finduser::" /*RemoteObject*/ ,(Object)(_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ )),(Object)(BA.ObjectToString(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("opaque")))))));Debug.locals.put("User", _user);Debug.locals.put("User", _user);
 BA.debugLineNum = 414;BA.debugLine="FindCredential(Fields.Get(\"Digest username\"))";
Debug.ShouldStop(536870912);
__ref.runClassMethod (b4i_servletrequest.class, "_findcredential:" /*RemoteObject*/ ,(Object)(BA.ObjectToString(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("Digest username")))))));
 BA.debugLineNum = 415;BA.debugLine="User.realm=CallServer.realm";
Debug.ShouldStop(1073741824);
_user.setField ("realm" /*RemoteObject*/ ,__ref.getField(false,"_callserver" /*RemoteObject*/ ).getField(true,"_realm" /*RemoteObject*/ ));
 BA.debugLineNum = 417;BA.debugLine="Dim HA1 As String = MD5($\"${UserName}:${Fields.";
Debug.ShouldStop(1);
_ha1 = __ref.runClassMethod (b4i_servletrequest.class, "_md5:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_username" /*RemoteObject*/ )))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("realm")))))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.getField(true,"_password" /*RemoteObject*/ )))),RemoteObject.createImmutable("")))));Debug.locals.put("HA1", _ha1);Debug.locals.put("HA1", _ha1);
 BA.debugLineNum = 418;BA.debugLine="Dim HA2 As String = MD5($\"${Request.Method.ToUp";
Debug.ShouldStop(2);
_ha2 = __ref.runClassMethod (b4i_servletrequest.class, "_md5:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_request.getField(true,"_method" /*RemoteObject*/ ).runMethod(true,"ToUpperCase")))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_request.getField(true,"_requesturi" /*RemoteObject*/ )))),RemoteObject.createImmutable("")))));Debug.locals.put("HA2", _ha2);Debug.locals.put("HA2", _ha2);
 BA.debugLineNum = 419;BA.debugLine="If Fields.ContainsKey(\"qop\") Then";
Debug.ShouldStop(4);
if (_fields.runMethod(true,"ContainsKey:",(Object)((RemoteObject.createImmutable("qop")))).getBoolean()) { 
 BA.debugLineNum = 420;BA.debugLine="Dim SS As String = $\"${HA1}:${Fields.Get(\"nonc";
Debug.ShouldStop(8);
_ss = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_ha1))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("nonce")))))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("nc")))))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("cnonce")))))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("qop")))))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_ha2))),RemoteObject.createImmutable("")));Debug.locals.put("SS", _ss);Debug.locals.put("SS", _ss);
 }else {
 BA.debugLineNum = 422;BA.debugLine="Dim SS As String = $\"${HA1}:${Fields.Get(\"nonc";
Debug.ShouldStop(32);
_ss = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_ha1))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("nonce")))))),RemoteObject.createImmutable(":"),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_ha2))),RemoteObject.createImmutable("")));Debug.locals.put("SS", _ss);Debug.locals.put("SS", _ss);
 };
 BA.debugLineNum = 424;BA.debugLine="Dim MD5response As String = MD5(SS)";
Debug.ShouldStop(128);
_md5response = __ref.runClassMethod (b4i_servletrequest.class, "_md5:" /*RemoteObject*/ ,(Object)(_ss));Debug.locals.put("MD5response", _md5response);Debug.locals.put("MD5response", _md5response);
 BA.debugLineNum = 426;BA.debugLine="LgP($\"HA1: ${HA1}\"$)";
Debug.ShouldStop(512);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("HA1: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_ha1))),RemoteObject.createImmutable("")))));
 BA.debugLineNum = 427;BA.debugLine="LgP($\"HA2: ${HA2}\"$)";
Debug.ShouldStop(1024);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("HA2: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_ha2))),RemoteObject.createImmutable("")))));
 BA.debugLineNum = 428;BA.debugLine="LgP(SS)";
Debug.ShouldStop(2048);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)(_ss));
 BA.debugLineNum = 429;BA.debugLine="LgP($\"MD5 send: ${Fields.get(\"response\")}\"$)";
Debug.ShouldStop(4096);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("MD5 send: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("response")))))),RemoteObject.createImmutable("")))));
 BA.debugLineNum = 430;BA.debugLine="LgP($\"MD5 Calc : ${MD5response}\"$)";
Debug.ShouldStop(8192);
__ref.runClassMethod (b4i_servletrequest.class, "_lgp:" /*RemoteObject*/ ,(Object)((RemoteObject.concat(RemoteObject.createImmutable("MD5 Calc : "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_md5response))),RemoteObject.createImmutable("")))));
 BA.debugLineNum = 432;BA.debugLine="Dim L As Int = HexToDec(Fields.Get(\"nc\"))";
Debug.ShouldStop(32768);
_l = __ref.runClassMethod (b4i_servletrequest.class, "_hextodec:" /*RemoteObject*/ ,(Object)(BA.ObjectToString(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("nc")))))));Debug.locals.put("L", _l);Debug.locals.put("L", _l);
 BA.debugLineNum = 434;BA.debugLine="LgC(\"opaque:\" & User.opaque & \" clnc.\" & L & \"";
Debug.ShouldStop(131072);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("opaque:"),_user.getField(true,"opaque" /*RemoteObject*/ ),RemoteObject.createImmutable(" clnc."),_l,RemoteObject.createImmutable(" nc."),_user.getField(true,"nc" /*RemoteObject*/ ))),(Object)(BA.numberCast(int.class, 0xff000f04)));
 BA.debugLineNum = 435;BA.debugLine="If MD5response=Fields.get(\"response\") Then";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean("=",_md5response,BA.ObjectToString(_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("response"))))))) { 
 BA.debugLineNum = 436;BA.debugLine="LgC(\"Digest: \" & MD5response,0xFF0000FF)";
Debug.ShouldStop(524288);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Digest: "),_md5response)),(Object)(BA.numberCast(int.class, 0xff0000ff)));
 BA.debugLineNum = 437;BA.debugLine="If  L>User.nc Or CallServer.IgnoreNC Then";
Debug.ShouldStop(1048576);
if (RemoteObject.solveBoolean(">",_l,BA.numberCast(double.class, _user.getField(true,"nc" /*RemoteObject*/ ))) || RemoteObject.solveBoolean(".",__ref.getField(false,"_callserver" /*RemoteObject*/ ).getField(true,"_ignorenc" /*RemoteObject*/ ))) { 
 BA.debugLineNum = 439;BA.debugLine="User.nonce=bc.HexFromBytes(bc.LongsToBytes(Ar";
Debug.ShouldStop(4194304);
_user.setField ("nonce" /*RemoteObject*/ ,__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(true,"HexFromBytes:",(Object)(__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"LongsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")}))))));
 BA.debugLineNum = 440;BA.debugLine="User.nc=Max(l,User.nc)";
Debug.ShouldStop(8388608);
_user.setField ("nc" /*RemoteObject*/ ,BA.numberCast(int.class, b4i_main.__c.runMethod(true,"Max::",(Object)(BA.numberCast(double.class, _l)),(Object)(BA.numberCast(double.class, _user.getField(true,"nc" /*RemoteObject*/ ))))));
 BA.debugLineNum = 441;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_LogIn\"";
Debug.ShouldStop(16777216);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_LogIn"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_LogIn"))),(Object)((__ref.getField(true,"_username" /*RemoteObject*/ ))),(Object)((_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ ))));};
 BA.debugLineNum = 442;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handle";
Debug.ShouldStop(33554432);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_Handle"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
 BA.debugLineNum = 443;BA.debugLine="SResponse.SetHeader(\"WWW-Authenticate\",$\"Dig";
Debug.ShouldStop(67108864);
_sresponse.runClassMethod (b4i_servletresponse.class, "_setheader::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("WWW-Authenticate")),(Object)((RemoteObject.concat(RemoteObject.createImmutable("Digest realm=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_user.getField(true,"realm" /*RemoteObject*/ )))),RemoteObject.createImmutable("\", qop=\"auth,auth-int\", nonce=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_user.getField(true,"nonce" /*RemoteObject*/ )))),RemoteObject.createImmutable("\", opaque=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_user.getField(true,"opaque" /*RemoteObject*/ )))),RemoteObject.createImmutable("\"")))));
 BA.debugLineNum = 444;BA.debugLine="SResponse.ContentType=\"text/html\"";
Debug.ShouldStop(134217728);
_sresponse.setField ("_contenttype" /*RemoteObject*/ ,BA.ObjectToString("text/html"));
 BA.debugLineNum = 445;BA.debugLine="CallSub3(mCallBack,mEventName & \"_Handle\",Re";
Debug.ShouldStop(268435456);
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_Handle"))),(Object)((_request)),(Object)((_sresponse)));
 };
 }else {
 BA.debugLineNum = 449;BA.debugLine="User.nc=Max(l,User.nc)";
Debug.ShouldStop(1);
_user.setField ("nc" /*RemoteObject*/ ,BA.numberCast(int.class, b4i_main.__c.runMethod(true,"Max::",(Object)(BA.numberCast(double.class, _l)),(Object)(BA.numberCast(double.class, _user.getField(true,"nc" /*RemoteObject*/ ))))));
 BA.debugLineNum = 450;BA.debugLine="SendRefuse(SResponse,User)";
Debug.ShouldStop(2);
__ref.runClassMethod (b4i_servletrequest.class, "_sendrefuse::" /*RemoteObject*/ ,(Object)(_sresponse),(Object)(_user));
 BA.debugLineNum = 451;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Refuse";
Debug.ShouldStop(4);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_RefusedWrongNC"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_RefusedWrongNC"))),(Object)((__ref.getField(true,"_username" /*RemoteObject*/ ))),(Object)((_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ ))));};
 };
 }else {
 BA.debugLineNum = 455;BA.debugLine="LgC(\"Wrong<> \" & MD5response & CRLF & \"Digest<";
Debug.ShouldStop(64);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Wrong<> "),_md5response,b4i_main.__c.runMethod(true,"CRLF"),RemoteObject.createImmutable("Digest<> "),_fields.runMethod(false,"Get:",(Object)((RemoteObject.createImmutable("response")))))),(Object)(BA.numberCast(int.class, 0xffff0000)));
 BA.debugLineNum = 456;BA.debugLine="User.nc=Max(l,User.nc)";
Debug.ShouldStop(128);
_user.setField ("nc" /*RemoteObject*/ ,BA.numberCast(int.class, b4i_main.__c.runMethod(true,"Max::",(Object)(BA.numberCast(double.class, _l)),(Object)(BA.numberCast(double.class, _user.getField(true,"nc" /*RemoteObject*/ ))))));
 BA.debugLineNum = 457;BA.debugLine="SendRefuse(SResponse,User)";
Debug.ShouldStop(256);
__ref.runClassMethod (b4i_servletrequest.class, "_sendrefuse::" /*RemoteObject*/ ,(Object)(_sresponse),(Object)(_user));
 BA.debugLineNum = 458;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Refused";
Debug.ShouldStop(512);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_RefusedWrongCredential"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_RefusedWrongCredential"))),(Object)((__ref.getField(true,"_username" /*RemoteObject*/ ))),(Object)((_request.runClassMethod (b4i_servletrequest.class, "_remoteaddress" /*RemoteObject*/ ))));};
 };
 };
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e66) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e66.toString()); BA.debugLineNum = 462;BA.debugLine="SResponse.Status = 500";
Debug.ShouldStop(8192);
_sresponse.setField ("_status" /*RemoteObject*/ ,BA.numberCast(int.class, 500));
 BA.debugLineNum = 463;BA.debugLine="Log(\"Error serving request: \" & LastException)";
Debug.ShouldStop(16384);
b4i_main.__c.runVoidMethod ("LogImpl:::","32883683",RemoteObject.concat(RemoteObject.createImmutable("Error serving request: "),b4i_main.__c.runMethod(false,"LastException")),0);
 BA.debugLineNum = 464;BA.debugLine="SResponse.SendString(\"Error serving request: \" &";
Debug.ShouldStop(32768);
_sresponse.runClassMethod (b4i_servletresponse.class, "_sendstring:" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Error serving request: "),b4i_main.__c.runMethod(false,"LastException"))));
 };
 BA.debugLineNum = 466;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _elaboratewebsocket(RemoteObject __ref,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("ElaborateWebSocket (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,716);
if (RapidSub.canDelegate("elaboratewebsocket")) { return __ref.runUserSub(false, "servletrequest","elaboratewebsocket", __ref, _data);}
RemoteObject _fin = RemoteObject.createImmutable(false);
RemoteObject _deflatecompress = RemoteObject.createImmutable(false);
RemoteObject _opcode = RemoteObject.createImmutable(0);
RemoteObject _masked = RemoteObject.createImmutable(false);
RemoteObject _maskingkey = null;
RemoteObject _payload = RemoteObject.createImmutable(0);
RemoteObject _startdate = RemoteObject.createImmutable(0);
RemoteObject _lng = RemoteObject.createImmutable(0L);
RemoteObject _dataclear = null;
RemoteObject _closecode = RemoteObject.createImmutable(0);
RemoteObject _closemessage = RemoteObject.createImmutable("");
Debug.locals.put("Data", _data);
 BA.debugLineNum = 716;BA.debugLine="Private Sub ElaborateWebSocket(Data() As Byte)";
Debug.ShouldStop(2048);
 BA.debugLineNum = 735;BA.debugLine="If Data.Length>=8 Then ' is frame";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean("g",_data.getField(true,"Length"),BA.numberCast(double.class, 8))) { 
 BA.debugLineNum = 736;BA.debugLine="Dim Fin As Boolean = (Bit.And(Data(0),128)>0)";
Debug.ShouldStop(-2147483648);
_fin = BA.ObjectToBoolean((RemoteObject.solveBoolean(">",b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 0)))),(Object)(BA.numberCast(int.class, 128))),BA.numberCast(double.class, 0))));Debug.locals.put("Fin", _fin);Debug.locals.put("Fin", _fin);
 BA.debugLineNum = 737;BA.debugLine="Dim DeflateCompress As Boolean  = (Bit.And(Data(";
Debug.ShouldStop(1);
_deflatecompress = BA.ObjectToBoolean((RemoteObject.solveBoolean(">",b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 0)))),(Object)(BA.numberCast(int.class, 64))),BA.numberCast(double.class, 0))));Debug.locals.put("DeflateCompress", _deflatecompress);Debug.locals.put("DeflateCompress", _deflatecompress);
 BA.debugLineNum = 738;BA.debugLine="Dim OpCode As Byte = Bit.And(Data(0),15)";
Debug.ShouldStop(2);
_opcode = BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 0)))),(Object)(BA.numberCast(int.class, 15))));Debug.locals.put("OpCode", _opcode);Debug.locals.put("OpCode", _opcode);
 BA.debugLineNum = 739;BA.debugLine="Dim Masked As Boolean = (Bit.And(Data(1),128)>0)";
Debug.ShouldStop(4);
_masked = BA.ObjectToBoolean((RemoteObject.solveBoolean(">",b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 1)))),(Object)(BA.numberCast(int.class, 128))),BA.numberCast(double.class, 0))));Debug.locals.put("Masked", _masked);Debug.locals.put("Masked", _masked);
 BA.debugLineNum = 740;BA.debugLine="Dim Maskingkey(4) As Byte";
Debug.ShouldStop(8);
_maskingkey = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {BA.numberCast(int.class, 4)});Debug.locals.put("Maskingkey", _maskingkey);
 BA.debugLineNum = 741;BA.debugLine="Dim Payload As Byte = Bit.And(Data(1),127)";
Debug.ShouldStop(16);
_payload = BA.numberCast(byte.class, b4i_main.__c.runMethod(false,"Bit").runMethod(true,"And::",(Object)(BA.numberCast(int.class, _data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 1)))),(Object)(BA.numberCast(int.class, 127))));Debug.locals.put("Payload", _payload);Debug.locals.put("Payload", _payload);
 BA.debugLineNum = 742;BA.debugLine="Dim Startdate As Int = 0";
Debug.ShouldStop(32);
_startdate = BA.numberCast(int.class, 0);Debug.locals.put("Startdate", _startdate);Debug.locals.put("Startdate", _startdate);
 BA.debugLineNum = 743;BA.debugLine="Dim Lng As Long";
Debug.ShouldStop(64);
_lng = RemoteObject.createImmutable(0L);Debug.locals.put("Lng", _lng);
 BA.debugLineNum = 745;BA.debugLine="If OpCode>0 Then LastOpCode=OpCode ' opcode cont";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean(">",_opcode,BA.numberCast(double.class, 0))) { 
__ref.setField ("_lastopcode" /*RemoteObject*/ ,_opcode);};
 BA.debugLineNum = 746;BA.debugLine="WebSocketString=\"\"";
Debug.ShouldStop(512);
__ref.setField ("_websocketstring" /*RemoteObject*/ ,BA.ObjectToString(""));
 BA.debugLineNum = 748;BA.debugLine="LgC(\"____________________________________\",0xFFC";
Debug.ShouldStop(2048);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(BA.ObjectToString("____________________________________")),(Object)(BA.numberCast(int.class, 0xffc0c0c0)));
 BA.debugLineNum = 749;BA.debugLine="LgC(\"Fin: \" & Fin,0xFFC0C0C0)";
Debug.ShouldStop(4096);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Fin: "),BA.BooleanToString(_fin))),(Object)(BA.numberCast(int.class, 0xffc0c0c0)));
 BA.debugLineNum = 750;BA.debugLine="LgC(\"OpCode: \" & OpCode,0xFFC0C0C0)";
Debug.ShouldStop(8192);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("OpCode: "),_opcode)),(Object)(BA.numberCast(int.class, 0xffc0c0c0)));
 BA.debugLineNum = 751;BA.debugLine="LgC(\"Mask: \" & Masked,0xFFC0C0C0)";
Debug.ShouldStop(16384);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Mask: "),BA.BooleanToString(_masked))),(Object)(BA.numberCast(int.class, 0xffc0c0c0)));
 BA.debugLineNum = 752;BA.debugLine="LgC(\"Payload: \" & Payload,0xFFC0C0C0)";
Debug.ShouldStop(32768);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(RemoteObject.concat(RemoteObject.createImmutable("Payload: "),_payload)),(Object)(BA.numberCast(int.class, 0xffc0c0c0)));
 BA.debugLineNum = 754;BA.debugLine="If Payload<126 Then";
Debug.ShouldStop(131072);
if (RemoteObject.solveBoolean("<",_payload,BA.numberCast(double.class, 126))) { 
 BA.debugLineNum = 756;BA.debugLine="Lng=Payload";
Debug.ShouldStop(524288);
_lng = BA.numberCast(long.class, _payload);Debug.locals.put("Lng", _lng);
 BA.debugLineNum = 757;BA.debugLine="Startdate=2";
Debug.ShouldStop(1048576);
_startdate = BA.numberCast(int.class, 2);Debug.locals.put("Startdate", _startdate);
 }else 
{ BA.debugLineNum = 758;BA.debugLine="else if Payload=126 Then";
Debug.ShouldStop(2097152);
if (RemoteObject.solveBoolean("=",_payload,BA.numberCast(double.class, 126))) { 
 BA.debugLineNum = 760;BA.debugLine="Lng = Data(2) *256 + Data(3)";
Debug.ShouldStop(8388608);
_lng = BA.numberCast(long.class, RemoteObject.solve(new RemoteObject[] {_data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 2)),RemoteObject.createImmutable(256),_data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 3))}, "*+",1, 1));Debug.locals.put("Lng", _lng);
 BA.debugLineNum = 761;BA.debugLine="Startdate=4";
Debug.ShouldStop(16777216);
_startdate = BA.numberCast(int.class, 4);Debug.locals.put("Startdate", _startdate);
 }else 
{ BA.debugLineNum = 762;BA.debugLine="Else if Payload = 127 Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean("=",_payload,BA.numberCast(double.class, 127))) { 
 BA.debugLineNum = 763;BA.debugLine="Dim bc As ByteConverter";
Debug.ShouldStop(67108864);
b4i_servletrequest._bc = RemoteObject.createNew ("B4IByteConverter");__ref.setField("_bc",b4i_servletrequest._bc);
 BA.debugLineNum = 764;BA.debugLine="Lng =  bc.IntsFromBytes(Array As Byte(Data(2),D";
Debug.ShouldStop(134217728);
_lng = BA.numberCast(long.class, __ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsFromBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {_data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 2)),_data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 3)),_data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 4)),_data.runMethod(true,"getByteFast:", BA.numberCast(int.class, 5))}))).runMethod(true,"getObjectFastN:", BA.numberCast(int.class, 0)));Debug.locals.put("Lng", _lng);
 BA.debugLineNum = 765;BA.debugLine="Startdate=6";
Debug.ShouldStop(268435456);
_startdate = BA.numberCast(int.class, 6);Debug.locals.put("Startdate", _startdate);
 }}}
;
 BA.debugLineNum = 767;BA.debugLine="If Masked Then";
Debug.ShouldStop(1073741824);
if (_masked.getBoolean()) { 
 BA.debugLineNum = 768;BA.debugLine="Bit.ArrayCopy(Data,Startdate,Maskingkey,0,4)";
Debug.ShouldStop(-2147483648);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data),(Object)(_startdate),(Object)(_maskingkey),(Object)(BA.numberCast(int.class, 0)),(Object)(BA.numberCast(int.class, 4)));
 BA.debugLineNum = 769;BA.debugLine="Startdate=Startdate+4";
Debug.ShouldStop(1);
_startdate = RemoteObject.solve(new RemoteObject[] {_startdate,RemoteObject.createImmutable(4)}, "+",1, 1);Debug.locals.put("Startdate", _startdate);
 };
 BA.debugLineNum = 771;BA.debugLine="BBB=ArrayAppend(BBB,DeMasked(Data,Maskingkey,Lng";
Debug.ShouldStop(4);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayappend::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(__ref.runClassMethod (b4i_servletrequest.class, "_demasked:::::" /*RemoteObject*/ ,(Object)(_data),(Object)(_maskingkey),(Object)(_lng),(Object)(BA.numberCast(long.class, _startdate)),(Object)(_masked)))));
 BA.debugLineNum = 773;BA.debugLine="If Fin Then";
Debug.ShouldStop(16);
if (_fin.getBoolean()) { 
 BA.debugLineNum = 774;BA.debugLine="If DeflateCompress Then  ' deflate header";
Debug.ShouldStop(32);
if (_deflatecompress.getBoolean()) { 
 BA.debugLineNum = 775;BA.debugLine="BBB=ArrayInsert(BBB,0,Array As Byte(120,156))";
Debug.ShouldStop(64);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinsert:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {BA.numberCast(byte.class, 120),BA.numberCast(byte.class, 156)}))));
 BA.debugLineNum = 776;BA.debugLine="Dim DataClear() As Byte = InflateDate(ArrayCop";
Debug.ShouldStop(128);
_dataclear = __ref.runClassMethod (b4i_servletrequest.class, "_inflatedate:" /*RemoteObject*/ ,(Object)(__ref.runClassMethod (b4i_servletrequest.class, "_arraycopy:" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )))));Debug.locals.put("DataClear", _dataclear);Debug.locals.put("DataClear", _dataclear);
 }else {
 BA.debugLineNum = 778;BA.debugLine="Dim DataClear() As Byte = ArrayCopy(BBB)";
Debug.ShouldStop(512);
_dataclear = __ref.runClassMethod (b4i_servletrequest.class, "_arraycopy:" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )));Debug.locals.put("DataClear", _dataclear);Debug.locals.put("DataClear", _dataclear);
 };
 BA.debugLineNum = 781;BA.debugLine="Select LastOpCode";
Debug.ShouldStop(4096);
switch (BA.switchObjectToInt(__ref.getField(true,"_lastopcode" /*RemoteObject*/ ),BA.numberCast(byte.class, 0),BA.numberCast(byte.class, 1),BA.numberCast(byte.class, 2),BA.numberCast(byte.class, 8),BA.numberCast(byte.class, 9),BA.numberCast(byte.class, 10))) {
case 0: {
 break; }
case 1: {
 BA.debugLineNum = 785;BA.debugLine="Try";
Debug.ShouldStop(65536);
try { BA.debugLineNum = 786;BA.debugLine="WebSocketString=BytesToString(DataClear,0,Da";
Debug.ShouldStop(131072);
__ref.setField ("_websocketstring" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"BytesToString::::",(Object)(_dataclear),(Object)(BA.numberCast(int.class, 0)),(Object)(_dataclear.getField(true,"Length")),(Object)(RemoteObject.createImmutable("UTF8"))));
 BA.debugLineNum = 787;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handl";
Debug.ShouldStop(262144);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_HandleWebSocket"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_HandleWebSocket"))),(Object)(__ref),(Object)((__ref.getField(false,"_response" /*RemoteObject*/ ))));};
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e47) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e47.toString()); BA.debugLineNum = 789;BA.debugLine="WebSocketString=\"\"";
Debug.ShouldStop(1048576);
__ref.setField ("_websocketstring" /*RemoteObject*/ ,BA.ObjectToString(""));
 BA.debugLineNum = 790;BA.debugLine="Log(LastException)";
Debug.ShouldStop(2097152);
b4i_main.__c.runVoidMethod ("LogImpl:::","33932234",BA.ObjectToString(b4i_main.__c.runMethod(false,"LastException")),0);
 };
 break; }
case 2: {
 BA.debugLineNum = 793;BA.debugLine="Try";
Debug.ShouldStop(16777216);
try { BA.debugLineNum = 794;BA.debugLine="WebSocketString=BytesToString(DataClear,0,Da";
Debug.ShouldStop(33554432);
__ref.setField ("_websocketstring" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"BytesToString::::",(Object)(_dataclear),(Object)(BA.numberCast(int.class, 0)),(Object)(_dataclear.getField(true,"Length")),(Object)(RemoteObject.createImmutable("UTF8"))));
 BA.debugLineNum = 795;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handl";
Debug.ShouldStop(67108864);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_HandleWebSocket"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_HandleWebSocket"))),(Object)(__ref),(Object)((__ref.getField(false,"_response" /*RemoteObject*/ ))));};
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e55) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e55.toString()); BA.debugLineNum = 797;BA.debugLine="WebSocketString=\"\"";
Debug.ShouldStop(268435456);
__ref.setField ("_websocketstring" /*RemoteObject*/ ,BA.ObjectToString(""));
 BA.debugLineNum = 798;BA.debugLine="Log(LastException)";
Debug.ShouldStop(536870912);
b4i_main.__c.runVoidMethod ("LogImpl:::","33932242",BA.ObjectToString(b4i_main.__c.runMethod(false,"LastException")),0);
 };
 break; }
case 3: {
 BA.debugLineNum = 801;BA.debugLine="Dim CloseCode As Int = DataClear(0)*256+DataC";
Debug.ShouldStop(1);
_closecode = RemoteObject.solve(new RemoteObject[] {_dataclear.runMethod(true,"getByteFast:", BA.numberCast(int.class, 0)),RemoteObject.createImmutable(256),_dataclear.runMethod(true,"getByteFast:", BA.numberCast(int.class, 1))}, "*+",1, 1);Debug.locals.put("CloseCode", _closecode);Debug.locals.put("CloseCode", _closecode);
 BA.debugLineNum = 802;BA.debugLine="Dim CloseMessage As String =\"\"";
Debug.ShouldStop(2);
_closemessage = BA.ObjectToString("");Debug.locals.put("CloseMessage", _closemessage);Debug.locals.put("CloseMessage", _closemessage);
 BA.debugLineNum = 804;BA.debugLine="Try";
Debug.ShouldStop(8);
try { BA.debugLineNum = 805;BA.debugLine="CloseMessage=BytesToString(DataClear,2,DataC";
Debug.ShouldStop(16);
_closemessage = b4i_main.__c.runMethod(true,"BytesToString::::",(Object)(_dataclear),(Object)(BA.numberCast(int.class, 2)),(Object)(RemoteObject.solve(new RemoteObject[] {_dataclear.getField(true,"Length"),RemoteObject.createImmutable(2)}, "-",1, 1)),(Object)(RemoteObject.createImmutable("UTF8")));Debug.locals.put("CloseMessage", _closemessage);
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e64) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e64.toString()); BA.debugLineNum = 807;BA.debugLine="Log(LastException)";
Debug.ShouldStop(64);
b4i_main.__c.runVoidMethod ("LogImpl:::","33932251",BA.ObjectToString(b4i_main.__c.runMethod(false,"LastException")),0);
 };
 BA.debugLineNum = 809;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_WebSoc";
Debug.ShouldStop(256);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_WebSocketClose"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_WebSocketClose"))),(Object)((_closecode)),(Object)((_closemessage)));};
 break; }
case 4: {
 BA.debugLineNum = 811;BA.debugLine="Response.SendWebSocketPong";
Debug.ShouldStop(1024);
__ref.getField(false,"_response" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_sendwebsocketpong" /*RemoteObject*/ );
 break; }
case 5: {
 break; }
default: {
 BA.debugLineNum = 815;BA.debugLine="Log(\"OpCode unknown \"& LastOpCode)";
Debug.ShouldStop(16384);
b4i_main.__c.runVoidMethod ("LogImpl:::","33932259",RemoteObject.concat(RemoteObject.createImmutable("OpCode unknown "),__ref.getField(true,"_lastopcode" /*RemoteObject*/ )),0);
 break; }
}
;
 BA.debugLineNum = 817;BA.debugLine="BBB=ArrayInitialize";
Debug.ShouldStop(65536);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 };
 };
 BA.debugLineNum = 821;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _encode(RemoteObject __ref,RemoteObject _s) throws Exception{
try {
		Debug.PushSubsStack("encode (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,656);
if (RapidSub.canDelegate("encode")) { return __ref.runUserSub(false, "servletrequest","encode", __ref, _s);}
RemoteObject _addr = RemoteObject.createImmutable("");
int _i = 0;
Debug.locals.put("S", _s);
 BA.debugLineNum = 656;BA.debugLine="Private Sub encode(S As String) As String 'ignore";
Debug.ShouldStop(32768);
 BA.debugLineNum = 657;BA.debugLine="Dim Addr As String = \"\"";
Debug.ShouldStop(65536);
_addr = BA.ObjectToString("");Debug.locals.put("Addr", _addr);Debug.locals.put("Addr", _addr);
 BA.debugLineNum = 658;BA.debugLine="For i=0 To S.Length-1";
Debug.ShouldStop(131072);
{
final int step2 = 1;
final int limit2 = RemoteObject.solve(new RemoteObject[] {_s.runMethod(true,"Length"),RemoteObject.createImmutable(1)}, "-",1, 1).<Number>get().intValue();
_i = 0 ;
for (;(step2 > 0 && _i <= limit2) || (step2 < 0 && _i >= limit2) ;_i = ((int)(0 + _i + step2))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 660;BA.debugLine="If Asc(S.CharAt(i))>=44 And Asc(S.CharAt(i))<=12";
Debug.ShouldStop(524288);
if (RemoteObject.solveBoolean("g",b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i))))),BA.numberCast(double.class, 44)) && RemoteObject.solveBoolean("k",b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i))))),BA.numberCast(double.class, 122)) || RemoteObject.solveBoolean("=",b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i))))),BA.numberCast(double.class, 38))) { 
 BA.debugLineNum = 661;BA.debugLine="Addr=Addr & S.CharAt(i)";
Debug.ShouldStop(1048576);
_addr = RemoteObject.concat(_addr,BA.CharToString(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i)))));Debug.locals.put("Addr", _addr);
 }else {
 BA.debugLineNum = 663;BA.debugLine="Addr=Addr & \"%\" &  DecToHex(Asc(S.CharAt(i)))";
Debug.ShouldStop(4194304);
_addr = RemoteObject.concat(_addr,RemoteObject.createImmutable("%"),__ref.runClassMethod (b4i_servletrequest.class, "_dectohex:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(true,"Asc:",(Object)(_s.runMethod(true,"CharAt:",(Object)(BA.numberCast(int.class, _i))))))));Debug.locals.put("Addr", _addr);
 };
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 667;BA.debugLine="Return Addr";
Debug.ShouldStop(67108864);
if (true) return _addr;
 BA.debugLineNum = 668;BA.debugLine="End Sub";
Debug.ShouldStop(134217728);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _extracthandshake(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("ExtractHandShake (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,202);
if (RapidSub.canDelegate("extracthandshake")) { return __ref.runUserSub(false, "servletrequest","extracthandshake", __ref);}
RemoteObject _chremove = RemoteObject.createImmutable(0);
RemoteObject _index = RemoteObject.createImmutable(0);
RemoteObject _handshake = RemoteObject.createImmutable("");
RemoteObject _data = null;
RemoteObject _d = null;
 BA.debugLineNum = 202;BA.debugLine="Private Sub ExtractHandShake";
Debug.ShouldStop(512);
 BA.debugLineNum = 203;BA.debugLine="If OtherData And ContentLength>0 Then ' continue";
Debug.ShouldStop(1024);
if (RemoteObject.solveBoolean(".",__ref.getField(true,"_otherdata" /*RemoteObject*/ )) && RemoteObject.solveBoolean(">",__ref.getField(true,"_contentlength" /*RemoteObject*/ ),BA.numberCast(long.class, 0))) { 
 BA.debugLineNum = 204;BA.debugLine="BBB=ArrayAppend(BBB,Cache)";
Debug.ShouldStop(2048);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayappend::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ ))));
 BA.debugLineNum = 205;BA.debugLine="Cache=ArrayInitialize";
Debug.ShouldStop(4096);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 BA.debugLineNum = 211;BA.debugLine="If ContentLength<=BBB.Length Then";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean("k",__ref.getField(true,"_contentlength" /*RemoteObject*/ ),BA.numberCast(long.class, __ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length")))) { 
 BA.debugLineNum = 212;BA.debugLine="If BBB.Length>ContentLength Then ' select only";
Debug.ShouldStop(524288);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length"),BA.numberCast(double.class, __ref.getField(true,"_contentlength" /*RemoteObject*/ )))) { 
 BA.debugLineNum = 213;BA.debugLine="Cache=ArrayAppend(Cache,SubArray(BBB,ContentLe";
Debug.ShouldStop(1048576);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayappend::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(__ref.runClassMethod (b4i_servletrequest.class, "_subarray::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, __ref.getField(true,"_contentlength" /*RemoteObject*/ )))))));
 BA.debugLineNum = 215;BA.debugLine="BBB=ArrayRemove(BBB,ContentLength,BBB.Length)";
Debug.ShouldStop(4194304);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayremove:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, __ref.getField(true,"_contentlength" /*RemoteObject*/ ))),(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length"))));
 };
 BA.debugLineNum = 218;BA.debugLine="OtherData=False";
Debug.ShouldStop(33554432);
__ref.setField ("_otherdata" /*RemoteObject*/ ,b4i_main.__c.runMethod(true,"False"));
 BA.debugLineNum = 219;BA.debugLine="extractParameterFromData";
Debug.ShouldStop(67108864);
__ref.runClassMethod (b4i_servletrequest.class, "_extractparameterfromdata" /*RemoteObject*/ );
 };
 }else {
 BA.debugLineNum = 223;BA.debugLine="Dim ChRemove As Int = 4";
Debug.ShouldStop(1073741824);
_chremove = BA.numberCast(int.class, 4);Debug.locals.put("ChRemove", _chremove);Debug.locals.put("ChRemove", _chremove);
 BA.debugLineNum = 224;BA.debugLine="Dim Index As Int = ArrayIndexOf(Cache,Array As B";
Debug.ShouldStop(-2147483648);
_index = __ref.runClassMethod (b4i_servletrequest.class, "_arrayindexof::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 10),BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 10)})));Debug.locals.put("Index", _index);Debug.locals.put("Index", _index);
 BA.debugLineNum = 225;BA.debugLine="If Index=-1 Then";
Debug.ShouldStop(1);
if (RemoteObject.solveBoolean("=",_index,BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 226;BA.debugLine="ChRemove = 2";
Debug.ShouldStop(2);
_chremove = BA.numberCast(int.class, 2);Debug.locals.put("ChRemove", _chremove);
 BA.debugLineNum = 227;BA.debugLine="Index = ArrayIndexOf(Cache,Array As Byte(10,10)";
Debug.ShouldStop(4);
_index = __ref.runClassMethod (b4i_servletrequest.class, "_arrayindexof::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {BA.numberCast(byte.class, 10),BA.numberCast(byte.class, 10)})));Debug.locals.put("Index", _index);
 BA.debugLineNum = 228;BA.debugLine="If Index=-1 Then Index = ArrayIndexOf(Cache,Arr";
Debug.ShouldStop(8);
if (RemoteObject.solveBoolean("=",_index,BA.numberCast(double.class, -(double) (0 + 1)))) { 
_index = __ref.runClassMethod (b4i_servletrequest.class, "_arrayindexof::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 13)})));Debug.locals.put("Index", _index);};
 };
 BA.debugLineNum = 232;BA.debugLine="If Index>-1 Then";
Debug.ShouldStop(128);
if (RemoteObject.solveBoolean(">",_index,BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 233;BA.debugLine="Dim HandShake As String = BytesToString(Cache,0";
Debug.ShouldStop(256);
_handshake = b4i_main.__c.runMethod(true,"BytesToString::::",(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(_index),(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )));Debug.locals.put("HandShake", _handshake);Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 234;BA.debugLine="Dim Data() As Byte = SubArray(Cache,Index+ChRem";
Debug.ShouldStop(512);
_data = __ref.runClassMethod (b4i_servletrequest.class, "_subarray::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )),(Object)(RemoteObject.solve(new RemoteObject[] {_index,_chremove}, "+",1, 1)));Debug.locals.put("Data", _data);Debug.locals.put("Data", _data);
 BA.debugLineNum = 235;BA.debugLine="Cache=ArrayInitialize";
Debug.ShouldStop(1024);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 BA.debugLineNum = 236;BA.debugLine="ElaborateHandShake(HandShake,Data)";
Debug.ShouldStop(2048);
__ref.runClassMethod (b4i_servletrequest.class, "_elaboratehandshake::" /*RemoteObject*/ ,(Object)(_handshake),(Object)(_data));
 }else {
 BA.debugLineNum = 239;BA.debugLine="If WebSocket Then";
Debug.ShouldStop(16384);
if (__ref.getField(true,"_websocket" /*RemoteObject*/ ).getBoolean()) { 
 BA.debugLineNum = 241;BA.debugLine="Dim D() As Byte = ArrayCopy(Cache)";
Debug.ShouldStop(65536);
_d = __ref.runClassMethod (b4i_servletrequest.class, "_arraycopy:" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_cache" /*RemoteObject*/ )));Debug.locals.put("D", _d);Debug.locals.put("D", _d);
 BA.debugLineNum = 242;BA.debugLine="ElaborateWebSocket(D)";
Debug.ShouldStop(131072);
__ref.runClassMethod (b4i_servletrequest.class, "_elaboratewebsocket:" /*RemoteObject*/ ,(Object)(_d));
 }else {
 BA.debugLineNum = 244;BA.debugLine="Log(\"No handshake: \"& Cache.Length)";
Debug.ShouldStop(524288);
b4i_main.__c.runVoidMethod ("LogImpl:::","32752554",RemoteObject.concat(RemoteObject.createImmutable("No handshake: "),__ref.getField(false,"_cache" /*RemoteObject*/ ).getField(true,"Length")),0);
 };
 BA.debugLineNum = 247;BA.debugLine="Cache = ArrayInitialize";
Debug.ShouldStop(4194304);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 };
 };
 BA.debugLineNum = 250;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _extractparameterfromdata(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("extractParameterFromData (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,468);
if (RapidSub.canDelegate("extractparameterfromdata")) { return __ref.runUserSub(false, "servletrequest","extractparameterfromdata", __ref);}
RemoteObject _index = RemoteObject.createImmutable(0);
RemoteObject _cnt = RemoteObject.createImmutable("");
RemoteObject _m = null;
RemoteObject _rw = RemoteObject.createImmutable("");
RemoteObject _name = RemoteObject.createImmutable("");
RemoteObject _rows = null;
RemoteObject _params = null;
RemoteObject _parm = RemoteObject.createImmutable("");
 BA.debugLineNum = 468;BA.debugLine="Private Sub extractParameterFromData";
Debug.ShouldStop(524288);
 BA.debugLineNum = 470;BA.debugLine="If BBB.Length>0 Then";
Debug.ShouldStop(2097152);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length"),BA.numberCast(double.class, 0))) { 
 BA.debugLineNum = 471;BA.debugLine="If ContentType.ToLowerCase=\"multipart/form-data\"";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean("=",__ref.getField(true,"_contenttype" /*RemoteObject*/ ).runMethod(true,"ToLowerCase"),BA.ObjectToString("multipart/form-data"))) { 
 BA.debugLineNum = 472;BA.debugLine="Dim Index As Int = ArrayIndexOf(BBB,Array As By";
Debug.ShouldStop(8388608);
_index = __ref.runClassMethod (b4i_servletrequest.class, "_arrayindexof::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 10),BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 10)})));Debug.locals.put("Index", _index);Debug.locals.put("Index", _index);
 BA.debugLineNum = 473;BA.debugLine="MultipartFilename.Clear";
Debug.ShouldStop(16777216);
__ref.getField(false,"_multipartfilename" /*RemoteObject*/ ).runVoidMethod ("Clear");
 BA.debugLineNum = 474;BA.debugLine="If Index>-1 Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean(">",_index,BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 475;BA.debugLine="Dim Cnt As String = BytesToString(BBB,0,Index,";
Debug.ShouldStop(67108864);
_cnt = b4i_main.__c.runMethod(true,"BytesToString::::",(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(_index),(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )));Debug.locals.put("Cnt", _cnt);Debug.locals.put("Cnt", _cnt);
 BA.debugLineNum = 476;BA.debugLine="BBB=ArrayRemove(BBB,0,Index+4)";
Debug.ShouldStop(134217728);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayremove:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(RemoteObject.solve(new RemoteObject[] {_index,RemoteObject.createImmutable(4)}, "+",1, 1))));
 BA.debugLineNum = 478;BA.debugLine="Index=ArrayIndexOf(BBB,Array As Byte(13,10,13,";
Debug.ShouldStop(536870912);
_index = __ref.runClassMethod (b4i_servletrequest.class, "_arrayindexof::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initBytesWithData:", (Object)new RemoteObject[] {BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 10),BA.numberCast(byte.class, 13),BA.numberCast(byte.class, 10)})));Debug.locals.put("Index", _index);
 BA.debugLineNum = 479;BA.debugLine="Dim M() As Byte = SubArray(BBB,Index)'ignore";
Debug.ShouldStop(1073741824);
_m = __ref.runClassMethod (b4i_servletrequest.class, "_subarray::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(_index));Debug.locals.put("M", _m);Debug.locals.put("M", _m);
 BA.debugLineNum = 480;BA.debugLine="BBB=ArrayRemove(BBB,Index,BBB.Length)";
Debug.ShouldStop(-2147483648);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayremove:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(_index),(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length"))));
 BA.debugLineNum = 482;BA.debugLine="For Each Rw As String  In Regex.Split(CRLF,Cnt";
Debug.ShouldStop(2);
{
final RemoteObject group11 = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(b4i_main.__c.runMethod(true,"CRLF")),(Object)(_cnt));
final int groupLen11 = group11.runMethod(true,"Size").<Integer>get()
;int index11 = 0;
;
for (; index11 < groupLen11;index11++){
_rw = group11.runMethod(true,"Get:",index11);Debug.locals.put("Rw", _rw);
Debug.locals.put("Rw", _rw);
 BA.debugLineNum = 483;BA.debugLine="If Rw.IndexOf(\"filename=\")>-1 Then";
Debug.ShouldStop(4);
if (RemoteObject.solveBoolean(">",_rw.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("filename="))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 484;BA.debugLine="Dim Name As String = Rw.SubString(Rw.IndexOf";
Debug.ShouldStop(8);
_name = _rw.runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_rw.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("filename="))),RemoteObject.createImmutable(9)}, "+",1, 1))).runMethod(true,"Replace::",(Object)(BA.CharToString(b4i_main.__c.runMethod(true,"Chr:",(Object)(BA.numberCast(int.class, 34))))),(Object)(RemoteObject.createImmutable(""))).runMethod(true,"Trim");Debug.locals.put("Name", _name);Debug.locals.put("Name", _name);
 BA.debugLineNum = 485;BA.debugLine="If Name<>\"\" Then";
Debug.ShouldStop(16);
if (RemoteObject.solveBoolean("!",_name,BA.ObjectToString(""))) { 
 BA.debugLineNum = 486;BA.debugLine="MultipartFilename.Put(Name,DateTime.Now & \"";
Debug.ShouldStop(32);
__ref.getField(false,"_multipartfilename" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_name)),(Object)((RemoteObject.concat(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now"),RemoteObject.createImmutable(".tmp")))));
 };
 };
 }
}Debug.locals.put("Rw", _rw);
;
 };
 BA.debugLineNum = 493;BA.debugLine="File.WriteBytes(File.DirTemp,MultipartFilename.";
Debug.ShouldStop(4096);
b4i_main.__c.runMethod(false,"File").runVoidMethod ("WriteBytes:::",(Object)(b4i_main.__c.runMethod(false,"File").runMethod(true,"DirTemp")),(Object)(BA.ObjectToString(__ref.getField(false,"_multipartfilename" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_name))))),(Object)(__ref.runClassMethod (b4i_servletrequest.class, "_subarray2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(RemoteObject.solve(new RemoteObject[] {__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length"),RemoteObject.createImmutable(89)}, "-",1, 1)))));
 BA.debugLineNum = 497;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Uploaded";
Debug.ShouldStop(65536);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_UploadedFile"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_UploadedFile"))),(Object)(__ref),(Object)((__ref.getField(false,"_response" /*RemoteObject*/ ))));};
 BA.debugLineNum = 498;BA.debugLine="BBB=ArrayInitialize";
Debug.ShouldStop(131072);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 BA.debugLineNum = 499;BA.debugLine="CallEvent";
Debug.ShouldStop(262144);
__ref.runClassMethod (b4i_servletrequest.class, "_callevent" /*RemoteObject*/ );
 }else {
 BA.debugLineNum = 501;BA.debugLine="If method.ToUpperCase=\"POST\" Then";
Debug.ShouldStop(1048576);
if (RemoteObject.solveBoolean("=",__ref.getField(true,"_method" /*RemoteObject*/ ).runMethod(true,"ToUpperCase"),BA.ObjectToString("POST"))) { 
 BA.debugLineNum = 503;BA.debugLine="Dim rows() As String = Regex.Split(CRLF,BytesT";
Debug.ShouldStop(4194304);
_rows = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(b4i_main.__c.runMethod(true,"CRLF")),(Object)(b4i_main.__c.runMethod(true,"BytesToString::::",(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ )),(Object)(BA.numberCast(int.class, 0)),(Object)(__ref.getField(false,"_bbb" /*RemoteObject*/ ).getField(true,"Length")),(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))));Debug.locals.put("rows", _rows);Debug.locals.put("rows", _rows);
 BA.debugLineNum = 504;BA.debugLine="For Each Rw As String In rows";
Debug.ShouldStop(8388608);
{
final RemoteObject group27 = _rows;
final int groupLen27 = group27.runMethod(true,"Size").<Integer>get()
;int index27 = 0;
;
for (; index27 < groupLen27;index27++){
_rw = group27.runMethod(true,"Get:",index27);Debug.locals.put("Rw", _rw);
Debug.locals.put("Rw", _rw);
 BA.debugLineNum = 505;BA.debugLine="Dim Params() As String = Regex.Split(\"&\",Rw)";
Debug.ShouldStop(16777216);
_params = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(BA.ObjectToString("&")),(Object)(_rw));Debug.locals.put("Params", _params);Debug.locals.put("Params", _params);
 BA.debugLineNum = 506;BA.debugLine="For Each parm As String In Params";
Debug.ShouldStop(33554432);
{
final RemoteObject group29 = _params;
final int groupLen29 = group29.runMethod(true,"Size").<Integer>get()
;int index29 = 0;
;
for (; index29 < groupLen29;index29++){
_parm = group29.runMethod(true,"Get:",index29);Debug.locals.put("parm", _parm);
Debug.locals.put("parm", _parm);
 BA.debugLineNum = 507;BA.debugLine="parm=decode(parm)";
Debug.ShouldStop(67108864);
_parm = __ref.runClassMethod (b4i_servletrequest.class, "_decode:" /*RemoteObject*/ ,(Object)(_parm));Debug.locals.put("parm", _parm);
 BA.debugLineNum = 508;BA.debugLine="If parm.IndexOf(\"=\")>-1 Then";
Debug.ShouldStop(134217728);
if (RemoteObject.solveBoolean(">",_parm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),BA.numberCast(double.class, -(double) (0 + 1)))) { 
 BA.debugLineNum = 509;BA.debugLine="RequestParameter.Put(parm.SubString2(0,parm";
Debug.ShouldStop(268435456);
__ref.getField(false,"_requestparameter" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_parm.runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(_parm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))))))),(Object)((_parm.runMethod(true,"SubString:",(Object)(RemoteObject.solve(new RemoteObject[] {_parm.runMethod(true,"IndexOf:",(Object)(RemoteObject.createImmutable("="))),RemoteObject.createImmutable(1)}, "+",1, 1))))));
 }else {
 BA.debugLineNum = 511;BA.debugLine="RequestPostDataRow.Add(parm)";
Debug.ShouldStop(1073741824);
__ref.getField(false,"_requestpostdatarow" /*RemoteObject*/ ).runVoidMethod ("Add:",(Object)((_parm)));
 };
 }
}Debug.locals.put("parm", _parm);
;
 }
}Debug.locals.put("Rw", _rw);
;
 };
 };
 };
 BA.debugLineNum = 519;BA.debugLine="If Cache.Length>0 Then ExtractHandShake";
Debug.ShouldStop(64);
if (RemoteObject.solveBoolean(">",__ref.getField(false,"_cache" /*RemoteObject*/ ).getField(true,"Length"),BA.numberCast(double.class, 0))) { 
__ref.runClassMethod (b4i_servletrequest.class, "_extracthandshake" /*RemoteObject*/ );};
 BA.debugLineNum = 520;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _findcredential(RemoteObject __ref,RemoteObject _un) throws Exception{
try {
		Debug.PushSubsStack("FindCredential (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,564);
if (RapidSub.canDelegate("findcredential")) { return __ref.runUserSub(false, "servletrequest","findcredential", __ref, _un);}
RemoteObject _s = RemoteObject.createImmutable("");
RemoteObject _cz = null;
Debug.locals.put("Un", _un);
 BA.debugLineNum = 564;BA.debugLine="Private Sub FindCredential(Un As String)";
Debug.ShouldStop(524288);
 BA.debugLineNum = 565;BA.debugLine="UserName  = \"\"";
Debug.ShouldStop(1048576);
__ref.setField ("_username" /*RemoteObject*/ ,BA.ObjectToString(""));
 BA.debugLineNum = 567;BA.debugLine="Password = \"\"";
Debug.ShouldStop(4194304);
__ref.setField ("_password" /*RemoteObject*/ ,BA.ObjectToString(""));
 BA.debugLineNum = 569;BA.debugLine="For Each S As String In CallServer.htdigest";
Debug.ShouldStop(16777216);
{
final RemoteObject group3 = __ref.getField(false,"_callserver" /*RemoteObject*/ ).getField(false,"_htdigest" /*RemoteObject*/ );
final int groupLen3 = group3.runMethod(true,"Size").<Integer>get()
;int index3 = 0;
;
for (; index3 < groupLen3;index3++){
_s = BA.ObjectToString(group3.runMethod(false,"Get:",index3));Debug.locals.put("S", _s);
Debug.locals.put("S", _s);
 BA.debugLineNum = 570;BA.debugLine="If S.StartsWith($\"${Un}:\"$) Then";
Debug.ShouldStop(33554432);
if (_s.runMethod(true,"StartsWith:",(Object)((RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_un))),RemoteObject.createImmutable(":"))))).getBoolean()) { 
 BA.debugLineNum = 571;BA.debugLine="Dim Cz() As String = Regex.Split(\":\",s)";
Debug.ShouldStop(67108864);
_cz = b4i_main.__c.runMethod(false,"Regex").runMethod(false,"Split::",(Object)(BA.ObjectToString(":")),(Object)(_s));Debug.locals.put("Cz", _cz);Debug.locals.put("Cz", _cz);
 BA.debugLineNum = 572;BA.debugLine="If Cz.Length=3 Then";
Debug.ShouldStop(134217728);
if (RemoteObject.solveBoolean("=",_cz.getField(true,"Length"),BA.numberCast(double.class, 3))) { 
 BA.debugLineNum = 573;BA.debugLine="UserName=Cz(0)";
Debug.ShouldStop(268435456);
__ref.setField ("_username" /*RemoteObject*/ ,_cz.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 0)));
 BA.debugLineNum = 574;BA.debugLine="CallServer.realm=Cz(1)";
Debug.ShouldStop(536870912);
__ref.getField(false,"_callserver" /*RemoteObject*/ ).setField ("_realm" /*RemoteObject*/ ,_cz.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 1)));
 BA.debugLineNum = 575;BA.debugLine="Password=Cz(2)";
Debug.ShouldStop(1073741824);
__ref.setField ("_password" /*RemoteObject*/ ,_cz.runMethod(true,"getObjectFast:", BA.numberCast(int.class, 2)));
 };
 };
 }
}Debug.locals.put("S", _s);
;
 BA.debugLineNum = 579;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _finduser(RemoteObject __ref,RemoteObject _naddress,RemoteObject _opaque) throws Exception{
try {
		Debug.PushSubsStack("FindUser (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,551);
if (RapidSub.canDelegate("finduser")) { return __ref.runUserSub(false, "servletrequest","finduser", __ref, _naddress, _opaque);}
RemoteObject _nuser = RemoteObject.declareNull("_tuser");
Debug.locals.put("nAddress", _naddress);
Debug.locals.put("opaque", _opaque);
 BA.debugLineNum = 551;BA.debugLine="Private Sub FindUser(nAddress As String,opaque As";
Debug.ShouldStop(64);
 BA.debugLineNum = 552;BA.debugLine="Dim nUser As tUser";
Debug.ShouldStop(128);
_nuser = RemoteObject.createNew ("_tuser");Debug.locals.put("nUser", _nuser);
 BA.debugLineNum = 554;BA.debugLine="If Users.ContainsKey(opaque) Then";
Debug.ShouldStop(512);
if (__ref.getField(false,"_users" /*RemoteObject*/ ).runMethod(true,"ContainsKey:",(Object)((_opaque))).getBoolean()) { 
 BA.debugLineNum = 555;BA.debugLine="nUser=Users.Get(opaque)";
Debug.ShouldStop(1024);
_nuser = (__ref.getField(false,"_users" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_opaque))));Debug.locals.put("nUser", _nuser);
 }else {
 BA.debugLineNum = 557;BA.debugLine="nUser=NewUser(nAddress)";
Debug.ShouldStop(4096);
_nuser = __ref.runClassMethod (b4i_servletrequest.class, "_newuser:" /*RemoteObject*/ ,(Object)(_naddress));Debug.locals.put("nUser", _nuser);
 BA.debugLineNum = 558;BA.debugLine="Users.Put(nUser.opaque,nUser)";
Debug.ShouldStop(8192);
__ref.getField(false,"_users" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_nuser.getField(true,"opaque" /*RemoteObject*/ ))),(Object)((_nuser)));
 };
 BA.debugLineNum = 561;BA.debugLine="Return nUser";
Debug.ShouldStop(65536);
if (true) return _nuser;
 BA.debugLineNum = 562;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getheader(RemoteObject __ref,RemoteObject _name) throws Exception{
try {
		Debug.PushSubsStack("GetHeader (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,137);
if (RapidSub.canDelegate("getheader")) { return __ref.runUserSub(false, "servletrequest","getheader", __ref, _name);}
Debug.locals.put("Name", _name);
 BA.debugLineNum = 137;BA.debugLine="Public Sub GetHeader(Name As String) As String";
Debug.ShouldStop(256);
 BA.debugLineNum = 138;BA.debugLine="Return RequestHeader.Get(Name)";
Debug.ShouldStop(512);
if (true) return BA.ObjectToString(__ref.getField(false,"_requestheader" /*RemoteObject*/ ).runMethod(false,"Get:",(Object)((_name))));
 BA.debugLineNum = 139;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getheadersname(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetHeadersName (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,147);
if (RapidSub.canDelegate("getheadersname")) { return __ref.runUserSub(false, "servletrequest","getheadersname", __ref);}
RemoteObject _l = RemoteObject.declareNull("B4IList");
RemoteObject _k = RemoteObject.declareNull("NSObject");
 BA.debugLineNum = 147;BA.debugLine="Public Sub GetHeadersName As List";
Debug.ShouldStop(262144);
 BA.debugLineNum = 148;BA.debugLine="Dim l As List";
Debug.ShouldStop(524288);
_l = RemoteObject.createNew ("B4IList");Debug.locals.put("l", _l);
 BA.debugLineNum = 149;BA.debugLine="l.Initialize";
Debug.ShouldStop(1048576);
_l.runVoidMethod ("Initialize");
 BA.debugLineNum = 151;BA.debugLine="For Each K In RequestHeader.keys";
Debug.ShouldStop(4194304);
{
final RemoteObject group3 = __ref.getField(false,"_requestheader" /*RemoteObject*/ ).runMethod(false,"Keys");
final int groupLen3 = group3.runMethod(true,"Size").<Integer>get()
;int index3 = 0;
;
for (; index3 < groupLen3;index3++){
_k = group3.runMethod(false,"Get:",index3);Debug.locals.put("K", _k);
Debug.locals.put("K", _k);
 BA.debugLineNum = 152;BA.debugLine="l.Add(K)";
Debug.ShouldStop(8388608);
_l.runVoidMethod ("Add:",(Object)(_k));
 }
}Debug.locals.put("K", _k);
;
 BA.debugLineNum = 154;BA.debugLine="Return l";
Debug.ShouldStop(33554432);
if (true) return _l;
 BA.debugLineNum = 155;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getinputstream(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetInputStream (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,101);
if (RapidSub.canDelegate("getinputstream")) { return __ref.runUserSub(false, "servletrequest","getinputstream", __ref);}
 BA.debugLineNum = 101;BA.debugLine="Public Sub GetInputStream As InputStream";
Debug.ShouldStop(16);
 BA.debugLineNum = 102;BA.debugLine="Return client.InputStream";
Debug.ShouldStop(32);
if (true) return __ref.getField(false,"_client" /*RemoteObject*/ ).runMethod(false,"InputStream");
 BA.debugLineNum = 103;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getmethod(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetMethod (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,126);
if (RapidSub.canDelegate("getmethod")) { return __ref.runUserSub(false, "servletrequest","getmethod", __ref);}
 BA.debugLineNum = 126;BA.debugLine="Public Sub GetMethod As String";
Debug.ShouldStop(536870912);
 BA.debugLineNum = 127;BA.debugLine="Return method";
Debug.ShouldStop(1073741824);
if (true) return __ref.getField(true,"_method" /*RemoteObject*/ );
 BA.debugLineNum = 128;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getrequesthost(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetRequestHOST (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,134);
if (RapidSub.canDelegate("getrequesthost")) { return __ref.runUserSub(false, "servletrequest","getrequesthost", __ref);}
 BA.debugLineNum = 134;BA.debugLine="Public Sub GetRequestHOST As String";
Debug.ShouldStop(32);
 BA.debugLineNum = 135;BA.debugLine="Return RequestHOST";
Debug.ShouldStop(64);
if (true) return __ref.getField(true,"_requesthost" /*RemoteObject*/ );
 BA.debugLineNum = 136;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getrequesturi(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetRequestURI (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,130);
if (RapidSub.canDelegate("getrequesturi")) { return __ref.runUserSub(false, "servletrequest","getrequesturi", __ref);}
 BA.debugLineNum = 130;BA.debugLine="Public Sub GetRequestURI As String";
Debug.ShouldStop(2);
 BA.debugLineNum = 131;BA.debugLine="Return RequestURI";
Debug.ShouldStop(4);
if (true) return __ref.getField(true,"_requesturi" /*RemoteObject*/ );
 BA.debugLineNum = 132;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getwebsocketcompressdeflateaccept(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetWebSocketCompressDeflateAccept (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,178);
if (RapidSub.canDelegate("getwebsocketcompressdeflateaccept")) { return __ref.runUserSub(false, "servletrequest","getwebsocketcompressdeflateaccept", __ref);}
 BA.debugLineNum = 178;BA.debugLine="Public Sub GetWebSocketCompressDeflateAccept As Bo";
Debug.ShouldStop(131072);
 BA.debugLineNum = 179;BA.debugLine="Return deflate";
Debug.ShouldStop(262144);
if (true) return __ref.getField(true,"_deflate" /*RemoteObject*/ );
 BA.debugLineNum = 180;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getwebsocketcompressgzipaccept(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetWebSocketCompressGzipAccept (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,182);
if (RapidSub.canDelegate("getwebsocketcompressgzipaccept")) { return __ref.runUserSub(false, "servletrequest","getwebsocketcompressgzipaccept", __ref);}
 BA.debugLineNum = 182;BA.debugLine="Public Sub GetWebSocketCompressGzipAccept As Boole";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 183;BA.debugLine="Return gzip";
Debug.ShouldStop(4194304);
if (true) return __ref.getField(true,"_gzip" /*RemoteObject*/ );
 BA.debugLineNum = 184;BA.debugLine="End Sub";
Debug.ShouldStop(8388608);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getwebsocketmapdata(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetWebSocketMapData (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,165);
if (RapidSub.canDelegate("getwebsocketmapdata")) { return __ref.runUserSub(false, "servletrequest","getwebsocketmapdata", __ref);}
RemoteObject _j = RemoteObject.declareNull("B4IJSONParser");
RemoteObject _m = RemoteObject.declareNull("B4IMap");
 BA.debugLineNum = 165;BA.debugLine="Public Sub GetWebSocketMapData As Map";
Debug.ShouldStop(16);
 BA.debugLineNum = 166;BA.debugLine="Dim J As JSONParser";
Debug.ShouldStop(32);
_j = RemoteObject.createNew ("B4IJSONParser");Debug.locals.put("J", _j);
 BA.debugLineNum = 167;BA.debugLine="Dim M As Map";
Debug.ShouldStop(64);
_m = RemoteObject.createNew ("B4IMap");Debug.locals.put("M", _m);
 BA.debugLineNum = 168;BA.debugLine="M.Initialize";
Debug.ShouldStop(128);
_m.runVoidMethod ("Initialize");
 BA.debugLineNum = 169;BA.debugLine="Try";
Debug.ShouldStop(256);
try { BA.debugLineNum = 170;BA.debugLine="j.Initialize(WebSocketString)";
Debug.ShouldStop(512);
_j.runVoidMethod ("Initialize:",(Object)(__ref.getField(true,"_websocketstring" /*RemoteObject*/ )));
 BA.debugLineNum = 171;BA.debugLine="M =J.NextObject";
Debug.ShouldStop(1024);
_m = _j.runMethod(false,"NextObject");Debug.locals.put("M", _m);
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e8) {
			BA.rdebugUtils.runVoidMethod("setLastException:",e8.toString()); BA.debugLineNum = 173;BA.debugLine="Log(LastException)";
Debug.ShouldStop(4096);
b4i_main.__c.runVoidMethod ("LogImpl:::","32359304",BA.ObjectToString(b4i_main.__c.runMethod(false,"LastException")),0);
 };
 BA.debugLineNum = 175;BA.debugLine="Return M";
Debug.ShouldStop(16384);
if (true) return _m;
 BA.debugLineNum = 176;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getwebsocketstringdata(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("GetWebSocketStringData (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,161);
if (RapidSub.canDelegate("getwebsocketstringdata")) { return __ref.runUserSub(false, "servletrequest","getwebsocketstringdata", __ref);}
 BA.debugLineNum = 161;BA.debugLine="Public Sub GetWebSocketStringData As String";
Debug.ShouldStop(1);
 BA.debugLineNum = 162;BA.debugLine="Return WebSocketString";
Debug.ShouldStop(2);
if (true) return __ref.getField(true,"_websocketstring" /*RemoteObject*/ );
 BA.debugLineNum = 163;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _hextodec(RemoteObject __ref,RemoteObject _hex) throws Exception{
try {
		Debug.PushSubsStack("HexToDec (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,522);
if (RapidSub.canDelegate("hextodec")) { return __ref.runUserSub(false, "servletrequest","hextodec", __ref, _hex);}
Debug.locals.put("Hex", _hex);
 BA.debugLineNum = 522;BA.debugLine="Private Sub HexToDec(Hex As String) As Int";
Debug.ShouldStop(512);
 BA.debugLineNum = 523;BA.debugLine="Return bc.IntsFromBytes(bc.HexToBytes(Hex))(0)";
Debug.ShouldStop(1024);
if (true) return __ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"IntsFromBytes:",(Object)(__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"HexToBytes:",(Object)(_hex)))).runMethod(true,"getObjectFastN:", BA.numberCast(int.class, 0));
 BA.debugLineNum = 524;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _inflatedate(RemoteObject __ref,RemoteObject _data) throws Exception{
try {
		Debug.PushSubsStack("InflateDate (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,838);
if (RapidSub.canDelegate("inflatedate")) { return __ref.runUserSub(false, "servletrequest","inflatedate", __ref, _data);}
RemoteObject _nativeme = RemoteObject.declareNull("B4INativeObject");
RemoteObject _infd = RemoteObject.declareNull("NSObject");
Debug.locals.put("data", _data);
 BA.debugLineNum = 838;BA.debugLine="Private Sub InflateDate(data() As Byte) As Byte()";
Debug.ShouldStop(32);
 BA.debugLineNum = 842;BA.debugLine="Dim NativeMe As NativeObject = Me";
Debug.ShouldStop(512);
_nativeme = RemoteObject.createNew ("B4INativeObject");
_nativeme = BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4INativeObject"), __ref);Debug.locals.put("NativeMe", _nativeme);
 BA.debugLineNum = 843;BA.debugLine="Dim infd As Object = NativeMe.RunMethod(\"gzipInf";
Debug.ShouldStop(1024);
_infd = ((_nativeme.runMethod(false,"RunMethod::",(Object)(BA.ObjectToString("gzipInflate:")),(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {_nativeme.runMethod(false,"ArrayToNSData:",(Object)(_data))})))).getObject());Debug.locals.put("infd", _infd);Debug.locals.put("infd", _infd);
 BA.debugLineNum = 844;BA.debugLine="Return NativeMe.ArrayToNSData(infd)";
Debug.ShouldStop(2048);
if (true) return (_nativeme.runMethod(false,"ArrayToNSData:",(Object)((_infd))));
 BA.debugLineNum = 861;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba,RemoteObject _callback,RemoteObject _eventname,RemoteObject _sck) throws Exception{
try {
		Debug.PushSubsStack("Initialize (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,57);
if (RapidSub.canDelegate("initialize")) { return __ref.runUserSub(false, "servletrequest","initialize", __ref, _ba, _callback, _eventname, _sck);}
__ref.runVoidMethodAndSync("initializeClassModule");
RemoteObject _no = RemoteObject.declareNull("B4INativeObject");
Debug.locals.put("ba", _ba);
Debug.locals.put("CallBack", _callback);
Debug.locals.put("EventName", _eventname);
Debug.locals.put("Sck", _sck);
 BA.debugLineNum = 57;BA.debugLine="Public Sub Initialize(CallBack As Object, EventNam";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 58;BA.debugLine="client = Sck";
Debug.ShouldStop(33554432);
__ref.setField ("_client" /*RemoteObject*/ ,_sck);
 BA.debugLineNum = 59;BA.debugLine="mCallBack=CallBack";
Debug.ShouldStop(67108864);
__ref.setField ("_mcallback" /*RemoteObject*/ ,_callback);
 BA.debugLineNum = 60;BA.debugLine="mEventName=EventName";
Debug.ShouldStop(134217728);
__ref.setField ("_meventname" /*RemoteObject*/ ,_eventname);
 BA.debugLineNum = 61;BA.debugLine="CallServer= mCallBack";
Debug.ShouldStop(268435456);
__ref.setField ("_callserver" /*RemoteObject*/ ,(__ref.getField(false,"_mcallback" /*RemoteObject*/ )));
 BA.debugLineNum = 63;BA.debugLine="Users.Initialize";
Debug.ShouldStop(1073741824);
__ref.getField(false,"_users" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 64;BA.debugLine="Cache=ArrayInitialize";
Debug.ShouldStop(-2147483648);
__ref.setField ("_cache" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 BA.debugLineNum = 65;BA.debugLine="BBB=ArrayInitialize";
Debug.ShouldStop(1);
__ref.setField ("_bbb" /*RemoteObject*/ ,__ref.runClassMethod (b4i_servletrequest.class, "_arrayinitialize" /*RemoteObject*/ ));
 BA.debugLineNum = 66;BA.debugLine="MultipartFilename.Initialize";
Debug.ShouldStop(2);
__ref.getField(false,"_multipartfilename" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 69;BA.debugLine="Dim no As NativeObject = Sck";
Debug.ShouldStop(16);
_no = RemoteObject.createNew ("B4INativeObject");
_no = BA.rdebugUtils.runMethod(false, "ConvertToWrapper::", RemoteObject.createNew("B4INativeObject"), _sck);Debug.locals.put("no", _no);
 BA.debugLineNum = 70;BA.debugLine="Address=no.GetField(\"socket\").GetField(\"host\").As";
Debug.ShouldStop(32);
__ref.setField ("_address" /*RemoteObject*/ ,_no.runMethod(false,"GetField:",(Object)(RemoteObject.createImmutable("socket"))).runMethod(false,"GetField:",(Object)(RemoteObject.createImmutable("host"))).runMethod(true,"AsString"));
 BA.debugLineNum = 71;BA.debugLine="ConnPort=no.GetField(\"socket\").GetField(\"port\").A";
Debug.ShouldStop(64);
__ref.setField ("_connport" /*RemoteObject*/ ,_no.runMethod(false,"GetField:",(Object)(RemoteObject.createImmutable("socket"))).runMethod(false,"GetField:",(Object)(RemoteObject.createImmutable("port"))).runMethod(true,"AsString"));
 BA.debugLineNum = 90;BA.debugLine="astream.Initialize(client.InputStream,client.Outp";
Debug.ShouldStop(33554432);
__ref.getField(false,"_astream" /*RemoteObject*/ ).runVoidMethod ("Initialize::::",__ref.getField(false, "bi"),(Object)(((__ref.getField(false,"_client" /*RemoteObject*/ ).runMethod(false,"InputStream")).getObject())),(Object)(((__ref.getField(false,"_client" /*RemoteObject*/ ).runMethod(false,"OutputStream")).getObject())),(Object)(RemoteObject.createImmutable("astream")));
 BA.debugLineNum = 91;BA.debugLine="Response.Initialize(Me,astream,client)";
Debug.ShouldStop(67108864);
__ref.getField(false,"_response" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_initialize::::" /*RemoteObject*/ ,__ref.getField(false, "bi"),(Object)((__ref)),(Object)(__ref.getField(false,"_astream" /*RemoteObject*/ )),(Object)(__ref.getField(false,"_client" /*RemoteObject*/ )));
 BA.debugLineNum = 93;BA.debugLine="RequestHeader.Initialize";
Debug.ShouldStop(268435456);
__ref.getField(false,"_requestheader" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 94;BA.debugLine="RequestParameter.Initialize";
Debug.ShouldStop(536870912);
__ref.getField(false,"_requestparameter" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 95;BA.debugLine="RequestCookies.Initialize";
Debug.ShouldStop(1073741824);
__ref.getField(false,"_requestcookies" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 96;BA.debugLine="RequestPostDataRow.Initialize";
Debug.ShouldStop(-2147483648);
__ref.getField(false,"_requestpostdatarow" /*RemoteObject*/ ).runVoidMethod ("Initialize");
 BA.debugLineNum = 98;BA.debugLine="BrowserTimeOut";
Debug.ShouldStop(2);
__ref.runClassMethod (b4i_servletrequest.class, "_browsertimeout" /*void*/ );
 BA.debugLineNum = 99;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _lgc(RemoteObject __ref,RemoteObject _message,RemoteObject _color) throws Exception{
try {
		Debug.PushSubsStack("LgC (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,863);
if (RapidSub.canDelegate("lgc")) { return __ref.runUserSub(false, "servletrequest","lgc", __ref, _message, _color);}
Debug.locals.put("Message", _message);
Debug.locals.put("Color", _color);
 BA.debugLineNum = 863;BA.debugLine="Private Sub LgC(Message As String, Color As Int) '";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 867;BA.debugLine="If LogActive Then LogColor(Message,Color)";
Debug.ShouldStop(4);
if (__ref.getField(true,"_logactive" /*RemoteObject*/ ).getBoolean()) { 
b4i_main.__c.runVoidMethod ("LogImpl:::","34128772",_message,_color);};
 BA.debugLineNum = 869;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _lgp(RemoteObject __ref,RemoteObject _message) throws Exception{
try {
		Debug.PushSubsStack("LgP (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,871);
if (RapidSub.canDelegate("lgp")) { return __ref.runUserSub(false, "servletrequest","lgp", __ref, _message);}
Debug.locals.put("Message", _message);
 BA.debugLineNum = 871;BA.debugLine="Private Sub LgP(Message As String)";
Debug.ShouldStop(64);
 BA.debugLineNum = 872;BA.debugLine="If LogPrivate Then Log(Message)";
Debug.ShouldStop(128);
if (__ref.getField(true,"_logprivate" /*RemoteObject*/ ).getBoolean()) { 
b4i_main.__c.runVoidMethod ("LogImpl:::","34194305",_message,0);};
 BA.debugLineNum = 873;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _md5(RemoteObject __ref,RemoteObject _stringtoconvert) throws Exception{
try {
		Debug.PushSubsStack("MD5 (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,529);
if (RapidSub.canDelegate("md5")) { return __ref.runUserSub(false, "servletrequest","md5", __ref, _stringtoconvert);}
RemoteObject _md = RemoteObject.declareNull("B4IMessageDigest");
RemoteObject _data = null;
Debug.locals.put("stringToConvert", _stringtoconvert);
 BA.debugLineNum = 529;BA.debugLine="Private Sub MD5(stringToConvert As String) As Stri";
Debug.ShouldStop(65536);
 BA.debugLineNum = 530;BA.debugLine="Dim MD As MessageDigest";
Debug.ShouldStop(131072);
_md = RemoteObject.createNew ("B4IMessageDigest");Debug.locals.put("MD", _md);
 BA.debugLineNum = 531;BA.debugLine="Dim Data() As Byte =  MD.GetMessageDigest(stringT";
Debug.ShouldStop(262144);
_data = _md.runMethod(false,"GetMessageDigest::",(Object)(_stringtoconvert.runMethod(false,"GetBytes:",(Object)(RemoteObject.createImmutable("UTF8")))),(Object)(RemoteObject.createImmutable("MD5")));Debug.locals.put("Data", _data);Debug.locals.put("Data", _data);
 BA.debugLineNum = 532;BA.debugLine="Return bc.HexFromBytes(Data).ToLowerCase";
Debug.ShouldStop(524288);
if (true) return __ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(true,"HexFromBytes:",(Object)(_data)).runMethod(true,"ToLowerCase");
 BA.debugLineNum = 533;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _newuser(RemoteObject __ref,RemoteObject _naddress) throws Exception{
try {
		Debug.PushSubsStack("NewUser (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,535);
if (RapidSub.canDelegate("newuser")) { return __ref.runUserSub(false, "servletrequest","newuser", __ref, _naddress);}
RemoteObject _nuser = RemoteObject.declareNull("_tuser");
Debug.locals.put("nAddress", _naddress);
 BA.debugLineNum = 535;BA.debugLine="Private Sub NewUser(nAddress As String) As tUser";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 536;BA.debugLine="Dim nUser As tUser";
Debug.ShouldStop(8388608);
_nuser = RemoteObject.createNew ("_tuser");Debug.locals.put("nUser", _nuser);
 BA.debugLineNum = 538;BA.debugLine="nUser.Initialize";
Debug.ShouldStop(33554432);
_nuser.runVoidMethod ("Initialize");
 BA.debugLineNum = 539;BA.debugLine="nUser.Address=nAddress";
Debug.ShouldStop(67108864);
_nuser.setField ("Address" /*RemoteObject*/ ,_naddress);
 BA.debugLineNum = 540;BA.debugLine="nUser.LastRequest=DateTime.Now";
Debug.ShouldStop(134217728);
_nuser.setField ("LastRequest" /*RemoteObject*/ ,b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now"));
 BA.debugLineNum = 541;BA.debugLine="nUser.opaque=nAddress.SubString2(0,3) & bc.HexFro";
Debug.ShouldStop(268435456);
_nuser.setField ("opaque" /*RemoteObject*/ ,RemoteObject.concat(_naddress.runMethod(true,"SubString2::",(Object)(BA.numberCast(int.class, 0)),(Object)(BA.numberCast(int.class, 3))),__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(true,"HexFromBytes:",(Object)(__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"LongsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")})))))));
 BA.debugLineNum = 542;BA.debugLine="nUser.nonce=bc.HexFromBytes(bc.LongsToBytes(Array";
Debug.ShouldStop(536870912);
_nuser.setField ("nonce" /*RemoteObject*/ ,__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(true,"HexFromBytes:",(Object)(__ref.getField(false,"_bc" /*RemoteObject*/ ).runMethod(false,"LongsToBytes:",(Object)(RemoteObject.createNew("B4IArray").runMethod(false, "initObjectsWithData:", (Object)new RemoteObject[] {b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")}))))));
 BA.debugLineNum = 543;BA.debugLine="nUser.nc=1";
Debug.ShouldStop(1073741824);
_nuser.setField ("nc" /*RemoteObject*/ ,BA.numberCast(int.class, 1));
 BA.debugLineNum = 544;BA.debugLine="nUser.realm=CallServer.realm";
Debug.ShouldStop(-2147483648);
_nuser.setField ("realm" /*RemoteObject*/ ,__ref.getField(false,"_callserver" /*RemoteObject*/ ).getField(true,"_realm" /*RemoteObject*/ ));
 BA.debugLineNum = 546;BA.debugLine="Users.Put(nUser.opaque,nUser)";
Debug.ShouldStop(2);
__ref.getField(false,"_users" /*RemoteObject*/ ).runVoidMethod ("Put::",(Object)((_nuser.getField(true,"opaque" /*RemoteObject*/ ))),(Object)((_nuser)));
 BA.debugLineNum = 548;BA.debugLine="Return nUser";
Debug.ShouldStop(8);
if (true) return _nuser;
 BA.debugLineNum = 549;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _parametermap(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("ParameterMap (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,157);
if (RapidSub.canDelegate("parametermap")) { return __ref.runUserSub(false, "servletrequest","parametermap", __ref);}
 BA.debugLineNum = 157;BA.debugLine="public Sub ParameterMap As Map";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 158;BA.debugLine="Return RequestParameter";
Debug.ShouldStop(536870912);
if (true) return __ref.getField(false,"_requestparameter" /*RemoteObject*/ );
 BA.debugLineNum = 159;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _remoteaddress(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("RemoteAddress (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,118);
if (RapidSub.canDelegate("remoteaddress")) { return __ref.runUserSub(false, "servletrequest","remoteaddress", __ref);}
 BA.debugLineNum = 118;BA.debugLine="Public Sub RemoteAddress As String";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 119;BA.debugLine="Return Address";
Debug.ShouldStop(4194304);
if (true) return __ref.getField(true,"_address" /*RemoteObject*/ );
 BA.debugLineNum = 120;BA.debugLine="End Sub";
Debug.ShouldStop(8388608);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _remoteport(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("RemotePort (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,122);
if (RapidSub.canDelegate("remoteport")) { return __ref.runUserSub(false, "servletrequest","remoteport", __ref);}
 BA.debugLineNum = 122;BA.debugLine="Public Sub RemotePort As Int";
Debug.ShouldStop(33554432);
 BA.debugLineNum = 123;BA.debugLine="Return ConnPort";
Debug.ShouldStop(67108864);
if (true) return BA.numberCast(int.class, __ref.getField(true,"_connport" /*RemoteObject*/ ));
 BA.debugLineNum = 124;BA.debugLine="End Sub";
Debug.ShouldStop(134217728);
return RemoteObject.createImmutable(0);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendacceptkeyws(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("SendAcceptKeyWs (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,697);
if (RapidSub.canDelegate("sendacceptkeyws")) { return __ref.runUserSub(false, "servletrequest","sendacceptkeyws", __ref);}
RemoteObject _acpdef = RemoteObject.createImmutable("");
RemoteObject _sw = RemoteObject.createImmutable("");
 BA.debugLineNum = 697;BA.debugLine="Private Sub SendAcceptKeyWs";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 698;BA.debugLine="Dim AcpDef As String = \"\" '\"Sec-WebSocket-Extensi";
Debug.ShouldStop(33554432);
_acpdef = BA.ObjectToString("");Debug.locals.put("AcpDef", _acpdef);Debug.locals.put("AcpDef", _acpdef);
 BA.debugLineNum = 699;BA.debugLine="Dim Sw As String = $\"HTTP/1.1 101 Switching Proto";
Debug.ShouldStop(67108864);
_sw = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.1 101 Switching Protocols\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletrequest.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Connection: Upgrade\n"),RemoteObject.createImmutable("Upgrade: WebSocket\n"),RemoteObject.createImmutable("Sec-WebSocket-Accept: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletrequest.class, "_websocketkey:" /*RemoteObject*/ ,(Object)(__ref.getField(true,"_keywebsocket" /*RemoteObject*/ )))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("Server: HttpServer (B4X)\n"),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_acpdef))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("")));Debug.locals.put("Sw", _sw);Debug.locals.put("Sw", _sw);
 BA.debugLineNum = 711;BA.debugLine="Response.Write(Sw)";
Debug.ShouldStop(64);
__ref.getField(false,"_response" /*RemoteObject*/ ).runClassMethod (b4i_servletresponse.class, "_write:" /*RemoteObject*/ ,(Object)(_sw));
 BA.debugLineNum = 713;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_SwitchToWe";
Debug.ShouldStop(256);
if (__ref.runClassMethod (b4i_servletrequest.class, "_subexists2:::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_SwitchToWebSocket"))),(Object)(BA.numberCast(int.class, 2))).getBoolean()) { 
b4i_main.__c.runMethodAndSync(false,"CallSub3:::::",__ref.getField(false, "bi"),(Object)(__ref.getField(false,"_mcallback" /*RemoteObject*/ )),(Object)(RemoteObject.concat(__ref.getField(true,"_meventname" /*RemoteObject*/ ),RemoteObject.createImmutable("_SwitchToWebSocket"))),(Object)(__ref),(Object)((__ref.getField(false,"_response" /*RemoteObject*/ ))));};
 BA.debugLineNum = 714;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sendrefuse(RemoteObject __ref,RemoteObject _sresponse,RemoteObject _user) throws Exception{
try {
		Debug.PushSubsStack("SendRefuse (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,581);
if (RapidSub.canDelegate("sendrefuse")) { return __ref.runUserSub(false, "servletrequest","sendrefuse", __ref, _sresponse, _user);}
RemoteObject _body = RemoteObject.createImmutable("");
RemoteObject _handshake = RemoteObject.createImmutable("");
Debug.locals.put("sResponse", _sresponse);
Debug.locals.put("User", _user);
 BA.debugLineNum = 581;BA.debugLine="private Sub SendRefuse (sResponse As ServletRespon";
Debug.ShouldStop(16);
 BA.debugLineNum = 592;BA.debugLine="Dim Body As String = $\"<!DOCTYPE html> <html>   <";
Debug.ShouldStop(32768);
_body = (RemoteObject.concat(RemoteObject.createImmutable("<!DOCTYPE html>\n"),RemoteObject.createImmutable("<html>\n"),RemoteObject.createImmutable("  <head>\n"),RemoteObject.createImmutable("    <meta charset=\"UTF-8\" />\n"),RemoteObject.createImmutable("    <title>Error</title>\n"),RemoteObject.createImmutable("  </head>\n"),RemoteObject.createImmutable("  <body>\n"),RemoteObject.createImmutable("    <h1>401 Unauthorized.</h1>\n"),RemoteObject.createImmutable("  </body>\n"),RemoteObject.createImmutable("</html>")));Debug.locals.put("Body", _body);Debug.locals.put("Body", _body);
 BA.debugLineNum = 602;BA.debugLine="Dim HandShake As String = $\"HTTP/1.0 401 Unauthor";
Debug.ShouldStop(33554432);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable("HTTP/1.0 401 Unauthorized\n"),RemoteObject.createImmutable("Server: HttpServer (B4X)\n"),RemoteObject.createImmutable("Date: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((__ref.runClassMethod (b4i_servletrequest.class, "_completedate:" /*RemoteObject*/ ,(Object)(b4i_main.__c.runMethod(false,"DateTime").runMethod(true,"Now")))))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 606;BA.debugLine="HandShake=$\"${HandShake}WWW-Authenticate: Digest";
Debug.ShouldStop(536870912);
_handshake = (RemoteObject.concat(RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_handshake))),RemoteObject.createImmutable("WWW-Authenticate: Digest realm=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_user.getField(true,"realm" /*RemoteObject*/ )))),RemoteObject.createImmutable("\", qop=\"auth,auth-int\", nonce=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_user.getField(true,"nonce" /*RemoteObject*/ )))),RemoteObject.createImmutable("\", opaque=\""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_user.getField(true,"opaque" /*RemoteObject*/ )))),RemoteObject.createImmutable("\"\n"),RemoteObject.createImmutable("Content-Type: text/html\n"),RemoteObject.createImmutable("Content-Length: "),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_body.runMethod(true,"Length")))),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable("\n"),RemoteObject.createImmutable(""),b4i_main.__c.runMethod(true,"SmartStringFormatter::",(Object)(BA.ObjectToString("")),(Object)((_body))),RemoteObject.createImmutable("")));Debug.locals.put("HandShake", _handshake);
 BA.debugLineNum = 611;BA.debugLine="sResponse.Write(HandShake)";
Debug.ShouldStop(4);
_sresponse.runClassMethod (b4i_servletresponse.class, "_write:" /*RemoteObject*/ ,(Object)(_handshake));
 BA.debugLineNum = 612;BA.debugLine="sResponse.Close";
Debug.ShouldStop(8);
_sresponse.runClassMethod (b4i_servletresponse.class, "_close" /*RemoteObject*/ );
 BA.debugLineNum = 613;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _subarray(RemoteObject __ref,RemoteObject _data,RemoteObject _pos) throws Exception{
try {
		Debug.PushSubsStack("SubArray (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,944);
if (RapidSub.canDelegate("subarray")) { return __ref.runUserSub(false, "servletrequest","subarray", __ref, _data, _pos);}
RemoteObject _fn = null;
Debug.locals.put("Data", _data);
Debug.locals.put("Pos", _pos);
 BA.debugLineNum = 944;BA.debugLine="Private Sub SubArray(Data() As Byte, Pos As Int) A";
Debug.ShouldStop(32768);
 BA.debugLineNum = 945;BA.debugLine="Dim Fn(Data.Length-Pos) As Byte";
Debug.ShouldStop(65536);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),_pos}, "-",1, 1)});Debug.locals.put("Fn", _fn);
 BA.debugLineNum = 947;BA.debugLine="Bit.ArrayCopy(Data,Pos,Fn,0,Data.Length-Pos)";
Debug.ShouldStop(262144);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data),(Object)(_pos),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(RemoteObject.solve(new RemoteObject[] {_data.getField(true,"Length"),_pos}, "-",1, 1)));
 BA.debugLineNum = 948;BA.debugLine="Return Fn";
Debug.ShouldStop(524288);
if (true) return _fn;
 BA.debugLineNum = 949;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _subarray2(RemoteObject __ref,RemoteObject _data,RemoteObject _start,RemoteObject _last) throws Exception{
try {
		Debug.PushSubsStack("SubArray2 (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,951);
if (RapidSub.canDelegate("subarray2")) { return __ref.runUserSub(false, "servletrequest","subarray2", __ref, _data, _start, _last);}
RemoteObject _fn = null;
Debug.locals.put("Data", _data);
Debug.locals.put("Start", _start);
Debug.locals.put("Last", _last);
 BA.debugLineNum = 951;BA.debugLine="Public Sub SubArray2(Data() As Byte,Start As Int,";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 952;BA.debugLine="Dim fn(Last - Start) As Byte";
Debug.ShouldStop(8388608);
_fn = RemoteObject.createNew("B4IArray").runMethod(false, "initBytes:", (Object)new RemoteObject[] {RemoteObject.solve(new RemoteObject[] {_last,_start}, "-",1, 1)});Debug.locals.put("fn", _fn);
 BA.debugLineNum = 953;BA.debugLine="Bit.ArrayCopy(Data, Start, fn, 0, fn.Length)";
Debug.ShouldStop(16777216);
b4i_main.__c.runMethod(false,"Bit").runVoidMethod ("ArrayCopy:::::",(Object)(_data),(Object)(_start),(Object)(_fn),(Object)(BA.numberCast(int.class, 0)),(Object)(_fn.getField(true,"Length")));
 BA.debugLineNum = 954;BA.debugLine="Return fn";
Debug.ShouldStop(33554432);
if (true) return _fn;
 BA.debugLineNum = 955;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _subexists2(RemoteObject __ref,RemoteObject _callobject,RemoteObject _eventname,RemoteObject _param) throws Exception{
try {
		Debug.PushSubsStack("SubExists2 (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,877);
if (RapidSub.canDelegate("subexists2")) { return __ref.runUserSub(false, "servletrequest","subexists2", __ref, _callobject, _eventname, _param);}
Debug.locals.put("CallObject", _callobject);
Debug.locals.put("EventName", _eventname);
Debug.locals.put("Param", _param);
 BA.debugLineNum = 877;BA.debugLine="Private Sub SubExists2(CallObject As Object, Event";
Debug.ShouldStop(4096);
 BA.debugLineNum = 879;BA.debugLine="Return SubExists(CallObject,EventName, param)";
Debug.ShouldStop(16384);
if (true) return b4i_main.__c.runMethod(true,"SubExists:::",(Object)(_callobject),(Object)(_eventname),(Object)(_param));
 BA.debugLineNum = 883;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _websocketkey(RemoteObject __ref,RemoteObject _key) throws Exception{
try {
		Debug.PushSubsStack("webSocketKey (servletrequest) ","servletrequest",2,__ref.getField(false, "bi"),__ref,686);
if (RapidSub.canDelegate("websocketkey")) { return __ref.runUserSub(false, "servletrequest","websocketkey", __ref, _key);}
RemoteObject _sha = RemoteObject.declareNull("B4IMessageDigest");
RemoteObject _data = null;
Debug.locals.put("Key", _key);
 BA.debugLineNum = 686;BA.debugLine="Private Sub webSocketKey(Key As String) As String";
Debug.ShouldStop(8192);
 BA.debugLineNum = 687;BA.debugLine="Dim sha As MessageDigest";
Debug.ShouldStop(16384);
_sha = RemoteObject.createNew ("B4IMessageDigest");Debug.locals.put("sha", _sha);
 BA.debugLineNum = 688;BA.debugLine="Log(\"HandShake WebSocket ID: \" & ID)";
Debug.ShouldStop(32768);
b4i_main.__c.runVoidMethod ("LogImpl:::","33801090",RemoteObject.concat(RemoteObject.createImmutable("HandShake WebSocket ID: "),__ref.getField(true,"_id" /*RemoteObject*/ )),0);
 BA.debugLineNum = 689;BA.debugLine="LgC(Key,0xFF000F00)";
Debug.ShouldStop(65536);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(_key),(Object)(BA.numberCast(int.class, 0xff000f00)));
 BA.debugLineNum = 691;BA.debugLine="Key=Key & \"258EAFA5-E914-47DA-95CA-C5AB0DC85B11\"";
Debug.ShouldStop(262144);
_key = RemoteObject.concat(_key,RemoteObject.createImmutable("258EAFA5-E914-47DA-95CA-C5AB0DC85B11"));Debug.locals.put("Key", _key);
 BA.debugLineNum = 692;BA.debugLine="Dim data() As Byte = sha.GetMessageDigest(Key.Get";
Debug.ShouldStop(524288);
_data = _sha.runMethod(false,"GetMessageDigest::",(Object)(_key.runMethod(false,"GetBytes:",(Object)(__ref.getField(true,"_characterencoding" /*RemoteObject*/ )))),(Object)(RemoteObject.createImmutable("SHA-1")));Debug.locals.put("data", _data);Debug.locals.put("data", _data);
 BA.debugLineNum = 693;BA.debugLine="LgC(su.EncodeBase64(data),0xFF000F00)";
Debug.ShouldStop(1048576);
__ref.runClassMethod (b4i_servletrequest.class, "_lgc::" /*RemoteObject*/ ,(Object)(__ref.getField(false,"_su" /*RemoteObject*/ ).runMethod(true,"EncodeBase64:",(Object)(_data))),(Object)(BA.numberCast(int.class, 0xff000f00)));
 BA.debugLineNum = 694;BA.debugLine="Return su.EncodeBase64(data)";
Debug.ShouldStop(2097152);
if (true) return __ref.getField(false,"_su" /*RemoteObject*/ ).runMethod(true,"EncodeBase64:",(Object)(_data));
 BA.debugLineNum = 695;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}