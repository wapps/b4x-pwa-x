
package net.wapps.httpserver;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class b4i_servletrequest {
    public static RemoteObject myClass;
	public b4i_servletrequest() {
	}
    public static PCBA staticBA = new PCBA(null, b4i_servletrequest.class);

public static RemoteObject __c = RemoteObject.declareNull("B4ICommon");
public static RemoteObject _dday = null;
public static RemoteObject _mmmonth = null;
public static RemoteObject _client = RemoteObject.declareNull("B4ISocketWrapper");
public static RemoteObject _astream = RemoteObject.declareNull("B4IAsyncStreams");
public static RemoteObject _mcallback = RemoteObject.declareNull("NSObject");
public static RemoteObject _meventname = RemoteObject.createImmutable("");
public static RemoteObject _bc = RemoteObject.declareNull("B4IByteConverter");
public static RemoteObject _method = RemoteObject.createImmutable("");
public static RemoteObject _requesturi = RemoteObject.createImmutable("");
public static RemoteObject _requesthost = RemoteObject.createImmutable("");
public static RemoteObject _address = RemoteObject.createImmutable("");
public static RemoteObject _connport = RemoteObject.createImmutable("");
public static RemoteObject _id = RemoteObject.createImmutable("");
public static RemoteObject _requestheader = RemoteObject.declareNull("B4IMap");
public static RemoteObject _requestparameter = RemoteObject.declareNull("B4IMap");
public static RemoteObject _requestcookies = RemoteObject.declareNull("B4IMap");
public static RemoteObject _requestpostdatarow = RemoteObject.declareNull("B4IList");
public static RemoteObject _connectionalive = RemoteObject.createImmutable(false);
public static RemoteObject _characterencoding = RemoteObject.createImmutable("");
public static RemoteObject _callserver = RemoteObject.declareNull("b4i_httpserver");
public static RemoteObject _response = RemoteObject.declareNull("b4i_servletresponse");
public static RemoteObject _users = RemoteObject.declareNull("B4IMap");
public static RemoteObject _username = RemoteObject.createImmutable("");
public static RemoteObject _password = RemoteObject.createImmutable("");
public static RemoteObject _otherdata = RemoteObject.createImmutable(false);
public static RemoteObject _cache = null;
public static RemoteObject _bbb = null;
public static RemoteObject _su = RemoteObject.declareNull("iStringUtils");
public static RemoteObject _logprivate = RemoteObject.createImmutable(false);
public static RemoteObject _logactive = RemoteObject.createImmutable(false);
public static RemoteObject _contenttype = RemoteObject.createImmutable("");
public static RemoteObject _contentlength = RemoteObject.createImmutable(0L);
public static RemoteObject _multipartfilename = RemoteObject.declareNull("B4IMap");
public static RemoteObject _logfirstrefuse = RemoteObject.createImmutable(false);
public static RemoteObject _timeout = RemoteObject.createImmutable(0L);
public static RemoteObject _lastbrowsercomunicate = RemoteObject.createImmutable(0L);
public static RemoteObject _gzip = RemoteObject.createImmutable(false);
public static RemoteObject _deflate = RemoteObject.createImmutable(false);
public static RemoteObject _websocket = RemoteObject.createImmutable(false);
public static RemoteObject _keywebsocket = RemoteObject.createImmutable("");
public static RemoteObject _websocketstring = RemoteObject.createImmutable("");
public static RemoteObject _lastopcode = RemoteObject.createImmutable(0);
public static b4i_main _main = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"Address",_ref.getField(false, "_address"),"astream",_ref.getField(false, "_astream"),"BBB",_ref.getField(false, "_bbb"),"bc",_ref.getField(false, "_bc"),"Cache",_ref.getField(false, "_cache"),"CallServer",_ref.getField(false, "_callserver"),"CharacterEncoding",_ref.getField(false, "_characterencoding"),"client",_ref.getField(false, "_client"),"ConnectionAlive",_ref.getField(false, "_connectionalive"),"ConnPort",_ref.getField(false, "_connport"),"ContentLength",_ref.getField(false, "_contentlength"),"ContentType",_ref.getField(false, "_contenttype"),"DDay",_ref.getField(false, "_dday"),"deflate",_ref.getField(false, "_deflate"),"gzip",_ref.getField(false, "_gzip"),"ID",_ref.getField(false, "_id"),"keyWebSocket",_ref.getField(false, "_keywebsocket"),"LastBrowserComunicate",_ref.getField(false, "_lastbrowsercomunicate"),"LastOpCode",_ref.getField(false, "_lastopcode"),"LogActive",_ref.getField(false, "_logactive"),"LogFirstRefuse",_ref.getField(false, "_logfirstrefuse"),"LogPrivate",_ref.getField(false, "_logprivate"),"mCallBack",_ref.getField(false, "_mcallback"),"method",_ref.getField(false, "_method"),"mEventName",_ref.getField(false, "_meventname"),"MMmonth",_ref.getField(false, "_mmmonth"),"MultipartFilename",_ref.getField(false, "_multipartfilename"),"OtherData",_ref.getField(false, "_otherdata"),"Password",_ref.getField(false, "_password"),"RequestCookies",_ref.getField(false, "_requestcookies"),"RequestHeader",_ref.getField(false, "_requestheader"),"RequestHOST",_ref.getField(false, "_requesthost"),"RequestParameter",_ref.getField(false, "_requestparameter"),"RequestPostDataRow",_ref.getField(false, "_requestpostdatarow"),"RequestURI",_ref.getField(false, "_requesturi"),"Response",_ref.getField(false, "_response"),"su",_ref.getField(false, "_su"),"Timeout",_ref.getField(false, "_timeout"),"UserName",_ref.getField(false, "_username"),"Users",_ref.getField(false, "_users"),"WebSocket",_ref.getField(false, "_websocket"),"WebSocketString",_ref.getField(false, "_websocketstring")};
}
}