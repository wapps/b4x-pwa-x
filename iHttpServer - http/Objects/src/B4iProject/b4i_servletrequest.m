
#import "b4i_servletrequest.h"
#import "b4i_main.h"
#import "b4i_httpserver.h"
#import "b4i_servletresponse.h"
#import "b4i_queryelement.h"

@interface ResumableSub_servletrequest_BrowserTimeOut :B4IResumableSub 
- (instancetype) init: (b4i_servletrequest*) parent1 :(b4i_servletrequest*) __ref1;
@end
@implementation ResumableSub_servletrequest_BrowserTimeOut {
b4i_servletrequest* parent;
b4i_servletrequest* __ref;
NSString* _body;
NSString* _handshake;
}
- (instancetype) init: (b4i_servletrequest*) parent1 :(b4i_servletrequest*) __ref1 {
self->__ref = __ref1;
self->parent = parent1;
self->__ref = parent;
return self;
}
b4i_servletrequest* __ref;
- (void) resume:(B4I*)bi1 :(NSArray*)result {
self.bi = bi1;
B4IRDebugUtils.currentModule=@"servletrequest";

    while (true) {
        switch (self->_state) {
            case -1:
return;

case 0:
//C
self->_state = 1;
B4IRDebugUtils.currentLine=3538945;
 //BA.debugLineNum = 3538945;BA.debugLine="Sleep(Timeout)";
[parent->___c Sleep:self.bi :[[B4IDelegatableResumableSub alloc]init:self :@"servletrequest" :@"browsertimeout"] :(int) (self->__ref->__timeout /*long long*/ )];
self->_state = 5;
return;
case 5:
//C
self->_state = 1;
;
B4IRDebugUtils.currentLine=3538946;
 //BA.debugLineNum = 3538946;BA.debugLine="If LastBrowserComunicate+Timeout=0 Then";
if (true) break;

case 1:
//if
self->_state = 4;
if (self->__ref->__lastbrowsercomunicate /*long long*/ +self->__ref->__timeout /*long long*/ ==0) { 
self->_state = 3;
}if (true) break;

case 3:
//C
self->_state = 4;
B4IRDebugUtils.currentLine=3538947;
 //BA.debugLineNum = 3538947;BA.debugLine="Log($\"TimeOut: ${ID}\"$)";
[parent->___c LogImpl:@"33538947" :([@[@"TimeOut: ",[parent->___c SmartStringFormatter:@"" :(NSObject*)(self->__ref->__id /*NSString**/ )],@""] componentsJoinedByString:@""]) :0];
B4IRDebugUtils.currentLine=3538948;
 //BA.debugLineNum = 3538948;BA.debugLine="Dim Body As String = $\"<!DOCTYPE html> <html>";
self->_body = ([@[@"<!DOCTYPE html>\n",@"<html>\n",@"  <head>\n",@"    <meta charset=\"UTF-8\" />\n",@"    <title>Error</title>\n",@"  </head>\n",@"  <body>\n",@"    <h1>401 Unauthorized.</h1>\n",@"  </body>\n",@"</html>"] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3538958;
 //BA.debugLineNum = 3538958;BA.debugLine="Dim HandShake As String = $\"HTTP/1.0 408 Request";
self->_handshake = ([@[@"HTTP/1.0 408 Request Time-out\n",@"Date: ",[parent->___c SmartStringFormatter:@"" :(NSObject*)([self->__ref _completedate /*NSString**/ :nil :[[parent->___c DateTime] Now]])],@"\n",@"Content-Type: text/html\n",@"Content-Length: ",[parent->___c SmartStringFormatter:@"" :(NSObject*)(@([self->_body Length]))],@"\n",@"\n",@"",[parent->___c SmartStringFormatter:@"" :(NSObject*)(self->_body)],@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3538964;
 //BA.debugLineNum = 3538964;BA.debugLine="Response.Write(HandShake)";
[self->__ref->__response /*b4i_servletresponse**/  _write /*NSString**/ :nil :self->_handshake];
B4IRDebugUtils.currentLine=3538965;
 //BA.debugLineNum = 3538965;BA.debugLine="Close";
[self->__ref _close /*NSString**/ :nil];
 if (true) break;

case 4:
//C
self->_state = -1;
;
B4IRDebugUtils.currentLine=3538967;
 //BA.debugLineNum = 3538967;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
@end
@implementation _tuser
-(void)Initialize{
self.IsInitialized = true;
self->_Address = @"";
self->_nc = 0;
self->_nonce = @"";
self->_opaque = @"";
self->_LastRequest = 0L;
self->_realm = @"";
}
- (NSString*)description {
return [B4I TypeToString:self :false];}
@end

@implementation b4i_servletrequest 


+ (B4I*)createBI {
    return [B4IShellBI alloc];
}

- (void)dealloc {
    if (self.bi != nil)
        NSLog(@"Class (b4i_servletrequest) instance released.");
}

- (NSString*)  _remoteaddress:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"remoteaddress"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"remoteaddress" :nil]);}
B4IRDebugUtils.currentLine=1769472;
 //BA.debugLineNum = 1769472;BA.debugLine="Public Sub RemoteAddress As String";
B4IRDebugUtils.currentLine=1769473;
 //BA.debugLineNum = 1769473;BA.debugLine="Return Address";
if (true) return __ref->__address /*NSString**/ ;
B4IRDebugUtils.currentLine=1769474;
 //BA.debugLineNum = 1769474;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _getrequesturi:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getrequesturi"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getrequesturi" :nil]);}
B4IRDebugUtils.currentLine=1966080;
 //BA.debugLineNum = 1966080;BA.debugLine="Public Sub GetRequestURI As String";
B4IRDebugUtils.currentLine=1966081;
 //BA.debugLineNum = 1966081;BA.debugLine="Return RequestURI";
if (true) return __ref->__requesturi /*NSString**/ ;
B4IRDebugUtils.currentLine=1966082;
 //BA.debugLineNum = 1966082;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _getmethod:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getmethod"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getmethod" :nil]);}
B4IRDebugUtils.currentLine=1900544;
 //BA.debugLineNum = 1900544;BA.debugLine="Public Sub GetMethod As String";
B4IRDebugUtils.currentLine=1900545;
 //BA.debugLineNum = 1900545;BA.debugLine="Return method";
if (true) return __ref->__method /*NSString**/ ;
B4IRDebugUtils.currentLine=1900546;
 //BA.debugLineNum = 1900546;BA.debugLine="End Sub";
return @"";
}
- (B4IMap*)  _parametermap:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"parametermap"])
	 {return ((B4IMap*) [B4IDebug delegate:self.bi :@"parametermap" :nil]);}
B4IRDebugUtils.currentLine=2228224;
 //BA.debugLineNum = 2228224;BA.debugLine="public Sub ParameterMap As Map";
B4IRDebugUtils.currentLine=2228225;
 //BA.debugLineNum = 2228225;BA.debugLine="Return RequestParameter";
if (true) return __ref->__requestparameter /*B4IMap**/ ;
B4IRDebugUtils.currentLine=2228226;
 //BA.debugLineNum = 2228226;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _initialize:(b4i_servletrequest*) __ref :(B4I*) _ba :(NSObject*) _callback :(NSString*) _eventname :(B4ISocketWrapper*) _sck{
__ref = self;
[self initializeClassModule];
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"initialize"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"initialize::::" :@[[B4I nilToNSNull:_ba],[B4I nilToNSNull:_callback],[B4I nilToNSNull:_eventname],[B4I nilToNSNull:_sck]]]);}
B4INativeObject* _no = nil;
B4IRDebugUtils.currentLine=1507328;
 //BA.debugLineNum = 1507328;BA.debugLine="Public Sub Initialize(CallBack As Object, EventNam";
B4IRDebugUtils.currentLine=1507329;
 //BA.debugLineNum = 1507329;BA.debugLine="client = Sck";
__ref->__client /*B4ISocketWrapper**/  = _sck;
B4IRDebugUtils.currentLine=1507330;
 //BA.debugLineNum = 1507330;BA.debugLine="mCallBack=CallBack";
__ref->__mcallback /*NSObject**/  = _callback;
B4IRDebugUtils.currentLine=1507331;
 //BA.debugLineNum = 1507331;BA.debugLine="mEventName=EventName";
__ref->__meventname /*NSString**/  = _eventname;
B4IRDebugUtils.currentLine=1507332;
 //BA.debugLineNum = 1507332;BA.debugLine="CallServer= mCallBack";
__ref->__callserver /*b4i_httpserver**/  = (b4i_httpserver*)(__ref->__mcallback /*NSObject**/ );
B4IRDebugUtils.currentLine=1507334;
 //BA.debugLineNum = 1507334;BA.debugLine="Users.Initialize";
[__ref->__users /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=1507335;
 //BA.debugLineNum = 1507335;BA.debugLine="Cache=ArrayInitialize";
__ref->__cache /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
B4IRDebugUtils.currentLine=1507336;
 //BA.debugLineNum = 1507336;BA.debugLine="BBB=ArrayInitialize";
__ref->__bbb /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
B4IRDebugUtils.currentLine=1507337;
 //BA.debugLineNum = 1507337;BA.debugLine="MultipartFilename.Initialize";
[__ref->__multipartfilename /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=1507340;
 //BA.debugLineNum = 1507340;BA.debugLine="Dim no As NativeObject = Sck";
_no = [B4INativeObject new];
_no = (B4INativeObject*) [B4IObjectWrapper createWrapper:[B4INativeObject new] object:(NSObject*)(_sck)];
B4IRDebugUtils.currentLine=1507341;
 //BA.debugLineNum = 1507341;BA.debugLine="Address=no.GetField(\"socket\").GetField(\"host\").As";
__ref->__address /*NSString**/  = [[[_no GetField:@"socket"] GetField:@"host"] AsString];
B4IRDebugUtils.currentLine=1507342;
 //BA.debugLineNum = 1507342;BA.debugLine="ConnPort=no.GetField(\"socket\").GetField(\"port\").A";
__ref->__connport /*NSString**/  = [[[_no GetField:@"socket"] GetField:@"port"] AsString];
B4IRDebugUtils.currentLine=1507361;
 //BA.debugLineNum = 1507361;BA.debugLine="astream.Initialize(client.InputStream,client.Outp";
[__ref->__astream /*B4IAsyncStreams**/  Initialize:self.bi :(NSInputStream*)(([__ref->__client /*B4ISocketWrapper**/  InputStream]).object) :(NSOutputStream*)(([__ref->__client /*B4ISocketWrapper**/  OutputStream]).object) :@"astream"];
B4IRDebugUtils.currentLine=1507362;
 //BA.debugLineNum = 1507362;BA.debugLine="Response.Initialize(Me,astream,client)";
[__ref->__response /*b4i_servletresponse**/  _initialize /*NSString**/ :nil :self.bi :(b4i_servletrequest*)(self) :__ref->__astream /*B4IAsyncStreams**/  :__ref->__client /*B4ISocketWrapper**/ ];
B4IRDebugUtils.currentLine=1507364;
 //BA.debugLineNum = 1507364;BA.debugLine="RequestHeader.Initialize";
[__ref->__requestheader /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=1507365;
 //BA.debugLineNum = 1507365;BA.debugLine="RequestParameter.Initialize";
[__ref->__requestparameter /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=1507366;
 //BA.debugLineNum = 1507366;BA.debugLine="RequestCookies.Initialize";
[__ref->__requestcookies /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=1507367;
 //BA.debugLineNum = 1507367;BA.debugLine="RequestPostDataRow.Initialize";
[__ref->__requestpostdatarow /*B4IList**/  Initialize];
B4IRDebugUtils.currentLine=1507369;
 //BA.debugLineNum = 1507369;BA.debugLine="BrowserTimeOut";
[__ref _browsertimeout /*void*/ :nil];
B4IRDebugUtils.currentLine=1507370;
 //BA.debugLineNum = 1507370;BA.debugLine="End Sub";
return @"";
}
- (B4IArray*)  _arrayappend:(b4i_servletrequest*) __ref :(B4IArray*) _data1 :(B4IArray*) _data2{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"arrayappend"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"arrayappend::" :@[[B4I nilToNSNull:_data1],[B4I nilToNSNull:_data2]]]);}
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=4390912;
 //BA.debugLineNum = 4390912;BA.debugLine="Private Sub ArrayAppend(Data1() As Byte,Data2() As";
B4IRDebugUtils.currentLine=4390913;
 //BA.debugLineNum = 4390913;BA.debugLine="Dim Fn(Data1.Length+Data2.Length) As Byte";
_fn = [[B4IArray alloc]initBytes:@[@((int) (_data1.Length+_data2.Length))]];
B4IRDebugUtils.currentLine=4390914;
 //BA.debugLineNum = 4390914;BA.debugLine="Bit.ArrayCopy(Data1,0,Fn,0,Data1.Length)";
B4I_MEMCPY((_data1)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + ((int) (0)),_data1.Length);
B4IRDebugUtils.currentLine=4390915;
 //BA.debugLineNum = 4390915;BA.debugLine="Bit.ArrayCopy(Data2,0,Fn,Data1.Length,Data2.Lengt";
B4I_MEMCPY((_data2)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + (_data1.Length),_data2.Length);
B4IRDebugUtils.currentLine=4390916;
 //BA.debugLineNum = 4390916;BA.debugLine="Return Fn";
if (true) return _fn;
B4IRDebugUtils.currentLine=4390917;
 //BA.debugLineNum = 4390917;BA.debugLine="End Sub";
return nil;
}
- (B4IArray*)  _arraycopy:(b4i_servletrequest*) __ref :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"arraycopy"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"arraycopy:" :@[[B4I nilToNSNull:_data]]]);}
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=4456448;
 //BA.debugLineNum = 4456448;BA.debugLine="Private Sub ArrayCopy(Data() As Byte) As Byte()";
B4IRDebugUtils.currentLine=4456449;
 //BA.debugLineNum = 4456449;BA.debugLine="Dim Fn(Data.Length) As Byte";
_fn = [[B4IArray alloc]initBytes:@[@(_data.Length)]];
B4IRDebugUtils.currentLine=4456450;
 //BA.debugLineNum = 4456450;BA.debugLine="Bit.ArrayCopy(Data,0,Fn,0,Data.Length)";
B4I_MEMCPY((_data)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + ((int) (0)),_data.Length);
B4IRDebugUtils.currentLine=4456451;
 //BA.debugLineNum = 4456451;BA.debugLine="Return Fn";
if (true) return _fn;
B4IRDebugUtils.currentLine=4456452;
 //BA.debugLineNum = 4456452;BA.debugLine="End Sub";
return nil;
}
- (int)  _arrayindexof:(b4i_servletrequest*) __ref :(B4IArray*) _data :(B4IArray*) _searchdata{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"arrayindexof"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"arrayindexof::" :@[[B4I nilToNSNull:_data],[B4I nilToNSNull:_searchdata]]]).intValue;}
int _i = 0;
int _sindex = 0;
B4IRDebugUtils.currentLine=4521984;
 //BA.debugLineNum = 4521984;BA.debugLine="Private Sub ArrayIndexOf(Data() As Byte,SearchData";
B4IRDebugUtils.currentLine=4521985;
 //BA.debugLineNum = 4521985;BA.debugLine="For i = 0 To Data.Length - SearchData.Length";
{
const int step1 = 1;
const int limit1 = (int) (_data.Length-_searchdata.Length);
_i = (int) (0) ;
for (;_i <= limit1 ;_i = _i + step1 ) {
B4IRDebugUtils.currentLine=4521986;
 //BA.debugLineNum = 4521986;BA.debugLine="If SearchData(0) = Data(i) Then";
if ([_searchdata getByteFast:(int) (0)]==[_data getByteFast:_i]) { 
B4IRDebugUtils.currentLine=4521987;
 //BA.debugLineNum = 4521987;BA.debugLine="For sindex = 0 To SearchData.Length - 1";
{
const int step3 = 1;
const int limit3 = (int) (_searchdata.Length-1);
_sindex = (int) (0) ;
for (;_sindex <= limit3 ;_sindex = _sindex + step3 ) {
B4IRDebugUtils.currentLine=4521988;
 //BA.debugLineNum = 4521988;BA.debugLine="If SearchData(sindex) <> Data(i + sindex) Then";
if ([_searchdata getByteFast:_sindex]!=[_data getByteFast:(int) (_i+_sindex)]) { 
B4IRDebugUtils.currentLine=4521989;
 //BA.debugLineNum = 4521989;BA.debugLine="Exit";
if (true) break;
 };
 }
};
B4IRDebugUtils.currentLine=4521992;
 //BA.debugLineNum = 4521992;BA.debugLine="If sindex = SearchData.Length Then";
if (_sindex==_searchdata.Length) { 
B4IRDebugUtils.currentLine=4521993;
 //BA.debugLineNum = 4521993;BA.debugLine="Return i";
if (true) return _i;
 };
 };
 }
};
B4IRDebugUtils.currentLine=4521997;
 //BA.debugLineNum = 4521997;BA.debugLine="Return -1";
if (true) return (int) (-1);
B4IRDebugUtils.currentLine=4521998;
 //BA.debugLineNum = 4521998;BA.debugLine="End Sub";
return 0;
}
- (B4IArray*)  _arrayinitialize:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"arrayinitialize"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"arrayinitialize" :nil]);}
B4IRDebugUtils.currentLine=4325376;
 //BA.debugLineNum = 4325376;BA.debugLine="Private Sub ArrayInitialize() As Byte()";
B4IRDebugUtils.currentLine=4325377;
 //BA.debugLineNum = 4325377;BA.debugLine="Return Array As Byte()";
if (true) return [[B4IArray alloc]initBytesWithData:@[]];
B4IRDebugUtils.currentLine=4325378;
 //BA.debugLineNum = 4325378;BA.debugLine="End Sub";
return nil;
}
- (B4IArray*)  _arrayinsert:(b4i_servletrequest*) __ref :(B4IArray*) _datasource :(int) _index :(B4IArray*) _datainsert{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"arrayinsert"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"arrayinsert:::" :@[[B4I nilToNSNull:_datasource],@(_index),[B4I nilToNSNull:_datainsert]]]);}
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=4587520;
 //BA.debugLineNum = 4587520;BA.debugLine="Public Sub ArrayInsert(DataSource() As Byte,Index";
B4IRDebugUtils.currentLine=4587521;
 //BA.debugLineNum = 4587521;BA.debugLine="Dim Fn(DataSource.Length+DataInsert.Length) As By";
_fn = [[B4IArray alloc]initBytes:@[@((int) (_datasource.Length+_datainsert.Length))]];
B4IRDebugUtils.currentLine=4587523;
 //BA.debugLineNum = 4587523;BA.debugLine="If Index>0 Then Bit.ArrayCopy(DataSource,0,Fn,0,I";
if (_index>0) { 
B4I_MEMCPY((_datasource)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + ((int) (0)),(int) (_index-1));};
B4IRDebugUtils.currentLine=4587524;
 //BA.debugLineNum = 4587524;BA.debugLine="Bit.ArrayCopy(DataInsert,0,Fn,Index,DataInsert.Le";
B4I_MEMCPY((_datainsert)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + (_index),_datainsert.Length);
B4IRDebugUtils.currentLine=4587525;
 //BA.debugLineNum = 4587525;BA.debugLine="If DataSource.Length-Index>0 Then Bit.ArrayCopy(D";
if (_datasource.Length-_index>0) { 
B4I_MEMCPY((_datasource)->internalBuffer + (_index), (_fn)->internalBuffer + ((int) (_datainsert.Length+_index)),(int) (_datasource.Length-_index));};
B4IRDebugUtils.currentLine=4587527;
 //BA.debugLineNum = 4587527;BA.debugLine="Return Fn";
if (true) return _fn;
B4IRDebugUtils.currentLine=4587528;
 //BA.debugLineNum = 4587528;BA.debugLine="End Sub";
return nil;
}
- (B4IArray*)  _arrayremove:(b4i_servletrequest*) __ref :(B4IArray*) _data :(int) _start :(int) _last{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"arrayremove"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"arrayremove:::" :@[[B4I nilToNSNull:_data],@(_start),@(_last)]]);}
int _eraseq = 0;
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=4653056;
 //BA.debugLineNum = 4653056;BA.debugLine="Public Sub ArrayRemove(Data() As Byte,Start As Int";
B4IRDebugUtils.currentLine=4653057;
 //BA.debugLineNum = 4653057;BA.debugLine="Dim EraseQ As Int = Last-Start";
_eraseq = (int) (_last-_start);
B4IRDebugUtils.currentLine=4653058;
 //BA.debugLineNum = 4653058;BA.debugLine="Dim Fn(Data.Length-EraseQ) As Byte";
_fn = [[B4IArray alloc]initBytes:@[@((int) (_data.Length-_eraseq))]];
B4IRDebugUtils.currentLine=4653060;
 //BA.debugLineNum = 4653060;BA.debugLine="If Data.Length-EraseQ=0 Then Return Array As Byte";
if (_data.Length-_eraseq==0) { 
if (true) return [[B4IArray alloc]initBytesWithData:@[]];};
B4IRDebugUtils.currentLine=4653062;
 //BA.debugLineNum = 4653062;BA.debugLine="If Start>0 Then";
if (_start>0) { 
B4IRDebugUtils.currentLine=4653063;
 //BA.debugLineNum = 4653063;BA.debugLine="Bit.ArrayCopy(Data,0,Fn,0,Start)";
B4I_MEMCPY((_data)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + ((int) (0)),_start);
 };
B4IRDebugUtils.currentLine=4653065;
 //BA.debugLineNum = 4653065;BA.debugLine="If Last<Data.Length Then";
if (_last<_data.Length) { 
B4IRDebugUtils.currentLine=4653066;
 //BA.debugLineNum = 4653066;BA.debugLine="Bit.ArrayCopy(Data,Last,Fn,Start,Data.Length-Las";
B4I_MEMCPY((_data)->internalBuffer + (_last), (_fn)->internalBuffer + (_start),(int) (_data.Length-_last));
 };
B4IRDebugUtils.currentLine=4653069;
 //BA.debugLineNum = 4653069;BA.debugLine="Return Fn";
if (true) return _fn;
B4IRDebugUtils.currentLine=4653070;
 //BA.debugLineNum = 4653070;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _astream_error:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"astream_error"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"astream_error" :nil]);}
B4IRDebugUtils.currentLine=2686976;
 //BA.debugLineNum = 2686976;BA.debugLine="Private Sub astream_Error";
B4IRDebugUtils.currentLine=2686977;
 //BA.debugLineNum = 2686977;BA.debugLine="Log($\"Error (${ID})\"$)";
[self->___c LogImpl:@"32686977" :([@[@"Error (",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__id /*NSString**/ )],@")"] componentsJoinedByString:@""]) :0];
B4IRDebugUtils.currentLine=2686978;
 //BA.debugLineNum = 2686978;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _astream_newdata:(b4i_servletrequest*) __ref :(B4IArray*) _buffer{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"astream_newdata"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"astream_newdata:" :@[[B4I nilToNSNull:_buffer]]]);}
B4IRDebugUtils.currentLine=2555904;
 //BA.debugLineNum = 2555904;BA.debugLine="private Sub astream_NewData (Buffer() As Byte)";
B4IRDebugUtils.currentLine=2555905;
 //BA.debugLineNum = 2555905;BA.debugLine="Cache=ArrayAppend(Cache,Buffer)";
__ref->__cache /*B4IArray**/  = [__ref _arrayappend /*B4IArray**/ :nil :__ref->__cache /*B4IArray**/  :_buffer];
B4IRDebugUtils.currentLine=2555906;
 //BA.debugLineNum = 2555906;BA.debugLine="LastBrowserComunicate=DateTime.Now";
__ref->__lastbrowsercomunicate /*long long*/  = [[self->___c DateTime] Now];
B4IRDebugUtils.currentLine=2555907;
 //BA.debugLineNum = 2555907;BA.debugLine="ExtractHandShake";
[__ref _extracthandshake /*NSString**/ :nil];
B4IRDebugUtils.currentLine=2555908;
 //BA.debugLineNum = 2555908;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _extracthandshake:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"extracthandshake"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"extracthandshake" :nil]);}
int _chremove = 0;
int _index = 0;
NSString* _handshake = @"";
B4IArray* _data = nil;
B4IArray* _d = nil;
B4IRDebugUtils.currentLine=2752512;
 //BA.debugLineNum = 2752512;BA.debugLine="Private Sub ExtractHandShake";
B4IRDebugUtils.currentLine=2752513;
 //BA.debugLineNum = 2752513;BA.debugLine="If OtherData And ContentLength>0 Then ' continue";
if (__ref->__otherdata /*BOOL*/  && __ref->__contentlength /*long long*/ >0) { 
B4IRDebugUtils.currentLine=2752514;
 //BA.debugLineNum = 2752514;BA.debugLine="BBB=ArrayAppend(BBB,Cache)";
__ref->__bbb /*B4IArray**/  = [__ref _arrayappend /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :__ref->__cache /*B4IArray**/ ];
B4IRDebugUtils.currentLine=2752515;
 //BA.debugLineNum = 2752515;BA.debugLine="Cache=ArrayInitialize";
__ref->__cache /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
B4IRDebugUtils.currentLine=2752521;
 //BA.debugLineNum = 2752521;BA.debugLine="If ContentLength<=BBB.Length Then";
if (__ref->__contentlength /*long long*/ <=__ref->__bbb /*B4IArray**/ .Length) { 
B4IRDebugUtils.currentLine=2752522;
 //BA.debugLineNum = 2752522;BA.debugLine="If BBB.Length>ContentLength Then ' select only";
if (__ref->__bbb /*B4IArray**/ .Length>__ref->__contentlength /*long long*/ ) { 
B4IRDebugUtils.currentLine=2752523;
 //BA.debugLineNum = 2752523;BA.debugLine="Cache=ArrayAppend(Cache,SubArray(BBB,ContentLe";
__ref->__cache /*B4IArray**/  = [__ref _arrayappend /*B4IArray**/ :nil :__ref->__cache /*B4IArray**/  :[__ref _subarray /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (__ref->__contentlength /*long long*/ )]];
B4IRDebugUtils.currentLine=2752525;
 //BA.debugLineNum = 2752525;BA.debugLine="BBB=ArrayRemove(BBB,ContentLength,BBB.Length)";
__ref->__bbb /*B4IArray**/  = [__ref _arrayremove /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (__ref->__contentlength /*long long*/ ) :__ref->__bbb /*B4IArray**/ .Length];
 };
B4IRDebugUtils.currentLine=2752528;
 //BA.debugLineNum = 2752528;BA.debugLine="OtherData=False";
__ref->__otherdata /*BOOL*/  = false;
B4IRDebugUtils.currentLine=2752529;
 //BA.debugLineNum = 2752529;BA.debugLine="extractParameterFromData";
[__ref _extractparameterfromdata /*NSString**/ :nil];
 };
 }else {
B4IRDebugUtils.currentLine=2752533;
 //BA.debugLineNum = 2752533;BA.debugLine="Dim ChRemove As Int = 4";
_chremove = (int) (4);
B4IRDebugUtils.currentLine=2752534;
 //BA.debugLineNum = 2752534;BA.debugLine="Dim Index As Int = ArrayIndexOf(Cache,Array As B";
_index = [__ref _arrayindexof /*int*/ :nil :__ref->__cache /*B4IArray**/  :[[B4IArray alloc]initBytesWithData:@[@((unsigned char) (13)),@((unsigned char) (10)),@((unsigned char) (13)),@((unsigned char) (10))]]];
B4IRDebugUtils.currentLine=2752535;
 //BA.debugLineNum = 2752535;BA.debugLine="If Index=-1 Then";
if (_index==-1) { 
B4IRDebugUtils.currentLine=2752536;
 //BA.debugLineNum = 2752536;BA.debugLine="ChRemove = 2";
_chremove = (int) (2);
B4IRDebugUtils.currentLine=2752537;
 //BA.debugLineNum = 2752537;BA.debugLine="Index = ArrayIndexOf(Cache,Array As Byte(10,10)";
_index = [__ref _arrayindexof /*int*/ :nil :__ref->__cache /*B4IArray**/  :[[B4IArray alloc]initBytesWithData:@[@((unsigned char) (10)),@((unsigned char) (10))]]];
B4IRDebugUtils.currentLine=2752538;
 //BA.debugLineNum = 2752538;BA.debugLine="If Index=-1 Then Index = ArrayIndexOf(Cache,Arr";
if (_index==-1) { 
_index = [__ref _arrayindexof /*int*/ :nil :__ref->__cache /*B4IArray**/  :[[B4IArray alloc]initBytesWithData:@[@((unsigned char) (13)),@((unsigned char) (13))]]];};
 };
B4IRDebugUtils.currentLine=2752542;
 //BA.debugLineNum = 2752542;BA.debugLine="If Index>-1 Then";
if (_index>-1) { 
B4IRDebugUtils.currentLine=2752543;
 //BA.debugLineNum = 2752543;BA.debugLine="Dim HandShake As String = BytesToString(Cache,0";
_handshake = [self->___c BytesToString:__ref->__cache /*B4IArray**/  :(int) (0) :_index :__ref->__characterencoding /*NSString**/ ];
B4IRDebugUtils.currentLine=2752544;
 //BA.debugLineNum = 2752544;BA.debugLine="Dim Data() As Byte = SubArray(Cache,Index+ChRem";
_data = [__ref _subarray /*B4IArray**/ :nil :__ref->__cache /*B4IArray**/  :(int) (_index+_chremove)];
B4IRDebugUtils.currentLine=2752545;
 //BA.debugLineNum = 2752545;BA.debugLine="Cache=ArrayInitialize";
__ref->__cache /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
B4IRDebugUtils.currentLine=2752546;
 //BA.debugLineNum = 2752546;BA.debugLine="ElaborateHandShake(HandShake,Data)";
[__ref _elaboratehandshake /*NSString**/ :nil :_handshake :_data];
 }else {
B4IRDebugUtils.currentLine=2752549;
 //BA.debugLineNum = 2752549;BA.debugLine="If WebSocket Then";
if (__ref->__websocket /*BOOL*/ ) { 
B4IRDebugUtils.currentLine=2752551;
 //BA.debugLineNum = 2752551;BA.debugLine="Dim D() As Byte = ArrayCopy(Cache)";
_d = [__ref _arraycopy /*B4IArray**/ :nil :__ref->__cache /*B4IArray**/ ];
B4IRDebugUtils.currentLine=2752552;
 //BA.debugLineNum = 2752552;BA.debugLine="ElaborateWebSocket(D)";
[__ref _elaboratewebsocket /*NSString**/ :nil :_d];
 }else {
B4IRDebugUtils.currentLine=2752554;
 //BA.debugLineNum = 2752554;BA.debugLine="Log(\"No handshake: \"& Cache.Length)";
[self->___c LogImpl:@"32752554" :[@[@"No handshake: ",[self.bi NumberToString:@(__ref->__cache /*B4IArray**/ .Length)]] componentsJoinedByString:@""] :0];
 };
B4IRDebugUtils.currentLine=2752557;
 //BA.debugLineNum = 2752557;BA.debugLine="Cache = ArrayInitialize";
__ref->__cache /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
 };
 };
B4IRDebugUtils.currentLine=2752560;
 //BA.debugLineNum = 2752560;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _astream_terminated:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"astream_terminated"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"astream_terminated" :nil]);}
B4IRDebugUtils.currentLine=2621440;
 //BA.debugLineNum = 2621440;BA.debugLine="Private Sub astream_Terminated";
B4IRDebugUtils.currentLine=2621441;
 //BA.debugLineNum = 2621441;BA.debugLine="Log($\"Client has terminated (${ID})\"$ )";
[self->___c LogImpl:@"32621441" :([@[@"Client has terminated (",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__id /*NSString**/ )],@")"] componentsJoinedByString:@""]) :0];
B4IRDebugUtils.currentLine=2621442;
 //BA.debugLineNum = 2621442;BA.debugLine="End Sub";
return @"";
}
- (void)  _browsertimeout:(b4i_servletrequest*) __ref{
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"browsertimeout"])
	 {[B4IDebug delegate:self.bi :@"browsertimeout" :nil]; return;}
ResumableSub_servletrequest_BrowserTimeOut* rsub = [[ResumableSub_servletrequest_BrowserTimeOut alloc] init:self : __ref];
[rsub resume:self.bi :nil];
}
//190085008
- (NSString*)  _completedate:(b4i_servletrequest*) __ref :(long long) _dt{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"completedate"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"completedate:" :@[@(_dt)]]);}
B4IRDebugUtils.currentLine=3473408;
 //BA.debugLineNum = 3473408;BA.debugLine="Private Sub CompleteDate(DT As Long) As String";
B4IRDebugUtils.currentLine=3473409;
 //BA.debugLineNum = 3473409;BA.debugLine="Return $\"${DDay(DateTime.GetDayOfWeek(DT))}, $2.0";
if (true) return ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(((NSString*)[__ref->__dday /*B4IArray**/  getObjectFast:[[self->___c DateTime] GetDayOfWeek:_dt]]))],@", ",[self->___c SmartStringFormatter:@"2.0" :(NSObject*)(@([[self->___c DateTime] GetDayOfMonth:_dt]))],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)([((NSString*)[__ref->__mmmonth /*B4IArray**/  getObjectFast:[[self->___c DateTime] GetMonth:_dt]]) SubString2:(int) (0) :(int) (3)])],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@([[self->___c DateTime] GetYear:_dt]))],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)([[self->___c DateTime] Time:_dt])],@" GMT"] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3473411;
 //BA.debugLineNum = 3473411;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _close:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"close"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"close" :nil]);}
B4IRDebugUtils.currentLine=1638400;
 //BA.debugLineNum = 1638400;BA.debugLine="Public Sub Close";
B4IRDebugUtils.currentLine=1638401;
 //BA.debugLineNum = 1638401;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=1638402;
 //BA.debugLineNum = 1638402;BA.debugLine="astream.SendAllAndClose";
[__ref->__astream /*B4IAsyncStreams**/  SendAllAndClose];
 } 
       @catch (NSException* e4) {
			[B4I SetException:e4];B4IRDebugUtils.currentLine=1638405;
 //BA.debugLineNum = 1638405;BA.debugLine="Log($\"Server error(${ID}): ${LastException.Messa";
[self->___c LogImpl:@"31638405" :([@[@"Server error(",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__id /*NSString**/ )],@"): ",[self->___c SmartStringFormatter:@"" :(NSObject*)([[self->___c LastException] Message])],@""] componentsJoinedByString:@""]) :0];
 };
B4IRDebugUtils.currentLine=1638407;
 //BA.debugLineNum = 1638407;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _callevent:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"callevent"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"callevent" :nil]);}
B4IRDebugUtils.currentLine=3604480;
 //BA.debugLineNum = 3604480;BA.debugLine="Private Sub CallEvent";
B4IRDebugUtils.currentLine=3604481;
 //BA.debugLineNum = 3604481;BA.debugLine="If CallServer.DigestAuthentication And RequestURI";
if (__ref->__callserver /*b4i_httpserver**/ ->__digestauthentication /*BOOL*/  && [__ref->__requesturi /*NSString**/  StartsWith:__ref->__callserver /*b4i_httpserver**/ ->__digestpath /*NSString**/ ]) { 
B4IRDebugUtils.currentLine=3604482;
 //BA.debugLineNum = 3604482;BA.debugLine="ElaborateHandSkakeDigestMessage(Me,Response)";
[__ref _elaboratehandskakedigestmessage /*NSString**/ :nil :(b4i_servletrequest*)(self) :__ref->__response /*b4i_servletresponse**/ ];
 }else {
B4IRDebugUtils.currentLine=3604484;
 //BA.debugLineNum = 3604484;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handle\",2";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_Handle"] componentsJoinedByString:@""] :(int) (2)]) { 
B4IRDebugUtils.currentLine=3604485;
 //BA.debugLineNum = 3604485;BA.debugLine="CallSub3(mCallBack,mEventName & \"_Handle\",Me,Re";
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_Handle"] componentsJoinedByString:@""] :self :(NSObject*)(__ref->__response /*b4i_servletresponse**/ )];
 };
B4IRDebugUtils.currentLine=3604487;
 //BA.debugLineNum = 3604487;BA.debugLine="If ConnectionAlive=False Then Close";
if (__ref->__connectionalive /*BOOL*/ ==false) { 
[__ref _close /*NSString**/ :nil];};
 };
B4IRDebugUtils.currentLine=3604489;
 //BA.debugLineNum = 3604489;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _elaboratehandskakedigestmessage:(b4i_servletrequest*) __ref :(b4i_servletrequest*) _request :(b4i_servletresponse*) _sresponse{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"elaboratehandskakedigestmessage"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"elaboratehandskakedigestmessage::" :@[[B4I nilToNSNull:_request],[B4I nilToNSNull:_sresponse]]]);}
NSString* _headerdigest = @"";
B4IMap* _headermap = nil;
B4IMap* _paramteremap = nil;
B4IMap* _fields = nil;
NSString* _fl = @"";
NSString* _key = @"";
NSString* _value = @"";
_tuser* _user = nil;
NSString* _ha1 = @"";
NSString* _ha2 = @"";
NSString* _ss = @"";
NSString* _md5response = @"";
int _l = 0;
B4IRDebugUtils.currentLine=2883584;
 //BA.debugLineNum = 2883584;BA.debugLine="Private Sub ElaborateHandSkakeDigestMessage(Reques";
B4IRDebugUtils.currentLine=2883585;
 //BA.debugLineNum = 2883585;BA.debugLine="Dim HeaderDigest As String = \"Authorization\"";
_headerdigest = @"Authorization";
B4IRDebugUtils.currentLine=2883586;
 //BA.debugLineNum = 2883586;BA.debugLine="Dim HeaderMap, ParamtereMap As Map";
_headermap = [B4IMap new];
_paramteremap = [B4IMap new];
B4IRDebugUtils.currentLine=2883588;
 //BA.debugLineNum = 2883588;BA.debugLine="HeaderMap.Initialize";
[_headermap Initialize];
B4IRDebugUtils.currentLine=2883589;
 //BA.debugLineNum = 2883589;BA.debugLine="ParamtereMap.Initialize";
[_paramteremap Initialize];
B4IRDebugUtils.currentLine=2883590;
 //BA.debugLineNum = 2883590;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=2883591;
 //BA.debugLineNum = 2883591;BA.debugLine="LgP(\"Client: \" & Request.RemoteAddress)";
[__ref _lgp /*NSString**/ :nil :[@[@"Client: ",[_request _remoteaddress /*NSString**/ :nil]] componentsJoinedByString:@""]];
B4IRDebugUtils.currentLine=2883592;
 //BA.debugLineNum = 2883592;BA.debugLine="LgP(Request.RequestURI)";
[__ref _lgp /*NSString**/ :nil :_request->__requesturi /*NSString**/ ];
B4IRDebugUtils.currentLine=2883612;
 //BA.debugLineNum = 2883612;BA.debugLine="If  Request.GetHeader(HeaderDigest)=\"\" Then";
if ([[_request _getheader /*NSString**/ :nil :_headerdigest] isEqual:@""]) { 
B4IRDebugUtils.currentLine=2883614;
 //BA.debugLineNum = 2883614;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_RefusedN";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_RefusedNoCredential"] componentsJoinedByString:@""] :(int) (1)] && __ref->__logfirstrefuse /*BOOL*/ ) { 
[self->___c CallSub2:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_RefusedNoCredential"] componentsJoinedByString:@""] :(NSObject*)([_request _remoteaddress /*NSString**/ :nil])];};
B4IRDebugUtils.currentLine=2883615;
 //BA.debugLineNum = 2883615;BA.debugLine="SendRefuse(SResponse,NewUser(Request.RemoteAddr";
[__ref _sendrefuse /*NSString**/ :nil :_sresponse :[__ref _newuser /*_tuser**/ :nil :[_request _remoteaddress /*NSString**/ :nil]]];
 }else {
B4IRDebugUtils.currentLine=2883618;
 //BA.debugLineNum = 2883618;BA.debugLine="Dim Fields As Map";
_fields = [B4IMap new];
B4IRDebugUtils.currentLine=2883619;
 //BA.debugLineNum = 2883619;BA.debugLine="Fields.Initialize";
[_fields Initialize];
B4IRDebugUtils.currentLine=2883621;
 //BA.debugLineNum = 2883621;BA.debugLine="For Each fl As String In Regex.Split(\",\",Reques";
{
const id<B4IIterable> group14 = [[self->___c Regex] Split:@"," :[_request _getheader /*NSString**/ :nil :_headerdigest]];
const int groupLen14 = group14.Size
;int index14 = 0;
;
for (; index14 < groupLen14;index14++){
_fl = [group14 Get:index14];
B4IRDebugUtils.currentLine=2883622;
 //BA.debugLineNum = 2883622;BA.debugLine="If fl.IndexOf(\"=\")>-1 Then";
if ([_fl IndexOf:@"="]>-1) { 
B4IRDebugUtils.currentLine=2883623;
 //BA.debugLineNum = 2883623;BA.debugLine="Dim Key As String = fl.SubString2(0,fl.IndexO";
_key = [[_fl SubString2:(int) (0) :[_fl IndexOf:@"="]] Trim];
B4IRDebugUtils.currentLine=2883624;
 //BA.debugLineNum = 2883624;BA.debugLine="Dim Value As String = fl.SubString(fl.IndexOf";
_value = [[[_fl SubString:(int) ([_fl IndexOf:@"="]+1)] Replace:@"\"" :@""] Trim];
B4IRDebugUtils.currentLine=2883625;
 //BA.debugLineNum = 2883625;BA.debugLine="Fields.Put(Key,Value)";
[_fields Put:(NSObject*)(_key) :(NSObject*)(_value)];
B4IRDebugUtils.currentLine=2883626;
 //BA.debugLineNum = 2883626;BA.debugLine="LgP($\"** -${Key}- -${Value}-\"$)";
[__ref _lgp /*NSString**/ :nil :([@[@"** -",[self->___c SmartStringFormatter:@"" :(NSObject*)(_key)],@"- -",[self->___c SmartStringFormatter:@"" :(NSObject*)(_value)],@"-"] componentsJoinedByString:@""])];
 }else {
B4IRDebugUtils.currentLine=2883628;
 //BA.debugLineNum = 2883628;BA.debugLine="Log(\"Error on Header: \" & fl)";
[self->___c LogImpl:@"32883628" :[@[@"Error on Header: ",_fl] componentsJoinedByString:@""] :0];
 };
 }
};
B4IRDebugUtils.currentLine=2883633;
 //BA.debugLineNum = 2883633;BA.debugLine="Dim User As tUser = FindUser(Request.RemoteAddr";
_user = [__ref _finduser /*_tuser**/ :nil :[_request _remoteaddress /*NSString**/ :nil] :[self.bi ObjectToString:[_fields Get:(NSObject*)(@"opaque")]]];
B4IRDebugUtils.currentLine=2883634;
 //BA.debugLineNum = 2883634;BA.debugLine="FindCredential(Fields.Get(\"Digest username\"))";
[__ref _findcredential /*NSString**/ :nil :[self.bi ObjectToString:[_fields Get:(NSObject*)(@"Digest username")]]];
B4IRDebugUtils.currentLine=2883635;
 //BA.debugLineNum = 2883635;BA.debugLine="User.realm=CallServer.realm";
_user->_realm /*NSString**/  = __ref->__callserver /*b4i_httpserver**/ ->__realm /*NSString**/ ;
B4IRDebugUtils.currentLine=2883637;
 //BA.debugLineNum = 2883637;BA.debugLine="Dim HA1 As String = MD5($\"${UserName}:${Fields.";
_ha1 = [__ref _md5 /*NSString**/ :nil :([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__username /*NSString**/ )],@":",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"realm")]],@":",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__password /*NSString**/ )],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883638;
 //BA.debugLineNum = 2883638;BA.debugLine="Dim HA2 As String = MD5($\"${Request.Method.ToUp";
_ha2 = [__ref _md5 /*NSString**/ :nil :([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)([_request->__method /*NSString**/  ToUpperCase])],@":",[self->___c SmartStringFormatter:@"" :(NSObject*)(_request->__requesturi /*NSString**/ )],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883639;
 //BA.debugLineNum = 2883639;BA.debugLine="If Fields.ContainsKey(\"qop\") Then";
if ([_fields ContainsKey:(NSObject*)(@"qop")]) { 
B4IRDebugUtils.currentLine=2883640;
 //BA.debugLineNum = 2883640;BA.debugLine="Dim SS As String = $\"${HA1}:${Fields.Get(\"nonc";
_ss = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_ha1)],@":",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"nonce")]],@":",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"nc")]],@":",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"cnonce")]],@":",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"qop")]],@":",[self->___c SmartStringFormatter:@"" :(NSObject*)(_ha2)],@""] componentsJoinedByString:@""]);
 }else {
B4IRDebugUtils.currentLine=2883642;
 //BA.debugLineNum = 2883642;BA.debugLine="Dim SS As String = $\"${HA1}:${Fields.Get(\"nonc";
_ss = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_ha1)],@":",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"nonce")]],@":",[self->___c SmartStringFormatter:@"" :(NSObject*)(_ha2)],@""] componentsJoinedByString:@""]);
 };
B4IRDebugUtils.currentLine=2883644;
 //BA.debugLineNum = 2883644;BA.debugLine="Dim MD5response As String = MD5(SS)";
_md5response = [__ref _md5 /*NSString**/ :nil :_ss];
B4IRDebugUtils.currentLine=2883646;
 //BA.debugLineNum = 2883646;BA.debugLine="LgP($\"HA1: ${HA1}\"$)";
[__ref _lgp /*NSString**/ :nil :([@[@"HA1: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_ha1)],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883647;
 //BA.debugLineNum = 2883647;BA.debugLine="LgP($\"HA2: ${HA2}\"$)";
[__ref _lgp /*NSString**/ :nil :([@[@"HA2: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_ha2)],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883648;
 //BA.debugLineNum = 2883648;BA.debugLine="LgP(SS)";
[__ref _lgp /*NSString**/ :nil :_ss];
B4IRDebugUtils.currentLine=2883649;
 //BA.debugLineNum = 2883649;BA.debugLine="LgP($\"MD5 send: ${Fields.get(\"response\")}\"$)";
[__ref _lgp /*NSString**/ :nil :([@[@"MD5 send: ",[self->___c SmartStringFormatter:@"" :[_fields Get:(NSObject*)(@"response")]],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883650;
 //BA.debugLineNum = 2883650;BA.debugLine="LgP($\"MD5 Calc : ${MD5response}\"$)";
[__ref _lgp /*NSString**/ :nil :([@[@"MD5 Calc : ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_md5response)],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883652;
 //BA.debugLineNum = 2883652;BA.debugLine="Dim L As Int = HexToDec(Fields.Get(\"nc\"))";
_l = [__ref _hextodec /*int*/ :nil :[self.bi ObjectToString:[_fields Get:(NSObject*)(@"nc")]]];
B4IRDebugUtils.currentLine=2883654;
 //BA.debugLineNum = 2883654;BA.debugLine="LgC(\"opaque:\" & User.opaque & \" clnc.\" & L & \"";
[__ref _lgc /*NSString**/ :nil :[@[@"opaque:",_user->_opaque /*NSString**/ ,@" clnc.",[self.bi NumberToString:@(_l)],@" nc.",[self.bi NumberToString:@(_user->_nc /*int*/ )]] componentsJoinedByString:@""] :(int) (0xff000f04)];
B4IRDebugUtils.currentLine=2883655;
 //BA.debugLineNum = 2883655;BA.debugLine="If MD5response=Fields.get(\"response\") Then";
if ([_md5response isEqual:[self.bi ObjectToString:[_fields Get:(NSObject*)(@"response")]]]) { 
B4IRDebugUtils.currentLine=2883656;
 //BA.debugLineNum = 2883656;BA.debugLine="LgC(\"Digest: \" & MD5response,0xFF0000FF)";
[__ref _lgc /*NSString**/ :nil :[@[@"Digest: ",_md5response] componentsJoinedByString:@""] :(int) (0xff0000ff)];
B4IRDebugUtils.currentLine=2883657;
 //BA.debugLineNum = 2883657;BA.debugLine="If  L>User.nc Or CallServer.IgnoreNC Then";
if (_l>_user->_nc /*int*/  || __ref->__callserver /*b4i_httpserver**/ ->__ignorenc /*BOOL*/ ) { 
B4IRDebugUtils.currentLine=2883659;
 //BA.debugLineNum = 2883659;BA.debugLine="User.nonce=bc.HexFromBytes(bc.LongsToBytes(Ar";
_user->_nonce /*NSString**/  = [__ref->__bc /*B4IByteConverter**/  HexFromBytes:[__ref->__bc /*B4IByteConverter**/  LongsToBytes:[[B4IArray alloc]initObjectsWithData:@[@([[self->___c DateTime] Now])]]]];
B4IRDebugUtils.currentLine=2883660;
 //BA.debugLineNum = 2883660;BA.debugLine="User.nc=Max(l,User.nc)";
_user->_nc /*int*/  = (int) (fmax(_l,_user->_nc /*int*/ ));
B4IRDebugUtils.currentLine=2883661;
 //BA.debugLineNum = 2883661;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_LogIn\"";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_LogIn"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_LogIn"] componentsJoinedByString:@""] :(NSObject*)(__ref->__username /*NSString**/ ) :(NSObject*)([_request _remoteaddress /*NSString**/ :nil])];};
B4IRDebugUtils.currentLine=2883662;
 //BA.debugLineNum = 2883662;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handle";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_Handle"] componentsJoinedByString:@""] :(int) (2)]) { 
B4IRDebugUtils.currentLine=2883663;
 //BA.debugLineNum = 2883663;BA.debugLine="SResponse.SetHeader(\"WWW-Authenticate\",$\"Dig";
[_sresponse _setheader /*NSString**/ :nil :@"WWW-Authenticate" :([@[@"Digest realm=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_user->_realm /*NSString**/ )],@"\", qop=\"auth,auth-int\", nonce=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_user->_nonce /*NSString**/ )],@"\", opaque=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_user->_opaque /*NSString**/ )],@"\""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=2883664;
 //BA.debugLineNum = 2883664;BA.debugLine="SResponse.ContentType=\"text/html\"";
_sresponse->__contenttype /*NSString**/  = @"text/html";
B4IRDebugUtils.currentLine=2883665;
 //BA.debugLineNum = 2883665;BA.debugLine="CallSub3(mCallBack,mEventName & \"_Handle\",Re";
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_Handle"] componentsJoinedByString:@""] :(NSObject*)(_request) :(NSObject*)(_sresponse)];
 };
 }else {
B4IRDebugUtils.currentLine=2883669;
 //BA.debugLineNum = 2883669;BA.debugLine="User.nc=Max(l,User.nc)";
_user->_nc /*int*/  = (int) (fmax(_l,_user->_nc /*int*/ ));
B4IRDebugUtils.currentLine=2883670;
 //BA.debugLineNum = 2883670;BA.debugLine="SendRefuse(SResponse,User)";
[__ref _sendrefuse /*NSString**/ :nil :_sresponse :_user];
B4IRDebugUtils.currentLine=2883671;
 //BA.debugLineNum = 2883671;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Refuse";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_RefusedWrongNC"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_RefusedWrongNC"] componentsJoinedByString:@""] :(NSObject*)(__ref->__username /*NSString**/ ) :(NSObject*)([_request _remoteaddress /*NSString**/ :nil])];};
 };
 }else {
B4IRDebugUtils.currentLine=2883675;
 //BA.debugLineNum = 2883675;BA.debugLine="LgC(\"Wrong<> \" & MD5response & CRLF & \"Digest<";
[__ref _lgc /*NSString**/ :nil :[@[@"Wrong<> ",_md5response,@"\n",@"Digest<> ",[self.bi ObjectToString:[_fields Get:(NSObject*)(@"response")]]] componentsJoinedByString:@""] :(int) (0xffff0000)];
B4IRDebugUtils.currentLine=2883676;
 //BA.debugLineNum = 2883676;BA.debugLine="User.nc=Max(l,User.nc)";
_user->_nc /*int*/  = (int) (fmax(_l,_user->_nc /*int*/ ));
B4IRDebugUtils.currentLine=2883677;
 //BA.debugLineNum = 2883677;BA.debugLine="SendRefuse(SResponse,User)";
[__ref _sendrefuse /*NSString**/ :nil :_sresponse :_user];
B4IRDebugUtils.currentLine=2883678;
 //BA.debugLineNum = 2883678;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Refused";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_RefusedWrongCredential"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_RefusedWrongCredential"] componentsJoinedByString:@""] :(NSObject*)(__ref->__username /*NSString**/ ) :(NSObject*)([_request _remoteaddress /*NSString**/ :nil])];};
 };
 };
 } 
       @catch (NSException* e66) {
			[B4I SetException:e66];B4IRDebugUtils.currentLine=2883682;
 //BA.debugLineNum = 2883682;BA.debugLine="SResponse.Status = 500";
_sresponse->__status /*int*/  = (int) (500);
B4IRDebugUtils.currentLine=2883683;
 //BA.debugLineNum = 2883683;BA.debugLine="Log(\"Error serving request: \" & LastException)";
[self->___c LogImpl:@"32883683" :[@[@"Error serving request: ",[self.bi ObjectToString:[self->___c LastException]]] componentsJoinedByString:@""] :0];
B4IRDebugUtils.currentLine=2883684;
 //BA.debugLineNum = 2883684;BA.debugLine="SResponse.SendString(\"Error serving request: \" &";
[_sresponse _sendstring /*NSString**/ :nil :[@[@"Error serving request: ",[self.bi ObjectToString:[self->___c LastException]]] componentsJoinedByString:@""]];
 };
B4IRDebugUtils.currentLine=2883686;
 //BA.debugLineNum = 2883686;BA.debugLine="End Sub";
return @"";
}
- (BOOL)  _subexists2:(b4i_servletrequest*) __ref :(NSObject*) _callobject :(NSString*) _eventname :(int) _param{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"subexists2"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"subexists2:::" :@[[B4I nilToNSNull:_callobject],[B4I nilToNSNull:_eventname],@(_param)]]).boolValue;}
B4IRDebugUtils.currentLine=4259840;
 //BA.debugLineNum = 4259840;BA.debugLine="Private Sub SubExists2(CallObject As Object, Event";
B4IRDebugUtils.currentLine=4259842;
 //BA.debugLineNum = 4259842;BA.debugLine="Return SubExists(CallObject,EventName, param)";
if (true) return [self->___c SubExists:_callobject :_eventname :_param];
B4IRDebugUtils.currentLine=4259846;
 //BA.debugLineNum = 4259846;BA.debugLine="End Sub";
return false;
}
- (NSString*)  _class_globals:(b4i_servletrequest*) __ref{
__ref = self;
self->__main=[b4i_main new];
B4IRDebugUtils.currentModule=@"servletrequest";
B4IRDebugUtils.currentLine=1441792;
 //BA.debugLineNum = 1441792;BA.debugLine="Sub Class_Globals";
B4IRDebugUtils.currentLine=1441793;
 //BA.debugLineNum = 1441793;BA.debugLine="Type tUser (Address As String, nc As Int, nonce A";
;
B4IRDebugUtils.currentLine=1441794;
 //BA.debugLineNum = 1441794;BA.debugLine="Private DDay() As String = Array As String(\"\",\"Su";
self->__dday = [[B4IArray alloc]initObjectsWithData:@[[B4I nilToNSNull:@""],[B4I nilToNSNull:@"Sun"],[B4I nilToNSNull:@"Mon"],[B4I nilToNSNull:@"Tue"],[B4I nilToNSNull:@"Wed"],[B4I nilToNSNull:@"Thu"],[B4I nilToNSNull:@"Fri"],[B4I nilToNSNull:@"Sat"]]];
B4IRDebugUtils.currentLine=1441795;
 //BA.debugLineNum = 1441795;BA.debugLine="Private MMmonth() As String = Array As String(\"\",";
self->__mmmonth = [[B4IArray alloc]initObjectsWithData:@[[B4I nilToNSNull:@""],[B4I nilToNSNull:@"January"],[B4I nilToNSNull:@"February"],[B4I nilToNSNull:@"March"],[B4I nilToNSNull:@"April"],[B4I nilToNSNull:@"May"],[B4I nilToNSNull:@"June"],[B4I nilToNSNull:@"July"],[B4I nilToNSNull:@"August"],[B4I nilToNSNull:@"September"],[B4I nilToNSNull:@"October"],[B4I nilToNSNull:@"November"],[B4I nilToNSNull:@"December"]]];
B4IRDebugUtils.currentLine=1441797;
 //BA.debugLineNum = 1441797;BA.debugLine="Private client As Socket";
self->__client = [B4ISocketWrapper new];
B4IRDebugUtils.currentLine=1441798;
 //BA.debugLineNum = 1441798;BA.debugLine="Private astream As AsyncStreams";
self->__astream = [B4IAsyncStreams new];
B4IRDebugUtils.currentLine=1441799;
 //BA.debugLineNum = 1441799;BA.debugLine="Private mCallBack As Object";
self->__mcallback = [NSObject new];
B4IRDebugUtils.currentLine=1441800;
 //BA.debugLineNum = 1441800;BA.debugLine="Private mEventName As String";
self->__meventname = @"";
B4IRDebugUtils.currentLine=1441801;
 //BA.debugLineNum = 1441801;BA.debugLine="Private bc As ByteConverter";
self->__bc = [B4IByteConverter new];
B4IRDebugUtils.currentLine=1441803;
 //BA.debugLineNum = 1441803;BA.debugLine="Private method As String = \"\"";
self->__method = @"";
B4IRDebugUtils.currentLine=1441804;
 //BA.debugLineNum = 1441804;BA.debugLine="Private RequestURI As String = \"\"";
self->__requesturi = @"";
B4IRDebugUtils.currentLine=1441805;
 //BA.debugLineNum = 1441805;BA.debugLine="Private RequestHOST As String = \"\"";
self->__requesthost = @"";
B4IRDebugUtils.currentLine=1441806;
 //BA.debugLineNum = 1441806;BA.debugLine="Private Address As String =\"\"";
self->__address = @"";
B4IRDebugUtils.currentLine=1441807;
 //BA.debugLineNum = 1441807;BA.debugLine="Private ConnPort As String = \"\"";
self->__connport = @"";
B4IRDebugUtils.currentLine=1441808;
 //BA.debugLineNum = 1441808;BA.debugLine="Public ID As String";
self->__id = @"";
B4IRDebugUtils.currentLine=1441810;
 //BA.debugLineNum = 1441810;BA.debugLine="Public RequestHeader As Map";
self->__requestheader = [B4IMap new];
B4IRDebugUtils.currentLine=1441811;
 //BA.debugLineNum = 1441811;BA.debugLine="Public RequestParameter As Map";
self->__requestparameter = [B4IMap new];
B4IRDebugUtils.currentLine=1441812;
 //BA.debugLineNum = 1441812;BA.debugLine="Public RequestCookies As Map";
self->__requestcookies = [B4IMap new];
B4IRDebugUtils.currentLine=1441813;
 //BA.debugLineNum = 1441813;BA.debugLine="Public RequestPostDataRow As List";
self->__requestpostdatarow = [B4IList new];
B4IRDebugUtils.currentLine=1441814;
 //BA.debugLineNum = 1441814;BA.debugLine="Public ConnectionAlive As Boolean = True";
self->__connectionalive = true;
B4IRDebugUtils.currentLine=1441815;
 //BA.debugLineNum = 1441815;BA.debugLine="Public CharacterEncoding As String = \"UTF-8\" '\"IS";
self->__characterencoding = @"UTF-8";
B4IRDebugUtils.currentLine=1441821;
 //BA.debugLineNum = 1441821;BA.debugLine="Private CallServer As httpServer";
self->__callserver = [b4i_httpserver new];
B4IRDebugUtils.currentLine=1441822;
 //BA.debugLineNum = 1441822;BA.debugLine="Private Response As ServletResponse";
self->__response = [b4i_servletresponse new];
B4IRDebugUtils.currentLine=1441823;
 //BA.debugLineNum = 1441823;BA.debugLine="Private Users As Map";
self->__users = [B4IMap new];
B4IRDebugUtils.currentLine=1441824;
 //BA.debugLineNum = 1441824;BA.debugLine="Private UserName As String = \"\"";
self->__username = @"";
B4IRDebugUtils.currentLine=1441825;
 //BA.debugLineNum = 1441825;BA.debugLine="Private Password As String = \"\"";
self->__password = @"";
B4IRDebugUtils.currentLine=1441826;
 //BA.debugLineNum = 1441826;BA.debugLine="Private OtherData As Boolean = False";
self->__otherdata = false;
B4IRDebugUtils.currentLine=1441827;
 //BA.debugLineNum = 1441827;BA.debugLine="Private Cache() As Byte";
self->__cache = [[B4IArray alloc]initBytes:@[@((int) (0))]];
B4IRDebugUtils.currentLine=1441828;
 //BA.debugLineNum = 1441828;BA.debugLine="Private BBB() As Byte";
self->__bbb = [[B4IArray alloc]initBytes:@[@((int) (0))]];
B4IRDebugUtils.currentLine=1441829;
 //BA.debugLineNum = 1441829;BA.debugLine="Private su As StringUtils";
self->__su = [iStringUtils new];
B4IRDebugUtils.currentLine=1441831;
 //BA.debugLineNum = 1441831;BA.debugLine="Private LogPrivate As Boolean = False";
self->__logprivate = false;
B4IRDebugUtils.currentLine=1441832;
 //BA.debugLineNum = 1441832;BA.debugLine="Public LogActive As Boolean = False";
self->__logactive = false;
B4IRDebugUtils.currentLine=1441833;
 //BA.debugLineNum = 1441833;BA.debugLine="Public ContentType As String = \"\"";
self->__contenttype = @"";
B4IRDebugUtils.currentLine=1441834;
 //BA.debugLineNum = 1441834;BA.debugLine="Public ContentLength As Long = 0";
self->__contentlength = (long long) (0);
B4IRDebugUtils.currentLine=1441835;
 //BA.debugLineNum = 1441835;BA.debugLine="Public MultipartFilename As Map";
self->__multipartfilename = [B4IMap new];
B4IRDebugUtils.currentLine=1441836;
 //BA.debugLineNum = 1441836;BA.debugLine="Public LogFirstRefuse As Boolean = False";
self->__logfirstrefuse = false;
B4IRDebugUtils.currentLine=1441837;
 //BA.debugLineNum = 1441837;BA.debugLine="Public Timeout As Long = 10000";
self->__timeout = (long long) (10000);
B4IRDebugUtils.currentLine=1441838;
 //BA.debugLineNum = 1441838;BA.debugLine="Private LastBrowserComunicate As Long = 0";
self->__lastbrowsercomunicate = (long long) (0);
B4IRDebugUtils.currentLine=1441839;
 //BA.debugLineNum = 1441839;BA.debugLine="Private gzip=False, deflate=False As Boolean";
self->__gzip = false;
self->__deflate = false;
B4IRDebugUtils.currentLine=1441841;
 //BA.debugLineNum = 1441841;BA.debugLine="Private WebSocket As Boolean = False";
self->__websocket = false;
B4IRDebugUtils.currentLine=1441842;
 //BA.debugLineNum = 1441842;BA.debugLine="Private keyWebSocket As String = \"\"";
self->__keywebsocket = @"";
B4IRDebugUtils.currentLine=1441843;
 //BA.debugLineNum = 1441843;BA.debugLine="Private WebSocketString As String";
self->__websocketstring = @"";
B4IRDebugUtils.currentLine=1441844;
 //BA.debugLineNum = 1441844;BA.debugLine="Private LastOpCode As Byte = 0";
self->__lastopcode = (unsigned char) (0);
B4IRDebugUtils.currentLine=1441845;
 //BA.debugLineNum = 1441845;BA.debugLine="End Sub";
return @"";
}
- (BOOL)  _connected:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"connected"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"connected" :nil]).boolValue;}
B4IRDebugUtils.currentLine=1703936;
 //BA.debugLineNum = 1703936;BA.debugLine="Public Sub Connected As Boolean";
B4IRDebugUtils.currentLine=1703937;
 //BA.debugLineNum = 1703937;BA.debugLine="Return client.Connected";
if (true) return [__ref->__client /*B4ISocketWrapper**/  Connected];
B4IRDebugUtils.currentLine=1703938;
 //BA.debugLineNum = 1703938;BA.debugLine="End Sub";
return false;
}
- (NSString*)  _decode:(b4i_servletrequest*) __ref :(NSString*) _s{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"decode"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"decode:" :@[[B4I nilToNSNull:_s]]]);}
NSString* _dec = @"";
int _i = 0;
int _ascii = 0;
B4IRDebugUtils.currentLine=3735552;
 //BA.debugLineNum = 3735552;BA.debugLine="Private Sub decode(S As String) As String";
B4IRDebugUtils.currentLine=3735553;
 //BA.debugLineNum = 3735553;BA.debugLine="Dim dec As String = \"\"";
_dec = @"";
B4IRDebugUtils.currentLine=3735554;
 //BA.debugLineNum = 3735554;BA.debugLine="For i=0 To S.Length-1";
{
const int step2 = 1;
const int limit2 = (int) ([_s Length]-1);
_i = (int) (0) ;
for (;_i <= limit2 ;_i = _i + step2 ) {
B4IRDebugUtils.currentLine=3735556;
 //BA.debugLineNum = 3735556;BA.debugLine="If Asc(S.CharAt(i))=37 Then";
if (((int)([_s CharAt:_i]))==37) { 
B4IRDebugUtils.currentLine=3735557;
 //BA.debugLineNum = 3735557;BA.debugLine="Dim Ascii As Int = ((Asc(S.CharAt(i+1))-48)*16)";
_ascii = (int) (((((int)([_s CharAt:(int) (_i+1)]))-48)*16)+(((int)([_s CharAt:(int) (_i+2)]))-48));
B4IRDebugUtils.currentLine=3735558;
 //BA.debugLineNum = 3735558;BA.debugLine="dec= dec & Chr(Ascii)";
_dec = [@[_dec,[self.bi CharToString:((unichar)(_ascii))]] componentsJoinedByString:@""];
B4IRDebugUtils.currentLine=3735559;
 //BA.debugLineNum = 3735559;BA.debugLine="i=i+3";
_i = (int) (_i+3);
 }else {
B4IRDebugUtils.currentLine=3735561;
 //BA.debugLineNum = 3735561;BA.debugLine="dec=dec & S.charat(i)";
_dec = [@[_dec,[self.bi CharToString:[_s CharAt:_i]]] componentsJoinedByString:@""];
 };
 }
};
B4IRDebugUtils.currentLine=3735565;
 //BA.debugLineNum = 3735565;BA.debugLine="Return dec";
if (true) return _dec;
B4IRDebugUtils.currentLine=3735566;
 //BA.debugLineNum = 3735566;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _dectohex:(b4i_servletrequest*) __ref :(int) _dec{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"dectohex"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"dectohex:" :@[@(_dec)]]);}
B4IRDebugUtils.currentLine=3080192;
 //BA.debugLineNum = 3080192;BA.debugLine="Private Sub DecToHex(Dec As Int) As String 'ignore";
B4IRDebugUtils.currentLine=3080193;
 //BA.debugLineNum = 3080193;BA.debugLine="Return bc.HexFromBytes(bc.IntsToBytes(Array As In";
if (true) return [__ref->__bc /*B4IByteConverter**/  HexFromBytes:[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_dec)]]]];
B4IRDebugUtils.currentLine=3080194;
 //BA.debugLineNum = 3080194;BA.debugLine="End Sub";
return @"";
}
- (B4IArray*)  _demasked:(b4i_servletrequest*) __ref :(B4IArray*) _maskdata :(B4IArray*) _maskingkey :(long long) _length :(long long) _spacement :(BOOL) _masked{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"demasked"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"demasked:::::" :@[[B4I nilToNSNull:_maskdata],[B4I nilToNSNull:_maskingkey],@(_length),@(_spacement),@(_masked)]]);}
B4IArray* _datafree = nil;
int _i = 0;
int _smask = 0;
B4IRDebugUtils.currentLine=3997696;
 //BA.debugLineNum = 3997696;BA.debugLine="Private Sub DeMasked(MaskData() As Byte, Maskingke";
B4IRDebugUtils.currentLine=3997697;
 //BA.debugLineNum = 3997697;BA.debugLine="Dim DataFree(Length) As Byte";
_datafree = [[B4IArray alloc]initBytes:@[@((int) (_length))]];
B4IRDebugUtils.currentLine=3997698;
 //BA.debugLineNum = 3997698;BA.debugLine="If Masked Then";
if (_masked) { 
B4IRDebugUtils.currentLine=3997701;
 //BA.debugLineNum = 3997701;BA.debugLine="For i=0 To Length-1";
{
const int step3 = 1;
const int limit3 = (int) (_length-1);
_i = (int) (0) ;
for (;_i <= limit3 ;_i = _i + step3 ) {
B4IRDebugUtils.currentLine=3997702;
 //BA.debugLineNum = 3997702;BA.debugLine="Dim Smask As Int = Bit.And(0xFF, MaskData(i+Spa";
_smask = (((int) (0xff)) & ((int) ([_maskdata getByteFast:(int) (_i+_spacement)])));
B4IRDebugUtils.currentLine=3997703;
 //BA.debugLineNum = 3997703;BA.debugLine="DataFree(i)=Bit.Xor(Smask,Bit.And(0xFF,Maskingk";
[_datafree setByteFast:_i:@((unsigned char) (((_smask) ^ ((((int) (0xff)) & ((int) ([_maskingkey getByteFast:(int) (_i%4)])))))))];
 }
};
 }else {
B4IRDebugUtils.currentLine=3997706;
 //BA.debugLineNum = 3997706;BA.debugLine="Bit.ArrayCopy(MaskData,Spacement,DataFree,0,Leng";
B4I_MEMCPY((_maskdata)->internalBuffer + ((int) (_spacement)), (_datafree)->internalBuffer + ((int) (0)),(int) (_length));
 };
B4IRDebugUtils.currentLine=3997708;
 //BA.debugLineNum = 3997708;BA.debugLine="Return DataFree";
if (true) return _datafree;
B4IRDebugUtils.currentLine=3997709;
 //BA.debugLineNum = 3997709;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _elaboratehandshake:(b4i_servletrequest*) __ref :(NSString*) _handshake :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"elaboratehandshake"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"elaboratehandshake::" :@[[B4I nilToNSNull:_handshake],[B4I nilToNSNull:_data]]]);}
NSString* _row = @"";
B4IArray* _p = nil;
NSString* _pm = @"";
B4IArray* _pms = nil;
NSString* _parm = @"";
B4IArray* _ck = nil;
NSString* _s = @"";
int _i = 0;
B4IRDebugUtils.currentLine=2818048;
 //BA.debugLineNum = 2818048;BA.debugLine="Private Sub ElaborateHandShake(HandShake As String";
B4IRDebugUtils.currentLine=2818050;
 //BA.debugLineNum = 2818050;BA.debugLine="BBB = ArrayInitialize";
__ref->__bbb /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
B4IRDebugUtils.currentLine=2818051;
 //BA.debugLineNum = 2818051;BA.debugLine="BBB = ArrayAppend(BBB,Data)";
__ref->__bbb /*B4IArray**/  = [__ref _arrayappend /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :_data];
B4IRDebugUtils.currentLine=2818054;
 //BA.debugLineNum = 2818054;BA.debugLine="RequestHeader.Clear";
[__ref->__requestheader /*B4IMap**/  Clear];
B4IRDebugUtils.currentLine=2818055;
 //BA.debugLineNum = 2818055;BA.debugLine="RequestParameter.Clear";
[__ref->__requestparameter /*B4IMap**/  Clear];
B4IRDebugUtils.currentLine=2818056;
 //BA.debugLineNum = 2818056;BA.debugLine="RequestCookies.Clear";
[__ref->__requestcookies /*B4IMap**/  Clear];
B4IRDebugUtils.currentLine=2818057;
 //BA.debugLineNum = 2818057;BA.debugLine="RequestPostDataRow.Clear";
[__ref->__requestpostdatarow /*B4IList**/  Clear];
B4IRDebugUtils.currentLine=2818059;
 //BA.debugLineNum = 2818059;BA.debugLine="For Each Row As String  In Regex.Split(CRLF,HandS";
{
const id<B4IIterable> group7 = [[self->___c Regex] Split:@"\n" :_handshake];
const int groupLen7 = group7.Size
;int index7 = 0;
;
for (; index7 < groupLen7;index7++){
_row = [group7 Get:index7];
B4IRDebugUtils.currentLine=2818060;
 //BA.debugLineNum = 2818060;BA.debugLine="Row=Row.Replace(Chr(13),\"\").Replace(Chr(10),\"\")";
_row = [[_row Replace:[self.bi CharToString:((unichar)((int) (13)))] :@""] Replace:[self.bi CharToString:((unichar)((int) (10)))] :@""];
B4IRDebugUtils.currentLine=2818061;
 //BA.debugLineNum = 2818061;BA.debugLine="Dim p() As String = Regex.Split(\" \",Row)";
_p = [[self->___c Regex] Split:@" " :_row];
B4IRDebugUtils.currentLine=2818063;
 //BA.debugLineNum = 2818063;BA.debugLine="Select p(0).ToUpperCase";
switch ([self.bi switchObjectToInt:[((NSString*)[_p getObjectFast:(int) (0)]) ToUpperCase] :@[@"GET",@"POST",@"CONTENT-LENGTH:",@"CONTENT-TYPE:",@"HOST:",@"COOKIE:",@"CONNECTION:",@"UPGRADE:",@"SEC-WEBSOCKET-KEY:",[@"Accept-Encoding:" ToUpperCase]]]) {
case 0: 
case 1: {
B4IRDebugUtils.currentLine=2818065;
 //BA.debugLineNum = 2818065;BA.debugLine="method=p(0).ToUpperCase";
__ref->__method /*NSString**/  = [((NSString*)[_p getObjectFast:(int) (0)]) ToUpperCase];
B4IRDebugUtils.currentLine=2818066;
 //BA.debugLineNum = 2818066;BA.debugLine="RequestURI=p(1)";
__ref->__requesturi /*NSString**/  = ((NSString*)[_p getObjectFast:(int) (1)]);
B4IRDebugUtils.currentLine=2818067;
 //BA.debugLineNum = 2818067;BA.debugLine="If RequestURI.IndexOf(\"?\")>-1 Then";
if ([__ref->__requesturi /*NSString**/  IndexOf:@"?"]>-1) { 
B4IRDebugUtils.currentLine=2818068;
 //BA.debugLineNum = 2818068;BA.debugLine="Dim pm As String = RequestURI.SubString(Reque";
_pm = [__ref->__requesturi /*NSString**/  SubString:(int) ([__ref->__requesturi /*NSString**/  IndexOf:@"?"]+1)];
B4IRDebugUtils.currentLine=2818069;
 //BA.debugLineNum = 2818069;BA.debugLine="RequestURI=RequestURI.SubString2(0,RequestURI";
__ref->__requesturi /*NSString**/  = [__ref->__requesturi /*NSString**/  SubString2:(int) (0) :[__ref->__requesturi /*NSString**/  IndexOf:@"?"]];
B4IRDebugUtils.currentLine=2818071;
 //BA.debugLineNum = 2818071;BA.debugLine="Dim pms() As String = Regex.Split(\"&\",pm)";
_pms = [[self->___c Regex] Split:@"&" :_pm];
B4IRDebugUtils.currentLine=2818073;
 //BA.debugLineNum = 2818073;BA.debugLine="For Each parm As String In pms";
{
const id<B4IIterable> group18 = _pms;
const int groupLen18 = group18.Size
;int index18 = 0;
;
for (; index18 < groupLen18;index18++){
_parm = [group18 Get:index18];
B4IRDebugUtils.currentLine=2818074;
 //BA.debugLineNum = 2818074;BA.debugLine="If parm.IndexOf(\"=\")>-1 Then";
if ([_parm IndexOf:@"="]>-1) { 
B4IRDebugUtils.currentLine=2818075;
 //BA.debugLineNum = 2818075;BA.debugLine="RequestParameter.Put(parm.SubString2(0,parm";
[__ref->__requestparameter /*B4IMap**/  Put:(NSObject*)([_parm SubString2:(int) (0) :[_parm IndexOf:@"="]]) :(NSObject*)([_parm SubString:(int) ([_parm IndexOf:@"="]+1)])];
 };
 }
};
 };
B4IRDebugUtils.currentLine=2818079;
 //BA.debugLineNum = 2818079;BA.debugLine="If p.Length>2 Then";
if (_p.Length>2) { 
B4IRDebugUtils.currentLine=2818081;
 //BA.debugLineNum = 2818081;BA.debugLine="If p(2).ToLowerCase.StartsWith(\"http/1\")=Fals";
if ([[((NSString*)[_p getObjectFast:(int) (2)]) ToLowerCase] StartsWith:@"http/1"]==false) { 
B4IRDebugUtils.currentLine=2818082;
 //BA.debugLineNum = 2818082;BA.debugLine="Close";
[__ref _close /*NSString**/ :nil];
 };
 }else {
B4IRDebugUtils.currentLine=2818086;
 //BA.debugLineNum = 2818086;BA.debugLine="Close";
[__ref _close /*NSString**/ :nil];
 };
 break; }
case 2: {
B4IRDebugUtils.currentLine=2818089;
 //BA.debugLineNum = 2818089;BA.debugLine="ContentLength=p(1)";
__ref->__contentlength /*long long*/  = [self.bi ObjectToLongNumber:((NSString*)[_p getObjectFast:(int) (1)])];
B4IRDebugUtils.currentLine=2818090;
 //BA.debugLineNum = 2818090;BA.debugLine="If ContentLength>BBB.Length Then";
if (__ref->__contentlength /*long long*/ >__ref->__bbb /*B4IArray**/ .Length) { 
B4IRDebugUtils.currentLine=2818091;
 //BA.debugLineNum = 2818091;BA.debugLine="OtherData=True";
__ref->__otherdata /*BOOL*/  = true;
 }else {
B4IRDebugUtils.currentLine=2818097;
 //BA.debugLineNum = 2818097;BA.debugLine="OtherData=False";
__ref->__otherdata /*BOOL*/  = false;
B4IRDebugUtils.currentLine=2818098;
 //BA.debugLineNum = 2818098;BA.debugLine="If ContentLength<BBB.Length Then";
if (__ref->__contentlength /*long long*/ <__ref->__bbb /*B4IArray**/ .Length) { 
B4IRDebugUtils.currentLine=2818100;
 //BA.debugLineNum = 2818100;BA.debugLine="Cache=ArrayAppend(Cache,SubArray(BBB,Content";
__ref->__cache /*B4IArray**/  = [__ref _arrayappend /*B4IArray**/ :nil :__ref->__cache /*B4IArray**/  :[__ref _subarray /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (__ref->__contentlength /*long long*/ )]];
B4IRDebugUtils.currentLine=2818102;
 //BA.debugLineNum = 2818102;BA.debugLine="BBB=ArrayRemove(BBB,ContentLength,BBB.Length";
__ref->__bbb /*B4IArray**/  = [__ref _arrayremove /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (__ref->__contentlength /*long long*/ ) :__ref->__bbb /*B4IArray**/ .Length];
 };
 };
 break; }
case 3: {
B4IRDebugUtils.currentLine=2818106;
 //BA.debugLineNum = 2818106;BA.debugLine="ContentType=p(1).Replace(\";\",\"\").Trim";
__ref->__contenttype /*NSString**/  = [[((NSString*)[_p getObjectFast:(int) (1)]) Replace:@";" :@""] Trim];
B4IRDebugUtils.currentLine=2818107;
 //BA.debugLineNum = 2818107;BA.debugLine="If p.Length>2 Then";
if (_p.Length>2) { 
B4IRDebugUtils.currentLine=2818108;
 //BA.debugLineNum = 2818108;BA.debugLine="If P(2).indexof(\"charset=\")>-1 Then Character";
if ([((NSString*)[_p getObjectFast:(int) (2)]) IndexOf:@"charset="]>-1) { 
__ref->__characterencoding /*NSString**/  = [[((NSString*)[_p getObjectFast:(int) (2)]) Replace:@"charset=" :@""] Trim];};
 };
 break; }
case 4: {
B4IRDebugUtils.currentLine=2818111;
 //BA.debugLineNum = 2818111;BA.debugLine="If p.Length>1 Then";
if (_p.Length>1) { 
B4IRDebugUtils.currentLine=2818112;
 //BA.debugLineNum = 2818112;BA.debugLine="RequestHOST=p(1)";
__ref->__requesthost /*NSString**/  = ((NSString*)[_p getObjectFast:(int) (1)]);
B4IRDebugUtils.currentLine=2818113;
 //BA.debugLineNum = 2818113;BA.debugLine="If RequestHOST.IndexOf(\":\")>-1 Then RequestHO";
if ([__ref->__requesthost /*NSString**/  IndexOf:@":"]>-1) { 
__ref->__requesthost /*NSString**/  = [__ref->__requesthost /*NSString**/  SubString2:(int) (0) :[__ref->__requesthost /*NSString**/  IndexOf:@":"]];};
 }else {
B4IRDebugUtils.currentLine=2818115;
 //BA.debugLineNum = 2818115;BA.debugLine="RequestHOST=\"\"";
__ref->__requesthost /*NSString**/  = @"";
 };
 break; }
case 5: {
B4IRDebugUtils.currentLine=2818118;
 //BA.debugLineNum = 2818118;BA.debugLine="Dim Ck() As String = Regex.Split(\";\",Row.Repla";
_ck = [[self->___c Regex] Split:@";" :[_row Replace:@"Cookie: " :@""]];
B4IRDebugUtils.currentLine=2818119;
 //BA.debugLineNum = 2818119;BA.debugLine="For Each s As String In Ck";
{
const id<B4IIterable> group56 = _ck;
const int groupLen56 = group56.Size
;int index56 = 0;
;
for (; index56 < groupLen56;index56++){
_s = [group56 Get:index56];
B4IRDebugUtils.currentLine=2818120;
 //BA.debugLineNum = 2818120;BA.debugLine="If s.IndexOf(\"=\")>-1 Then";
if ([_s IndexOf:@"="]>-1) { 
B4IRDebugUtils.currentLine=2818121;
 //BA.debugLineNum = 2818121;BA.debugLine="RequestCookies.Put(s.SubString2(0, s.IndexOf";
[__ref->__requestcookies /*B4IMap**/  Put:(NSObject*)([[_s SubString2:(int) (0) :[_s IndexOf:@"="]] Trim]) :(NSObject*)([_s SubString:(int) ([_s IndexOf:@"="]+1)])];
 };
 }
};
 break; }
case 6: {
B4IRDebugUtils.currentLine=2818125;
 //BA.debugLineNum = 2818125;BA.debugLine="If p(1).ToLowerCase.IndexOf(\"keep-alive\")>-1 T";
if ([[((NSString*)[_p getObjectFast:(int) (1)]) ToLowerCase] IndexOf:@"keep-alive"]>-1) { 
B4IRDebugUtils.currentLine=2818126;
 //BA.debugLineNum = 2818126;BA.debugLine="ConnectionAlive=True";
__ref->__connectionalive /*BOOL*/  = true;
 }else if([[((NSString*)[_p getObjectFast:(int) (1)]) ToLowerCase] IndexOf:@"close"]>-1) { 
B4IRDebugUtils.currentLine=2818128;
 //BA.debugLineNum = 2818128;BA.debugLine="ConnectionAlive=False";
__ref->__connectionalive /*BOOL*/  = false;
 };
 break; }
case 7: {
B4IRDebugUtils.currentLine=2818131;
 //BA.debugLineNum = 2818131;BA.debugLine="If p(1).ToLowerCase=\"websocket\" Then WebSocket";
if ([[((NSString*)[_p getObjectFast:(int) (1)]) ToLowerCase] isEqual:@"websocket"]) { 
__ref->__websocket /*BOOL*/  = true;};
 break; }
case 8: {
B4IRDebugUtils.currentLine=2818133;
 //BA.debugLineNum = 2818133;BA.debugLine="WebSocket=True";
__ref->__websocket /*BOOL*/  = true;
B4IRDebugUtils.currentLine=2818134;
 //BA.debugLineNum = 2818134;BA.debugLine="keyWebSocket=p(1)";
__ref->__keywebsocket /*NSString**/  = ((NSString*)[_p getObjectFast:(int) (1)]);
 break; }
case 9: {
B4IRDebugUtils.currentLine=2818136;
 //BA.debugLineNum = 2818136;BA.debugLine="For i=1 To p.Length-1";
{
const int step73 = 1;
const int limit73 = (int) (_p.Length-1);
_i = (int) (1) ;
for (;_i <= limit73 ;_i = _i + step73 ) {
B4IRDebugUtils.currentLine=2818137;
 //BA.debugLineNum = 2818137;BA.debugLine="If p(I).Contains(\"gzip\") Then gzip=True";
if ([((NSString*)[_p getObjectFast:_i]) Contains:@"gzip"]) { 
__ref->__gzip /*BOOL*/  = true;};
B4IRDebugUtils.currentLine=2818138;
 //BA.debugLineNum = 2818138;BA.debugLine="If p(I).Contains(\"deflate\") Then deflate=True";
if ([((NSString*)[_p getObjectFast:_i]) Contains:@"deflate"]) { 
__ref->__deflate /*BOOL*/  = true;};
 }
};
 break; }
default: {
B4IRDebugUtils.currentLine=2818141;
 //BA.debugLineNum = 2818141;BA.debugLine="If p(0).IndexOf(\":\")>-1 Then";
if ([((NSString*)[_p getObjectFast:(int) (0)]) IndexOf:@":"]>-1) { 
B4IRDebugUtils.currentLine=2818142;
 //BA.debugLineNum = 2818142;BA.debugLine="RequestHeader.Put(Row.SubString2(0, Row.Index";
[__ref->__requestheader /*B4IMap**/  Put:(NSObject*)([_row SubString2:(int) (0) :[_row IndexOf:@":"]]) :(NSObject*)([_row SubString:(int) ([_row IndexOf:@":"]+1)])];
 };
 break; }
}
;
 }
};
B4IRDebugUtils.currentLine=2818147;
 //BA.debugLineNum = 2818147;BA.debugLine="If RequestHOST=\"\" Then";
if ([__ref->__requesthost /*NSString**/  isEqual:@""]) { 
B4IRDebugUtils.currentLine=2818149;
 //BA.debugLineNum = 2818149;BA.debugLine="Close";
[__ref _close /*NSString**/ :nil];
 }else if(__ref->__websocket /*BOOL*/ ) { 
B4IRDebugUtils.currentLine=2818151;
 //BA.debugLineNum = 2818151;BA.debugLine="LgC(HandShake.Replace(Chr(13),\"\"),0xFF0000FF)";
[__ref _lgc /*NSString**/ :nil :[_handshake Replace:[self.bi CharToString:((unichar)((int) (13)))] :@""] :(int) (0xff0000ff)];
B4IRDebugUtils.currentLine=2818152;
 //BA.debugLineNum = 2818152;BA.debugLine="SendAcceptKeyWs";
[__ref _sendacceptkeyws /*NSString**/ :nil];
B4IRDebugUtils.currentLine=2818153;
 //BA.debugLineNum = 2818153;BA.debugLine="BBB = ArrayInitialize";
__ref->__bbb /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
 }else if(__ref->__otherdata /*BOOL*/ ==false) { 
B4IRDebugUtils.currentLine=2818155;
 //BA.debugLineNum = 2818155;BA.debugLine="extractParameterFromData";
[__ref _extractparameterfromdata /*NSString**/ :nil];
B4IRDebugUtils.currentLine=2818156;
 //BA.debugLineNum = 2818156;BA.debugLine="CallEvent";
[__ref _callevent /*NSString**/ :nil];
 };
B4IRDebugUtils.currentLine=2818158;
 //BA.debugLineNum = 2818158;BA.debugLine="End Sub";
return @"";
}
- (B4IArray*)  _subarray:(b4i_servletrequest*) __ref :(B4IArray*) _data :(int) _pos{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"subarray"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"subarray::" :@[[B4I nilToNSNull:_data],@(_pos)]]);}
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=4718592;
 //BA.debugLineNum = 4718592;BA.debugLine="Private Sub SubArray(Data() As Byte, Pos As Int) A";
B4IRDebugUtils.currentLine=4718593;
 //BA.debugLineNum = 4718593;BA.debugLine="Dim Fn(Data.Length-Pos) As Byte";
_fn = [[B4IArray alloc]initBytes:@[@((int) (_data.Length-_pos))]];
B4IRDebugUtils.currentLine=4718595;
 //BA.debugLineNum = 4718595;BA.debugLine="Bit.ArrayCopy(Data,Pos,Fn,0,Data.Length-Pos)";
B4I_MEMCPY((_data)->internalBuffer + (_pos), (_fn)->internalBuffer + ((int) (0)),(int) (_data.Length-_pos));
B4IRDebugUtils.currentLine=4718596;
 //BA.debugLineNum = 4718596;BA.debugLine="Return Fn";
if (true) return _fn;
B4IRDebugUtils.currentLine=4718597;
 //BA.debugLineNum = 4718597;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _lgc:(b4i_servletrequest*) __ref :(NSString*) _message :(int) _color{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"lgc"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"lgc::" :@[[B4I nilToNSNull:_message],@(_color)]]);}
B4IRDebugUtils.currentLine=4128768;
 //BA.debugLineNum = 4128768;BA.debugLine="Private Sub LgC(Message As String, Color As Int) '";
B4IRDebugUtils.currentLine=4128772;
 //BA.debugLineNum = 4128772;BA.debugLine="If LogActive Then LogColor(Message,Color)";
if (__ref->__logactive /*BOOL*/ ) { 
[self->___c LogImpl:@"34128772" :_message :_color];};
B4IRDebugUtils.currentLine=4128774;
 //BA.debugLineNum = 4128774;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendacceptkeyws:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"sendacceptkeyws"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendacceptkeyws" :nil]);}
NSString* _acpdef = @"";
NSString* _sw = @"";
B4IRDebugUtils.currentLine=3866624;
 //BA.debugLineNum = 3866624;BA.debugLine="Private Sub SendAcceptKeyWs";
B4IRDebugUtils.currentLine=3866625;
 //BA.debugLineNum = 3866625;BA.debugLine="Dim AcpDef As String = \"\" '\"Sec-WebSocket-Extensi";
_acpdef = @"";
B4IRDebugUtils.currentLine=3866626;
 //BA.debugLineNum = 3866626;BA.debugLine="Dim Sw As String = $\"HTTP/1.1 101 Switching Proto";
_sw = ([@[@"HTTP/1.1 101 Switching Protocols\n",@"Date: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@"Connection: Upgrade\n",@"Upgrade: WebSocket\n",@"Sec-WebSocket-Accept: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _websocketkey /*NSString**/ :nil :__ref->__keywebsocket /*NSString**/ ])],@"\n",@"Server: HttpServer (B4X)\n",@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_acpdef)],@"\n",@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3866638;
 //BA.debugLineNum = 3866638;BA.debugLine="Response.Write(Sw)";
[__ref->__response /*b4i_servletresponse**/  _write /*NSString**/ :nil :_sw];
B4IRDebugUtils.currentLine=3866640;
 //BA.debugLineNum = 3866640;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_SwitchToWe";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_SwitchToWebSocket"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_SwitchToWebSocket"] componentsJoinedByString:@""] :self :(NSObject*)(__ref->__response /*b4i_servletresponse**/ )];};
B4IRDebugUtils.currentLine=3866641;
 //BA.debugLineNum = 3866641;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _extractparameterfromdata:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"extractparameterfromdata"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"extractparameterfromdata" :nil]);}
int _index = 0;
NSString* _cnt = @"";
B4IArray* _m = nil;
NSString* _rw = @"";
NSString* _name = @"";
B4IArray* _rows = nil;
B4IArray* _params = nil;
NSString* _parm = @"";
B4IRDebugUtils.currentLine=2949120;
 //BA.debugLineNum = 2949120;BA.debugLine="Private Sub extractParameterFromData";
B4IRDebugUtils.currentLine=2949122;
 //BA.debugLineNum = 2949122;BA.debugLine="If BBB.Length>0 Then";
if (__ref->__bbb /*B4IArray**/ .Length>0) { 
B4IRDebugUtils.currentLine=2949123;
 //BA.debugLineNum = 2949123;BA.debugLine="If ContentType.ToLowerCase=\"multipart/form-data\"";
if ([[__ref->__contenttype /*NSString**/  ToLowerCase] isEqual:@"multipart/form-data"]) { 
B4IRDebugUtils.currentLine=2949124;
 //BA.debugLineNum = 2949124;BA.debugLine="Dim Index As Int = ArrayIndexOf(BBB,Array As By";
_index = [__ref _arrayindexof /*int*/ :nil :__ref->__bbb /*B4IArray**/  :[[B4IArray alloc]initBytesWithData:@[@((unsigned char) (13)),@((unsigned char) (10)),@((unsigned char) (13)),@((unsigned char) (10))]]];
B4IRDebugUtils.currentLine=2949125;
 //BA.debugLineNum = 2949125;BA.debugLine="MultipartFilename.Clear";
[__ref->__multipartfilename /*B4IMap**/  Clear];
B4IRDebugUtils.currentLine=2949126;
 //BA.debugLineNum = 2949126;BA.debugLine="If Index>-1 Then";
if (_index>-1) { 
B4IRDebugUtils.currentLine=2949127;
 //BA.debugLineNum = 2949127;BA.debugLine="Dim Cnt As String = BytesToString(BBB,0,Index,";
_cnt = [self->___c BytesToString:__ref->__bbb /*B4IArray**/  :(int) (0) :_index :__ref->__characterencoding /*NSString**/ ];
B4IRDebugUtils.currentLine=2949128;
 //BA.debugLineNum = 2949128;BA.debugLine="BBB=ArrayRemove(BBB,0,Index+4)";
__ref->__bbb /*B4IArray**/  = [__ref _arrayremove /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (0) :(int) (_index+4)];
B4IRDebugUtils.currentLine=2949130;
 //BA.debugLineNum = 2949130;BA.debugLine="Index=ArrayIndexOf(BBB,Array As Byte(13,10,13,";
_index = [__ref _arrayindexof /*int*/ :nil :__ref->__bbb /*B4IArray**/  :[[B4IArray alloc]initBytesWithData:@[@((unsigned char) (13)),@((unsigned char) (10)),@((unsigned char) (13)),@((unsigned char) (10))]]];
B4IRDebugUtils.currentLine=2949131;
 //BA.debugLineNum = 2949131;BA.debugLine="Dim M() As Byte = SubArray(BBB,Index)'ignore";
_m = [__ref _subarray /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :_index];
B4IRDebugUtils.currentLine=2949132;
 //BA.debugLineNum = 2949132;BA.debugLine="BBB=ArrayRemove(BBB,Index,BBB.Length)";
__ref->__bbb /*B4IArray**/  = [__ref _arrayremove /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :_index :__ref->__bbb /*B4IArray**/ .Length];
B4IRDebugUtils.currentLine=2949134;
 //BA.debugLineNum = 2949134;BA.debugLine="For Each Rw As String  In Regex.Split(CRLF,Cnt";
{
const id<B4IIterable> group11 = [[self->___c Regex] Split:@"\n" :_cnt];
const int groupLen11 = group11.Size
;int index11 = 0;
;
for (; index11 < groupLen11;index11++){
_rw = [group11 Get:index11];
B4IRDebugUtils.currentLine=2949135;
 //BA.debugLineNum = 2949135;BA.debugLine="If Rw.IndexOf(\"filename=\")>-1 Then";
if ([_rw IndexOf:@"filename="]>-1) { 
B4IRDebugUtils.currentLine=2949136;
 //BA.debugLineNum = 2949136;BA.debugLine="Dim Name As String = Rw.SubString(Rw.IndexOf";
_name = [[[_rw SubString:(int) ([_rw IndexOf:@"filename="]+9)] Replace:[self.bi CharToString:((unichar)((int) (34)))] :@""] Trim];
B4IRDebugUtils.currentLine=2949137;
 //BA.debugLineNum = 2949137;BA.debugLine="If Name<>\"\" Then";
if ([_name isEqual:@""] == false) { 
B4IRDebugUtils.currentLine=2949138;
 //BA.debugLineNum = 2949138;BA.debugLine="MultipartFilename.Put(Name,DateTime.Now & \"";
[__ref->__multipartfilename /*B4IMap**/  Put:(NSObject*)(_name) :(NSObject*)([@[[self.bi NumberToString:@([[self->___c DateTime] Now])],@".tmp"] componentsJoinedByString:@""])];
 };
 };
 }
};
 };
B4IRDebugUtils.currentLine=2949145;
 //BA.debugLineNum = 2949145;BA.debugLine="File.WriteBytes(File.DirTemp,MultipartFilename.";
[[self->___c File] WriteBytes:[[self->___c File] DirTemp] :[self.bi ObjectToString:[__ref->__multipartfilename /*B4IMap**/  Get:(NSObject*)(_name)]] :[__ref _subarray2 /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (0) :(int) (__ref->__bbb /*B4IArray**/ .Length-89)]];
B4IRDebugUtils.currentLine=2949149;
 //BA.debugLineNum = 2949149;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Uploaded";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_UploadedFile"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_UploadedFile"] componentsJoinedByString:@""] :self :(NSObject*)(__ref->__response /*b4i_servletresponse**/ )];};
B4IRDebugUtils.currentLine=2949150;
 //BA.debugLineNum = 2949150;BA.debugLine="BBB=ArrayInitialize";
__ref->__bbb /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
B4IRDebugUtils.currentLine=2949151;
 //BA.debugLineNum = 2949151;BA.debugLine="CallEvent";
[__ref _callevent /*NSString**/ :nil];
 }else {
B4IRDebugUtils.currentLine=2949153;
 //BA.debugLineNum = 2949153;BA.debugLine="If method.ToUpperCase=\"POST\" Then";
if ([[__ref->__method /*NSString**/  ToUpperCase] isEqual:@"POST"]) { 
B4IRDebugUtils.currentLine=2949155;
 //BA.debugLineNum = 2949155;BA.debugLine="Dim rows() As String = Regex.Split(CRLF,BytesT";
_rows = [[self->___c Regex] Split:@"\n" :[self->___c BytesToString:__ref->__bbb /*B4IArray**/  :(int) (0) :__ref->__bbb /*B4IArray**/ .Length :__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=2949156;
 //BA.debugLineNum = 2949156;BA.debugLine="For Each Rw As String In rows";
{
const id<B4IIterable> group27 = _rows;
const int groupLen27 = group27.Size
;int index27 = 0;
;
for (; index27 < groupLen27;index27++){
_rw = [group27 Get:index27];
B4IRDebugUtils.currentLine=2949157;
 //BA.debugLineNum = 2949157;BA.debugLine="Dim Params() As String = Regex.Split(\"&\",Rw)";
_params = [[self->___c Regex] Split:@"&" :_rw];
B4IRDebugUtils.currentLine=2949158;
 //BA.debugLineNum = 2949158;BA.debugLine="For Each parm As String In Params";
{
const id<B4IIterable> group29 = _params;
const int groupLen29 = group29.Size
;int index29 = 0;
;
for (; index29 < groupLen29;index29++){
_parm = [group29 Get:index29];
B4IRDebugUtils.currentLine=2949159;
 //BA.debugLineNum = 2949159;BA.debugLine="parm=decode(parm)";
_parm = [__ref _decode /*NSString**/ :nil :_parm];
B4IRDebugUtils.currentLine=2949160;
 //BA.debugLineNum = 2949160;BA.debugLine="If parm.IndexOf(\"=\")>-1 Then";
if ([_parm IndexOf:@"="]>-1) { 
B4IRDebugUtils.currentLine=2949161;
 //BA.debugLineNum = 2949161;BA.debugLine="RequestParameter.Put(parm.SubString2(0,parm";
[__ref->__requestparameter /*B4IMap**/  Put:(NSObject*)([_parm SubString2:(int) (0) :[_parm IndexOf:@"="]]) :(NSObject*)([_parm SubString:(int) ([_parm IndexOf:@"="]+1)])];
 }else {
B4IRDebugUtils.currentLine=2949163;
 //BA.debugLineNum = 2949163;BA.debugLine="RequestPostDataRow.Add(parm)";
[__ref->__requestpostdatarow /*B4IList**/  Add:(NSObject*)(_parm)];
 };
 }
};
 }
};
 };
 };
 };
B4IRDebugUtils.currentLine=2949171;
 //BA.debugLineNum = 2949171;BA.debugLine="If Cache.Length>0 Then ExtractHandShake";
if (__ref->__cache /*B4IArray**/ .Length>0) { 
[__ref _extracthandshake /*NSString**/ :nil];};
B4IRDebugUtils.currentLine=2949172;
 //BA.debugLineNum = 2949172;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _lgp:(b4i_servletrequest*) __ref :(NSString*) _message{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"lgp"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"lgp:" :@[[B4I nilToNSNull:_message]]]);}
B4IRDebugUtils.currentLine=4194304;
 //BA.debugLineNum = 4194304;BA.debugLine="Private Sub LgP(Message As String)";
B4IRDebugUtils.currentLine=4194305;
 //BA.debugLineNum = 4194305;BA.debugLine="If LogPrivate Then Log(Message)";
if (__ref->__logprivate /*BOOL*/ ) { 
[self->___c LogImpl:@"34194305" :_message :0];};
B4IRDebugUtils.currentLine=4194306;
 //BA.debugLineNum = 4194306;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _getheader:(b4i_servletrequest*) __ref :(NSString*) _name{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getheader"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getheader:" :@[[B4I nilToNSNull:_name]]]);}
B4IRDebugUtils.currentLine=2097152;
 //BA.debugLineNum = 2097152;BA.debugLine="Public Sub GetHeader(Name As String) As String";
B4IRDebugUtils.currentLine=2097153;
 //BA.debugLineNum = 2097153;BA.debugLine="Return RequestHeader.Get(Name)";
if (true) return [self.bi ObjectToString:[__ref->__requestheader /*B4IMap**/  Get:(NSObject*)(_name)]];
B4IRDebugUtils.currentLine=2097154;
 //BA.debugLineNum = 2097154;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendrefuse:(b4i_servletrequest*) __ref :(b4i_servletresponse*) _sresponse :(_tuser*) _user{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"sendrefuse"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendrefuse::" :@[[B4I nilToNSNull:_sresponse],[B4I nilToNSNull:_user]]]);}
NSString* _body = @"";
NSString* _handshake = @"";
B4IRDebugUtils.currentLine=3407872;
 //BA.debugLineNum = 3407872;BA.debugLine="private Sub SendRefuse (sResponse As ServletRespon";
B4IRDebugUtils.currentLine=3407883;
 //BA.debugLineNum = 3407883;BA.debugLine="Dim Body As String = $\"<!DOCTYPE html> <html>   <";
_body = ([@[@"<!DOCTYPE html>\n",@"<html>\n",@"  <head>\n",@"    <meta charset=\"UTF-8\" />\n",@"    <title>Error</title>\n",@"  </head>\n",@"  <body>\n",@"    <h1>401 Unauthorized.</h1>\n",@"  </body>\n",@"</html>"] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3407893;
 //BA.debugLineNum = 3407893;BA.debugLine="Dim HandShake As String = $\"HTTP/1.0 401 Unauthor";
_handshake = ([@[@"HTTP/1.0 401 Unauthorized\n",@"Server: HttpServer (B4X)\n",@"Date: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3407897;
 //BA.debugLineNum = 3407897;BA.debugLine="HandShake=$\"${HandShake}WWW-Authenticate: Digest";
_handshake = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"WWW-Authenticate: Digest realm=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_user->_realm /*NSString**/ )],@"\", qop=\"auth,auth-int\", nonce=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_user->_nonce /*NSString**/ )],@"\", opaque=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_user->_opaque /*NSString**/ )],@"\"\n",@"Content-Type: text/html\n",@"Content-Length: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@([_body Length]))],@"\n",@"\n",@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_body)],@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=3407902;
 //BA.debugLineNum = 3407902;BA.debugLine="sResponse.Write(HandShake)";
[_sresponse _write /*NSString**/ :nil :_handshake];
B4IRDebugUtils.currentLine=3407903;
 //BA.debugLineNum = 3407903;BA.debugLine="sResponse.Close";
[_sresponse _close /*NSString**/ :nil];
B4IRDebugUtils.currentLine=3407904;
 //BA.debugLineNum = 3407904;BA.debugLine="End Sub";
return @"";
}
- (_tuser*)  _newuser:(b4i_servletrequest*) __ref :(NSString*) _naddress{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"newuser"])
	 {return ((_tuser*) [B4IDebug delegate:self.bi :@"newuser:" :@[[B4I nilToNSNull:_naddress]]]);}
_tuser* _nuser = nil;
B4IRDebugUtils.currentLine=3211264;
 //BA.debugLineNum = 3211264;BA.debugLine="Private Sub NewUser(nAddress As String) As tUser";
B4IRDebugUtils.currentLine=3211265;
 //BA.debugLineNum = 3211265;BA.debugLine="Dim nUser As tUser";
_nuser = [_tuser new];
B4IRDebugUtils.currentLine=3211267;
 //BA.debugLineNum = 3211267;BA.debugLine="nUser.Initialize";
[_nuser Initialize];
B4IRDebugUtils.currentLine=3211268;
 //BA.debugLineNum = 3211268;BA.debugLine="nUser.Address=nAddress";
_nuser->_Address /*NSString**/  = _naddress;
B4IRDebugUtils.currentLine=3211269;
 //BA.debugLineNum = 3211269;BA.debugLine="nUser.LastRequest=DateTime.Now";
_nuser->_LastRequest /*long long*/  = [[self->___c DateTime] Now];
B4IRDebugUtils.currentLine=3211270;
 //BA.debugLineNum = 3211270;BA.debugLine="nUser.opaque=nAddress.SubString2(0,3) & bc.HexFro";
_nuser->_opaque /*NSString**/  = [@[[_naddress SubString2:(int) (0) :(int) (3)],[__ref->__bc /*B4IByteConverter**/  HexFromBytes:[__ref->__bc /*B4IByteConverter**/  LongsToBytes:[[B4IArray alloc]initObjectsWithData:@[@([[self->___c DateTime] Now])]]]]] componentsJoinedByString:@""];
B4IRDebugUtils.currentLine=3211271;
 //BA.debugLineNum = 3211271;BA.debugLine="nUser.nonce=bc.HexFromBytes(bc.LongsToBytes(Array";
_nuser->_nonce /*NSString**/  = [__ref->__bc /*B4IByteConverter**/  HexFromBytes:[__ref->__bc /*B4IByteConverter**/  LongsToBytes:[[B4IArray alloc]initObjectsWithData:@[@([[self->___c DateTime] Now])]]]];
B4IRDebugUtils.currentLine=3211272;
 //BA.debugLineNum = 3211272;BA.debugLine="nUser.nc=1";
_nuser->_nc /*int*/  = (int) (1);
B4IRDebugUtils.currentLine=3211273;
 //BA.debugLineNum = 3211273;BA.debugLine="nUser.realm=CallServer.realm";
_nuser->_realm /*NSString**/  = __ref->__callserver /*b4i_httpserver**/ ->__realm /*NSString**/ ;
B4IRDebugUtils.currentLine=3211275;
 //BA.debugLineNum = 3211275;BA.debugLine="Users.Put(nUser.opaque,nUser)";
[__ref->__users /*B4IMap**/  Put:(NSObject*)(_nuser->_opaque /*NSString**/ ) :(NSObject*)(_nuser)];
B4IRDebugUtils.currentLine=3211277;
 //BA.debugLineNum = 3211277;BA.debugLine="Return nUser";
if (true) return _nuser;
B4IRDebugUtils.currentLine=3211278;
 //BA.debugLineNum = 3211278;BA.debugLine="End Sub";
return nil;
}
- (_tuser*)  _finduser:(b4i_servletrequest*) __ref :(NSString*) _naddress :(NSString*) _opaque{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"finduser"])
	 {return ((_tuser*) [B4IDebug delegate:self.bi :@"finduser::" :@[[B4I nilToNSNull:_naddress],[B4I nilToNSNull:_opaque]]]);}
_tuser* _nuser = nil;
B4IRDebugUtils.currentLine=3276800;
 //BA.debugLineNum = 3276800;BA.debugLine="Private Sub FindUser(nAddress As String,opaque As";
B4IRDebugUtils.currentLine=3276801;
 //BA.debugLineNum = 3276801;BA.debugLine="Dim nUser As tUser";
_nuser = [_tuser new];
B4IRDebugUtils.currentLine=3276803;
 //BA.debugLineNum = 3276803;BA.debugLine="If Users.ContainsKey(opaque) Then";
if ([__ref->__users /*B4IMap**/  ContainsKey:(NSObject*)(_opaque)]) { 
B4IRDebugUtils.currentLine=3276804;
 //BA.debugLineNum = 3276804;BA.debugLine="nUser=Users.Get(opaque)";
_nuser = (_tuser*)([__ref->__users /*B4IMap**/  Get:(NSObject*)(_opaque)]);
 }else {
B4IRDebugUtils.currentLine=3276806;
 //BA.debugLineNum = 3276806;BA.debugLine="nUser=NewUser(nAddress)";
_nuser = [__ref _newuser /*_tuser**/ :nil :_naddress];
B4IRDebugUtils.currentLine=3276807;
 //BA.debugLineNum = 3276807;BA.debugLine="Users.Put(nUser.opaque,nUser)";
[__ref->__users /*B4IMap**/  Put:(NSObject*)(_nuser->_opaque /*NSString**/ ) :(NSObject*)(_nuser)];
 };
B4IRDebugUtils.currentLine=3276810;
 //BA.debugLineNum = 3276810;BA.debugLine="Return nUser";
if (true) return _nuser;
B4IRDebugUtils.currentLine=3276811;
 //BA.debugLineNum = 3276811;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _findcredential:(b4i_servletrequest*) __ref :(NSString*) _un{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"findcredential"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"findcredential:" :@[[B4I nilToNSNull:_un]]]);}
NSString* _s = @"";
B4IArray* _cz = nil;
B4IRDebugUtils.currentLine=3342336;
 //BA.debugLineNum = 3342336;BA.debugLine="Private Sub FindCredential(Un As String)";
B4IRDebugUtils.currentLine=3342337;
 //BA.debugLineNum = 3342337;BA.debugLine="UserName  = \"\"";
__ref->__username /*NSString**/  = @"";
B4IRDebugUtils.currentLine=3342339;
 //BA.debugLineNum = 3342339;BA.debugLine="Password = \"\"";
__ref->__password /*NSString**/  = @"";
B4IRDebugUtils.currentLine=3342341;
 //BA.debugLineNum = 3342341;BA.debugLine="For Each S As String In CallServer.htdigest";
{
const id<B4IIterable> group3 = __ref->__callserver /*b4i_httpserver**/ ->__htdigest /*B4IList**/ ;
const int groupLen3 = group3.Size
;int index3 = 0;
;
for (; index3 < groupLen3;index3++){
_s = [self.bi ObjectToString:[group3 Get:index3]];
B4IRDebugUtils.currentLine=3342342;
 //BA.debugLineNum = 3342342;BA.debugLine="If S.StartsWith($\"${Un}:\"$) Then";
if ([_s StartsWith:([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_un)],@":"] componentsJoinedByString:@""])]) { 
B4IRDebugUtils.currentLine=3342343;
 //BA.debugLineNum = 3342343;BA.debugLine="Dim Cz() As String = Regex.Split(\":\",s)";
_cz = [[self->___c Regex] Split:@":" :_s];
B4IRDebugUtils.currentLine=3342344;
 //BA.debugLineNum = 3342344;BA.debugLine="If Cz.Length=3 Then";
if (_cz.Length==3) { 
B4IRDebugUtils.currentLine=3342345;
 //BA.debugLineNum = 3342345;BA.debugLine="UserName=Cz(0)";
__ref->__username /*NSString**/  = ((NSString*)[_cz getObjectFast:(int) (0)]);
B4IRDebugUtils.currentLine=3342346;
 //BA.debugLineNum = 3342346;BA.debugLine="CallServer.realm=Cz(1)";
__ref->__callserver /*b4i_httpserver**/ ->__realm /*NSString**/  = ((NSString*)[_cz getObjectFast:(int) (1)]);
B4IRDebugUtils.currentLine=3342347;
 //BA.debugLineNum = 3342347;BA.debugLine="Password=Cz(2)";
__ref->__password /*NSString**/  = ((NSString*)[_cz getObjectFast:(int) (2)]);
 };
 };
 }
};
B4IRDebugUtils.currentLine=3342351;
 //BA.debugLineNum = 3342351;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _md5:(b4i_servletrequest*) __ref :(NSString*) _stringtoconvert{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"md5"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"md5:" :@[[B4I nilToNSNull:_stringtoconvert]]]);}
B4IMessageDigest* _md = nil;
B4IArray* _data = nil;
B4IRDebugUtils.currentLine=3145728;
 //BA.debugLineNum = 3145728;BA.debugLine="Private Sub MD5(stringToConvert As String) As Stri";
B4IRDebugUtils.currentLine=3145729;
 //BA.debugLineNum = 3145729;BA.debugLine="Dim MD As MessageDigest";
_md = [B4IMessageDigest new];
B4IRDebugUtils.currentLine=3145730;
 //BA.debugLineNum = 3145730;BA.debugLine="Dim Data() As Byte =  MD.GetMessageDigest(stringT";
_data = [_md GetMessageDigest:[_stringtoconvert GetBytes:@"UTF8"] :@"MD5"];
B4IRDebugUtils.currentLine=3145731;
 //BA.debugLineNum = 3145731;BA.debugLine="Return bc.HexFromBytes(Data).ToLowerCase";
if (true) return [[__ref->__bc /*B4IByteConverter**/  HexFromBytes:_data] ToLowerCase];
B4IRDebugUtils.currentLine=3145732;
 //BA.debugLineNum = 3145732;BA.debugLine="End Sub";
return @"";
}
- (int)  _hextodec:(b4i_servletrequest*) __ref :(NSString*) _hex{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"hextodec"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"hextodec:" :@[[B4I nilToNSNull:_hex]]]).intValue;}
B4IRDebugUtils.currentLine=3014656;
 //BA.debugLineNum = 3014656;BA.debugLine="Private Sub HexToDec(Hex As String) As Int";
B4IRDebugUtils.currentLine=3014657;
 //BA.debugLineNum = 3014657;BA.debugLine="Return bc.IntsFromBytes(bc.HexToBytes(Hex))(0)";
if (true) return ((NSNumber*)[[__ref->__bc /*B4IByteConverter**/  IntsFromBytes:[__ref->__bc /*B4IByteConverter**/  HexToBytes:_hex]] getObjectFastN:(int) (0)]).intValue;
B4IRDebugUtils.currentLine=3014658;
 //BA.debugLineNum = 3014658;BA.debugLine="End Sub";
return 0;
}
- (NSString*)  _elaboratewebsocket:(b4i_servletrequest*) __ref :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"elaboratewebsocket"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"elaboratewebsocket:" :@[[B4I nilToNSNull:_data]]]);}
BOOL _fin = false;
BOOL _deflatecompress = false;
unsigned char _opcode = 0;
BOOL _masked = false;
B4IArray* _maskingkey = nil;
unsigned char _payload = 0;
int _startdate = 0;
long long _lng = 0L;
B4IArray* _dataclear = nil;
int _closecode = 0;
NSString* _closemessage = @"";
B4IRDebugUtils.currentLine=3932160;
 //BA.debugLineNum = 3932160;BA.debugLine="Private Sub ElaborateWebSocket(Data() As Byte)";
B4IRDebugUtils.currentLine=3932179;
 //BA.debugLineNum = 3932179;BA.debugLine="If Data.Length>=8 Then ' is frame";
if (_data.Length>=8) { 
B4IRDebugUtils.currentLine=3932180;
 //BA.debugLineNum = 3932180;BA.debugLine="Dim Fin As Boolean = (Bit.And(Data(0),128)>0)";
_fin = ((((int) ([_data getByteFast:(int) (0)])) & ((int) (128)))>0);
B4IRDebugUtils.currentLine=3932181;
 //BA.debugLineNum = 3932181;BA.debugLine="Dim DeflateCompress As Boolean  = (Bit.And(Data(";
_deflatecompress = ((((int) ([_data getByteFast:(int) (0)])) & ((int) (64)))>0);
B4IRDebugUtils.currentLine=3932182;
 //BA.debugLineNum = 3932182;BA.debugLine="Dim OpCode As Byte = Bit.And(Data(0),15)";
_opcode = (unsigned char) ((((int) ([_data getByteFast:(int) (0)])) & ((int) (15))));
B4IRDebugUtils.currentLine=3932183;
 //BA.debugLineNum = 3932183;BA.debugLine="Dim Masked As Boolean = (Bit.And(Data(1),128)>0)";
_masked = ((((int) ([_data getByteFast:(int) (1)])) & ((int) (128)))>0);
B4IRDebugUtils.currentLine=3932184;
 //BA.debugLineNum = 3932184;BA.debugLine="Dim Maskingkey(4) As Byte";
_maskingkey = [[B4IArray alloc]initBytes:@[@((int) (4))]];
B4IRDebugUtils.currentLine=3932185;
 //BA.debugLineNum = 3932185;BA.debugLine="Dim Payload As Byte = Bit.And(Data(1),127)";
_payload = (unsigned char) ((((int) ([_data getByteFast:(int) (1)])) & ((int) (127))));
B4IRDebugUtils.currentLine=3932186;
 //BA.debugLineNum = 3932186;BA.debugLine="Dim Startdate As Int = 0";
_startdate = (int) (0);
B4IRDebugUtils.currentLine=3932187;
 //BA.debugLineNum = 3932187;BA.debugLine="Dim Lng As Long";
_lng = 0L;
B4IRDebugUtils.currentLine=3932189;
 //BA.debugLineNum = 3932189;BA.debugLine="If OpCode>0 Then LastOpCode=OpCode ' opcode cont";
if (_opcode>0) { 
__ref->__lastopcode /*unsigned char*/  = _opcode;};
B4IRDebugUtils.currentLine=3932190;
 //BA.debugLineNum = 3932190;BA.debugLine="WebSocketString=\"\"";
__ref->__websocketstring /*NSString**/  = @"";
B4IRDebugUtils.currentLine=3932192;
 //BA.debugLineNum = 3932192;BA.debugLine="LgC(\"____________________________________\",0xFFC";
[__ref _lgc /*NSString**/ :nil :@"____________________________________" :(int) (0xffc0c0c0)];
B4IRDebugUtils.currentLine=3932193;
 //BA.debugLineNum = 3932193;BA.debugLine="LgC(\"Fin: \" & Fin,0xFFC0C0C0)";
[__ref _lgc /*NSString**/ :nil :[@[@"Fin: ",[self.bi BooleanToString:_fin]] componentsJoinedByString:@""] :(int) (0xffc0c0c0)];
B4IRDebugUtils.currentLine=3932194;
 //BA.debugLineNum = 3932194;BA.debugLine="LgC(\"OpCode: \" & OpCode,0xFFC0C0C0)";
[__ref _lgc /*NSString**/ :nil :[@[@"OpCode: ",[self.bi NumberToString:@(_opcode)]] componentsJoinedByString:@""] :(int) (0xffc0c0c0)];
B4IRDebugUtils.currentLine=3932195;
 //BA.debugLineNum = 3932195;BA.debugLine="LgC(\"Mask: \" & Masked,0xFFC0C0C0)";
[__ref _lgc /*NSString**/ :nil :[@[@"Mask: ",[self.bi BooleanToString:_masked]] componentsJoinedByString:@""] :(int) (0xffc0c0c0)];
B4IRDebugUtils.currentLine=3932196;
 //BA.debugLineNum = 3932196;BA.debugLine="LgC(\"Payload: \" & Payload,0xFFC0C0C0)";
[__ref _lgc /*NSString**/ :nil :[@[@"Payload: ",[self.bi NumberToString:@(_payload)]] componentsJoinedByString:@""] :(int) (0xffc0c0c0)];
B4IRDebugUtils.currentLine=3932198;
 //BA.debugLineNum = 3932198;BA.debugLine="If Payload<126 Then";
if (_payload<126) { 
B4IRDebugUtils.currentLine=3932200;
 //BA.debugLineNum = 3932200;BA.debugLine="Lng=Payload";
_lng = (long long) (_payload);
B4IRDebugUtils.currentLine=3932201;
 //BA.debugLineNum = 3932201;BA.debugLine="Startdate=2";
_startdate = (int) (2);
 }else if(_payload==126) { 
B4IRDebugUtils.currentLine=3932204;
 //BA.debugLineNum = 3932204;BA.debugLine="Lng = Data(2) *256 + Data(3)";
_lng = (long long) ([_data getByteFast:(int) (2)]*256+[_data getByteFast:(int) (3)]);
B4IRDebugUtils.currentLine=3932205;
 //BA.debugLineNum = 3932205;BA.debugLine="Startdate=4";
_startdate = (int) (4);
 }else if(_payload==127) { 
B4IRDebugUtils.currentLine=3932207;
 //BA.debugLineNum = 3932207;BA.debugLine="Dim bc As ByteConverter";
self->__bc = [B4IByteConverter new];
B4IRDebugUtils.currentLine=3932208;
 //BA.debugLineNum = 3932208;BA.debugLine="Lng =  bc.IntsFromBytes(Array As Byte(Data(2),D";
_lng = (long long) (((NSNumber*)[[__ref->__bc /*B4IByteConverter**/  IntsFromBytes:[[B4IArray alloc]initBytesWithData:@[@([_data getByteFast:(int) (2)]),@([_data getByteFast:(int) (3)]),@([_data getByteFast:(int) (4)]),@([_data getByteFast:(int) (5)])]]] getObjectFastN:(int) (0)]).intValue);
B4IRDebugUtils.currentLine=3932209;
 //BA.debugLineNum = 3932209;BA.debugLine="Startdate=6";
_startdate = (int) (6);
 };
B4IRDebugUtils.currentLine=3932211;
 //BA.debugLineNum = 3932211;BA.debugLine="If Masked Then";
if (_masked) { 
B4IRDebugUtils.currentLine=3932212;
 //BA.debugLineNum = 3932212;BA.debugLine="Bit.ArrayCopy(Data,Startdate,Maskingkey,0,4)";
B4I_MEMCPY((_data)->internalBuffer + (_startdate), (_maskingkey)->internalBuffer + ((int) (0)),(int) (4));
B4IRDebugUtils.currentLine=3932213;
 //BA.debugLineNum = 3932213;BA.debugLine="Startdate=Startdate+4";
_startdate = (int) (_startdate+4);
 };
B4IRDebugUtils.currentLine=3932215;
 //BA.debugLineNum = 3932215;BA.debugLine="BBB=ArrayAppend(BBB,DeMasked(Data,Maskingkey,Lng";
__ref->__bbb /*B4IArray**/  = [__ref _arrayappend /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :[__ref _demasked /*B4IArray**/ :nil :_data :_maskingkey :_lng :(long long) (_startdate) :_masked]];
B4IRDebugUtils.currentLine=3932217;
 //BA.debugLineNum = 3932217;BA.debugLine="If Fin Then";
if (_fin) { 
B4IRDebugUtils.currentLine=3932218;
 //BA.debugLineNum = 3932218;BA.debugLine="If DeflateCompress Then  ' deflate header";
if (_deflatecompress) { 
B4IRDebugUtils.currentLine=3932219;
 //BA.debugLineNum = 3932219;BA.debugLine="BBB=ArrayInsert(BBB,0,Array As Byte(120,156))";
__ref->__bbb /*B4IArray**/  = [__ref _arrayinsert /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/  :(int) (0) :[[B4IArray alloc]initBytesWithData:@[@((unsigned char) (120)),@((unsigned char) (156))]]];
B4IRDebugUtils.currentLine=3932220;
 //BA.debugLineNum = 3932220;BA.debugLine="Dim DataClear() As Byte = InflateDate(ArrayCop";
_dataclear = [__ref _inflatedate /*B4IArray**/ :nil :[__ref _arraycopy /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/ ]];
 }else {
B4IRDebugUtils.currentLine=3932222;
 //BA.debugLineNum = 3932222;BA.debugLine="Dim DataClear() As Byte = ArrayCopy(BBB)";
_dataclear = [__ref _arraycopy /*B4IArray**/ :nil :__ref->__bbb /*B4IArray**/ ];
 };
B4IRDebugUtils.currentLine=3932225;
 //BA.debugLineNum = 3932225;BA.debugLine="Select LastOpCode";
switch ([self.bi switchObjectToInt:@(__ref->__lastopcode /*unsigned char*/ ) :@[@((unsigned char) (0)),@((unsigned char) (1)),@((unsigned char) (2)),@((unsigned char) (8)),@((unsigned char) (9)),@((unsigned char) (10))]]) {
case 0: {
 break; }
case 1: {
B4IRDebugUtils.currentLine=3932229;
 //BA.debugLineNum = 3932229;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=3932230;
 //BA.debugLineNum = 3932230;BA.debugLine="WebSocketString=BytesToString(DataClear,0,Da";
__ref->__websocketstring /*NSString**/  = [self->___c BytesToString:_dataclear :(int) (0) :_dataclear.Length :@"UTF8"];
B4IRDebugUtils.currentLine=3932231;
 //BA.debugLineNum = 3932231;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handl";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_HandleWebSocket"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_HandleWebSocket"] componentsJoinedByString:@""] :self :(NSObject*)(__ref->__response /*b4i_servletresponse**/ )];};
 } 
       @catch (NSException* e47) {
			[B4I SetException:e47];B4IRDebugUtils.currentLine=3932233;
 //BA.debugLineNum = 3932233;BA.debugLine="WebSocketString=\"\"";
__ref->__websocketstring /*NSString**/  = @"";
B4IRDebugUtils.currentLine=3932234;
 //BA.debugLineNum = 3932234;BA.debugLine="Log(LastException)";
[self->___c LogImpl:@"33932234" :[self.bi ObjectToString:[self->___c LastException]] :0];
 };
 break; }
case 2: {
B4IRDebugUtils.currentLine=3932237;
 //BA.debugLineNum = 3932237;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=3932238;
 //BA.debugLineNum = 3932238;BA.debugLine="WebSocketString=BytesToString(DataClear,0,Da";
__ref->__websocketstring /*NSString**/  = [self->___c BytesToString:_dataclear :(int) (0) :_dataclear.Length :@"UTF8"];
B4IRDebugUtils.currentLine=3932239;
 //BA.debugLineNum = 3932239;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handl";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_HandleWebSocket"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_HandleWebSocket"] componentsJoinedByString:@""] :self :(NSObject*)(__ref->__response /*b4i_servletresponse**/ )];};
 } 
       @catch (NSException* e55) {
			[B4I SetException:e55];B4IRDebugUtils.currentLine=3932241;
 //BA.debugLineNum = 3932241;BA.debugLine="WebSocketString=\"\"";
__ref->__websocketstring /*NSString**/  = @"";
B4IRDebugUtils.currentLine=3932242;
 //BA.debugLineNum = 3932242;BA.debugLine="Log(LastException)";
[self->___c LogImpl:@"33932242" :[self.bi ObjectToString:[self->___c LastException]] :0];
 };
 break; }
case 3: {
B4IRDebugUtils.currentLine=3932245;
 //BA.debugLineNum = 3932245;BA.debugLine="Dim CloseCode As Int = DataClear(0)*256+DataC";
_closecode = (int) ([_dataclear getByteFast:(int) (0)]*256+[_dataclear getByteFast:(int) (1)]);
B4IRDebugUtils.currentLine=3932246;
 //BA.debugLineNum = 3932246;BA.debugLine="Dim CloseMessage As String =\"\"";
_closemessage = @"";
B4IRDebugUtils.currentLine=3932248;
 //BA.debugLineNum = 3932248;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=3932249;
 //BA.debugLineNum = 3932249;BA.debugLine="CloseMessage=BytesToString(DataClear,2,DataC";
_closemessage = [self->___c BytesToString:_dataclear :(int) (2) :(int) (_dataclear.Length-2) :@"UTF8"];
 } 
       @catch (NSException* e64) {
			[B4I SetException:e64];B4IRDebugUtils.currentLine=3932251;
 //BA.debugLineNum = 3932251;BA.debugLine="Log(LastException)";
[self->___c LogImpl:@"33932251" :[self.bi ObjectToString:[self->___c LastException]] :0];
 };
B4IRDebugUtils.currentLine=3932253;
 //BA.debugLineNum = 3932253;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_WebSoc";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_WebSocketClose"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_WebSocketClose"] componentsJoinedByString:@""] :(NSObject*)(@(_closecode)) :(NSObject*)(_closemessage)];};
 break; }
case 4: {
B4IRDebugUtils.currentLine=3932255;
 //BA.debugLineNum = 3932255;BA.debugLine="Response.SendWebSocketPong";
[__ref->__response /*b4i_servletresponse**/  _sendwebsocketpong /*NSString**/ :nil];
 break; }
case 5: {
 break; }
default: {
B4IRDebugUtils.currentLine=3932259;
 //BA.debugLineNum = 3932259;BA.debugLine="Log(\"OpCode unknown \"& LastOpCode)";
[self->___c LogImpl:@"33932259" :[@[@"OpCode unknown ",[self.bi NumberToString:@(__ref->__lastopcode /*unsigned char*/ )]] componentsJoinedByString:@""] :0];
 break; }
}
;
B4IRDebugUtils.currentLine=3932261;
 //BA.debugLineNum = 3932261;BA.debugLine="BBB=ArrayInitialize";
__ref->__bbb /*B4IArray**/  = [__ref _arrayinitialize /*B4IArray**/ :nil];
 };
 };
B4IRDebugUtils.currentLine=3932265;
 //BA.debugLineNum = 3932265;BA.debugLine="End Sub";
return @"";
}
- (B4IArray*)  _inflatedate:(b4i_servletrequest*) __ref :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"inflatedate"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"inflatedate:" :@[[B4I nilToNSNull:_data]]]);}
B4INativeObject* _nativeme = nil;
NSObject* _infd = nil;
B4IRDebugUtils.currentLine=4063232;
 //BA.debugLineNum = 4063232;BA.debugLine="Private Sub InflateDate(data() As Byte) As Byte()";
B4IRDebugUtils.currentLine=4063236;
 //BA.debugLineNum = 4063236;BA.debugLine="Dim NativeMe As NativeObject = Me";
_nativeme = [B4INativeObject new];
_nativeme = (B4INativeObject*) [B4IObjectWrapper createWrapper:[B4INativeObject new] object:(NSObject*)(self)];
B4IRDebugUtils.currentLine=4063237;
 //BA.debugLineNum = 4063237;BA.debugLine="Dim infd As Object = NativeMe.RunMethod(\"gzipInf";
_infd = (NSObject*)(([_nativeme RunMethod:@"gzipInflate:" :[[B4IArray alloc]initObjectsWithData:@[[B4I nilToNSNull:[_nativeme ArrayToNSData:_data]]]]]).object);
B4IRDebugUtils.currentLine=4063238;
 //BA.debugLineNum = 4063238;BA.debugLine="Return NativeMe.ArrayToNSData(infd)";
if (true) return (B4IArray*)([_nativeme ArrayToNSData:(B4IArray*)(_infd)]);
B4IRDebugUtils.currentLine=4063255;
 //BA.debugLineNum = 4063255;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _encode:(b4i_servletrequest*) __ref :(NSString*) _s{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"encode"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"encode:" :@[[B4I nilToNSNull:_s]]]);}
NSString* _addr = @"";
int _i = 0;
B4IRDebugUtils.currentLine=3670016;
 //BA.debugLineNum = 3670016;BA.debugLine="Private Sub encode(S As String) As String 'ignore";
B4IRDebugUtils.currentLine=3670017;
 //BA.debugLineNum = 3670017;BA.debugLine="Dim Addr As String = \"\"";
_addr = @"";
B4IRDebugUtils.currentLine=3670018;
 //BA.debugLineNum = 3670018;BA.debugLine="For i=0 To S.Length-1";
{
const int step2 = 1;
const int limit2 = (int) ([_s Length]-1);
_i = (int) (0) ;
for (;_i <= limit2 ;_i = _i + step2 ) {
B4IRDebugUtils.currentLine=3670020;
 //BA.debugLineNum = 3670020;BA.debugLine="If Asc(S.CharAt(i))>=44 And Asc(S.CharAt(i))<=12";
if (((int)([_s CharAt:_i]))>=44 && ((int)([_s CharAt:_i]))<=122 || ((int)([_s CharAt:_i]))==38) { 
B4IRDebugUtils.currentLine=3670021;
 //BA.debugLineNum = 3670021;BA.debugLine="Addr=Addr & S.CharAt(i)";
_addr = [@[_addr,[self.bi CharToString:[_s CharAt:_i]]] componentsJoinedByString:@""];
 }else {
B4IRDebugUtils.currentLine=3670023;
 //BA.debugLineNum = 3670023;BA.debugLine="Addr=Addr & \"%\" &  DecToHex(Asc(S.CharAt(i)))";
_addr = [@[_addr,@"%",[__ref _dectohex /*NSString**/ :nil :((int)([_s CharAt:_i]))]] componentsJoinedByString:@""];
 };
 }
};
B4IRDebugUtils.currentLine=3670027;
 //BA.debugLineNum = 3670027;BA.debugLine="Return Addr";
if (true) return _addr;
B4IRDebugUtils.currentLine=3670028;
 //BA.debugLineNum = 3670028;BA.debugLine="End Sub";
return @"";
}
- (B4IArray*)  _subarray2:(b4i_servletrequest*) __ref :(B4IArray*) _data :(int) _start :(int) _last{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"subarray2"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"subarray2:::" :@[[B4I nilToNSNull:_data],@(_start),@(_last)]]);}
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=4784128;
 //BA.debugLineNum = 4784128;BA.debugLine="Public Sub SubArray2(Data() As Byte,Start As Int,";
B4IRDebugUtils.currentLine=4784129;
 //BA.debugLineNum = 4784129;BA.debugLine="Dim fn(Last - Start) As Byte";
_fn = [[B4IArray alloc]initBytes:@[@((int) (_last-_start))]];
B4IRDebugUtils.currentLine=4784130;
 //BA.debugLineNum = 4784130;BA.debugLine="Bit.ArrayCopy(Data, Start, fn, 0, fn.Length)";
B4I_MEMCPY((_data)->internalBuffer + (_start), (_fn)->internalBuffer + ((int) (0)),_fn.Length);
B4IRDebugUtils.currentLine=4784131;
 //BA.debugLineNum = 4784131;BA.debugLine="Return fn";
if (true) return _fn;
B4IRDebugUtils.currentLine=4784132;
 //BA.debugLineNum = 4784132;BA.debugLine="End Sub";
return nil;
}
- (B4IList*)  _getheadersname:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getheadersname"])
	 {return ((B4IList*) [B4IDebug delegate:self.bi :@"getheadersname" :nil]);}
B4IList* _l = nil;
NSObject* _k = nil;
B4IRDebugUtils.currentLine=2162688;
 //BA.debugLineNum = 2162688;BA.debugLine="Public Sub GetHeadersName As List";
B4IRDebugUtils.currentLine=2162689;
 //BA.debugLineNum = 2162689;BA.debugLine="Dim l As List";
_l = [B4IList new];
B4IRDebugUtils.currentLine=2162690;
 //BA.debugLineNum = 2162690;BA.debugLine="l.Initialize";
[_l Initialize];
B4IRDebugUtils.currentLine=2162692;
 //BA.debugLineNum = 2162692;BA.debugLine="For Each K In RequestHeader.keys";
{
const id<B4IIterable> group3 = [__ref->__requestheader /*B4IMap**/  Keys];
const int groupLen3 = group3.Size
;int index3 = 0;
;
for (; index3 < groupLen3;index3++){
_k = [group3 Get:index3];
B4IRDebugUtils.currentLine=2162693;
 //BA.debugLineNum = 2162693;BA.debugLine="l.Add(K)";
[_l Add:_k];
 }
};
B4IRDebugUtils.currentLine=2162695;
 //BA.debugLineNum = 2162695;BA.debugLine="Return l";
if (true) return _l;
B4IRDebugUtils.currentLine=2162696;
 //BA.debugLineNum = 2162696;BA.debugLine="End Sub";
return nil;
}
- (B4IInputStream*)  _getinputstream:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getinputstream"])
	 {return ((B4IInputStream*) [B4IDebug delegate:self.bi :@"getinputstream" :nil]);}
B4IRDebugUtils.currentLine=1572864;
 //BA.debugLineNum = 1572864;BA.debugLine="Public Sub GetInputStream As InputStream";
B4IRDebugUtils.currentLine=1572865;
 //BA.debugLineNum = 1572865;BA.debugLine="Return client.InputStream";
if (true) return [__ref->__client /*B4ISocketWrapper**/  InputStream];
B4IRDebugUtils.currentLine=1572866;
 //BA.debugLineNum = 1572866;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _getrequesthost:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getrequesthost"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getrequesthost" :nil]);}
B4IRDebugUtils.currentLine=2031616;
 //BA.debugLineNum = 2031616;BA.debugLine="Public Sub GetRequestHOST As String";
B4IRDebugUtils.currentLine=2031617;
 //BA.debugLineNum = 2031617;BA.debugLine="Return RequestHOST";
if (true) return __ref->__requesthost /*NSString**/ ;
B4IRDebugUtils.currentLine=2031618;
 //BA.debugLineNum = 2031618;BA.debugLine="End Sub";
return @"";
}
- (BOOL)  _getwebsocketcompressdeflateaccept:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getwebsocketcompressdeflateaccept"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"getwebsocketcompressdeflateaccept" :nil]).boolValue;}
B4IRDebugUtils.currentLine=2424832;
 //BA.debugLineNum = 2424832;BA.debugLine="Public Sub GetWebSocketCompressDeflateAccept As Bo";
B4IRDebugUtils.currentLine=2424833;
 //BA.debugLineNum = 2424833;BA.debugLine="Return deflate";
if (true) return __ref->__deflate /*BOOL*/ ;
B4IRDebugUtils.currentLine=2424834;
 //BA.debugLineNum = 2424834;BA.debugLine="End Sub";
return false;
}
- (BOOL)  _getwebsocketcompressgzipaccept:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getwebsocketcompressgzipaccept"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"getwebsocketcompressgzipaccept" :nil]).boolValue;}
B4IRDebugUtils.currentLine=2490368;
 //BA.debugLineNum = 2490368;BA.debugLine="Public Sub GetWebSocketCompressGzipAccept As Boole";
B4IRDebugUtils.currentLine=2490369;
 //BA.debugLineNum = 2490369;BA.debugLine="Return gzip";
if (true) return __ref->__gzip /*BOOL*/ ;
B4IRDebugUtils.currentLine=2490370;
 //BA.debugLineNum = 2490370;BA.debugLine="End Sub";
return false;
}
- (B4IMap*)  _getwebsocketmapdata:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getwebsocketmapdata"])
	 {return ((B4IMap*) [B4IDebug delegate:self.bi :@"getwebsocketmapdata" :nil]);}
B4IJSONParser* _j = nil;
B4IMap* _m = nil;
B4IRDebugUtils.currentLine=2359296;
 //BA.debugLineNum = 2359296;BA.debugLine="Public Sub GetWebSocketMapData As Map";
B4IRDebugUtils.currentLine=2359297;
 //BA.debugLineNum = 2359297;BA.debugLine="Dim J As JSONParser";
_j = [B4IJSONParser new];
B4IRDebugUtils.currentLine=2359298;
 //BA.debugLineNum = 2359298;BA.debugLine="Dim M As Map";
_m = [B4IMap new];
B4IRDebugUtils.currentLine=2359299;
 //BA.debugLineNum = 2359299;BA.debugLine="M.Initialize";
[_m Initialize];
B4IRDebugUtils.currentLine=2359300;
 //BA.debugLineNum = 2359300;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=2359301;
 //BA.debugLineNum = 2359301;BA.debugLine="j.Initialize(WebSocketString)";
[_j Initialize:__ref->__websocketstring /*NSString**/ ];
B4IRDebugUtils.currentLine=2359302;
 //BA.debugLineNum = 2359302;BA.debugLine="M =J.NextObject";
_m = [_j NextObject];
 } 
       @catch (NSException* e8) {
			[B4I SetException:e8];B4IRDebugUtils.currentLine=2359304;
 //BA.debugLineNum = 2359304;BA.debugLine="Log(LastException)";
[self->___c LogImpl:@"32359304" :[self.bi ObjectToString:[self->___c LastException]] :0];
 };
B4IRDebugUtils.currentLine=2359306;
 //BA.debugLineNum = 2359306;BA.debugLine="Return M";
if (true) return _m;
B4IRDebugUtils.currentLine=2359307;
 //BA.debugLineNum = 2359307;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _getwebsocketstringdata:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"getwebsocketstringdata"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getwebsocketstringdata" :nil]);}
B4IRDebugUtils.currentLine=2293760;
 //BA.debugLineNum = 2293760;BA.debugLine="Public Sub GetWebSocketStringData As String";
B4IRDebugUtils.currentLine=2293761;
 //BA.debugLineNum = 2293761;BA.debugLine="Return WebSocketString";
if (true) return __ref->__websocketstring /*NSString**/ ;
B4IRDebugUtils.currentLine=2293762;
 //BA.debugLineNum = 2293762;BA.debugLine="End Sub";
return @"";
}
- (int)  _remoteport:(b4i_servletrequest*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"remoteport"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"remoteport" :nil]).intValue;}
B4IRDebugUtils.currentLine=1835008;
 //BA.debugLineNum = 1835008;BA.debugLine="Public Sub RemotePort As Int";
B4IRDebugUtils.currentLine=1835009;
 //BA.debugLineNum = 1835009;BA.debugLine="Return ConnPort";
if (true) return [self.bi ObjectToNumber:__ref->__connport /*NSString**/ ].intValue;
B4IRDebugUtils.currentLine=1835010;
 //BA.debugLineNum = 1835010;BA.debugLine="End Sub";
return 0;
}
- (NSString*)  _websocketkey:(b4i_servletrequest*) __ref :(NSString*) _key{
__ref = self;
B4IRDebugUtils.currentModule=@"servletrequest";
if ([B4IDebug shouldDelegate: @"websocketkey"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"websocketkey:" :@[[B4I nilToNSNull:_key]]]);}
B4IMessageDigest* _sha = nil;
B4IArray* _data = nil;
B4IRDebugUtils.currentLine=3801088;
 //BA.debugLineNum = 3801088;BA.debugLine="Private Sub webSocketKey(Key As String) As String";
B4IRDebugUtils.currentLine=3801089;
 //BA.debugLineNum = 3801089;BA.debugLine="Dim sha As MessageDigest";
_sha = [B4IMessageDigest new];
B4IRDebugUtils.currentLine=3801090;
 //BA.debugLineNum = 3801090;BA.debugLine="Log(\"HandShake WebSocket ID: \" & ID)";
[self->___c LogImpl:@"33801090" :[@[@"HandShake WebSocket ID: ",__ref->__id /*NSString**/ ] componentsJoinedByString:@""] :0];
B4IRDebugUtils.currentLine=3801091;
 //BA.debugLineNum = 3801091;BA.debugLine="LgC(Key,0xFF000F00)";
[__ref _lgc /*NSString**/ :nil :_key :(int) (0xff000f00)];
B4IRDebugUtils.currentLine=3801093;
 //BA.debugLineNum = 3801093;BA.debugLine="Key=Key & \"258EAFA5-E914-47DA-95CA-C5AB0DC85B11\"";
_key = [@[_key,@"258EAFA5-E914-47DA-95CA-C5AB0DC85B11"] componentsJoinedByString:@""];
B4IRDebugUtils.currentLine=3801094;
 //BA.debugLineNum = 3801094;BA.debugLine="Dim data() As Byte = sha.GetMessageDigest(Key.Get";
_data = [_sha GetMessageDigest:[_key GetBytes:__ref->__characterencoding /*NSString**/ ] :@"SHA-1"];
B4IRDebugUtils.currentLine=3801095;
 //BA.debugLineNum = 3801095;BA.debugLine="LgC(su.EncodeBase64(data),0xFF000F00)";
[__ref _lgc /*NSString**/ :nil :[__ref->__su /*iStringUtils**/  EncodeBase64:_data] :(int) (0xff000f00)];
B4IRDebugUtils.currentLine=3801096;
 //BA.debugLineNum = 3801096;BA.debugLine="Return su.EncodeBase64(data)";
if (true) return [__ref->__su /*iStringUtils**/  EncodeBase64:_data];
B4IRDebugUtils.currentLine=3801097;
 //BA.debugLineNum = 3801097;BA.debugLine="End Sub";
return @"";
}
@end