
#import "b4i_servletresponse.h"
#import "b4i_main.h"
#import "b4i_httpserver.h"
#import "b4i_servletrequest.h"
#import "b4i_queryelement.h"


@implementation b4i_servletresponse 


+ (B4I*)createBI {
    return [B4IShellBI alloc];
}

- (void)dealloc {
    if (self.bi != nil)
        NSLog(@"Class (b4i_servletresponse) instance released.");
}

- (NSString*)  _sendstring:(b4i_servletresponse*) __ref :(NSString*) _text{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendstring"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendstring:" :@[[B4I nilToNSNull:_text]]]);}
B4IArray* _b = nil;
NSString* _s = @"";
B4IRDebugUtils.currentLine=5505024;
 //BA.debugLineNum = 5505024;BA.debugLine="Public Sub SendString(Text As String)";
B4IRDebugUtils.currentLine=5505025;
 //BA.debugLineNum = 5505025;BA.debugLine="If ContentType=\"\" Then ContentType=$\"text/html; c";
if ([__ref->__contenttype /*NSString**/  isEqual:@""]) { 
__ref->__contenttype /*NSString**/  = ([@[@"text/html; charset=",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__characterencoding /*NSString**/ )],@";"] componentsJoinedByString:@""]);};
B4IRDebugUtils.currentLine=5505026;
 //BA.debugLineNum = 5505026;BA.debugLine="Dim B() As Byte = Text.GetBytes(CharacterEncoding";
_b = [_text GetBytes:__ref->__characterencoding /*NSString**/ ];
B4IRDebugUtils.currentLine=5505027;
 //BA.debugLineNum = 5505027;BA.debugLine="ContentLenght=B.Length";
__ref->__contentlenght /*int*/  = _b.Length;
B4IRDebugUtils.currentLine=5505028;
 //BA.debugLineNum = 5505028;BA.debugLine="Dim S As String = HandShakeString & Text";
_s = [@[[__ref _handshakestring /*NSString**/ :nil],_text] componentsJoinedByString:@""];
B4IRDebugUtils.currentLine=5505029;
 //BA.debugLineNum = 5505029;BA.debugLine="astream_write(s.GetBytes(CharacterEncoding))";
[__ref _astream_write /*NSString**/ :nil :[_s GetBytes:__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=5505030;
 //BA.debugLineNum = 5505030;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendfile:(b4i_servletresponse*) __ref :(NSString*) _dir :(NSString*) _filename{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendfile"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendfile::" :@[[B4I nilToNSNull:_dir],[B4I nilToNSNull:_filename]]]);}
long long _filesize = 0L;
NSString* _lastmodified = @"";
B4IRDebugUtils.currentLine=5636096;
 //BA.debugLineNum = 5636096;BA.debugLine="Public Sub SendFile(Dir As String, fileName As Str";
B4IRDebugUtils.currentLine=5636097;
 //BA.debugLineNum = 5636097;BA.debugLine="If File.Exists(Dir, fileName) Then";
if ([[self->___c File] Exists:_dir :_filename]) { 
B4IRDebugUtils.currentLine=5636098;
 //BA.debugLineNum = 5636098;BA.debugLine="Dim FileSize As Long = File.Size(Dir, fileName)";
_filesize = [[self->___c File] Size:_dir :_filename];
B4IRDebugUtils.currentLine=5636099;
 //BA.debugLineNum = 5636099;BA.debugLine="Dim lastModified As String = CompleteDate(File.L";
_lastmodified = [__ref _completedate /*NSString**/ :nil :[[self->___c File] LastModified:_dir :_filename]];
B4IRDebugUtils.currentLine=5636101;
 //BA.debugLineNum = 5636101;BA.debugLine="ContentType=SearchMime(fileName)";
__ref->__contenttype /*NSString**/  = [__ref _searchmime /*NSString**/ :nil :_filename];
B4IRDebugUtils.currentLine=5636102;
 //BA.debugLineNum = 5636102;BA.debugLine="astream_write(HandShakeFile(lastModified,FileSiz";
[__ref _astream_write /*NSString**/ :nil :[[__ref _handshakefile /*NSString**/ :nil :_lastmodified :_filesize] GetBytes:__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=5636103;
 //BA.debugLineNum = 5636103;BA.debugLine="astream_write(File.ReadBytes(Dir, fileName))";
[__ref _astream_write /*NSString**/ :nil :[[self->___c File] ReadBytes:_dir :_filename]];
 }else {
B4IRDebugUtils.currentLine=5636105;
 //BA.debugLineNum = 5636105;BA.debugLine="SendNotFound(fileName)";
[__ref _sendnotfound /*NSString**/ :nil :_filename];
 };
B4IRDebugUtils.currentLine=5636107;
 //BA.debugLineNum = 5636107;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendnotfound:(b4i_servletresponse*) __ref :(NSString*) _filenamenotfound{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendnotfound"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendnotfound:" :@[[B4I nilToNSNull:_filenamenotfound]]]);}
NSString* _s = @"";
B4IRDebugUtils.currentLine=5832704;
 //BA.debugLineNum = 5832704;BA.debugLine="Public Sub SendNotFound(filenameNotFound As String";
B4IRDebugUtils.currentLine=5832705;
 //BA.debugLineNum = 5832705;BA.debugLine="Dim S As String = $\"HTTP/1.1 404 Not Found Date:";
_s = ([@[@"HTTP/1.1 404 Not Found\n",@"Date: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@"Cache-Control: must-revalidate,no-cache,no-store\n",@"Content-Type: text/html; charset=",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__characterencoding /*NSString**/ )],@";\n",@"Content-Length: 320\n",@"Server: HttpServer (B4X)\n",@"\n",@"<html>\n",@"<head>\n",@"<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"/>\n",@"<title>Error 404 Not Found</title>\n",@"</head>\n",@"<body><h2>HTTP ERROR 404</h2>\n",@"<p>Problem accessing ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_filenamenotfound)],@". Reason:\n",@"<pre>    Not Found</pre></p>\n",@"</body>\n",@"</html>"] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=5832722;
 //BA.debugLineNum = 5832722;BA.debugLine="astream_write(S.GetBytes(CharacterEncoding))";
[__ref _astream_write /*NSString**/ :nil :[_s GetBytes:__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=5832723;
 //BA.debugLineNum = 5832723;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _write:(b4i_servletresponse*) __ref :(NSString*) _text{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"write"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"write:" :@[[B4I nilToNSNull:_text]]]);}
B4IRDebugUtils.currentLine=5439488;
 //BA.debugLineNum = 5439488;BA.debugLine="Public Sub Write(Text As String)";
B4IRDebugUtils.currentLine=5439489;
 //BA.debugLineNum = 5439489;BA.debugLine="astream_write(Text.GetBytes(CharacterEncoding))";
[__ref _astream_write /*NSString**/ :nil :[_text GetBytes:__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=5439490;
 //BA.debugLineNum = 5439490;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setheader:(b4i_servletresponse*) __ref :(NSString*) _name :(NSString*) _value{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"setheader"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setheader::" :@[[B4I nilToNSNull:_name],[B4I nilToNSNull:_value]]]);}
B4IRDebugUtils.currentLine=4980736;
 //BA.debugLineNum = 4980736;BA.debugLine="Public Sub SetHeader(Name As String, Value As Stri";
B4IRDebugUtils.currentLine=4980737;
 //BA.debugLineNum = 4980737;BA.debugLine="ResponseHeader.Put(Name,Value)";
[__ref->__responseheader /*B4IMap**/  Put:(NSObject*)(_name) :(NSObject*)(_value)];
B4IRDebugUtils.currentLine=4980738;
 //BA.debugLineNum = 4980738;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendwebsocketpong:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendwebsocketpong"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendwebsocketpong" :nil]);}
B4IArray* _b = nil;
B4IRDebugUtils.currentLine=6553600;
 //BA.debugLineNum = 6553600;BA.debugLine="Public Sub SendWebSocketPong";
B4IRDebugUtils.currentLine=6553601;
 //BA.debugLineNum = 6553601;BA.debugLine="Dim B(2) As Byte";
_b = [[B4IArray alloc]initBytes:@[@((int) (2))]];
B4IRDebugUtils.currentLine=6553602;
 //BA.debugLineNum = 6553602;BA.debugLine="B(0)=Bit.Or(128,10)";
[_b setByteFast:(int) (0):@((unsigned char) ((((int) (128)) | ((int) (10)))))];
B4IRDebugUtils.currentLine=6553603;
 //BA.debugLineNum = 6553603;BA.debugLine="B(1)=0";
[_b setByteFast:(int) (1):@((unsigned char) (0))];
B4IRDebugUtils.currentLine=6553605;
 //BA.debugLineNum = 6553605;BA.debugLine="astream_write(B)";
[__ref _astream_write /*NSString**/ :nil :_b];
B4IRDebugUtils.currentLine=6553606;
 //BA.debugLineNum = 6553606;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _initialize:(b4i_servletresponse*) __ref :(B4I*) _ba :(b4i_servletrequest*) _req :(B4IAsyncStreams*) _ast :(B4ISocketWrapper*) _sck{
__ref = self;
[self initializeClassModule];
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"initialize"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"initialize::::" :@[[B4I nilToNSNull:_ba],[B4I nilToNSNull:_req],[B4I nilToNSNull:_ast],[B4I nilToNSNull:_sck]]]);}
B4IRDebugUtils.currentLine=4915200;
 //BA.debugLineNum = 4915200;BA.debugLine="Public Sub Initialize(Req As ServletRequest, ast A";
B4IRDebugUtils.currentLine=4915201;
 //BA.debugLineNum = 4915201;BA.debugLine="Request=Req";
__ref->__request /*b4i_servletrequest**/  = _req;
B4IRDebugUtils.currentLine=4915202;
 //BA.debugLineNum = 4915202;BA.debugLine="astream=ast";
__ref->__astream /*B4IAsyncStreams**/  = _ast;
B4IRDebugUtils.currentLine=4915203;
 //BA.debugLineNum = 4915203;BA.debugLine="Client=Sck";
__ref->__client /*B4ISocketWrapper**/  = _sck;
B4IRDebugUtils.currentLine=4915205;
 //BA.debugLineNum = 4915205;BA.debugLine="ResponseHeader.Initialize";
[__ref->__responseheader /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=4915206;
 //BA.debugLineNum = 4915206;BA.debugLine="ResponseParameter.Initialize";
[__ref->__responseparameter /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=4915207;
 //BA.debugLineNum = 4915207;BA.debugLine="ResponseCookies.Initialize";
[__ref->__responsecookies /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=4915209;
 //BA.debugLineNum = 4915209;BA.debugLine="ResponseHeader.Put(\"Accept-Ranges\",\"bytes\")";
[__ref->__responseheader /*B4IMap**/  Put:(NSObject*)(@"Accept-Ranges") :(NSObject*)(@"bytes")];
B4IRDebugUtils.currentLine=4915210;
 //BA.debugLineNum = 4915210;BA.debugLine="ResponseHeader.Put(\"Server\",\"b4x (0.0.1)\")";
[__ref->__responseheader /*B4IMap**/  Put:(NSObject*)(@"Server") :(NSObject*)(@"b4x (0.0.1)")];
B4IRDebugUtils.currentLine=4915211;
 //BA.debugLineNum = 4915211;BA.debugLine="ResponseHeader.Put(\"Accept-Charset\",\"utf-8\")";
[__ref->__responseheader /*B4IMap**/  Put:(NSObject*)(@"Accept-Charset") :(NSObject*)(@"utf-8")];
B4IRDebugUtils.currentLine=4915212;
 //BA.debugLineNum = 4915212;BA.debugLine="ResponseHeader.Put(\"Cache-control\",\"no-cache\")";
[__ref->__responseheader /*B4IMap**/  Put:(NSObject*)(@"Cache-control") :(NSObject*)(@"no-cache")];
B4IRDebugUtils.currentLine=4915215;
 //BA.debugLineNum = 4915215;BA.debugLine="myQuery.Initialize(Me)";
[__ref->__myquery /*b4i_queryelement**/  _initialize /*NSString**/ :nil :self.bi :(b4i_servletresponse*)(self)];
B4IRDebugUtils.currentLine=4915218;
 //BA.debugLineNum = 4915218;BA.debugLine="Code=CreateMap(100:\"Continue\", 101:\"Switching Pro";
__ref->__code /*B4IMap**/  = [self->___c createMap:@[(NSObject*)(@(100)),(NSObject*)(@"Continue"),(NSObject*)(@(101)),(NSObject*)(@"Switching Protocols"),(NSObject*)(@(200)),(NSObject*)(@"OK"),(NSObject*)(@(201)),(NSObject*)(@"Created"),(NSObject*)(@(202)),(NSObject*)(@"Accepted"),(NSObject*)(@(203)),(NSObject*)(@"Non-Authoritative Informatio"),(NSObject*)(@(204)),(NSObject*)(@"No Content"),(NSObject*)(@(205)),(NSObject*)(@"Reset Content"),(NSObject*)(@(206)),(NSObject*)(@"Partial Content"),(NSObject*)(@(300)),(NSObject*)(@"Multiple Choices"),(NSObject*)(@(301)),(NSObject*)(@"Moved Permanently"),(NSObject*)(@(302)),(NSObject*)(@"Found"),(NSObject*)(@(303)),(NSObject*)(@"See Other"),(NSObject*)(@(304)),(NSObject*)(@"Not Modified"),(NSObject*)(@(305)),(NSObject*)(@"Use Proxy"),(NSObject*)(@(307)),(NSObject*)(@"Temporary Redirect"),(NSObject*)(@(400)),(NSObject*)(@"Bad Request"),(NSObject*)(@(401)),(NSObject*)(@"Unauthorized"),(NSObject*)(@(402)),(NSObject*)(@"Payment Required"),(NSObject*)(@(403)),(NSObject*)(@"Forbidden"),(NSObject*)(@(404)),(NSObject*)(@"Not Found"),(NSObject*)(@(405)),(NSObject*)(@"	Method Not Allowed"),(NSObject*)(@(406)),(NSObject*)(@"Not Acceptable"),(NSObject*)(@(407)),(NSObject*)(@"Proxy Authentication Required"),(NSObject*)(@(408)),(NSObject*)(@"Request Time-out"),(NSObject*)(@(409)),(NSObject*)(@"Conflict"),(NSObject*)(@(410)),(NSObject*)(@"Gone"),(NSObject*)(@(411)),(NSObject*)(@"Length Required"),(NSObject*)(@(412)),(NSObject*)(@"Precondition Failed"),(NSObject*)(@(413)),(NSObject*)(@"Request Entity Too Large"),(NSObject*)(@(414)),(NSObject*)(@"Request-URI Too Large"),(NSObject*)(@(415)),(NSObject*)(@"Unsupported Media Type"),(NSObject*)(@(416)),(NSObject*)(@"Requested range not satisfiable"),(NSObject*)(@(417)),(NSObject*)(@"Expectation Failed"),(NSObject*)(@(500)),(NSObject*)(@"Internal Server Error"),(NSObject*)(@(502)),(NSObject*)(@"Bad Gateway"),(NSObject*)(@(503)),(NSObject*)(@"Service Unavailable"),(NSObject*)(@(504)),(NSObject*)(@"Gateway Time-out"),(NSObject*)(@(505)),(NSObject*)(@"HTTP Version Not Supported.")]];
B4IRDebugUtils.currentLine=4915227;
 //BA.debugLineNum = 4915227;BA.debugLine="MIME=CreateMap(\"htm\":\"text/html\",\"html\":\"text/htm";
__ref->__mime /*B4IMap**/  = [self->___c createMap:@[(NSObject*)(@"htm"),(NSObject*)(@"text/html"),(NSObject*)(@"html"),(NSObject*)(@"text/html"),(NSObject*)(@"xml"),(NSObject*)(@"text/xml"),(NSObject*)(@"txt"),(NSObject*)(@"text/plain"),(NSObject*)(@"pdf"),(NSObject*)(@"application/pdf"),(NSObject*)(@"css"),(NSObject*)(@"text/css"),(NSObject*)(@"csv"),(NSObject*)(@"text/csv"),(NSObject*)(@"js"),(NSObject*)(@"text/javascript"),(NSObject*)(@"json"),(NSObject*)(@"application/json"),(NSObject*)(@"doc"),(NSObject*)(@"application/msword"),(NSObject*)(@"docx"),(NSObject*)(@"application/vnd.openxmlformats-officedocument.wordprocessingml.document"),(NSObject*)(@"xls"),(NSObject*)(@"application/vnd.ms-excel"),(NSObject*)(@"xlsx"),(NSObject*)(@"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),(NSObject*)(@"odt"),(NSObject*)(@"application/vnd.oasis.opendocument.text"),(NSObject*)(@"ods"),(NSObject*)(@"application/vnd.oasis.opendocument.spreadsheet"),(NSObject*)(@"epub"),(NSObject*)(@"application/epub+zip"),(NSObject*)(@"jar"),(NSObject*)(@"application/java-archive"),(NSObject*)(@"rar"),(NSObject*)(@"application/vnd.rar"),(NSObject*)(@"zip"),(NSObject*)(@"application/zip"),(NSObject*)(@"jpeg"),(NSObject*)(@"image/jpeg"),(NSObject*)(@"jpg"),(NSObject*)(@"image/jpeg"),(NSObject*)(@"bmp"),(NSObject*)(@"image/bmp"),(NSObject*)(@"png"),(NSObject*)(@"image/png"),(NSObject*)(@"gif"),(NSObject*)(@"image/gif"),(NSObject*)(@"ico"),(NSObject*)(@"image/vnd.microsoft.icon"),(NSObject*)(@"mp3"),(NSObject*)(@"audio/mpeg"),(NSObject*)(@"avi"),(NSObject*)(@"video/x-msvideo"),(NSObject*)(@"mpeg"),(NSObject*)(@"video/mpeg")]];
B4IRDebugUtils.currentLine=4915237;
 //BA.debugLineNum = 4915237;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _close:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"close"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"close" :nil]);}
B4IRDebugUtils.currentLine=5177344;
 //BA.debugLineNum = 5177344;BA.debugLine="Public Sub Close";
B4IRDebugUtils.currentLine=5177345;
 //BA.debugLineNum = 5177345;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=5177346;
 //BA.debugLineNum = 5177346;BA.debugLine="Request.Close";
[__ref->__request /*b4i_servletrequest**/  _close /*NSString**/ :nil];
 } 
       @catch (NSException* e4) {
			[B4I SetException:e4];B4IRDebugUtils.currentLine=5177348;
 //BA.debugLineNum = 5177348;BA.debugLine="Log(\"Server error:\" & LastException.Message)";
[self->___c LogImpl:@"35177348" :[@[@"Server error:",[[self->___c LastException] Message]] componentsJoinedByString:@""] :0];
 };
B4IRDebugUtils.currentLine=5177350;
 //BA.debugLineNum = 5177350;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _appenfinal:(b4i_servletresponse*) __ref :(B4IArray*) _b{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"appenfinal"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"appenfinal:" :@[[B4I nilToNSNull:_b]]]);}
B4IArray* _fn = nil;
B4IRDebugUtils.currentLine=6750208;
 //BA.debugLineNum = 6750208;BA.debugLine="Private Sub AppenFinal(B() As Byte)";
B4IRDebugUtils.currentLine=6750209;
 //BA.debugLineNum = 6750209;BA.debugLine="Dim Fn(Final.Length+b.Length) As Byte";
_fn = [[B4IArray alloc]initBytes:@[@((int) (__ref->__final /*B4IArray**/ .Length+_b.Length))]];
B4IRDebugUtils.currentLine=6750210;
 //BA.debugLineNum = 6750210;BA.debugLine="Bit.ArrayCopy(Final,0,Fn,0,Final.Length)";
B4I_MEMCPY((__ref->__final /*B4IArray**/ )->internalBuffer + ((int) (0)), (_fn)->internalBuffer + ((int) (0)),__ref->__final /*B4IArray**/ .Length);
B4IRDebugUtils.currentLine=6750211;
 //BA.debugLineNum = 6750211;BA.debugLine="Bit.ArrayCopy(B,0,Fn,Final.Length,B.Length)";
B4I_MEMCPY((_b)->internalBuffer + ((int) (0)), (_fn)->internalBuffer + (__ref->__final /*B4IArray**/ .Length),_b.Length);
B4IRDebugUtils.currentLine=6750212;
 //BA.debugLineNum = 6750212;BA.debugLine="Final=Fn";
__ref->__final /*B4IArray**/  = _fn;
B4IRDebugUtils.currentLine=6750213;
 //BA.debugLineNum = 6750213;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _astream_write:(b4i_servletresponse*) __ref :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"astream_write"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"astream_write:" :@[[B4I nilToNSNull:_data]]]);}
B4IRDebugUtils.currentLine=6225920;
 //BA.debugLineNum = 6225920;BA.debugLine="Private Sub astream_write(Data() As Byte)";
B4IRDebugUtils.currentLine=6225921;
 //BA.debugLineNum = 6225921;BA.debugLine="Try";
@try {B4IRDebugUtils.currentLine=6225922;
 //BA.debugLineNum = 6225922;BA.debugLine="astream.Write(Data)";
[__ref->__astream /*B4IAsyncStreams**/  Write:_data];
 } 
       @catch (NSException* e4) {
			[B4I SetException:e4];B4IRDebugUtils.currentLine=6225924;
 //BA.debugLineNum = 6225924;BA.debugLine="Log(LastException)";
[self->___c LogImpl:@"36225924" :[self.bi ObjectToString:[self->___c LastException]] :0];
 };
B4IRDebugUtils.currentLine=6225926;
 //BA.debugLineNum = 6225926;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _class_globals:(b4i_servletresponse*) __ref{
__ref = self;
self->__main=[b4i_main new];
B4IRDebugUtils.currentModule=@"servletresponse";
B4IRDebugUtils.currentLine=4849664;
 //BA.debugLineNum = 4849664;BA.debugLine="Sub Class_Globals";
B4IRDebugUtils.currentLine=4849665;
 //BA.debugLineNum = 4849665;BA.debugLine="Private Request As ServletRequest";
self->__request = [b4i_servletrequest new];
B4IRDebugUtils.currentLine=4849666;
 //BA.debugLineNum = 4849666;BA.debugLine="Private astream As AsyncStreams";
self->__astream = [B4IAsyncStreams new];
B4IRDebugUtils.currentLine=4849668;
 //BA.debugLineNum = 4849668;BA.debugLine="Private ResponseHeader As Map";
self->__responseheader = [B4IMap new];
B4IRDebugUtils.currentLine=4849669;
 //BA.debugLineNum = 4849669;BA.debugLine="Private ResponseParameter As Map";
self->__responseparameter = [B4IMap new];
B4IRDebugUtils.currentLine=4849670;
 //BA.debugLineNum = 4849670;BA.debugLine="Private ResponseCookies As Map";
self->__responsecookies = [B4IMap new];
B4IRDebugUtils.currentLine=4849671;
 //BA.debugLineNum = 4849671;BA.debugLine="Private Code As Map";
self->__code = [B4IMap new];
B4IRDebugUtils.currentLine=4849672;
 //BA.debugLineNum = 4849672;BA.debugLine="Private MIME As Map";
self->__mime = [B4IMap new];
B4IRDebugUtils.currentLine=4849673;
 //BA.debugLineNum = 4849673;BA.debugLine="Private Client As Socket";
self->__client = [B4ISocketWrapper new];
B4IRDebugUtils.currentLine=4849675;
 //BA.debugLineNum = 4849675;BA.debugLine="Public Status As Int = 200";
self->__status = (int) (200);
B4IRDebugUtils.currentLine=4849676;
 //BA.debugLineNum = 4849676;BA.debugLine="Public ContentType As String = \"\"";
self->__contenttype = @"";
B4IRDebugUtils.currentLine=4849677;
 //BA.debugLineNum = 4849677;BA.debugLine="Public CharacterEncoding As String = \"UTF-8\" '\"IS";
self->__characterencoding = @"UTF-8";
B4IRDebugUtils.currentLine=4849678;
 //BA.debugLineNum = 4849678;BA.debugLine="Public ContentLenght As Int = 0";
self->__contentlenght = (int) (0);
B4IRDebugUtils.currentLine=4849680;
 //BA.debugLineNum = 4849680;BA.debugLine="Private DDay() As String = Array As String(\"\",\"Su";
self->__dday = [[B4IArray alloc]initObjectsWithData:@[[B4I nilToNSNull:@""],[B4I nilToNSNull:@"Sun"],[B4I nilToNSNull:@"Mon"],[B4I nilToNSNull:@"Tue"],[B4I nilToNSNull:@"Wed"],[B4I nilToNSNull:@"Thu"],[B4I nilToNSNull:@"Fri"],[B4I nilToNSNull:@"Sat"]]];
B4IRDebugUtils.currentLine=4849681;
 //BA.debugLineNum = 4849681;BA.debugLine="Private MMmonth() As String = Array As String(\"\",";
self->__mmmonth = [[B4IArray alloc]initObjectsWithData:@[[B4I nilToNSNull:@""],[B4I nilToNSNull:@"January"],[B4I nilToNSNull:@"February"],[B4I nilToNSNull:@"March"],[B4I nilToNSNull:@"April"],[B4I nilToNSNull:@"May"],[B4I nilToNSNull:@"June"],[B4I nilToNSNull:@"July"],[B4I nilToNSNull:@"August"],[B4I nilToNSNull:@"September"],[B4I nilToNSNull:@"October"],[B4I nilToNSNull:@"November"],[B4I nilToNSNull:@"December"]]];
B4IRDebugUtils.currentLine=4849683;
 //BA.debugLineNum = 4849683;BA.debugLine="Private BC As ByteConverter";
self->__bc = [B4IByteConverter new];
B4IRDebugUtils.currentLine=4849684;
 //BA.debugLineNum = 4849684;BA.debugLine="Private myQuery As QueryElement";
self->__myquery = [b4i_queryelement new];
B4IRDebugUtils.currentLine=4849685;
 //BA.debugLineNum = 4849685;BA.debugLine="Private Final() As Byte";
self->__final = [[B4IArray alloc]initBytes:@[@((int) (0))]];
B4IRDebugUtils.currentLine=4849691;
 //BA.debugLineNum = 4849691;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _completedate:(b4i_servletresponse*) __ref :(long long) _dt{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"completedate"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"completedate:" :@[@(_dt)]]);}
B4IRDebugUtils.currentLine=5898240;
 //BA.debugLineNum = 5898240;BA.debugLine="Private Sub CompleteDate(DT As Long) As String";
B4IRDebugUtils.currentLine=5898241;
 //BA.debugLineNum = 5898241;BA.debugLine="Return $\"${DDay(DateTime.GetDayOfWeek(DT))}, $2.0";
if (true) return ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(((NSString*)[__ref->__dday /*B4IArray**/  getObjectFast:[[self->___c DateTime] GetDayOfWeek:_dt]]))],@", ",[self->___c SmartStringFormatter:@"2.0" :(NSObject*)(@([[self->___c DateTime] GetDayOfMonth:_dt]))],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)([((NSString*)[__ref->__mmmonth /*B4IArray**/  getObjectFast:[[self->___c DateTime] GetMonth:_dt]]) SubString2:(int) (0) :(int) (3)])],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@([[self->___c DateTime] GetYear:_dt]))],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)([[self->___c DateTime] Time:_dt])],@" GMT"] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=5898242;
 //BA.debugLineNum = 5898242;BA.debugLine="End Sub";
return @"";
}
- (BOOL)  _connected:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"connected"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"connected" :nil]).boolValue;}
B4IRDebugUtils.currentLine=5308416;
 //BA.debugLineNum = 5308416;BA.debugLine="Public Sub Connected As Boolean";
B4IRDebugUtils.currentLine=5308417;
 //BA.debugLineNum = 5308417;BA.debugLine="Return Client.Connected";
if (true) return [__ref->__client /*B4ISocketWrapper**/  Connected];
B4IRDebugUtils.currentLine=5308418;
 //BA.debugLineNum = 5308418;BA.debugLine="End Sub";
return false;
}
- (B4IArray*)  _datamask:(b4i_servletresponse*) __ref :(B4IArray*) _data :(B4IArray*) _maskingkey{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"datamask"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"datamask::" :@[[B4I nilToNSNull:_data],[B4I nilToNSNull:_maskingkey]]]);}
B4IArray* _datafree = nil;
int _i = 0;
int _smask = 0;
B4IRDebugUtils.currentLine=6619136;
 //BA.debugLineNum = 6619136;BA.debugLine="Private Sub DataMask(Data() As Byte, Maskingkey()";
B4IRDebugUtils.currentLine=6619139;
 //BA.debugLineNum = 6619139;BA.debugLine="Dim DataFree(Data.Length) As Byte";
_datafree = [[B4IArray alloc]initBytes:@[@(_data.Length)]];
B4IRDebugUtils.currentLine=6619140;
 //BA.debugLineNum = 6619140;BA.debugLine="For i=0 To Data.Length-1";
{
const int step2 = 1;
const int limit2 = (int) (_data.Length-1);
_i = (int) (0) ;
for (;_i <= limit2 ;_i = _i + step2 ) {
B4IRDebugUtils.currentLine=6619141;
 //BA.debugLineNum = 6619141;BA.debugLine="Dim Smask As Int = Bit.And(0xFF, Data(i))";
_smask = (((int) (0xff)) & ((int) ([_data getByteFast:_i])));
B4IRDebugUtils.currentLine=6619142;
 //BA.debugLineNum = 6619142;BA.debugLine="DataFree(i)=Bit.Xor(Smask,Bit.And(0xFF,Maskingke";
[_datafree setByteFast:_i:@((unsigned char) (((_smask) ^ ((((int) (0xff)) & ((int) ([_maskingkey getByteFast:(int) (_i%4)])))))))];
 }
};
B4IRDebugUtils.currentLine=6619145;
 //BA.debugLineNum = 6619145;BA.debugLine="Return DataFree";
if (true) return _datafree;
B4IRDebugUtils.currentLine=6619146;
 //BA.debugLineNum = 6619146;BA.debugLine="End Sub";
return nil;
}
- (B4IArray*)  _deflatedate:(b4i_servletresponse*) __ref :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"deflatedate"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"deflatedate:" :@[[B4I nilToNSNull:_data]]]);}
B4INativeObject* _nativeme = nil;
NSObject* _infobject = nil;
B4IArray* _raw = nil;
B4IArray* _inft = nil;
B4IRDebugUtils.currentLine=6684672;
 //BA.debugLineNum = 6684672;BA.debugLine="Private Sub DeflateDate(data() As Byte) As Byte()";
B4IRDebugUtils.currentLine=6684676;
 //BA.debugLineNum = 6684676;BA.debugLine="Dim NativeMe As NativeObject = Me";
_nativeme = [B4INativeObject new];
_nativeme = (B4INativeObject*) [B4IObjectWrapper createWrapper:[B4INativeObject new] object:(NSObject*)(self)];
B4IRDebugUtils.currentLine=6684677;
 //BA.debugLineNum = 6684677;BA.debugLine="Dim infObject As Object = NativeMe.RunMethod(\"gz";
_infobject = (NSObject*)(([_nativeme RunMethod:@"gzipInflate:" :[[B4IArray alloc]initObjectsWithData:@[[B4I nilToNSNull:[_nativeme ArrayToNSData:_data]]]]]).object);
B4IRDebugUtils.currentLine=6684678;
 //BA.debugLineNum = 6684678;BA.debugLine="Dim raw() As Byte= NativeMe.NSDataToArray(infObj";
_raw = [_nativeme NSDataToArray:_infobject];
B4IRDebugUtils.currentLine=6684679;
 //BA.debugLineNum = 6684679;BA.debugLine="Dim Inft(raw.Length-2) As Byte";
_inft = [[B4IArray alloc]initBytes:@[@((int) (_raw.Length-2))]];
B4IRDebugUtils.currentLine=6684680;
 //BA.debugLineNum = 6684680;BA.debugLine="Bit.ArrayCopy(raw,2,Inft,0,raw.Length-2)";
B4I_MEMCPY((_raw)->internalBuffer + ((int) (2)), (_inft)->internalBuffer + ((int) (0)),(int) (_raw.Length-2));
B4IRDebugUtils.currentLine=6684681;
 //BA.debugLineNum = 6684681;BA.debugLine="Return Inft";
if (true) return _inft;
B4IRDebugUtils.currentLine=6684698;
 //BA.debugLineNum = 6684698;BA.debugLine="End Sub";
return nil;
}
- (B4IOutputStream*)  _getoutputstream:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"getoutputstream"])
	 {return ((B4IOutputStream*) [B4IDebug delegate:self.bi :@"getoutputstream" :nil]);}
B4IRDebugUtils.currentLine=5373952;
 //BA.debugLineNum = 5373952;BA.debugLine="Public Sub getOutputStream As OutputStream";
B4IRDebugUtils.currentLine=5373953;
 //BA.debugLineNum = 5373953;BA.debugLine="Return Client.OutputStream";
if (true) return [__ref->__client /*B4ISocketWrapper**/  OutputStream];
B4IRDebugUtils.currentLine=5373954;
 //BA.debugLineNum = 5373954;BA.debugLine="End Sub";
return nil;
}
- (b4i_queryelement*)  _getquery:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"getquery"])
	 {return ((b4i_queryelement*) [B4IDebug delegate:self.bi :@"getquery" :nil]);}
B4IRDebugUtils.currentLine=5242880;
 //BA.debugLineNum = 5242880;BA.debugLine="Public Sub getQuery As QueryElement";
B4IRDebugUtils.currentLine=5242881;
 //BA.debugLineNum = 5242881;BA.debugLine="Return myQuery";
if (true) return __ref->__myquery /*b4i_queryelement**/ ;
B4IRDebugUtils.currentLine=5242882;
 //BA.debugLineNum = 5242882;BA.debugLine="End Sub";
return nil;
}
- (NSString*)  _handshakefile:(b4i_servletresponse*) __ref :(NSString*) _lastmodified :(long long) _filesize{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"handshakefile"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"handshakefile::" :@[[B4I nilToNSNull:_lastmodified],@(_filesize)]]);}
NSString* _handshake = @"";
NSString* _k = @"";
NSString* _scks = @"";
B4IRDebugUtils.currentLine=6094848;
 //BA.debugLineNum = 6094848;BA.debugLine="Private Sub HandShakeFile(lastModified As String,";
B4IRDebugUtils.currentLine=6094849;
 //BA.debugLineNum = 6094849;BA.debugLine="Dim HandShake As String = \"\"";
_handshake = @"";
B4IRDebugUtils.currentLine=6094852;
 //BA.debugLineNum = 6094852;BA.debugLine="For Each K As String In ResponseHeader.Keys";
{
const id<B4IIterable> group2 = [__ref->__responseheader /*B4IMap**/  Keys];
const int groupLen2 = group2.Size
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_k = [self.bi ObjectToString:[group2 Get:index2]];
B4IRDebugUtils.currentLine=6094853;
 //BA.debugLineNum = 6094853;BA.debugLine="HandShake=$\"${HandShake}${k}: ${ResponseHeader.G";
_handshake = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_k)],@": ",[self->___c SmartStringFormatter:@"" :[__ref->__responseheader /*B4IMap**/  Get:(NSObject*)(_k)]],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@""] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=6094856;
 //BA.debugLineNum = 6094856;BA.debugLine="Dim Scks As String = \"\"";
_scks = @"";
B4IRDebugUtils.currentLine=6094857;
 //BA.debugLineNum = 6094857;BA.debugLine="For Each K As String In ResponseCookies.Keys";
{
const id<B4IIterable> group6 = [__ref->__responsecookies /*B4IMap**/  Keys];
const int groupLen6 = group6.Size
;int index6 = 0;
;
for (; index6 < groupLen6;index6++){
_k = [self.bi ObjectToString:[group6 Get:index6]];
B4IRDebugUtils.currentLine=6094858;
 //BA.debugLineNum = 6094858;BA.debugLine="Scks=$\"${Scks}${k}=${ResponseCookies.Get(K)}; \"$";
_scks = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_k)],@"=",[self->___c SmartStringFormatter:@"" :[__ref->__responsecookies /*B4IMap**/  Get:(NSObject*)(_k)]],@"; "] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=6094860;
 //BA.debugLineNum = 6094860;BA.debugLine="If Scks<>\"\" Then Scks=$\"Set-Cookie: ${Scks}${CRLF";
if ([_scks isEqual:@""] == false) { 
_scks = ([@[@"Set-Cookie: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@"Expires=",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :(long long) ([[self->___c DateTime] Now]+([[self->___c DateTime] TicksPerDay]*365))])],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@""] componentsJoinedByString:@""]);};
B4IRDebugUtils.currentLine=6094862;
 //BA.debugLineNum = 6094862;BA.debugLine="HandShake=$\"HTTP/1.1 200 OK Date: ${CompleteDate(";
_handshake = ([@[@"HTTP/1.1 200 OK\n",@"Date: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@"Last-Modified: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_lastmodified)],@"\n",@"Content-Type: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__contenttype /*NSString**/ )],@"\n",@"Accept-Ranges: bytes\n",@"Content-Length: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@(_filesize))],@"\n",@"Server: HttpServer (B4X)\n",@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"\n",@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=6094871;
 //BA.debugLineNum = 6094871;BA.debugLine="Return HandShake.Replace(CRLF,Chr(13) & Chr(10))";
if (true) return [_handshake Replace:@"\n" :[@[[self.bi CharToString:((unichar)((int) (13)))],[self.bi CharToString:((unichar)((int) (10)))]] componentsJoinedByString:@""]];
B4IRDebugUtils.currentLine=6094872;
 //BA.debugLineNum = 6094872;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _handshakestring:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"handshakestring"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"handshakestring" :nil]);}
NSString* _handshake = @"";
NSString* _k = @"";
NSString* _scks = @"";
B4IRDebugUtils.currentLine=6029312;
 //BA.debugLineNum = 6029312;BA.debugLine="Private Sub HandShakeString As String";
B4IRDebugUtils.currentLine=6029313;
 //BA.debugLineNum = 6029313;BA.debugLine="Dim HandShake As String = \"\"";
_handshake = @"";
B4IRDebugUtils.currentLine=6029316;
 //BA.debugLineNum = 6029316;BA.debugLine="For Each K As String In ResponseHeader.Keys";
{
const id<B4IIterable> group2 = [__ref->__responseheader /*B4IMap**/  Keys];
const int groupLen2 = group2.Size
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_k = [self.bi ObjectToString:[group2 Get:index2]];
B4IRDebugUtils.currentLine=6029317;
 //BA.debugLineNum = 6029317;BA.debugLine="HandShake=$\"${HandShake}${k}: ${ResponseHeader.G";
_handshake = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_k)],@": ",[self->___c SmartStringFormatter:@"" :[__ref->__responseheader /*B4IMap**/  Get:(NSObject*)(_k)]],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@""] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=6029320;
 //BA.debugLineNum = 6029320;BA.debugLine="Dim Scks As String = \"\"";
_scks = @"";
B4IRDebugUtils.currentLine=6029321;
 //BA.debugLineNum = 6029321;BA.debugLine="For Each K As String In ResponseCookies.Keys";
{
const id<B4IIterable> group6 = [__ref->__responsecookies /*B4IMap**/  Keys];
const int groupLen6 = group6.Size
;int index6 = 0;
;
for (; index6 < groupLen6;index6++){
_k = [self.bi ObjectToString:[group6 Get:index6]];
B4IRDebugUtils.currentLine=6029322;
 //BA.debugLineNum = 6029322;BA.debugLine="Scks=$\"${Scks}${k}=${ResponseCookies.Get(K)}; \"$";
_scks = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_k)],@"=",[self->___c SmartStringFormatter:@"" :[__ref->__responsecookies /*B4IMap**/  Get:(NSObject*)(_k)]],@"; "] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=6029324;
 //BA.debugLineNum = 6029324;BA.debugLine="If Scks<>\"\" Then Scks=$\"Set-Cookie: ${Scks}${CRLF";
if ([_scks isEqual:@""] == false) { 
_scks = ([@[@"Set-Cookie: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@"Expires=",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :(long long) ([[self->___c DateTime] Now]+([[self->___c DateTime] TicksPerDay]*365))])],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@""] componentsJoinedByString:@""]);};
B4IRDebugUtils.currentLine=6029327;
 //BA.debugLineNum = 6029327;BA.debugLine="HandShake=$\"HTTP/1.1 ${Status} ${StatusCode(Statu";
_handshake = ([@[@"HTTP/1.1 ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@(__ref->__status /*int*/ ))],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _statuscode /*NSString**/ :nil :__ref->__status /*int*/ ])],@"\n",@"Server: HttpServer (B4X)\n",@"Date: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@"Last-Modified: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@"Content-Type: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__contenttype /*NSString**/ )],@"; charset=",[self->___c SmartStringFormatter:@"" :(NSObject*)(__ref->__characterencoding /*NSString**/ )],@"\n",@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"Content-Length: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@(__ref->__contentlenght /*int*/ ))],@"\n",@"\n",@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=6029335;
 //BA.debugLineNum = 6029335;BA.debugLine="Return HandShake";
if (true) return _handshake;
B4IRDebugUtils.currentLine=6029336;
 //BA.debugLineNum = 6029336;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _statuscode:(b4i_servletresponse*) __ref :(int) _sts{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"statuscode"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"statuscode:" :@[@(_sts)]]);}
B4IRDebugUtils.currentLine=5963776;
 //BA.debugLineNum = 5963776;BA.debugLine="Private Sub StatusCode(Sts As Int) As String";
B4IRDebugUtils.currentLine=5963777;
 //BA.debugLineNum = 5963777;BA.debugLine="If Code.ContainsKey(Sts) Then";
if ([__ref->__code /*B4IMap**/  ContainsKey:(NSObject*)(@(_sts))]) { 
B4IRDebugUtils.currentLine=5963778;
 //BA.debugLineNum = 5963778;BA.debugLine="Return Code.Get(Sts)";
if (true) return [self.bi ObjectToString:[__ref->__code /*B4IMap**/  Get:(NSObject*)(@(_sts))]];
 }else {
B4IRDebugUtils.currentLine=5963780;
 //BA.debugLineNum = 5963780;BA.debugLine="Return \"\"";
if (true) return @"";
 };
B4IRDebugUtils.currentLine=5963782;
 //BA.debugLineNum = 5963782;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _resetcookies:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"resetcookies"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"resetcookies" :nil]);}
B4IRDebugUtils.currentLine=5111808;
 //BA.debugLineNum = 5111808;BA.debugLine="Public Sub ResetCookies";
B4IRDebugUtils.currentLine=5111809;
 //BA.debugLineNum = 5111809;BA.debugLine="ResponseCookies.Clear";
[__ref->__responsecookies /*B4IMap**/  Clear];
B4IRDebugUtils.currentLine=5111810;
 //BA.debugLineNum = 5111810;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _searchmime:(b4i_servletresponse*) __ref :(NSString*) _nm{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"searchmime"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"searchmime:" :@[[B4I nilToNSNull:_nm]]]);}
B4IRDebugUtils.currentLine=6160384;
 //BA.debugLineNum = 6160384;BA.debugLine="private Sub SearchMime(Nm As String) As String";
B4IRDebugUtils.currentLine=6160385;
 //BA.debugLineNum = 6160385;BA.debugLine="If Nm.IndexOf(\".\")>-1 Then";
if ([_nm IndexOf:@"."]>-1) { 
B4IRDebugUtils.currentLine=6160386;
 //BA.debugLineNum = 6160386;BA.debugLine="Nm=Nm.ToLowerCase.SubString(Nm.IndexOf(\".\")+1)";
_nm = [[_nm ToLowerCase] SubString:(int) ([_nm IndexOf:@"."]+1)];
B4IRDebugUtils.currentLine=6160387;
 //BA.debugLineNum = 6160387;BA.debugLine="If MIME.ContainsKey(Nm) Then";
if ([__ref->__mime /*B4IMap**/  ContainsKey:(NSObject*)(_nm)]) { 
B4IRDebugUtils.currentLine=6160388;
 //BA.debugLineNum = 6160388;BA.debugLine="Return MIME.Get(Nm)";
if (true) return [self.bi ObjectToString:[__ref->__mime /*B4IMap**/  Get:(NSObject*)(_nm)]];
 };
 };
B4IRDebugUtils.currentLine=6160392;
 //BA.debugLineNum = 6160392;BA.debugLine="Return \"application/*.*\"";
if (true) return @"application/*.*";
B4IRDebugUtils.currentLine=6160393;
 //BA.debugLineNum = 6160393;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendfile2:(b4i_servletresponse*) __ref :(NSString*) _dir :(NSString*) _filename :(NSString*) _content_type{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendfile2"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendfile2:::" :@[[B4I nilToNSNull:_dir],[B4I nilToNSNull:_filename],[B4I nilToNSNull:_content_type]]]);}
long long _filesize = 0L;
NSString* _lastmodified = @"";
B4IRDebugUtils.currentLine=5701632;
 //BA.debugLineNum = 5701632;BA.debugLine="Public Sub SendFile2(Dir As String, fileName As St";
B4IRDebugUtils.currentLine=5701633;
 //BA.debugLineNum = 5701633;BA.debugLine="If File.Exists(Dir, fileName) Then";
if ([[self->___c File] Exists:_dir :_filename]) { 
B4IRDebugUtils.currentLine=5701634;
 //BA.debugLineNum = 5701634;BA.debugLine="Dim FileSize As Long = File.Size(Dir, fileName)";
_filesize = [[self->___c File] Size:_dir :_filename];
B4IRDebugUtils.currentLine=5701635;
 //BA.debugLineNum = 5701635;BA.debugLine="Dim lastModified As String = CompleteDate(File.L";
_lastmodified = [__ref _completedate /*NSString**/ :nil :[[self->___c File] LastModified:_dir :_filename]];
B4IRDebugUtils.currentLine=5701637;
 //BA.debugLineNum = 5701637;BA.debugLine="ContentType=Content_Type";
__ref->__contenttype /*NSString**/  = _content_type;
B4IRDebugUtils.currentLine=5701638;
 //BA.debugLineNum = 5701638;BA.debugLine="astream_write(HandShakeFile(lastModified,FileSiz";
[__ref _astream_write /*NSString**/ :nil :[[__ref _handshakefile /*NSString**/ :nil :_lastmodified :_filesize] GetBytes:__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=5701640;
 //BA.debugLineNum = 5701640;BA.debugLine="astream_write(File.ReadBytes(Dir, fileName))";
[__ref _astream_write /*NSString**/ :nil :[[self->___c File] ReadBytes:_dir :_filename]];
 }else {
B4IRDebugUtils.currentLine=5701642;
 //BA.debugLineNum = 5701642;BA.debugLine="SendNotFound(fileName)";
[__ref _sendnotfound /*NSString**/ :nil :_filename];
 };
B4IRDebugUtils.currentLine=5701644;
 //BA.debugLineNum = 5701644;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendraw:(b4i_servletresponse*) __ref :(B4IArray*) _data{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendraw"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendraw:" :@[[B4I nilToNSNull:_data]]]);}
B4IRDebugUtils.currentLine=5570560;
 //BA.debugLineNum = 5570560;BA.debugLine="Public Sub SendRaw(Data() As Byte)";
B4IRDebugUtils.currentLine=5570561;
 //BA.debugLineNum = 5570561;BA.debugLine="astream_write(Data)";
[__ref _astream_write /*NSString**/ :nil :_data];
B4IRDebugUtils.currentLine=5570562;
 //BA.debugLineNum = 5570562;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendredirect:(b4i_servletresponse*) __ref :(NSString*) _address{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendredirect"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendredirect:" :@[[B4I nilToNSNull:_address]]]);}
NSString* _handshake = @"";
NSString* _k = @"";
NSString* _scks = @"";
NSString* _s = @"";
B4IRDebugUtils.currentLine=5767168;
 //BA.debugLineNum = 5767168;BA.debugLine="Public Sub SendRedirect(Address As String)";
B4IRDebugUtils.currentLine=5767169;
 //BA.debugLineNum = 5767169;BA.debugLine="Dim HandShake As String = \"\"";
_handshake = @"";
B4IRDebugUtils.currentLine=5767172;
 //BA.debugLineNum = 5767172;BA.debugLine="For Each K As String In ResponseHeader.Keys";
{
const id<B4IIterable> group2 = [__ref->__responseheader /*B4IMap**/  Keys];
const int groupLen2 = group2.Size
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_k = [self.bi ObjectToString:[group2 Get:index2]];
B4IRDebugUtils.currentLine=5767173;
 //BA.debugLineNum = 5767173;BA.debugLine="HandShake=$\"${HandShake}${k}: ${ResponseHeader.G";
_handshake = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_k)],@": ",[self->___c SmartStringFormatter:@"" :[__ref->__responseheader /*B4IMap**/  Get:(NSObject*)(_k)]],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@""] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=5767176;
 //BA.debugLineNum = 5767176;BA.debugLine="Dim Scks As String = \"\"";
_scks = @"";
B4IRDebugUtils.currentLine=5767177;
 //BA.debugLineNum = 5767177;BA.debugLine="For Each K As String In ResponseCookies.Keys";
{
const id<B4IIterable> group6 = [__ref->__responsecookies /*B4IMap**/  Keys];
const int groupLen6 = group6.Size
;int index6 = 0;
;
for (; index6 < groupLen6;index6++){
_k = [self.bi ObjectToString:[group6 Get:index6]];
B4IRDebugUtils.currentLine=5767178;
 //BA.debugLineNum = 5767178;BA.debugLine="Scks=$\"${Scks}${k}=${ResponseCookies.Get(K)}; \"$";
_scks = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_k)],@"=",[self->___c SmartStringFormatter:@"" :[__ref->__responsecookies /*B4IMap**/  Get:(NSObject*)(_k)]],@"; "] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=5767180;
 //BA.debugLineNum = 5767180;BA.debugLine="If Scks<>\"\" Then Scks=$\"Set-Cookie: ${Scks}${CRLF";
if ([_scks isEqual:@""] == false) { 
_scks = ([@[@"Set-Cookie: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@"Expires=",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :(long long) ([[self->___c DateTime] Now]+([[self->___c DateTime] TicksPerDay]*365))])],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(@"\n")],@""] componentsJoinedByString:@""]);};
B4IRDebugUtils.currentLine=5767183;
 //BA.debugLineNum = 5767183;BA.debugLine="Dim S As String = $\"HTTP/1.1 302 Found Date: ${Co";
_s = ([@[@"HTTP/1.1 302 Found\n",@"Date: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([__ref _completedate /*NSString**/ :nil :[[self->___c DateTime] Now]])],@"\n",@"Location: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_address)],@"\n",@"Content-Length: 0\n",@"Server: HttpServer (B4X)\n",@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_handshake)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_scks)],@"\n",@""] componentsJoinedByString:@""]);
B4IRDebugUtils.currentLine=5767190;
 //BA.debugLineNum = 5767190;BA.debugLine="astream_write(S.GetBytes(CharacterEncoding))";
[__ref _astream_write /*NSString**/ :nil :[_s GetBytes:__ref->__characterencoding /*NSString**/ ]];
B4IRDebugUtils.currentLine=5767191;
 //BA.debugLineNum = 5767191;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendwebsocketbinary:(b4i_servletresponse*) __ref :(B4IArray*) _data :(BOOL) _masked{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendwebsocketbinary"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendwebsocketbinary::" :@[[B4I nilToNSNull:_data],@(_masked)]]);}
B4IRDebugUtils.currentLine=6356992;
 //BA.debugLineNum = 6356992;BA.debugLine="Public Sub SendWebSocketBinary(Data() As Byte, Mas";
B4IRDebugUtils.currentLine=6356994;
 //BA.debugLineNum = 6356994;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendwebsocketclose:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendwebsocketclose"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendwebsocketclose" :nil]);}
B4IArray* _b = nil;
B4IRDebugUtils.currentLine=6422528;
 //BA.debugLineNum = 6422528;BA.debugLine="Public Sub SendWebSocketClose";
B4IRDebugUtils.currentLine=6422529;
 //BA.debugLineNum = 6422529;BA.debugLine="Dim B(2) As Byte";
_b = [[B4IArray alloc]initBytes:@[@((int) (2))]];
B4IRDebugUtils.currentLine=6422530;
 //BA.debugLineNum = 6422530;BA.debugLine="B(0)=Bit.Or(128,8)";
[_b setByteFast:(int) (0):@((unsigned char) ((((int) (128)) | ((int) (8)))))];
B4IRDebugUtils.currentLine=6422531;
 //BA.debugLineNum = 6422531;BA.debugLine="B(1)=0";
[_b setByteFast:(int) (1):@((unsigned char) (0))];
B4IRDebugUtils.currentLine=6422533;
 //BA.debugLineNum = 6422533;BA.debugLine="astream_write(B)";
[__ref _astream_write /*NSString**/ :nil :_b];
B4IRDebugUtils.currentLine=6422534;
 //BA.debugLineNum = 6422534;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendwebsocketping:(b4i_servletresponse*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendwebsocketping"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendwebsocketping" :nil]);}
B4IArray* _b = nil;
B4IRDebugUtils.currentLine=6488064;
 //BA.debugLineNum = 6488064;BA.debugLine="Public Sub SendWebSocketPing";
B4IRDebugUtils.currentLine=6488065;
 //BA.debugLineNum = 6488065;BA.debugLine="Dim B(2) As Byte";
_b = [[B4IArray alloc]initBytes:@[@((int) (2))]];
B4IRDebugUtils.currentLine=6488066;
 //BA.debugLineNum = 6488066;BA.debugLine="B(0)=Bit.Or(128,9)";
[_b setByteFast:(int) (0):@((unsigned char) ((((int) (128)) | ((int) (9)))))];
B4IRDebugUtils.currentLine=6488067;
 //BA.debugLineNum = 6488067;BA.debugLine="B(1)=0";
[_b setByteFast:(int) (1):@((unsigned char) (0))];
B4IRDebugUtils.currentLine=6488069;
 //BA.debugLineNum = 6488069;BA.debugLine="astream_write(B)";
[__ref _astream_write /*NSString**/ :nil :_b];
B4IRDebugUtils.currentLine=6488070;
 //BA.debugLineNum = 6488070;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sendwebsocketstring:(b4i_servletresponse*) __ref :(NSString*) _text :(BOOL) _masked :(NSString*) _compressed{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"sendwebsocketstring"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sendwebsocketstring:::" :@[[B4I nilToNSNull:_text],@(_masked),[B4I nilToNSNull:_compressed]]]);}
B4IArray* _payload = nil;
B4IArray* _head = nil;
B4IArray* _maskkey = nil;
int _i = 0;
B4IRDebugUtils.currentLine=6291456;
 //BA.debugLineNum = 6291456;BA.debugLine="Public Sub SendWebSocketString(Text As String, Mas";
B4IRDebugUtils.currentLine=6291457;
 //BA.debugLineNum = 6291457;BA.debugLine="Compressed=\"\"";
_compressed = @"";
B4IRDebugUtils.currentLine=6291458;
 //BA.debugLineNum = 6291458;BA.debugLine="Dim PayLoad() As Byte";
_payload = [[B4IArray alloc]initBytes:@[@((int) (0))]];
B4IRDebugUtils.currentLine=6291459;
 //BA.debugLineNum = 6291459;BA.debugLine="If Compressed<>\"none\" And Compressed<>\"\" Then";
if ([_compressed isEqual:@"none"] == false && [_compressed isEqual:@""] == false) { 
B4IRDebugUtils.currentLine=6291460;
 //BA.debugLineNum = 6291460;BA.debugLine="PayLoad=DeflateDate(Text.GetBytes(\"UTF8\"))";
_payload = [__ref _deflatedate /*B4IArray**/ :nil :[_text GetBytes:@"UTF8"]];
 }else {
B4IRDebugUtils.currentLine=6291462;
 //BA.debugLineNum = 6291462;BA.debugLine="PayLoad= Text.GetBytes(\"UTF8\")";
_payload = [_text GetBytes:@"UTF8"];
 };
B4IRDebugUtils.currentLine=6291465;
 //BA.debugLineNum = 6291465;BA.debugLine="Final= Array As Byte()";
__ref->__final /*B4IArray**/  = [[B4IArray alloc]initBytesWithData:@[]];
B4IRDebugUtils.currentLine=6291466;
 //BA.debugLineNum = 6291466;BA.debugLine="If PayLoad.Length<126 Then";
if (_payload.Length<126) { 
B4IRDebugUtils.currentLine=6291467;
 //BA.debugLineNum = 6291467;BA.debugLine="Dim Head(2) As Byte";
_head = [[B4IArray alloc]initBytes:@[@((int) (2))]];
B4IRDebugUtils.currentLine=6291468;
 //BA.debugLineNum = 6291468;BA.debugLine="Head(0)=129 ' Final+ Compressed (64-100) + Text";
[_head setByteFast:(int) (0):@((unsigned char) (129))];
B4IRDebugUtils.currentLine=6291469;
 //BA.debugLineNum = 6291469;BA.debugLine="Head(1)=PayLoad.Length";
[_head setByteFast:(int) (1):@((unsigned char) (_payload.Length))];
 }else if(_payload.Length<65536) { 
B4IRDebugUtils.currentLine=6291471;
 //BA.debugLineNum = 6291471;BA.debugLine="Dim Head(4) As Byte";
_head = [[B4IArray alloc]initBytes:@[@((int) (4))]];
B4IRDebugUtils.currentLine=6291472;
 //BA.debugLineNum = 6291472;BA.debugLine="Head(0)=129 ' Final+ reserved (64-100) + Text";
[_head setByteFast:(int) (0):@((unsigned char) (129))];
B4IRDebugUtils.currentLine=6291473;
 //BA.debugLineNum = 6291473;BA.debugLine="Head(1)=126 ' 2 Byte";
[_head setByteFast:(int) (1):@((unsigned char) (126))];
B4IRDebugUtils.currentLine=6291474;
 //BA.debugLineNum = 6291474;BA.debugLine="Head(2)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
[_head setByteFast:(int) (2):@([[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_payload.Length)]]] getByteFast:(int) (2)])];
B4IRDebugUtils.currentLine=6291475;
 //BA.debugLineNum = 6291475;BA.debugLine="Head(3)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
[_head setByteFast:(int) (3):@([[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_payload.Length)]]] getByteFast:(int) (3)])];
 }else {
B4IRDebugUtils.currentLine=6291477;
 //BA.debugLineNum = 6291477;BA.debugLine="Dim Head(10) As Byte";
_head = [[B4IArray alloc]initBytes:@[@((int) (10))]];
B4IRDebugUtils.currentLine=6291478;
 //BA.debugLineNum = 6291478;BA.debugLine="Head(0)=129 ' Final+ reserved (64-100) + Text";
[_head setByteFast:(int) (0):@((unsigned char) (129))];
B4IRDebugUtils.currentLine=6291479;
 //BA.debugLineNum = 6291479;BA.debugLine="Head(1)=127 ' 4 byte";
[_head setByteFast:(int) (1):@((unsigned char) (127))];
B4IRDebugUtils.currentLine=6291480;
 //BA.debugLineNum = 6291480;BA.debugLine="Head(2)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
[_head setByteFast:(int) (2):@([[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_payload.Length)]]] getByteFast:(int) (0)])];
B4IRDebugUtils.currentLine=6291481;
 //BA.debugLineNum = 6291481;BA.debugLine="Head(3)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
[_head setByteFast:(int) (3):@([[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_payload.Length)]]] getByteFast:(int) (1)])];
B4IRDebugUtils.currentLine=6291482;
 //BA.debugLineNum = 6291482;BA.debugLine="Head(4)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
[_head setByteFast:(int) (4):@([[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_payload.Length)]]] getByteFast:(int) (2)])];
B4IRDebugUtils.currentLine=6291483;
 //BA.debugLineNum = 6291483;BA.debugLine="Head(5)=BC.IntsToBytes(Array As Int(PayLoad.Leng";
[_head setByteFast:(int) (5):@([[__ref->__bc /*B4IByteConverter**/  IntsToBytes:[[B4IArray alloc]initObjectsWithData:@[@(_payload.Length)]]] getByteFast:(int) (3)])];
B4IRDebugUtils.currentLine=6291484;
 //BA.debugLineNum = 6291484;BA.debugLine="Head(6)=0";
[_head setByteFast:(int) (6):@((unsigned char) (0))];
B4IRDebugUtils.currentLine=6291485;
 //BA.debugLineNum = 6291485;BA.debugLine="Head(7)=0";
[_head setByteFast:(int) (7):@((unsigned char) (0))];
B4IRDebugUtils.currentLine=6291486;
 //BA.debugLineNum = 6291486;BA.debugLine="Head(8)=0";
[_head setByteFast:(int) (8):@((unsigned char) (0))];
B4IRDebugUtils.currentLine=6291487;
 //BA.debugLineNum = 6291487;BA.debugLine="Head(9)=0";
[_head setByteFast:(int) (9):@((unsigned char) (0))];
 };
B4IRDebugUtils.currentLine=6291489;
 //BA.debugLineNum = 6291489;BA.debugLine="If Compressed<>\"\" And Compressed<>\"none\" Then Hea";
if ([_compressed isEqual:@""] == false && [_compressed isEqual:@"none"] == false) { 
[_head setByteFast:(int) (0):@((unsigned char) ((((int) (129)) | ((int) (64)))))];};
B4IRDebugUtils.currentLine=6291491;
 //BA.debugLineNum = 6291491;BA.debugLine="If Masked Then";
if (_masked) { 
B4IRDebugUtils.currentLine=6291492;
 //BA.debugLineNum = 6291492;BA.debugLine="Head(1)=Bit.Or(128,Head(1))";
[_head setByteFast:(int) (1):@((unsigned char) ((((int) (128)) | ((int) ([_head getByteFast:(int) (1)])))))];
B4IRDebugUtils.currentLine=6291493;
 //BA.debugLineNum = 6291493;BA.debugLine="Dim MaskKey(4) As Byte";
_maskkey = [[B4IArray alloc]initBytes:@[@((int) (4))]];
B4IRDebugUtils.currentLine=6291494;
 //BA.debugLineNum = 6291494;BA.debugLine="For i=0 To 3";
{
const int step36 = 1;
const int limit36 = (int) (3);
_i = (int) (0) ;
for (;_i <= limit36 ;_i = _i + step36 ) {
B4IRDebugUtils.currentLine=6291495;
 //BA.debugLineNum = 6291495;BA.debugLine="MaskKey(0)=Rnd(0,256)";
[_maskkey setByteFast:(int) (0):@((unsigned char) ([self->___c Rnd:(int) (0) :(int) (256)]))];
 }
};
B4IRDebugUtils.currentLine=6291497;
 //BA.debugLineNum = 6291497;BA.debugLine="AppenFinal(MaskKey)";
[__ref _appenfinal /*NSString**/ :nil :_maskkey];
B4IRDebugUtils.currentLine=6291498;
 //BA.debugLineNum = 6291498;BA.debugLine="AppenFinal(DataMask(PayLoad,MaskKey))";
[__ref _appenfinal /*NSString**/ :nil :[__ref _datamask /*B4IArray**/ :nil :_payload :_maskkey]];
 }else {
B4IRDebugUtils.currentLine=6291502;
 //BA.debugLineNum = 6291502;BA.debugLine="AppenFinal(Head)";
[__ref _appenfinal /*NSString**/ :nil :_head];
B4IRDebugUtils.currentLine=6291503;
 //BA.debugLineNum = 6291503;BA.debugLine="AppenFinal(PayLoad)";
[__ref _appenfinal /*NSString**/ :nil :_payload];
 };
B4IRDebugUtils.currentLine=6291506;
 //BA.debugLineNum = 6291506;BA.debugLine="astream_write(Final)";
[__ref _astream_write /*NSString**/ :nil :__ref->__final /*B4IArray**/ ];
B4IRDebugUtils.currentLine=6291507;
 //BA.debugLineNum = 6291507;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setcookies:(b4i_servletresponse*) __ref :(NSString*) _name :(NSString*) _value{
__ref = self;
B4IRDebugUtils.currentModule=@"servletresponse";
if ([B4IDebug shouldDelegate: @"setcookies"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setcookies::" :@[[B4I nilToNSNull:_name],[B4I nilToNSNull:_value]]]);}
B4IRDebugUtils.currentLine=5046272;
 //BA.debugLineNum = 5046272;BA.debugLine="Public Sub SetCookies(Name As String,Value As Stri";
B4IRDebugUtils.currentLine=5046273;
 //BA.debugLineNum = 5046273;BA.debugLine="ResponseCookies.Put(Name,Value)";
[__ref->__responsecookies /*B4IMap**/  Put:(NSObject*)(_name) :(NSObject*)(_value)];
B4IRDebugUtils.currentLine=5046274;
 //BA.debugLineNum = 5046274;BA.debugLine="End Sub";
return @"";
}
@end