
#import "b4i_httpserver.h"
#import "b4i_main.h"
#import "b4i_servletrequest.h"
#import "b4i_servletresponse.h"
#import "b4i_queryelement.h"

@interface ResumableSub_httpserver_Start :B4IResumableSub 
- (instancetype) init: (b4i_httpserver*) parent1 :(b4i_httpserver*) __ref1 :(int) _port1;
@end
@implementation ResumableSub_httpserver_Start {
b4i_httpserver* parent;
b4i_httpserver* __ref;
int _port;
BOOL _successful;
B4ISocketWrapper* _newsocket;
b4i_servletrequest* _sr;
}
- (instancetype) init: (b4i_httpserver*) parent1 :(b4i_httpserver*) __ref1 :(int) _port1 {
self->__ref = __ref1;
self->_port = _port1;
self->parent = parent1;
self->__ref = parent;
return self;
}
b4i_httpserver* __ref;
- (void) resume:(B4I*)bi1 :(NSArray*)result {
self.bi = bi1;
B4IRDebugUtils.currentModule=@"httpserver";

    while (true) {
        switch (self->_state) {
            case -1:
return;

case 0:
//C
self->_state = 1;
B4IRDebugUtils.currentLine=720897;
 //BA.debugLineNum = 720897;BA.debugLine="Serv.Initialize(Port,\"Serv\")";
[self->__ref->__serv /*B4IServerSocketWrapper**/  Initialize:self.bi :self->_port :@"Serv"];
B4IRDebugUtils.currentLine=720899;
 //BA.debugLineNum = 720899;BA.debugLine="Work=True";
self->__ref->__work /*BOOL*/  = true;
B4IRDebugUtils.currentLine=720900;
 //BA.debugLineNum = 720900;BA.debugLine="Do While Work";
if (true) break;

case 1:
//do while
self->_state = 14;
while (self->__ref->__work /*BOOL*/ ) {
self->_state = 3;
if (true) break;
}
if (true) break;

case 3:
//C
self->_state = 4;
B4IRDebugUtils.currentLine=720901;
 //BA.debugLineNum = 720901;BA.debugLine="Serv.Listen";
[self->__ref->__serv /*B4IServerSocketWrapper**/  Listen];
B4IRDebugUtils.currentLine=720902;
 //BA.debugLineNum = 720902;BA.debugLine="Wait For Serv_NewConnection (Successful As Boole";
[parent->___c WaitFor:@"serv_newconnection::" :self.bi :[[B4IDelegatableResumableSub alloc]init:self :@"httpserver" :@"start"] :nil];
self->_state = 15;
return;
case 15:
//C
self->_state = 4;
self->_successful = ((NSNumber*) result[1]).boolValue;
self->_newsocket = ((B4ISocketWrapper*) result[2]);
;
B4IRDebugUtils.currentLine=720904;
 //BA.debugLineNum = 720904;BA.debugLine="If Successful Then";
if (true) break;

case 4:
//if
self->_state = 13;
if (self->_successful) { 
self->_state = 6;
}if (true) break;

case 6:
//C
self->_state = 7;
B4IRDebugUtils.currentLine=720905;
 //BA.debugLineNum = 720905;BA.debugLine="Dim SR As ServletRequest";
self->_sr = [b4i_servletrequest new];
B4IRDebugUtils.currentLine=720906;
 //BA.debugLineNum = 720906;BA.debugLine="SR.Timeout=Timeout";
self->_sr->__timeout /*long long*/  = (long long) (self->__ref->__timeout /*int*/ );
B4IRDebugUtils.currentLine=720907;
 //BA.debugLineNum = 720907;BA.debugLine="SR.Initialize(Me,\"Data\",NewSocket)";
[self->_sr _initialize /*NSString**/ :nil :self.bi :parent :@"Data" :self->_newsocket];
B4IRDebugUtils.currentLine=720909;
 //BA.debugLineNum = 720909;BA.debugLine="If SubExists(mCallBack,mEventName & \"_NewConect";
if (true) break;

case 7:
//if
self->_state = 12;
if ([parent->___c SubExists:self->__ref->__mcallback /*NSObject**/  :[@[self->__ref->__meventname /*NSString**/ ,@"_NewConection"] componentsJoinedByString:@""] :(int) (1)]) { 
self->_state = 9;
;}if (true) break;

case 9:
//C
self->_state = 12;
[parent->___c CallSub2:self.bi :self->__ref->__mcallback /*NSObject**/  :[@[self->__ref->__meventname /*NSString**/ ,@"_NewConection"] componentsJoinedByString:@""] :(NSObject*)(self->_sr)];
if (true) break;

case 12:
//C
self->_state = 13;
;
 if (true) break;

case 13:
//C
self->_state = 1;
;
 if (true) break;

case 14:
//C
self->_state = -1;
;
B4IRDebugUtils.currentLine=720915;
 //BA.debugLineNum = 720915;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
@end

@implementation b4i_httpserver 


+ (B4I*)createBI {
    return [B4IShellBI alloc];
}

- (void)dealloc {
    if (self.bi != nil)
        NSLog(@"Class (b4i_httpserver) instance released.");
}

- (NSString*)  _initialize:(b4i_httpserver*) __ref :(B4I*) _ba :(NSObject*) _callback :(NSString*) _eventname{
__ref = self;
[self initializeClassModule];
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"initialize"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"initialize:::" :@[[B4I nilToNSNull:_ba],[B4I nilToNSNull:_callback],[B4I nilToNSNull:_eventname]]]);}
B4IRDebugUtils.currentLine=655360;
 //BA.debugLineNum = 655360;BA.debugLine="Public Sub Initialize(CallBack As Object, EventNam";
B4IRDebugUtils.currentLine=655361;
 //BA.debugLineNum = 655361;BA.debugLine="Users.Initialize";
[__ref->__users /*B4IMap**/  Initialize];
B4IRDebugUtils.currentLine=655362;
 //BA.debugLineNum = 655362;BA.debugLine="mCallBack=CallBack";
__ref->__mcallback /*NSObject**/  = _callback;
B4IRDebugUtils.currentLine=655363;
 //BA.debugLineNum = 655363;BA.debugLine="mEventName=EventName";
__ref->__meventname /*NSString**/  = _eventname;
B4IRDebugUtils.currentLine=655365;
 //BA.debugLineNum = 655365;BA.debugLine="htdigest.Initialize";
[__ref->__htdigest /*B4IList**/  Initialize];
B4IRDebugUtils.currentLine=655366;
 //BA.debugLineNum = 655366;BA.debugLine="End Sub";
return @"";
}
- (void)  _start:(b4i_httpserver*) __ref :(int) _port{
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"start"])
	 {[B4IDebug delegate:self.bi :@"start:" :@[@(_port)]]; return;}
ResumableSub_httpserver_Start* rsub = [[ResumableSub_httpserver_Start alloc] init:self : __ref: _port];
[rsub resume:self.bi :nil];
}
//-1887435628
- (NSString*)  _getmywifiip:(b4i_httpserver*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"getmywifiip"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getmywifiip" :nil]);}
B4IRDebugUtils.currentLine=917504;
 //BA.debugLineNum = 917504;BA.debugLine="Public Sub GetMyWifiIp As String";
B4IRDebugUtils.currentLine=917508;
 //BA.debugLineNum = 917508;BA.debugLine="Return Serv.GetMyWifiIp";
if (true) return [__ref->__serv /*B4IServerSocketWrapper**/  GetMyWifiIp];
B4IRDebugUtils.currentLine=917510;
 //BA.debugLineNum = 917510;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _class_globals:(b4i_httpserver*) __ref{
__ref = self;
self->__main=[b4i_main new];
B4IRDebugUtils.currentModule=@"httpserver";
B4IRDebugUtils.currentLine=589824;
 //BA.debugLineNum = 589824;BA.debugLine="Sub Class_Globals";
B4IRDebugUtils.currentLine=589825;
 //BA.debugLineNum = 589825;BA.debugLine="Private Users As Map";
self->__users = [B4IMap new];
B4IRDebugUtils.currentLine=589826;
 //BA.debugLineNum = 589826;BA.debugLine="Private Serv As ServerSocket";
self->__serv = [B4IServerSocketWrapper new];
B4IRDebugUtils.currentLine=589828;
 //BA.debugLineNum = 589828;BA.debugLine="Private Work As Boolean = False";
self->__work = false;
B4IRDebugUtils.currentLine=589829;
 //BA.debugLineNum = 589829;BA.debugLine="Private mCallBack As Object";
self->__mcallback = [NSObject new];
B4IRDebugUtils.currentLine=589830;
 //BA.debugLineNum = 589830;BA.debugLine="Private mEventName As String";
self->__meventname = @"";
B4IRDebugUtils.currentLine=589833;
 //BA.debugLineNum = 589833;BA.debugLine="Public Const DigestAuthentication As Boolean = Fa";
self->__digestauthentication = false;
B4IRDebugUtils.currentLine=589834;
 //BA.debugLineNum = 589834;BA.debugLine="Public DigestPath As String = \"/\"";
self->__digestpath = @"/";
B4IRDebugUtils.currentLine=589835;
 //BA.debugLineNum = 589835;BA.debugLine="Public realm As String = \"\"";
self->__realm = @"";
B4IRDebugUtils.currentLine=589836;
 //BA.debugLineNum = 589836;BA.debugLine="Public htdigest As List";
self->__htdigest = [B4IList new];
B4IRDebugUtils.currentLine=589837;
 //BA.debugLineNum = 589837;BA.debugLine="Public IgnoreNC As Boolean = False";
self->__ignorenc = false;
B4IRDebugUtils.currentLine=589838;
 //BA.debugLineNum = 589838;BA.debugLine="Public Timeout As Int = 5000";
self->__timeout = (int) (5000);
B4IRDebugUtils.currentLine=589839;
 //BA.debugLineNum = 589839;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _data_handle:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _res{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"data_handle"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"data_handle::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_res]]]);}
B4IRDebugUtils.currentLine=1048576;
 //BA.debugLineNum = 1048576;BA.debugLine="Private Sub Data_Handle(Req As ServletRequest, Res";
B4IRDebugUtils.currentLine=1048577;
 //BA.debugLineNum = 1048577;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_Handle\",2)";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_Handle"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_Handle"] componentsJoinedByString:@""] :(NSObject*)(_req) :(NSObject*)(_res)];};
B4IRDebugUtils.currentLine=1048578;
 //BA.debugLineNum = 1048578;BA.debugLine="End Sub";
return @"";
}
- (BOOL)  _subexists2:(b4i_httpserver*) __ref :(NSObject*) _callobject :(NSString*) _eventname :(int) _param{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"subexists2"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"subexists2:::" :@[[B4I nilToNSNull:_callobject],[B4I nilToNSNull:_eventname],@(_param)]]).boolValue;}
B4IRDebugUtils.currentLine=1376256;
 //BA.debugLineNum = 1376256;BA.debugLine="Private Sub SubExists2(CallObject As Object, Event";
B4IRDebugUtils.currentLine=1376258;
 //BA.debugLineNum = 1376258;BA.debugLine="Return SubExists(CallObject,EventName, param)";
if (true) return [self->___c SubExists:_callobject :_eventname :_param];
B4IRDebugUtils.currentLine=1376262;
 //BA.debugLineNum = 1376262;BA.debugLine="End Sub";
return false;
}
- (NSString*)  _data_handlewebsocket:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"data_handlewebsocket"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"data_handlewebsocket::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_resp]]]);}
B4IRDebugUtils.currentLine=1245184;
 //BA.debugLineNum = 1245184;BA.debugLine="Private Sub Data_HandleWebSocket (req As ServletRe";
B4IRDebugUtils.currentLine=1245185;
 //BA.debugLineNum = 1245185;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_HandleWebS";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_HandleWebSocket"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_HandleWebSocket"] componentsJoinedByString:@""] :(NSObject*)(_req) :(NSObject*)(_resp)];};
B4IRDebugUtils.currentLine=1245186;
 //BA.debugLineNum = 1245186;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _data_switchtowebsocket:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"data_switchtowebsocket"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"data_switchtowebsocket::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_resp]]]);}
B4IRDebugUtils.currentLine=1179648;
 //BA.debugLineNum = 1179648;BA.debugLine="Private Sub Data_SwitchToWebSocket (req As Servlet";
B4IRDebugUtils.currentLine=1179649;
 //BA.debugLineNum = 1179649;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_SwitchToWe";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_SwitchToWebSocket"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_SwitchToWebSocket"] componentsJoinedByString:@""] :(NSObject*)(_req) :(NSObject*)(_resp)];};
B4IRDebugUtils.currentLine=1179650;
 //BA.debugLineNum = 1179650;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _data_uploadedfile:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"data_uploadedfile"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"data_uploadedfile::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_resp]]]);}
B4IRDebugUtils.currentLine=1114112;
 //BA.debugLineNum = 1114112;BA.debugLine="Private Sub Data_UploadedFile(req As ServletReques";
B4IRDebugUtils.currentLine=1114113;
 //BA.debugLineNum = 1114113;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_UploadedFi";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_UploadedFile"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_UploadedFile"] componentsJoinedByString:@""] :(NSObject*)(_req) :(NSObject*)(_resp)];};
B4IRDebugUtils.currentLine=1114114;
 //BA.debugLineNum = 1114114;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _data_websocketclose:(b4i_httpserver*) __ref :(int) _closecode :(NSString*) _closemessage{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"data_websocketclose"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"data_websocketclose::" :@[@(_closecode),[B4I nilToNSNull:_closemessage]]]);}
B4IRDebugUtils.currentLine=1310720;
 //BA.debugLineNum = 1310720;BA.debugLine="Private Sub Data_WebSocketClose(CloseCode As Int,";
B4IRDebugUtils.currentLine=1310721;
 //BA.debugLineNum = 1310721;BA.debugLine="If SubExists2(mCallBack,mEventName & \"_WebSocketC";
if ([__ref _subexists2 /*BOOL*/ :nil :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_WebSocketClose"] componentsJoinedByString:@""] :(int) (2)]) { 
[self->___c CallSub3:self.bi :__ref->__mcallback /*NSObject**/  :[@[__ref->__meventname /*NSString**/ ,@"_WebSocketClose"] componentsJoinedByString:@""] :(NSObject*)(@(_closecode)) :(NSObject*)(_closemessage)];};
B4IRDebugUtils.currentLine=1310722;
 //BA.debugLineNum = 1310722;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _getmyip:(b4i_httpserver*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"getmyip"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getmyip" :nil]);}
B4IRDebugUtils.currentLine=851968;
 //BA.debugLineNum = 851968;BA.debugLine="Public Sub GetMyIP As String";
B4IRDebugUtils.currentLine=851969;
 //BA.debugLineNum = 851969;BA.debugLine="Return Serv.GetMyIP";
if (true) return [__ref->__serv /*B4IServerSocketWrapper**/  GetMyIP];
B4IRDebugUtils.currentLine=851970;
 //BA.debugLineNum = 851970;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _gettemppath:(b4i_httpserver*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"gettemppath"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"gettemppath" :nil]);}
B4IRDebugUtils.currentLine=983040;
 //BA.debugLineNum = 983040;BA.debugLine="Public Sub getTempPath As String";
B4IRDebugUtils.currentLine=983044;
 //BA.debugLineNum = 983044;BA.debugLine="Return File.DirTemp";
if (true) return [[self->___c File] DirTemp];
B4IRDebugUtils.currentLine=983046;
 //BA.debugLineNum = 983046;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _stop:(b4i_httpserver*) __ref{
__ref = self;
B4IRDebugUtils.currentModule=@"httpserver";
if ([B4IDebug shouldDelegate: @"stop"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"stop" :nil]);}
B4IRDebugUtils.currentLine=786432;
 //BA.debugLineNum = 786432;BA.debugLine="Public Sub Stop";
B4IRDebugUtils.currentLine=786433;
 //BA.debugLineNum = 786433;BA.debugLine="Work=False";
__ref->__work /*BOOL*/  = false;
B4IRDebugUtils.currentLine=786434;
 //BA.debugLineNum = 786434;BA.debugLine="End Sub";
return @"";
}
@end