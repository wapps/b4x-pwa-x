#import "iArchiver.h"
#import "iCore.h"
#import "iEncryption.h"
#import "iJSON.h"
#import "iNetwork.h"
#import "iRandomAccessFile.h"
#import "iReleaseLogger.h"
#import "iStringUtils.h"
#import "iXUI.h"
#import "iDebug2.h"
@class b4i_main;
@class b4i_httpserver;
@class _tuser;
@class b4i_servletrequest;
@class b4i_servletresponse;
@interface b4i_queryelement : B4IClass
{
@public NSString* __event_change;
@public NSString* __event_click;
@public NSString* __event_dblclick;
@public NSString* __event_focus;
@public NSString* __event_focusin;
@public NSString* __event_focusout;
@public NSString* __event_keyup;
@public NSString* __event_mousedown;
@public NSString* __event_mouseenter;
@public NSString* __event_mouseleave;
@public NSString* __event_mousemove;
@public NSString* __event_mouseup;
@public B4IArray* __noevent;
@public b4i_servletresponse* __resp;
@public b4i_main* __main;

}- (NSString*)  _class_globals:(b4i_queryelement*) __ref;
@property (nonatomic)NSString* _event_change;
@property (nonatomic)NSString* _event_click;
@property (nonatomic)NSString* _event_dblclick;
@property (nonatomic)NSString* _event_focus;
@property (nonatomic)NSString* _event_focusin;
@property (nonatomic)NSString* _event_focusout;
@property (nonatomic)NSString* _event_keyup;
@property (nonatomic)NSString* _event_mousedown;
@property (nonatomic)NSString* _event_mouseenter;
@property (nonatomic)NSString* _event_mouseleave;
@property (nonatomic)NSString* _event_mousemove;
@property (nonatomic)NSString* _event_mouseup;
@property (nonatomic)B4IArray* _noevent;
@property (nonatomic)b4i_servletresponse* _resp;
@property (nonatomic)b4i_main* _main;
- (B4IArray*)  _createevent:(b4i_queryelement*) __ref :(NSString*) _objectname :(NSString*) _event :(B4IArray*) _otherevent;
- (NSString*)  _escapehtml:(b4i_queryelement*) __ref :(NSString*) _raw;
- (NSString*)  _eval:(b4i_queryelement*) __ref :(NSString*) _script :(B4IList*) _params;
- (NSString*)  _evalwithresult:(b4i_queryelement*) __ref :(NSString*) _script :(B4IList*) _params;
- (NSString*)  _getpropriety:(b4i_queryelement*) __ref :(NSString*) _property :(B4IList*) _value;
- (NSString*)  _getval:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _valuelist;
- (NSString*)  _initialize:(b4i_queryelement*) __ref :(B4I*) _ba :(b4i_servletresponse*) _response;
- (BOOL)  _isnull:(b4i_queryelement*) __ref :(NSObject*) _o;
- (NSString*)  _runfunction:(b4i_queryelement*) __ref :(NSString*) _function :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _runfunctionwithresult:(b4i_queryelement*) __ref :(NSString*) _function :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _runmethod:(b4i_queryelement*) __ref :(NSString*) _method :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _runmethodwithresult:(b4i_queryelement*) __ref :(NSString*) _method :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _selectelement:(b4i_queryelement*) __ref :(NSString*) _id;
- (NSString*)  _setautomaticevents:(b4i_queryelement*) __ref :(B4IArray*) _arg;
- (NSString*)  _setautomaticeventsfrompage:(b4i_queryelement*) __ref :(NSString*) _html;
- (NSString*)  _setcommand:(b4i_queryelement*) __ref :(NSString*) _etype :(NSString*) _method :(NSString*) _property :(NSString*) _id :(B4IList*) _params :(B4IList*) _arg;
- (NSString*)  _setcss:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _setdialog:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _sethtml:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _params;
- (NSString*)  _setpropriety:(b4i_queryelement*) __ref :(NSString*) _property :(B4IList*) _value;
- (NSString*)  _settext:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _textlist;
- (NSString*)  _setval:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _valuelist;
@end
