#import "iArchiver.h"
#import "iCore.h"
#import "iEncryption.h"
#import "iJSON.h"
#import "iNetwork.h"
#import "iRandomAccessFile.h"
#import "iReleaseLogger.h"
#import "iStringUtils.h"
#import "iXUI.h"
#import "iDebug2.h"
@class b4i_main;
@class b4i_httpserver;
@class _tuser;
@class b4i_servletresponse;
@class b4i_queryelement;
@interface b4i_servletrequest : B4IClass
{
@public B4IArray* __dday;
@public B4IArray* __mmmonth;
@public B4ISocketWrapper* __client;
@public B4IAsyncStreams* __astream;
@public NSObject* __mcallback;
@public NSString* __meventname;
@public B4IByteConverter* __bc;
@public NSString* __method;
@public NSString* __requesturi;
@public NSString* __requesthost;
@public NSString* __address;
@public NSString* __connport;
@public NSString* __id;
@public B4IMap* __requestheader;
@public B4IMap* __requestparameter;
@public B4IMap* __requestcookies;
@public B4IList* __requestpostdatarow;
@public BOOL __connectionalive;
@public NSString* __characterencoding;
@public b4i_httpserver* __callserver;
@public b4i_servletresponse* __response;
@public B4IMap* __users;
@public NSString* __username;
@public NSString* __password;
@public BOOL __otherdata;
@public B4IArray* __cache;
@public B4IArray* __bbb;
@public iStringUtils* __su;
@public BOOL __logprivate;
@public BOOL __logactive;
@public NSString* __contenttype;
@public long long __contentlength;
@public B4IMap* __multipartfilename;
@public BOOL __logfirstrefuse;
@public long long __timeout;
@public long long __lastbrowsercomunicate;
@public BOOL __gzip;
@public BOOL __deflate;
@public BOOL __websocket;
@public NSString* __keywebsocket;
@public NSString* __websocketstring;
@public unsigned char __lastopcode;
@public b4i_main* __main;

}- (B4IArray*)  _arrayappend:(b4i_servletrequest*) __ref :(B4IArray*) _data1 :(B4IArray*) _data2;
- (B4IArray*)  _arraycopy:(b4i_servletrequest*) __ref :(B4IArray*) _data;
- (int)  _arrayindexof:(b4i_servletrequest*) __ref :(B4IArray*) _data :(B4IArray*) _searchdata;
- (B4IArray*)  _arrayinitialize:(b4i_servletrequest*) __ref;
- (B4IArray*)  _arrayinsert:(b4i_servletrequest*) __ref :(B4IArray*) _datasource :(int) _index :(B4IArray*) _datainsert;
- (B4IArray*)  _arrayremove:(b4i_servletrequest*) __ref :(B4IArray*) _data :(int) _start :(int) _last;
- (NSString*)  _astream_error:(b4i_servletrequest*) __ref;
- (NSString*)  _astream_newdata:(b4i_servletrequest*) __ref :(B4IArray*) _buffer;
- (NSString*)  _astream_terminated:(b4i_servletrequest*) __ref;
- (void)  _browsertimeout:(b4i_servletrequest*) __ref;
- (NSString*)  _callevent:(b4i_servletrequest*) __ref;
- (NSString*)  _class_globals:(b4i_servletrequest*) __ref;
@property (nonatomic)B4IArray* _dday;
@property (nonatomic)B4IArray* _mmmonth;
@property (nonatomic)B4ISocketWrapper* _client;
@property (nonatomic)B4IAsyncStreams* _astream;
@property (nonatomic)NSObject* _mcallback;
@property (nonatomic)NSString* _meventname;
@property (nonatomic)B4IByteConverter* _bc;
@property (nonatomic)NSString* _method;
@property (nonatomic)NSString* _requesturi;
@property (nonatomic)NSString* _requesthost;
@property (nonatomic)NSString* _address;
@property (nonatomic)NSString* _connport;
@property (nonatomic)NSString* _id;
@property (nonatomic)B4IMap* _requestheader;
@property (nonatomic)B4IMap* _requestparameter;
@property (nonatomic)B4IMap* _requestcookies;
@property (nonatomic)B4IList* _requestpostdatarow;
@property (nonatomic)BOOL _connectionalive;
@property (nonatomic)NSString* _characterencoding;
@property (nonatomic)b4i_httpserver* _callserver;
@property (nonatomic)b4i_servletresponse* _response;
@property (nonatomic)B4IMap* _users;
@property (nonatomic)NSString* _username;
@property (nonatomic)NSString* _password;
@property (nonatomic)BOOL _otherdata;
@property (nonatomic)B4IArray* _cache;
@property (nonatomic)B4IArray* _bbb;
@property (nonatomic)iStringUtils* _su;
@property (nonatomic)BOOL _logprivate;
@property (nonatomic)BOOL _logactive;
@property (nonatomic)NSString* _contenttype;
@property (nonatomic)long long _contentlength;
@property (nonatomic)B4IMap* _multipartfilename;
@property (nonatomic)BOOL _logfirstrefuse;
@property (nonatomic)long long _timeout;
@property (nonatomic)long long _lastbrowsercomunicate;
@property (nonatomic)BOOL _gzip;
@property (nonatomic)BOOL _deflate;
@property (nonatomic)BOOL _websocket;
@property (nonatomic)NSString* _keywebsocket;
@property (nonatomic)NSString* _websocketstring;
@property (nonatomic)unsigned char _lastopcode;
@property (nonatomic)b4i_main* _main;
- (NSString*)  _close:(b4i_servletrequest*) __ref;
- (NSString*)  _completedate:(b4i_servletrequest*) __ref :(long long) _dt;
- (BOOL)  _connected:(b4i_servletrequest*) __ref;
- (NSString*)  _decode:(b4i_servletrequest*) __ref :(NSString*) _s;
- (NSString*)  _dectohex:(b4i_servletrequest*) __ref :(int) _dec;
- (B4IArray*)  _demasked:(b4i_servletrequest*) __ref :(B4IArray*) _maskdata :(B4IArray*) _maskingkey :(long long) _length :(long long) _spacement :(BOOL) _masked;
- (NSString*)  _elaboratehandshake:(b4i_servletrequest*) __ref :(NSString*) _handshake :(B4IArray*) _data;
- (NSString*)  _elaboratehandskakedigestmessage:(b4i_servletrequest*) __ref :(b4i_servletrequest*) _request :(b4i_servletresponse*) _sresponse;
- (NSString*)  _elaboratewebsocket:(b4i_servletrequest*) __ref :(B4IArray*) _data;
- (NSString*)  _encode:(b4i_servletrequest*) __ref :(NSString*) _s;
- (NSString*)  _extracthandshake:(b4i_servletrequest*) __ref;
- (NSString*)  _extractparameterfromdata:(b4i_servletrequest*) __ref;
- (NSString*)  _findcredential:(b4i_servletrequest*) __ref :(NSString*) _un;
- (_tuser*)  _finduser:(b4i_servletrequest*) __ref :(NSString*) _naddress :(NSString*) _opaque;
- (NSString*)  _getheader:(b4i_servletrequest*) __ref :(NSString*) _name;
- (B4IList*)  _getheadersname:(b4i_servletrequest*) __ref;
- (B4IInputStream*)  _getinputstream:(b4i_servletrequest*) __ref;
- (NSString*)  _getmethod:(b4i_servletrequest*) __ref;
- (NSString*)  _getrequesthost:(b4i_servletrequest*) __ref;
- (NSString*)  _getrequesturi:(b4i_servletrequest*) __ref;
- (BOOL)  _getwebsocketcompressdeflateaccept:(b4i_servletrequest*) __ref;
- (BOOL)  _getwebsocketcompressgzipaccept:(b4i_servletrequest*) __ref;
- (B4IMap*)  _getwebsocketmapdata:(b4i_servletrequest*) __ref;
- (NSString*)  _getwebsocketstringdata:(b4i_servletrequest*) __ref;
- (int)  _hextodec:(b4i_servletrequest*) __ref :(NSString*) _hex;
- (B4IArray*)  _inflatedate:(b4i_servletrequest*) __ref :(B4IArray*) _data;
- (NSString*)  _initialize:(b4i_servletrequest*) __ref :(B4I*) _ba :(NSObject*) _callback :(NSString*) _eventname :(B4ISocketWrapper*) _sck;
- (NSString*)  _lgc:(b4i_servletrequest*) __ref :(NSString*) _message :(int) _color;
- (NSString*)  _lgp:(b4i_servletrequest*) __ref :(NSString*) _message;
- (NSString*)  _md5:(b4i_servletrequest*) __ref :(NSString*) _stringtoconvert;
- (_tuser*)  _newuser:(b4i_servletrequest*) __ref :(NSString*) _naddress;
- (B4IMap*)  _parametermap:(b4i_servletrequest*) __ref;
- (NSString*)  _remoteaddress:(b4i_servletrequest*) __ref;
- (int)  _remoteport:(b4i_servletrequest*) __ref;
- (NSString*)  _sendacceptkeyws:(b4i_servletrequest*) __ref;
- (NSString*)  _sendrefuse:(b4i_servletrequest*) __ref :(b4i_servletresponse*) _sresponse :(_tuser*) _user;
- (B4IArray*)  _subarray:(b4i_servletrequest*) __ref :(B4IArray*) _data :(int) _pos;
- (B4IArray*)  _subarray2:(b4i_servletrequest*) __ref :(B4IArray*) _data :(int) _start :(int) _last;
- (BOOL)  _subexists2:(b4i_servletrequest*) __ref :(NSObject*) _callobject :(NSString*) _eventname :(int) _param;
- (NSString*)  _websocketkey:(b4i_servletrequest*) __ref :(NSString*) _key;
@end
@interface _tuser:NSObject
{
@public BOOL _IsInitialized;
@public NSString* _Address;
@public int _nc;
@public NSString* _nonce;
@public NSString* _opaque;
@public long long _LastRequest;
@public NSString* _realm;

}
@property (nonatomic)BOOL IsInitialized;
@property (nonatomic)NSString* Address;
@property (nonatomic)int nc;
@property (nonatomic)NSString* nonce;
@property (nonatomic)NSString* opaque;
@property (nonatomic)long long LastRequest;
@property (nonatomic)NSString* realm;
-(void)Initialize;
@end
