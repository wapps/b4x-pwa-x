#import "iArchiver.h"
#import "iCore.h"
#import "iEncryption.h"
#import "iJSON.h"
#import "iNetwork.h"
#import "iRandomAccessFile.h"
#import "iReleaseLogger.h"
#import "iStringUtils.h"
#import "iXUI.h"
#import "iDebug2.h"
@class b4i_httpserver;
@class _tuser;
@class b4i_servletrequest;
@class b4i_servletresponse;
@class b4i_queryelement;
@interface b4i_main : B4IStaticModule
{
@public B4IApplicationWrapper* __app;
@public B4INavigationControllerWrapper* __navcontrol;
@public B4IPage* __page1;
@public B4IXUI* __xui;
@public B4ILabelWrapper* __label1;
@public B4ILabelWrapper* __labelupload;
@public B4IImageViewWrapper* __imageview1;
@public b4i_httpserver* __svr;
@public int __counterconenction;

}- (NSString*)  _application_start:(B4INavigationControllerWrapper*) _nav;
- (NSString*)  _button1_click;
- (NSString*)  _dinamicpage:(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp;
- (NSString*)  _page1_resize:(int) _width :(int) _height;
- (NSString*)  _process_globals;
@property (nonatomic)B4IApplicationWrapper* _app;
@property (nonatomic)B4INavigationControllerWrapper* _navcontrol;
@property (nonatomic)B4IPage* _page1;
@property (nonatomic)B4IXUI* _xui;
@property (nonatomic)B4ILabelWrapper* _label1;
@property (nonatomic)B4ILabelWrapper* _labelupload;
@property (nonatomic)B4IImageViewWrapper* _imageview1;
@property (nonatomic)b4i_httpserver* _svr;
@property (nonatomic)int _counterconenction;
- (NSString*)  _svr_handle:(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp;
- (NSString*)  _svr_newconection:(b4i_servletrequest*) _req;
- (NSString*)  _svr_uploadedfile:(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp;
- (NSString*)  _svr_uploadprogress:(b4i_servletresponse*) _resp :(float) _progress;
@end
