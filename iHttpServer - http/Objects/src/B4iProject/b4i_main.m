
#import "b4i_main.h"
#import "b4i_httpserver.h"
#import "b4i_servletrequest.h"
#import "b4i_servletresponse.h"
#import "b4i_queryelement.h"


@implementation b4i_main 


+ (instancetype)new {
    static b4i_main* shared = nil;
    if (shared == nil) {
        shared = [self alloc];
        shared.bi = [[B4IShellBI alloc] init:shared];
        shared.__c = [B4ICommon new];
    }
    return shared;
}
- (int)debugAppId {
    return 42;
}


- (NSString*)  _application_start:(B4INavigationControllerWrapper*) _nav{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"application_start"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"application_start:" :@[[B4I nilToNSNull:_nav]]]);}
B4IReleaseLogger* _rl = nil;
B4IArchiver* _unzip = nil;
B4IRDebugUtils.currentLine=65536;
 //BA.debugLineNum = 65536;BA.debugLine="Private Sub Application_Start (Nav As NavigationCo";
B4IRDebugUtils.currentLine=65537;
 //BA.debugLineNum = 65537;BA.debugLine="NavControl = Nav";
self->__navcontrol = _nav;
B4IRDebugUtils.currentLine=65538;
 //BA.debugLineNum = 65538;BA.debugLine="Page1.Initialize(\"Page1\")";
[self->__page1 Initialize:self.bi :@"Page1"];
B4IRDebugUtils.currentLine=65539;
 //BA.debugLineNum = 65539;BA.debugLine="Page1.RootPanel.LoadLayout(\"Page1\")";
[[self->__page1 RootPanel] LoadLayout:@"Page1" :self.bi];
B4IRDebugUtils.currentLine=65540;
 //BA.debugLineNum = 65540;BA.debugLine="NavControl.ShowPage(Page1)";
[self->__navcontrol ShowPage:(UIViewController*)((self->__page1).object)];
B4IRDebugUtils.currentLine=65542;
 //BA.debugLineNum = 65542;BA.debugLine="Dim rl As ReleaseLogger";
_rl = [B4IReleaseLogger new];
B4IRDebugUtils.currentLine=65543;
 //BA.debugLineNum = 65543;BA.debugLine="rl.Initialize(\"192.168.1.105\", 54323)";
[_rl Initialize:self.bi :@"192.168.1.105" :(int) (54323)];
B4IRDebugUtils.currentLine=65545;
 //BA.debugLineNum = 65545;BA.debugLine="Svr.Initialize(Me,\"Svr\")";
[self->__svr _initialize /*NSString**/ :nil :self.bi :self :@"Svr"];
B4IRDebugUtils.currentLine=65546;
 //BA.debugLineNum = 65546;BA.debugLine="File.Copy(File.DirAssets,\"file.htdigest\",File.Dir";
[[self->___c File] Copy:[[self->___c File] DirAssets] :@"file.htdigest" :[[self->___c File] DirTemp] :@"file.htdigest"];
B4IRDebugUtils.currentLine=65547;
 //BA.debugLineNum = 65547;BA.debugLine="Svr.htdigest=File.ReadList(File.DirTemp,\"file.htd";
self->__svr->__htdigest /*B4IList**/  = [[self->___c File] ReadList:[[self->___c File] DirTemp] :@"file.htdigest"];
B4IRDebugUtils.currentLine=65548;
 //BA.debugLineNum = 65548;BA.debugLine="Svr.Start(51051)";
[self->__svr _start /*void*/ :nil :(int) (51051)];
B4IRDebugUtils.currentLine=65549;
 //BA.debugLineNum = 65549;BA.debugLine="Svr.DigestPath=\"/dir/\"";
self->__svr->__digestpath /*NSString**/  = @"/dir/";
B4IRDebugUtils.currentLine=65550;
 //BA.debugLineNum = 65550;BA.debugLine="Svr.realm=\"testrealm@host.com\"";
self->__svr->__realm /*NSString**/  = @"testrealm@host.com";
B4IRDebugUtils.currentLine=65551;
 //BA.debugLineNum = 65551;BA.debugLine="Label1.Text=$\"Server ip: ${Svr.GetMyWifiIp} ${510";
[self->__label1 setText:([@[@"Server ip: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([self->__svr _getmywifiip /*NSString**/ :nil])],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)(@(51051))],@""] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=65553;
 //BA.debugLineNum = 65553;BA.debugLine="Dim unzip As Archiver";
_unzip = [B4IArchiver new];
B4IRDebugUtils.currentLine=65554;
 //BA.debugLineNum = 65554;BA.debugLine="unzip.Unzip(File.DirAssets,\"www.zip\",File.DirTemp";
[_unzip Unzip:[[self->___c File] DirAssets] :@"www.zip" :[[self->___c File] DirTemp] :@""];
B4IRDebugUtils.currentLine=65555;
 //BA.debugLineNum = 65555;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _button1_click{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"button1_click"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"button1_click" :nil]);}
B4IRDebugUtils.currentLine=131072;
 //BA.debugLineNum = 131072;BA.debugLine="Sub Button1_Click";
B4IRDebugUtils.currentLine=131073;
 //BA.debugLineNum = 131073;BA.debugLine="xui.MsgboxAsync(\"Hello world!\", \"B4X\")";
[self->__xui MsgboxAsync:self.bi :@"Hello world!" :@"B4X"];
B4IRDebugUtils.currentLine=131074;
 //BA.debugLineNum = 131074;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _dinamicpage:(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"dinamicpage"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"dinamicpage::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_resp]]]);}
B4IRDebugUtils.currentLine=393216;
 //BA.debugLineNum = 393216;BA.debugLine="Private Sub DinamicPage(req As ServletRequest,resp";
B4IRDebugUtils.currentLine=393217;
 //BA.debugLineNum = 393217;BA.debugLine="resp.ContentType = \"text/html\"";
_resp->__contenttype /*NSString**/  = @"text/html";
B4IRDebugUtils.currentLine=393218;
 //BA.debugLineNum = 393218;BA.debugLine="resp.SendString($\"<img src='logo.png'/ width=100";
[_resp _sendstring /*NSString**/ :nil :([@[@"<img src='logo.png'/ width=100 height=100><br/>\n",@"<b>Hello world!!!</b><br/>\n",@"Your ip address is: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([_req _remoteaddress /*NSString**/ :nil])],@"<br/>\n",@"The time here is: ",[self->___c SmartStringFormatter:@"" :(NSObject*)([[self->___c DateTime] Date:[[self->___c DateTime] Now]])],@" ",[self->___c SmartStringFormatter:@"" :(NSObject*)([[self->___c DateTime] Time:[[self->___c DateTime] Now]])],@"<br/>\n",@"<a href='/'>Back</a>"] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=393223;
 //BA.debugLineNum = 393223;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _page1_resize:(int) _width :(int) _height{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"page1_resize"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"page1_resize::" :@[@(_width),@(_height)]]);}
B4IRDebugUtils.currentLine=196608;
 //BA.debugLineNum = 196608;BA.debugLine="Private Sub Page1_Resize(Width As Int, Height As I";
B4IRDebugUtils.currentLine=196610;
 //BA.debugLineNum = 196610;BA.debugLine="End Sub";
return @"";
}

- (void)initializeStaticModules {
    [[b4i_main new]initializeModule];

}
- (NSString*)  _process_globals{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"process_globals"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"process_globals" :nil]);}
B4IRDebugUtils.currentLine=0;
 //BA.debugLineNum = 0;BA.debugLine="Sub Process_Globals";
B4IRDebugUtils.currentLine=3;
 //BA.debugLineNum = 3;BA.debugLine="Public App As Application";
self->__app = [B4IApplicationWrapper new];
B4IRDebugUtils.currentLine=4;
 //BA.debugLineNum = 4;BA.debugLine="Public NavControl As NavigationController";
self->__navcontrol = [B4INavigationControllerWrapper new];
B4IRDebugUtils.currentLine=5;
 //BA.debugLineNum = 5;BA.debugLine="Private Page1 As Page";
self->__page1 = [B4IPage new];
B4IRDebugUtils.currentLine=6;
 //BA.debugLineNum = 6;BA.debugLine="Private xui As XUI";
self->__xui = [B4IXUI new];
B4IRDebugUtils.currentLine=8;
 //BA.debugLineNum = 8;BA.debugLine="Private Label1 As Label";
self->__label1 = [B4ILabelWrapper new];
B4IRDebugUtils.currentLine=9;
 //BA.debugLineNum = 9;BA.debugLine="Private LabelUpload As Label";
self->__labelupload = [B4ILabelWrapper new];
B4IRDebugUtils.currentLine=10;
 //BA.debugLineNum = 10;BA.debugLine="Private ImageView1 As ImageView";
self->__imageview1 = [B4IImageViewWrapper new];
B4IRDebugUtils.currentLine=11;
 //BA.debugLineNum = 11;BA.debugLine="Private Svr As httpServer";
self->__svr = [b4i_httpserver new];
B4IRDebugUtils.currentLine=12;
 //BA.debugLineNum = 12;BA.debugLine="Private CounterConenction As Int = 0";
self->__counterconenction = (int) (0);
B4IRDebugUtils.currentLine=13;
 //BA.debugLineNum = 13;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _svr_handle:(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"svr_handle"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"svr_handle::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_resp]]]);}
NSString* _fn = @"";
NSString* _filename = @"";
NSString* _field = @"";
NSString* _param = @"";
B4IRDebugUtils.currentLine=327680;
 //BA.debugLineNum = 327680;BA.debugLine="Private Sub Svr_Handle(req As ServletRequest, resp";
B4IRDebugUtils.currentLine=327681;
 //BA.debugLineNum = 327681;BA.debugLine="Dim fn As String = req.GetRequestURI.ToLowerCase";
_fn = [[_req _getrequesturi /*NSString**/ :nil] ToLowerCase];
B4IRDebugUtils.currentLine=327682;
 //BA.debugLineNum = 327682;BA.debugLine="Log(\"Request URI: \" & fn)";
[self->___c LogImpl:@"3327682" :[@[@"Request URI: ",_fn] componentsJoinedByString:@""] :0];
B4IRDebugUtils.currentLine=327684;
 //BA.debugLineNum = 327684;BA.debugLine="Select req.GetMethod";
switch ([self.bi switchObjectToInt:[_req _getmethod /*NSString**/ :nil] :@[@"GET",@"POST"]]) {
case 0: {
B4IRDebugUtils.currentLine=327686;
 //BA.debugLineNum = 327686;BA.debugLine="If fn = \"/\" Then fn = \"/index.html\"";
if ([_fn isEqual:@"/"]) { 
_fn = @"/index.html";};
B4IRDebugUtils.currentLine=327687;
 //BA.debugLineNum = 327687;BA.debugLine="If File.Exists(File.DirTemp,fn ) Then";
if ([[self->___c File] Exists:[[self->___c File] DirTemp] :_fn]) { 
B4IRDebugUtils.currentLine=327689;
 //BA.debugLineNum = 327689;BA.debugLine="resp.SendFile(File.DirTemp,fn )";
[_resp _sendfile /*NSString**/ :nil :[[self->___c File] DirTemp] :_fn];
 }else if([[_req _getrequesturi /*NSString**/ :nil] isEqual:@"/hello"]) { 
B4IRDebugUtils.currentLine=327691;
 //BA.debugLineNum = 327691;BA.debugLine="DinamicPage(req ,resp)";
[self _dinamicpage:_req :_resp];
 }else {
B4IRDebugUtils.currentLine=327694;
 //BA.debugLineNum = 327694;BA.debugLine="resp.SendNotFound(fn)";
[_resp _sendnotfound /*NSString**/ :nil :_fn];
 };
 break; }
case 1: {
B4IRDebugUtils.currentLine=327697;
 //BA.debugLineNum = 327697;BA.debugLine="If req.ContentType=\"multipart/form-data\" Then";
if ([_req->__contenttype /*NSString**/  isEqual:@"multipart/form-data"]) { 
B4IRDebugUtils.currentLine=327699;
 //BA.debugLineNum = 327699;BA.debugLine="If req.MultipartFilename.Size>0 Then";
if ([_req->__multipartfilename /*B4IMap**/  Size]>0) { 
B4IRDebugUtils.currentLine=327700;
 //BA.debugLineNum = 327700;BA.debugLine="For Each filename As String In req.MultipartF";
{
const id<B4IIterable> group16 = [_req->__multipartfilename /*B4IMap**/  Keys];
const int groupLen16 = group16.Size
;int index16 = 0;
;
for (; index16 < groupLen16;index16++){
_filename = [self.bi ObjectToString:[group16 Get:index16]];
B4IRDebugUtils.currentLine=327701;
 //BA.debugLineNum = 327701;BA.debugLine="Log(\"Start uploading: \" & filename)";
[self->___c LogImpl:@"3327701" :[@[@"Start uploading: ",_filename] componentsJoinedByString:@""] :0];
 }
};
 };
 }else {
B4IRDebugUtils.currentLine=327706;
 //BA.debugLineNum = 327706;BA.debugLine="Dim Field As String = \"\"";
_field = @"";
B4IRDebugUtils.currentLine=327707;
 //BA.debugLineNum = 327707;BA.debugLine="For Each Param As String In req.ParameterMap.K";
{
const id<B4IIterable> group22 = [[_req _parametermap /*B4IMap**/ :nil] Keys];
const int groupLen22 = group22.Size
;int index22 = 0;
;
for (; index22 < groupLen22;index22++){
_param = [self.bi ObjectToString:[group22 Get:index22]];
B4IRDebugUtils.currentLine=327708;
 //BA.debugLineNum = 327708;BA.debugLine="Field=$\"${Field}${Param}=${req.ParameterMap.G";
_field = ([@[@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_field)],@"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_param)],@"=",[self->___c SmartStringFormatter:@"" :[[_req _parametermap /*B4IMap**/ :nil] Get:(NSObject*)(_param)]],@"<BR>"] componentsJoinedByString:@""]);
 }
};
B4IRDebugUtils.currentLine=327710;
 //BA.debugLineNum = 327710;BA.debugLine="resp.SendString(Field & $\"<B>Recorded data</B>";
[_resp _sendstring /*NSString**/ :nil :[@[_field,(@"<B>Recorded data</B><BR><a href=\"/index.html\">Back</a>")] componentsJoinedByString:@""]];
 };
 break; }
}
;
B4IRDebugUtils.currentLine=327713;
 //BA.debugLineNum = 327713;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _svr_newconection:(b4i_servletrequest*) _req{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"svr_newconection"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"svr_newconection:" :@[[B4I nilToNSNull:_req]]]);}
B4IRDebugUtils.currentLine=262144;
 //BA.debugLineNum = 262144;BA.debugLine="Private Sub Svr_NewConection(req As ServletRequest";
B4IRDebugUtils.currentLine=262145;
 //BA.debugLineNum = 262145;BA.debugLine="CounterConenction=CounterConenction+1";
self->__counterconenction = (int) (self->__counterconenction+1);
B4IRDebugUtils.currentLine=262146;
 //BA.debugLineNum = 262146;BA.debugLine="Log(\"New connection: \" & req.RemoteAddress & \" Co";
[self->___c LogImpl:@"3262146" :[@[@"New connection: ",[_req _remoteaddress /*NSString**/ :nil],@" Counter: ",[self.bi NumberToString:@(self->__counterconenction)]] componentsJoinedByString:@""] :0];
B4IRDebugUtils.currentLine=262147;
 //BA.debugLineNum = 262147;BA.debugLine="req.ID=CounterConenction";
_req->__id /*NSString**/  = [self.bi NumberToString:@(self->__counterconenction)];
B4IRDebugUtils.currentLine=262148;
 //BA.debugLineNum = 262148;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _svr_uploadedfile:(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"svr_uploadedfile"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"svr_uploadedfile::" :@[[B4I nilToNSNull:_req],[B4I nilToNSNull:_resp]]]);}
NSString* _tempfilename = @"";
NSString* _originalefilename = @"";
NSString* _filename = @"";
B4IRDebugUtils.currentLine=524288;
 //BA.debugLineNum = 524288;BA.debugLine="Private Sub Svr_UploadedFile (req As ServletReques";
B4IRDebugUtils.currentLine=524289;
 //BA.debugLineNum = 524289;BA.debugLine="Dim tempfilename As String";
_tempfilename = @"";
B4IRDebugUtils.currentLine=524290;
 //BA.debugLineNum = 524290;BA.debugLine="Dim OriginaleFileName As String";
_originalefilename = @"";
B4IRDebugUtils.currentLine=524291;
 //BA.debugLineNum = 524291;BA.debugLine="If req.MultipartFilename.Size>0 Then";
if ([_req->__multipartfilename /*B4IMap**/  Size]>0) { 
B4IRDebugUtils.currentLine=524292;
 //BA.debugLineNum = 524292;BA.debugLine="For Each Filename As String In req.MultipartFile";
{
const id<B4IIterable> group4 = [_req->__multipartfilename /*B4IMap**/  Keys];
const int groupLen4 = group4.Size
;int index4 = 0;
;
for (; index4 < groupLen4;index4++){
_filename = [self.bi ObjectToString:[group4 Get:index4]];
B4IRDebugUtils.currentLine=524293;
 //BA.debugLineNum = 524293;BA.debugLine="tempfilename=req.MultipartFilename.Get(Filename";
_tempfilename = [self.bi ObjectToString:[_req->__multipartfilename /*B4IMap**/  Get:(NSObject*)(_filename)]];
B4IRDebugUtils.currentLine=524294;
 //BA.debugLineNum = 524294;BA.debugLine="OriginaleFileName=Filename";
_originalefilename = _filename;
 }
};
 };
B4IRDebugUtils.currentLine=524299;
 //BA.debugLineNum = 524299;BA.debugLine="If OriginaleFileName.ToLowerCase.EndsWith(\"png\")";
if ([[_originalefilename ToLowerCase] EndsWith:@"png"] || [[_originalefilename ToLowerCase] EndsWith:@"jpg"]) { 
B4IRDebugUtils.currentLine=524302;
 //BA.debugLineNum = 524302;BA.debugLine="ImageView1.Bitmap=LoadBitmap(File.DirTemp,tempfi";
[self->__imageview1 setBitmap:[self->___c LoadBitmap:[[self->___c File] DirTemp] :_tempfilename]];
B4IRDebugUtils.currentLine=524303;
 //BA.debugLineNum = 524303;BA.debugLine="resp.Status=200";
_resp->__status /*int*/  = (int) (200);
B4IRDebugUtils.currentLine=524304;
 //BA.debugLineNum = 524304;BA.debugLine="resp.SendString($\"<!DOCTYPE html> <html lang=\"en";
[_resp _sendstring /*NSString**/ :nil :([@[@"<!DOCTYPE html>\n",@"<html lang=\"en\" dir=\"ltr\">\n",@"\n",@"<head>\n",@"  <meta charset=\"utf-8\">\n",@"  <title>UPload</title>\n",@"</head>\n",@"\n",@"<body id=\"body\">\n",@"<B>Download: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_filename)],@"</B><BR>\n",@"<img src=\"",[self->___c SmartStringFormatter:@"" :(NSObject*)(_tempfilename)],@"\"><BR>\n",@"<a href=\"/fileupload.html\">Back To Upload</a><BR>\n",@"<a href=\"/index.html\">Back To Home</a>\n",@"</body>\n",@"</html>"] componentsJoinedByString:@""])];
 }else {
B4IRDebugUtils.currentLine=524321;
 //BA.debugLineNum = 524321;BA.debugLine="resp.Status=200";
_resp->__status /*int*/  = (int) (200);
B4IRDebugUtils.currentLine=524322;
 //BA.debugLineNum = 524322;BA.debugLine="resp.SendString($\"<!DOCTYPE html> <html lang=\"en";
[_resp _sendstring /*NSString**/ :nil :([@[@"<!DOCTYPE html>\n",@"<html lang=\"en\" dir=\"ltr\">\n",@"\n",@"<head>\n",@"  <meta charset=\"utf-8\">\n",@"  <title>UPload</title>\n",@"</head>\n",@"\n",@"<body id=\"body\">\n",@"<B>Download: ",[self->___c SmartStringFormatter:@"" :(NSObject*)(_filename)],@"</B><BR>\n",@"<a href=\"/fileupload.html\">Back to Upload</a><BR>\n",@"<a href=\"/index.html\">Back to Home</a>\n",@"</body>\n",@"</html>"] componentsJoinedByString:@""])];
 };
B4IRDebugUtils.currentLine=524338;
 //BA.debugLineNum = 524338;BA.debugLine="Log(\"Download: \" & Filename)";
[self->___c LogImpl:@"3524338" :[@[@"Download: ",_filename] componentsJoinedByString:@""] :0];
B4IRDebugUtils.currentLine=524339;
 //BA.debugLineNum = 524339;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _svr_uploadprogress:(b4i_servletresponse*) _resp :(float) _progress{
B4IRDebugUtils.currentModule=@"main";
if ([B4IDebug shouldDelegate: @"svr_uploadprogress"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"svr_uploadprogress::" :@[[B4I nilToNSNull:_resp],@(_progress)]]);}
B4IRDebugUtils.currentLine=458752;
 //BA.debugLineNum = 458752;BA.debugLine="Private Sub Svr_UploadProgress (resp As ServletRes";
B4IRDebugUtils.currentLine=458753;
 //BA.debugLineNum = 458753;BA.debugLine="LabelUpload.Text=$\"Download $1.0{Progress*100}%\"$";
[self->__labelupload setText:([@[@"Download ",[self->___c SmartStringFormatter:@"1.0" :(NSObject*)(@(_progress*100))],@"%"] componentsJoinedByString:@""])];
B4IRDebugUtils.currentLine=458754;
 //BA.debugLineNum = 458754;BA.debugLine="End Sub";
return @"";
}
@end