#import "iArchiver.h"
#import "iCore.h"
#import "iEncryption.h"
#import "iJSON.h"
#import "iNetwork.h"
#import "iRandomAccessFile.h"
#import "iReleaseLogger.h"
#import "iStringUtils.h"
#import "iXUI.h"
#import "iDebug2.h"
@class b4i_main;
@class b4i_httpserver;
@class _tuser;
@class b4i_servletrequest;
@class b4i_queryelement;
@interface b4i_servletresponse : B4IClass
{
@public b4i_servletrequest* __request;
@public B4IAsyncStreams* __astream;
@public B4IMap* __responseheader;
@public B4IMap* __responseparameter;
@public B4IMap* __responsecookies;
@public B4IMap* __code;
@public B4IMap* __mime;
@public B4ISocketWrapper* __client;
@public int __status;
@public NSString* __contenttype;
@public NSString* __characterencoding;
@public int __contentlenght;
@public B4IArray* __dday;
@public B4IArray* __mmmonth;
@public B4IByteConverter* __bc;
@public b4i_queryelement* __myquery;
@public B4IArray* __final;
@public b4i_main* __main;

}- (NSString*)  _appenfinal:(b4i_servletresponse*) __ref :(B4IArray*) _b;
- (NSString*)  _astream_write:(b4i_servletresponse*) __ref :(B4IArray*) _data;
- (NSString*)  _class_globals:(b4i_servletresponse*) __ref;
@property (nonatomic)b4i_servletrequest* _request;
@property (nonatomic)B4IAsyncStreams* _astream;
@property (nonatomic)B4IMap* _responseheader;
@property (nonatomic)B4IMap* _responseparameter;
@property (nonatomic)B4IMap* _responsecookies;
@property (nonatomic)B4IMap* _code;
@property (nonatomic)B4IMap* _mime;
@property (nonatomic)B4ISocketWrapper* _client;
@property (nonatomic)int _status;
@property (nonatomic)NSString* _contenttype;
@property (nonatomic)NSString* _characterencoding;
@property (nonatomic)int _contentlenght;
@property (nonatomic)B4IArray* _dday;
@property (nonatomic)B4IArray* _mmmonth;
@property (nonatomic)B4IByteConverter* _bc;
@property (nonatomic)b4i_queryelement* _myquery;
@property (nonatomic)B4IArray* _final;
@property (nonatomic)b4i_main* _main;
- (NSString*)  _close:(b4i_servletresponse*) __ref;
- (NSString*)  _completedate:(b4i_servletresponse*) __ref :(long long) _dt;
- (BOOL)  _connected:(b4i_servletresponse*) __ref;
- (B4IArray*)  _datamask:(b4i_servletresponse*) __ref :(B4IArray*) _data :(B4IArray*) _maskingkey;
- (B4IArray*)  _deflatedate:(b4i_servletresponse*) __ref :(B4IArray*) _data;
- (B4IOutputStream*)  _getoutputstream:(b4i_servletresponse*) __ref;
- (b4i_queryelement*)  _getquery:(b4i_servletresponse*) __ref;
- (NSString*)  _handshakefile:(b4i_servletresponse*) __ref :(NSString*) _lastmodified :(long long) _filesize;
- (NSString*)  _handshakestring:(b4i_servletresponse*) __ref;
- (NSString*)  _initialize:(b4i_servletresponse*) __ref :(B4I*) _ba :(b4i_servletrequest*) _req :(B4IAsyncStreams*) _ast :(B4ISocketWrapper*) _sck;
- (NSString*)  _resetcookies:(b4i_servletresponse*) __ref;
- (NSString*)  _searchmime:(b4i_servletresponse*) __ref :(NSString*) _nm;
- (NSString*)  _sendfile:(b4i_servletresponse*) __ref :(NSString*) _dir :(NSString*) _filename;
- (NSString*)  _sendfile2:(b4i_servletresponse*) __ref :(NSString*) _dir :(NSString*) _filename :(NSString*) _content_type;
- (NSString*)  _sendnotfound:(b4i_servletresponse*) __ref :(NSString*) _filenamenotfound;
- (NSString*)  _sendraw:(b4i_servletresponse*) __ref :(B4IArray*) _data;
- (NSString*)  _sendredirect:(b4i_servletresponse*) __ref :(NSString*) _address;
- (NSString*)  _sendstring:(b4i_servletresponse*) __ref :(NSString*) _text;
- (NSString*)  _sendwebsocketbinary:(b4i_servletresponse*) __ref :(B4IArray*) _data :(BOOL) _masked;
- (NSString*)  _sendwebsocketclose:(b4i_servletresponse*) __ref;
- (NSString*)  _sendwebsocketping:(b4i_servletresponse*) __ref;
- (NSString*)  _sendwebsocketpong:(b4i_servletresponse*) __ref;
- (NSString*)  _sendwebsocketstring:(b4i_servletresponse*) __ref :(NSString*) _text :(BOOL) _masked :(NSString*) _compressed;
- (NSString*)  _setcookies:(b4i_servletresponse*) __ref :(NSString*) _name :(NSString*) _value;
- (NSString*)  _setheader:(b4i_servletresponse*) __ref :(NSString*) _name :(NSString*) _value;
- (NSString*)  _statuscode:(b4i_servletresponse*) __ref :(int) _sts;
- (NSString*)  _write:(b4i_servletresponse*) __ref :(NSString*) _text;
@end
