
#import "b4i_queryelement.h"
#import "b4i_main.h"
#import "b4i_httpserver.h"
#import "b4i_servletrequest.h"
#import "b4i_servletresponse.h"


@implementation b4i_queryelement 


+ (B4I*)createBI {
    return [B4IShellBI alloc];
}

- (void)dealloc {
    if (self.bi != nil)
        NSLog(@"Class (b4i_queryelement) instance released.");
}

- (NSString*)  _initialize:(b4i_queryelement*) __ref :(B4I*) _ba :(b4i_servletresponse*) _response{
__ref = self;
[self initializeClassModule];
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"initialize"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"initialize::" :@[[B4I nilToNSNull:_ba],[B4I nilToNSNull:_response]]]);}
B4IRDebugUtils.currentLine=6881280;
 //BA.debugLineNum = 6881280;BA.debugLine="Public Sub Initialize(Response As ServletResponse)";
B4IRDebugUtils.currentLine=6881281;
 //BA.debugLineNum = 6881281;BA.debugLine="Resp=Response";
__ref->__resp /*b4i_servletresponse**/  = _response;
B4IRDebugUtils.currentLine=6881282;
 //BA.debugLineNum = 6881282;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _class_globals:(b4i_queryelement*) __ref{
__ref = self;
self->__main=[b4i_main new];
B4IRDebugUtils.currentModule=@"queryelement";
B4IRDebugUtils.currentLine=6815744;
 //BA.debugLineNum = 6815744;BA.debugLine="Sub Class_Globals";
B4IRDebugUtils.currentLine=6815745;
 //BA.debugLineNum = 6815745;BA.debugLine="Public Const Event_change As String = \"change\"";
self->__event_change = @"change";
B4IRDebugUtils.currentLine=6815746;
 //BA.debugLineNum = 6815746;BA.debugLine="Public Const Event_click As String = \"click\"";
self->__event_click = @"click";
B4IRDebugUtils.currentLine=6815747;
 //BA.debugLineNum = 6815747;BA.debugLine="Public Const Event_dblclick As String = \"dblclick";
self->__event_dblclick = @"dblclick";
B4IRDebugUtils.currentLine=6815748;
 //BA.debugLineNum = 6815748;BA.debugLine="Public Const Event_focus As String = \"focus\"";
self->__event_focus = @"focus";
B4IRDebugUtils.currentLine=6815749;
 //BA.debugLineNum = 6815749;BA.debugLine="Public Const Event_focusin As String = \"focusin\"";
self->__event_focusin = @"focusin";
B4IRDebugUtils.currentLine=6815750;
 //BA.debugLineNum = 6815750;BA.debugLine="Public Const Event_focusout As String = \"focusout";
self->__event_focusout = @"focusout";
B4IRDebugUtils.currentLine=6815751;
 //BA.debugLineNum = 6815751;BA.debugLine="Public Const Event_keyup As String = \"keyup\"";
self->__event_keyup = @"keyup";
B4IRDebugUtils.currentLine=6815752;
 //BA.debugLineNum = 6815752;BA.debugLine="Public Const Event_mousedown As String = \"mousedo";
self->__event_mousedown = @"mousedown";
B4IRDebugUtils.currentLine=6815753;
 //BA.debugLineNum = 6815753;BA.debugLine="Public Const Event_mouseenter As String = \"mousee";
self->__event_mouseenter = @"mouseenter";
B4IRDebugUtils.currentLine=6815754;
 //BA.debugLineNum = 6815754;BA.debugLine="Public Const Event_mouseleave As String = \"mousel";
self->__event_mouseleave = @"mouseleave";
B4IRDebugUtils.currentLine=6815755;
 //BA.debugLineNum = 6815755;BA.debugLine="Public Const Event_mousemove As String = \"mousemo";
self->__event_mousemove = @"mousemove";
B4IRDebugUtils.currentLine=6815756;
 //BA.debugLineNum = 6815756;BA.debugLine="Public Const Event_mouseup As String = \"mouseup\"";
self->__event_mouseup = @"mouseup";
B4IRDebugUtils.currentLine=6815758;
 //BA.debugLineNum = 6815758;BA.debugLine="Public const NoEvent() As Map=Array As Map()";
self->__noevent = [[B4IArray alloc]initObjectsWithData:@[]];
B4IRDebugUtils.currentLine=6815760;
 //BA.debugLineNum = 6815760;BA.debugLine="Private Resp As ServletResponse";
self->__resp = [b4i_servletresponse new];
B4IRDebugUtils.currentLine=6815761;
 //BA.debugLineNum = 6815761;BA.debugLine="End Sub";
return @"";
}
- (B4IArray*)  _createevent:(b4i_queryelement*) __ref :(NSString*) _objectname :(NSString*) _event :(B4IArray*) _otherevent{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"createevent"])
	 {return ((B4IArray*) [B4IDebug delegate:self.bi :@"createevent:::" :@[[B4I nilToNSNull:_objectname],[B4I nilToNSNull:_event],[B4I nilToNSNull:_otherevent]]]);}
B4IArray* _ev = nil;
int _i = 0;
B4IRDebugUtils.currentLine=7471104;
 //BA.debugLineNum = 7471104;BA.debugLine="Public Sub CreateEvent(ObjectName As String,Event";
B4IRDebugUtils.currentLine=7471105;
 //BA.debugLineNum = 7471105;BA.debugLine="If isnull(OtherEvent)=False Then";
if ([__ref _isnull /*BOOL*/ :nil :(NSObject*)(_otherevent)]==false) { 
B4IRDebugUtils.currentLine=7471106;
 //BA.debugLineNum = 7471106;BA.debugLine="Dim Ev(OtherEvent.Length+1) As Map";
_ev = [[B4IArray alloc]initObjects:@[@((int) (_otherevent.Length+1))] :nil :@"B4IMap"];
B4IRDebugUtils.currentLine=7471107;
 //BA.debugLineNum = 7471107;BA.debugLine="For i=0 To OtherEvent.Length-1";
{
const int step3 = 1;
const int limit3 = (int) (_otherevent.Length-1);
_i = (int) (0) ;
for (;_i <= limit3 ;_i = _i + step3 ) {
B4IRDebugUtils.currentLine=7471108;
 //BA.debugLineNum = 7471108;BA.debugLine="Ev(i)=OtherEvent(i)";
[_ev setObjectFast:_i:((B4IMap*)[_otherevent getObjectFast:_i])];
 }
};
B4IRDebugUtils.currentLine=7471110;
 //BA.debugLineNum = 7471110;BA.debugLine="Ev(OtherEvent.Length).Initialize";
[((B4IMap*)[_ev getObjectFast:_otherevent.Length]) Initialize];
B4IRDebugUtils.currentLine=7471111;
 //BA.debugLineNum = 7471111;BA.debugLine="Ev(OtherEvent.Length).Put(\"id\",ObjectName)";
[((B4IMap*)[_ev getObjectFast:_otherevent.Length]) Put:(NSObject*)(@"id") :(NSObject*)(_objectname)];
B4IRDebugUtils.currentLine=7471112;
 //BA.debugLineNum = 7471112;BA.debugLine="Ev(OtherEvent.Length).Put(\"event\",Event)";
[((B4IMap*)[_ev getObjectFast:_otherevent.Length]) Put:(NSObject*)(@"event") :(NSObject*)(_event)];
 }else {
B4IRDebugUtils.currentLine=7471114;
 //BA.debugLineNum = 7471114;BA.debugLine="Dim Ev(1) As Map";
_ev = [[B4IArray alloc]initObjects:@[@((int) (1))] :nil :@"B4IMap"];
B4IRDebugUtils.currentLine=7471115;
 //BA.debugLineNum = 7471115;BA.debugLine="Ev(0).Initialize";
[((B4IMap*)[_ev getObjectFast:(int) (0)]) Initialize];
B4IRDebugUtils.currentLine=7471116;
 //BA.debugLineNum = 7471116;BA.debugLine="Ev(0).Put(\"id\",ObjectName)";
[((B4IMap*)[_ev getObjectFast:(int) (0)]) Put:(NSObject*)(@"id") :(NSObject*)(_objectname)];
B4IRDebugUtils.currentLine=7471117;
 //BA.debugLineNum = 7471117;BA.debugLine="Ev(0).Put(\"event\",Event)";
[((B4IMap*)[_ev getObjectFast:(int) (0)]) Put:(NSObject*)(@"event") :(NSObject*)(_event)];
 };
B4IRDebugUtils.currentLine=7471120;
 //BA.debugLineNum = 7471120;BA.debugLine="Return Ev";
if (true) return _ev;
B4IRDebugUtils.currentLine=7471121;
 //BA.debugLineNum = 7471121;BA.debugLine="End Sub";
return nil;
}
- (BOOL)  _isnull:(b4i_queryelement*) __ref :(NSObject*) _o{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"isnull"])
	 {return ((NSNumber*) [B4IDebug delegate:self.bi :@"isnull:" :@[[B4I nilToNSNull:_o]]]).boolValue;}
B4IRDebugUtils.currentLine=7536640;
 //BA.debugLineNum = 7536640;BA.debugLine="Private Sub isnull(O As Object) As Boolean";
B4IRDebugUtils.currentLine=7536641;
 //BA.debugLineNum = 7536641;BA.debugLine="Return (o = Null)";
if (true) return (_o== nil);
B4IRDebugUtils.currentLine=7536642;
 //BA.debugLineNum = 7536642;BA.debugLine="End Sub";
return false;
}
- (NSString*)  _escapehtml:(b4i_queryelement*) __ref :(NSString*) _raw{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"escapehtml"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"escapehtml:" :@[[B4I nilToNSNull:_raw]]]);}
B4IRDebugUtils.currentLine=8257536;
 //BA.debugLineNum = 8257536;BA.debugLine="Public Sub EscapeHtml(Raw As String) As String";
B4IRDebugUtils.currentLine=8257537;
 //BA.debugLineNum = 8257537;BA.debugLine="Return Raw.Replace(QUOTE,\"&quot;\").Replace(\"'\",\"&";
if (true) return [[[[[_raw Replace:@"\"" :@"&quot;"] Replace:@"'" :@"&#39;"] Replace:@"<" :@"&lt;"] Replace:@"<" :@"&gt;"] Replace:@"&" :@"&amp;"];
B4IRDebugUtils.currentLine=8257538;
 //BA.debugLineNum = 8257538;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _eval:(b4i_queryelement*) __ref :(NSString*) _script :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"eval"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"eval::" :@[[B4I nilToNSNull:_script],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7340032;
 //BA.debugLineNum = 7340032;BA.debugLine="Public Sub Eval(Script As String, Params As List)";
B4IRDebugUtils.currentLine=7340033;
 //BA.debugLineNum = 7340033;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"eval\",\"\",Scr";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"eval" :@"" :_script :@"" :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)] :_params] :false :@""];
B4IRDebugUtils.currentLine=7340034;
 //BA.debugLineNum = 7340034;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setcommand:(b4i_queryelement*) __ref :(NSString*) _etype :(NSString*) _method :(NSString*) _property :(NSString*) _id :(B4IList*) _params :(B4IList*) _arg{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setcommand"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setcommand::::::" :@[[B4I nilToNSNull:_etype],[B4I nilToNSNull:_method],[B4I nilToNSNull:_property],[B4I nilToNSNull:_id],[B4I nilToNSNull:_params],[B4I nilToNSNull:_arg]]]);}
B4IJSONGenerator* _j = nil;
B4IMap* _m = nil;
B4IRDebugUtils.currentLine=6946816;
 //BA.debugLineNum = 6946816;BA.debugLine="Public Sub SetCommand (etype As String,Method As S";
B4IRDebugUtils.currentLine=6946817;
 //BA.debugLineNum = 6946817;BA.debugLine="Dim J As JSONGenerator";
_j = [B4IJSONGenerator new];
B4IRDebugUtils.currentLine=6946818;
 //BA.debugLineNum = 6946818;BA.debugLine="Dim M As Map";
_m = [B4IMap new];
B4IRDebugUtils.currentLine=6946819;
 //BA.debugLineNum = 6946819;BA.debugLine="M.Initialize";
[_m Initialize];
B4IRDebugUtils.currentLine=6946820;
 //BA.debugLineNum = 6946820;BA.debugLine="M.Put(\"etype\",etype)";
[_m Put:(NSObject*)(@"etype") :(NSObject*)(_etype)];
B4IRDebugUtils.currentLine=6946821;
 //BA.debugLineNum = 6946821;BA.debugLine="If Method<>\"\" Then M.Put(\"method\",Method)";
if ([_method isEqual:@""] == false) { 
[_m Put:(NSObject*)(@"method") :(NSObject*)(_method)];};
B4IRDebugUtils.currentLine=6946822;
 //BA.debugLineNum = 6946822;BA.debugLine="If ID<>\"\" Then M.Put(\"id\",ID)";
if ([_id isEqual:@""] == false) { 
[_m Put:(NSObject*)(@"id") :(NSObject*)(_id)];};
B4IRDebugUtils.currentLine=6946823;
 //BA.debugLineNum = 6946823;BA.debugLine="If property<>\"\" Then M.Put(\"prop\",property)";
if ([_property isEqual:@""] == false) { 
[_m Put:(NSObject*)(@"prop") :(NSObject*)(_property)];};
B4IRDebugUtils.currentLine=6946824;
 //BA.debugLineNum = 6946824;BA.debugLine="If Params.IsInitialized Then M.Put(\"params\",Param";
if ([_params IsInitialized]) { 
[_m Put:(NSObject*)(@"params") :(NSObject*)((_params).object)];};
B4IRDebugUtils.currentLine=6946825;
 //BA.debugLineNum = 6946825;BA.debugLine="If Arg.IsInitialized Then M.Put(\"value\",Arg)";
if ([_arg IsInitialized]) { 
[_m Put:(NSObject*)(@"value") :(NSObject*)((_arg).object)];};
B4IRDebugUtils.currentLine=6946826;
 //BA.debugLineNum = 6946826;BA.debugLine="J.Initialize(M)";
[_j Initialize:_m];
B4IRDebugUtils.currentLine=6946828;
 //BA.debugLineNum = 6946828;BA.debugLine="Return J.ToString";
if (true) return [_j ToString];
B4IRDebugUtils.currentLine=6946829;
 //BA.debugLineNum = 6946829;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _evalwithresult:(b4i_queryelement*) __ref :(NSString*) _script :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"evalwithresult"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"evalwithresult::" :@[[B4I nilToNSNull:_script],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7405568;
 //BA.debugLineNum = 7405568;BA.debugLine="Public Sub EvalWithResult(Script As String, Params";
B4IRDebugUtils.currentLine=7405569;
 //BA.debugLineNum = 7405569;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"evalWithResu";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"evalWithResult" :@"" :_script :@"" :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)] :_params] :false :@""];
B4IRDebugUtils.currentLine=7405570;
 //BA.debugLineNum = 7405570;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _getpropriety:(b4i_queryelement*) __ref :(NSString*) _property :(B4IList*) _value{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"getpropriety"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getpropriety::" :@[[B4I nilToNSNull:_property],[B4I nilToNSNull:_value]]]);}
B4IRDebugUtils.currentLine=7995392;
 //BA.debugLineNum = 7995392;BA.debugLine="Public Sub GetPropriety (Property As String, Value";
B4IRDebugUtils.currentLine=7995393;
 //BA.debugLineNum = 7995393;BA.debugLine="RunMethodWithResult(\"prop\",Property,Value)";
[__ref _runmethodwithresult /*NSString**/ :nil :@"prop" :_property :_value];
B4IRDebugUtils.currentLine=7995394;
 //BA.debugLineNum = 7995394;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _runmethodwithresult:(b4i_queryelement*) __ref :(NSString*) _method :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"runmethodwithresult"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"runmethodwithresult:::" :@[[B4I nilToNSNull:_method],[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7274496;
 //BA.debugLineNum = 7274496;BA.debugLine="Public Sub RunMethodWithResult (Method As String,";
B4IRDebugUtils.currentLine=7274497;
 //BA.debugLineNum = 7274497;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runmethodWit";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"runmethodWithResult" :_method :@"" :_id :_params :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)]] :false :@""];
B4IRDebugUtils.currentLine=7274498;
 //BA.debugLineNum = 7274498;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _getval:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _valuelist{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"getval"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"getval::" :@[[B4I nilToNSNull:_id],[B4I nilToNSNull:_valuelist]]]);}
B4IRDebugUtils.currentLine=7667712;
 //BA.debugLineNum = 7667712;BA.debugLine="Public Sub GetVal (ID As String, ValueList As List";
B4IRDebugUtils.currentLine=7667713;
 //BA.debugLineNum = 7667713;BA.debugLine="RunMethodWithResult(\"val\",ID,ValueList)";
[__ref _runmethodwithresult /*NSString**/ :nil :@"val" :_id :_valuelist];
B4IRDebugUtils.currentLine=7667714;
 //BA.debugLineNum = 7667714;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _runfunction:(b4i_queryelement*) __ref :(NSString*) _function :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"runfunction"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"runfunction:::" :@[[B4I nilToNSNull:_function],[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7077888;
 //BA.debugLineNum = 7077888;BA.debugLine="Public Sub RunFunction (function As String, ID As";
B4IRDebugUtils.currentLine=7077889;
 //BA.debugLineNum = 7077889;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runFunction\"";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"runFunction" :@"" :_function :_id :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)] :_params] :false :@""];
B4IRDebugUtils.currentLine=7077890;
 //BA.debugLineNum = 7077890;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _runfunctionwithresult:(b4i_queryelement*) __ref :(NSString*) _function :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"runfunctionwithresult"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"runfunctionwithresult:::" :@[[B4I nilToNSNull:_function],[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7143424;
 //BA.debugLineNum = 7143424;BA.debugLine="Public Sub RunFunctionWithResult (function As Stri";
B4IRDebugUtils.currentLine=7143425;
 //BA.debugLineNum = 7143425;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runFunctionW";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"runFunctionWithResult" :@"" :_function :_id :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)] :_params] :false :@""];
B4IRDebugUtils.currentLine=7143426;
 //BA.debugLineNum = 7143426;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _runmethod:(b4i_queryelement*) __ref :(NSString*) _method :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"runmethod"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"runmethod:::" :@[[B4I nilToNSNull:_method],[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7208960;
 //BA.debugLineNum = 7208960;BA.debugLine="Public Sub RunMethod (Method As String, ID As Stri";
B4IRDebugUtils.currentLine=7208961;
 //BA.debugLineNum = 7208961;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runmethod\",M";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"runmethod" :_method :@"" :_id :_params :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)]] :false :@""];
B4IRDebugUtils.currentLine=7208962;
 //BA.debugLineNum = 7208962;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _selectelement:(b4i_queryelement*) __ref :(NSString*) _id{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"selectelement"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"selectelement:" :@[[B4I nilToNSNull:_id]]]);}
B4IRDebugUtils.currentLine=8192000;
 //BA.debugLineNum = 8192000;BA.debugLine="Public Sub SelectElement (ID As String)";
B4IRDebugUtils.currentLine=8192001;
 //BA.debugLineNum = 8192001;BA.debugLine="Resp.SendWebSocketString(SetCommand(\"runmethod\",\"";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[__ref _setcommand /*NSString**/ :nil :@"runmethod" :@"select" :@"" :_id :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)] :(B4IList*) [B4IObjectWrapper createWrapper:[B4IList new] object:(NSArray*)(nil)]] :false :@""];
B4IRDebugUtils.currentLine=8192002;
 //BA.debugLineNum = 8192002;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setautomaticevents:(b4i_queryelement*) __ref :(B4IArray*) _arg{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setautomaticevents"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setautomaticevents:" :@[[B4I nilToNSNull:_arg]]]);}
B4IJSONGenerator* _j = nil;
B4IMap* _m = nil;
B4IRDebugUtils.currentLine=7012352;
 //BA.debugLineNum = 7012352;BA.debugLine="Public Sub setAutomaticEvents(Arg() As Map)";
B4IRDebugUtils.currentLine=7012353;
 //BA.debugLineNum = 7012353;BA.debugLine="Dim J As JSONGenerator";
_j = [B4IJSONGenerator new];
B4IRDebugUtils.currentLine=7012354;
 //BA.debugLineNum = 7012354;BA.debugLine="Dim M As Map";
_m = [B4IMap new];
B4IRDebugUtils.currentLine=7012355;
 //BA.debugLineNum = 7012355;BA.debugLine="M.Initialize";
[_m Initialize];
B4IRDebugUtils.currentLine=7012356;
 //BA.debugLineNum = 7012356;BA.debugLine="M.Put(\"etype\",\"setAutomaticEvents\")";
[_m Put:(NSObject*)(@"etype") :(NSObject*)(@"setAutomaticEvents")];
B4IRDebugUtils.currentLine=7012357;
 //BA.debugLineNum = 7012357;BA.debugLine="M.Put(\"data\",Arg)";
[_m Put:(NSObject*)(@"data") :(NSObject*)(_arg)];
B4IRDebugUtils.currentLine=7012358;
 //BA.debugLineNum = 7012358;BA.debugLine="J.Initialize(M)";
[_j Initialize:_m];
B4IRDebugUtils.currentLine=7012360;
 //BA.debugLineNum = 7012360;BA.debugLine="Resp.SendWebSocketString(J.ToString,False,\"\")";
[__ref->__resp /*b4i_servletresponse**/  _sendwebsocketstring /*NSString**/ :nil :[_j ToString] :false :@""];
B4IRDebugUtils.currentLine=7012361;
 //BA.debugLineNum = 7012361;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setautomaticeventsfrompage:(b4i_queryelement*) __ref :(NSString*) _html{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setautomaticeventsfrompage"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setautomaticeventsfrompage:" :@[[B4I nilToNSNull:_html]]]);}
B4IRDebugUtils.currentLine=7602176;
 //BA.debugLineNum = 7602176;BA.debugLine="Private Sub setAutomaticEventsFromPage(Html As Str";
B4IRDebugUtils.currentLine=7602177;
 //BA.debugLineNum = 7602177;BA.debugLine="Log(Html)";
[self->___c LogImpl:@"37602177" :_html :0];
B4IRDebugUtils.currentLine=7602178;
 //BA.debugLineNum = 7602178;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setcss:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setcss"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setcss::" :@[[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7798784;
 //BA.debugLineNum = 7798784;BA.debugLine="Public Sub SetCSS (id As String, Params As List)";
B4IRDebugUtils.currentLine=7798785;
 //BA.debugLineNum = 7798785;BA.debugLine="RunMethod(\"css\",id,Params)";
[__ref _runmethod /*NSString**/ :nil :@"css" :_id :_params];
B4IRDebugUtils.currentLine=7798786;
 //BA.debugLineNum = 7798786;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setdialog:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setdialog"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setdialog::" :@[[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7733248;
 //BA.debugLineNum = 7733248;BA.debugLine="Public Sub SetDialog (id As String, Params As List";
B4IRDebugUtils.currentLine=7733249;
 //BA.debugLineNum = 7733249;BA.debugLine="Resp.Query.RunMethod(\"dialog\",id,Params)";
[[__ref->__resp /*b4i_servletresponse**/  _getquery /*b4i_queryelement**/ :nil] _runmethod /*NSString**/ :nil :@"dialog" :_id :_params];
B4IRDebugUtils.currentLine=7733250;
 //BA.debugLineNum = 7733250;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _sethtml:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _params{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"sethtml"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"sethtml::" :@[[B4I nilToNSNull:_id],[B4I nilToNSNull:_params]]]);}
B4IRDebugUtils.currentLine=7864320;
 //BA.debugLineNum = 7864320;BA.debugLine="Public Sub SetHtml (id As String,  Params As List)";
B4IRDebugUtils.currentLine=7864321;
 //BA.debugLineNum = 7864321;BA.debugLine="RunMethod(\"html\",id,Params)";
[__ref _runmethod /*NSString**/ :nil :@"html" :_id :_params];
B4IRDebugUtils.currentLine=7864322;
 //BA.debugLineNum = 7864322;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setpropriety:(b4i_queryelement*) __ref :(NSString*) _property :(B4IList*) _value{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setpropriety"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setpropriety::" :@[[B4I nilToNSNull:_property],[B4I nilToNSNull:_value]]]);}
B4IRDebugUtils.currentLine=7929856;
 //BA.debugLineNum = 7929856;BA.debugLine="Public Sub SetPropriety (Property As String, Value";
B4IRDebugUtils.currentLine=7929857;
 //BA.debugLineNum = 7929857;BA.debugLine="RunMethod(\"prop\",Property,Value)";
[__ref _runmethod /*NSString**/ :nil :@"prop" :_property :_value];
B4IRDebugUtils.currentLine=7929858;
 //BA.debugLineNum = 7929858;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _settext:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _textlist{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"settext"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"settext::" :@[[B4I nilToNSNull:_id],[B4I nilToNSNull:_textlist]]]);}
B4IRDebugUtils.currentLine=8060928;
 //BA.debugLineNum = 8060928;BA.debugLine="Public Sub SetText (ID As String, TextList As List";
B4IRDebugUtils.currentLine=8060929;
 //BA.debugLineNum = 8060929;BA.debugLine="RunMethod(\"text\",ID,TextList)";
[__ref _runmethod /*NSString**/ :nil :@"text" :_id :_textlist];
B4IRDebugUtils.currentLine=8060930;
 //BA.debugLineNum = 8060930;BA.debugLine="End Sub";
return @"";
}
- (NSString*)  _setval:(b4i_queryelement*) __ref :(NSString*) _id :(B4IList*) _valuelist{
__ref = self;
B4IRDebugUtils.currentModule=@"queryelement";
if ([B4IDebug shouldDelegate: @"setval"])
	 {return ((NSString*) [B4IDebug delegate:self.bi :@"setval::" :@[[B4I nilToNSNull:_id],[B4I nilToNSNull:_valuelist]]]);}
B4IRDebugUtils.currentLine=8126464;
 //BA.debugLineNum = 8126464;BA.debugLine="Public Sub SetVal (ID As String, ValueList As List";
B4IRDebugUtils.currentLine=8126465;
 //BA.debugLineNum = 8126465;BA.debugLine="RunMethod(\"val\",ID,ValueList)";
[__ref _runmethod /*NSString**/ :nil :@"val" :_id :_valuelist];
B4IRDebugUtils.currentLine=8126466;
 //BA.debugLineNum = 8126466;BA.debugLine="End Sub";
return @"";
}
@end