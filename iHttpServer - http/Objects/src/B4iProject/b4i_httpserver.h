#import "iArchiver.h"
#import "iCore.h"
#import "iEncryption.h"
#import "iJSON.h"
#import "iNetwork.h"
#import "iRandomAccessFile.h"
#import "iReleaseLogger.h"
#import "iStringUtils.h"
#import "iXUI.h"
#import "iDebug2.h"
@class b4i_main;
@class _tuser;
@class b4i_servletrequest;
@class b4i_servletresponse;
@class b4i_queryelement;
@interface b4i_httpserver : B4IClass
{
@public B4IMap* __users;
@public B4IServerSocketWrapper* __serv;
@public BOOL __work;
@public NSObject* __mcallback;
@public NSString* __meventname;
@public BOOL __digestauthentication;
@public NSString* __digestpath;
@public NSString* __realm;
@public B4IList* __htdigest;
@public BOOL __ignorenc;
@public int __timeout;
@public b4i_main* __main;

}- (NSString*)  _class_globals:(b4i_httpserver*) __ref;
@property (nonatomic)B4IMap* _users;
@property (nonatomic)B4IServerSocketWrapper* _serv;
@property (nonatomic)BOOL _work;
@property (nonatomic)NSObject* _mcallback;
@property (nonatomic)NSString* _meventname;
@property (nonatomic)BOOL _digestauthentication;
@property (nonatomic)NSString* _digestpath;
@property (nonatomic)NSString* _realm;
@property (nonatomic)B4IList* _htdigest;
@property (nonatomic)BOOL _ignorenc;
@property (nonatomic)int _timeout;
@property (nonatomic)b4i_main* _main;
- (NSString*)  _data_handle:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _res;
- (NSString*)  _data_handlewebsocket:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp;
- (NSString*)  _data_switchtowebsocket:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp;
- (NSString*)  _data_uploadedfile:(b4i_httpserver*) __ref :(b4i_servletrequest*) _req :(b4i_servletresponse*) _resp;
- (NSString*)  _data_websocketclose:(b4i_httpserver*) __ref :(int) _closecode :(NSString*) _closemessage;
- (NSString*)  _getmyip:(b4i_httpserver*) __ref;
- (NSString*)  _getmywifiip:(b4i_httpserver*) __ref;
- (NSString*)  _gettemppath:(b4i_httpserver*) __ref;
- (NSString*)  _initialize:(b4i_httpserver*) __ref :(B4I*) _ba :(NSObject*) _callback :(NSString*) _eventname;
- (void)  _start:(b4i_httpserver*) __ref :(int) _port;
- (NSString*)  _stop:(b4i_httpserver*) __ref;
- (BOOL)  _subexists2:(b4i_httpserver*) __ref :(NSObject*) _callobject :(NSString*) _eventname :(int) _param;
@end
