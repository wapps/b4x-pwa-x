﻿B4i=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6.8
@EndOfDesignText@
Sub Class_Globals
	Type tUser (Address As String, nc As Int, nonce As String, opaque As String, LastRequest As Long, realm As String)
	Private DDay() As String = Array As String("","Sun","Mon","Tue","Wed","Thu","Fri","Sat")
	Private MMmonth() As String = Array As String("","January","February","March","April","May","June","July","August","September","October","November","December")
	
	Private client As Socket
	Private astream As AsyncStreams
	Private mCallBack As Object
	Private mEventName As String
	Private bc As ByteConverter

	Private method As String = ""
	Private RequestURI As String = ""
	Private RequestHOST As String = ""
	Private Address As String =""
	Private ConnPort As String = ""
	Public ID As String
	
	Public RequestHeader As Map
	Public RequestParameter As Map
	Public RequestCookies As Map
	Public RequestPostDataRow As List
	Public ConnectionAlive As Boolean = True
	Public CharacterEncoding As String = "UTF-8" '"ISO-8859-1,UTF-8;"
	
	'-----------REQUEST
	'Encoding, ContentType, ContentLengh
	'GetParameter,GetMultiPartData,GetSession
	'InputStream, RemoteAddress, Secure(boolean)
	Private CallServer As httpServer 
	Private Response As ServletResponse
	Private Users As Map
	Private UserName As String = ""
	Private Password As String = ""
	Private OtherData As Boolean = False
	Private Cache() As Byte
	Private BBB() As Byte
	Private su As StringUtils
	
	Private LogPrivate As Boolean = False
	Public LogActive As Boolean = False
	Public ContentType As String = ""
	Public ContentLength As Long = 0
	Public MultipartFilename As Map
	Public LogFirstRefuse As Boolean = False
	Public Timeout As Long = 10000
	Private LastBrowserComunicate As Long = 0
	Private gzip=False, deflate=False As Boolean
	
	Private WebSocket As Boolean = False
	Private keyWebSocket As String = ""
	Private WebSocketString As String
	Private LastOpCode As Byte = 0
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize(CallBack As Object, EventName As String, Sck As Socket)
	client = Sck
	mCallBack=CallBack
	mEventName=EventName
	CallServer= mCallBack
	
	Users.Initialize
	Cache=ArrayInitialize
	BBB=ArrayInitialize
	MultipartFilename.Initialize
	
	#IF B4I
	Dim no As NativeObject = Sck
	Address=no.GetField("socket").GetField("host").AsString
	ConnPort=no.GetField("socket").GetField("port").AsString
	#Else IF B4a
	Dim Sock As Reflector
	Sock.Target=Sck
	Dim sor As JavaObject = Sock.GetField("socket")
	Address=sor.RunMethod("getRemoteSocketAddress",Null)
	ConnPort=sor.RunMethod("getPort",Null)
	Address=Address.Replace("/","")
	Address=Regex.Split(":",Address)(0)
	#Else IF B4J
	Dim Sock As Reflector
	Sock.Target=Sck
	Dim sor As JavaObject = Sock.GetField("socket")
	Address=sor.RunMethod("getRemoteSocketAddress",Null)
	ConnPort=sor.RunMethod("getPort",Null)
	Address=Address.Replace("/","")
	Address=Regex.Split(":",Address)(0)
	#END IF
	
	astream.Initialize(client.InputStream,client.OutputStream,"astream")
	Response.Initialize(Me,astream,client)
	
	RequestHeader.Initialize
	RequestParameter.Initialize
	RequestCookies.Initialize
	RequestPostDataRow.Initialize

	BrowserTimeOut
End Sub

Public Sub GetInputStream As InputStream
	Return client.InputStream
End Sub

Public Sub Close
	Try
		astream.SendAllAndClose
		'If client.IsInitialized Then client.Close
	Catch
		Log($"Server error(${ID}): ${LastException.Message}"$ )
	End Try
End Sub

Public Sub Connected As Boolean
	Return client.Connected
End Sub

Public Sub RemoteAddress As String
	Return Address
End Sub

Public Sub RemotePort As Int
	Return ConnPort
End Sub

Public Sub GetMethod As String
	Return method
End Sub

Public Sub GetRequestURI As String
	Return RequestURI
End Sub

Public Sub GetRequestHOST As String
	Return RequestHOST
End Sub
Public Sub GetHeader(Name As String) As String
	Return RequestHeader.Get(Name)
End Sub

'can be used to iterate over Header
'Example
'<code>
'For Each Name As String In ServletRequest.GetHeadersName
'    Log("Value = " & ServletRequest.GetHeader(Name))
'Next</code>
Public Sub GetHeadersName As List
	Dim l As List
	l.Initialize
	
	For Each K In RequestHeader.keys
		l.Add(K)
	Next
	Return l
End Sub

public Sub ParameterMap As Map
	Return RequestParameter
End Sub

Public Sub GetWebSocketStringData As String
	Return WebSocketString
End Sub

Public Sub GetWebSocketMapData As Map
	Dim J As JSONParser
	Dim M As Map
	M.Initialize
	Try
		j.Initialize(WebSocketString)
		M =J.NextObject
	Catch
		Log(LastException)
	End Try
	Return M
End Sub

Public Sub GetWebSocketCompressDeflateAccept As Boolean
	Return deflate
End Sub

Public Sub GetWebSocketCompressGzipAccept As Boolean
	Return gzip
End Sub

' ------------------------------------------------------- PRIVATE -----------------------------------------

private Sub astream_NewData (Buffer() As Byte)
	Cache=ArrayAppend(Cache,Buffer)
	LastBrowserComunicate=DateTime.Now
	ExtractHandShake
End Sub

Private Sub astream_Terminated
	Log($"Client has terminated (${ID})"$ )
End Sub

Private Sub astream_Error
	Log($"Error (${ID})"$)
End Sub

Private Sub ExtractHandShake
	If OtherData And ContentLength>0 Then ' continue read DATA from last request
		BBB=ArrayAppend(BBB,Cache)
		Cache=ArrayInitialize

'		If SubExists2(mCallBack,mEventName & "_UploadProgress",2) Then
'			Dim F As Float = BBB.Length/ContentLength
'			CallSub3(mCallBack,mEventName & "_UploadProgress",Response,F)
'		End If
		If ContentLength<=BBB.Length Then
			If BBB.Length>ContentLength Then ' select only data and extract other byte
				Cache=ArrayAppend(Cache,SubArray(BBB,ContentLength))
				'Cache.Append(BBB.SubArray(ContentLength))
				BBB=ArrayRemove(BBB,ContentLength,BBB.Length)
				'BBB.Remove(ContentLength,BBB.Length)
			End If
			OtherData=False
			extractParameterFromData
		End If
	Else
		' new request
		Dim ChRemove As Int = 4
		Dim Index As Int = ArrayIndexOf(Cache,Array As Byte(13,10,13,10))
		If Index=-1 Then 
			ChRemove = 2
			Index = ArrayIndexOf(Cache,Array As Byte(10,10))
			If Index=-1 Then Index = ArrayIndexOf(Cache,Array As Byte(13,13))
		End If
		'Dim rows As String = BytesToString(Buffer,0,Buffer.Length,"UTF8")
	
		If Index>-1 Then
			Dim HandShake As String = BytesToString(Cache,0,Index,CharacterEncoding)
			Dim Data() As Byte = SubArray(Cache,Index+ChRemove)
			Cache=ArrayInitialize
			ElaborateHandShake(HandShake,Data)
		Else
			'No HandShake
			If WebSocket Then 
				'elaborate websocket
				Dim D() As Byte = ArrayCopy(Cache)
				ElaborateWebSocket(D)
			Else
				Log("No handshake: "& Cache.Length)
			End If
		
			Cache = ArrayInitialize
		End If
	End If
End Sub

Private Sub ElaborateHandShake(HandShake As String, Data() As Byte)
	' Examine Head
	BBB = ArrayInitialize
	BBB = ArrayAppend(BBB,Data)
	
	' RESET PARAMETER
	RequestHeader.Clear
	RequestParameter.Clear
	RequestCookies.Clear
	RequestPostDataRow.Clear
	
	For Each Row As String  In Regex.Split(CRLF,HandShake)
		Row=Row.Replace(Chr(13),"").Replace(Chr(10),"")
		Dim p() As String = Regex.Split(" ",Row)
		
		Select p(0).ToUpperCase
			Case "GET", "POST"
				method=p(0).ToUpperCase
				RequestURI=p(1)
				If RequestURI.IndexOf("?")>-1 Then
					Dim pm As String = RequestURI.SubString(RequestURI.IndexOf("?")+1)
					RequestURI=RequestURI.SubString2(0,RequestURI.IndexOf("?"))
					
					Dim pms() As String = Regex.Split("&",pm)
					
					For Each parm As String In pms
						If parm.IndexOf("=")>-1 Then
							RequestParameter.Put(parm.SubString2(0,parm.IndexOf("=")),parm.SubString(parm.IndexOf("=")+1))
						End If
					Next
				End If
				If p.Length>2 Then 
					' missing http/1.1
					If p(2).ToLowerCase.StartsWith("http/1")=False Then 
						Close
					End If
				Else
					' missing parameter http
					Close
				End If		
			Case "CONTENT-LENGTH:"
				ContentLength=p(1)
				If ContentLength>BBB.Length Then
					OtherData=True 
'					If SubExists2(mCallBack,mEventName & "_UploadProgress",2) Then
'						Dim F As Float = 0
'						CallSub3(mCallBack,mEventName & "_UploadProgress",Response,F)
'					End If
				Else
					OtherData=False
					If ContentLength<BBB.Length Then
						'Cache.Append(BBB.SubArray(ContentLength))
						Cache=ArrayAppend(Cache,SubArray(BBB,ContentLength))
						'BBB.Remove(ContentLength,BBB.Length)
						BBB=ArrayRemove(BBB,ContentLength,BBB.Length)
					End If
				End If
			Case "CONTENT-TYPE:"
				ContentType=p(1).Replace(";","").Trim
				If p.Length>2 Then 
					If P(2).indexof("charset=")>-1 Then CharacterEncoding=P(2).Replace("charset=","").Trim
				End If
			Case "HOST:"
				If p.Length>1 Then
					RequestHOST=p(1)
					If RequestHOST.IndexOf(":")>-1 Then RequestHOST=RequestHOST.SubString2(0,RequestHOST.IndexOf(":"))
				Else
					RequestHOST=""
				End If
			Case "COOKIE:"
				Dim Ck() As String = Regex.Split(";",Row.Replace("Cookie: ",""))
				For Each s As String In Ck
					If s.IndexOf("=")>-1 Then
						RequestCookies.Put(s.SubString2(0, s.IndexOf("=")).Trim,s.SubString(s.IndexOf("=")+1))
					End If
				Next
			Case "CONNECTION:"
				If p(1).ToLowerCase.IndexOf("keep-alive")>-1 Then 
					ConnectionAlive=True
				else if p(1).ToLowerCase.IndexOf("close")>-1 Then 
					ConnectionAlive=False
				End If
			Case "UPGRADE:"
				If p(1).ToLowerCase="websocket" Then WebSocket=True	
			Case "SEC-WEBSOCKET-KEY:"
				WebSocket=True
				keyWebSocket=p(1)
			Case "Accept-Encoding:".ToUpperCase
				For i=1 To p.Length-1
					If p(I).Contains("gzip") Then gzip=True
					If p(I).Contains("deflate") Then deflate=True
				Next
			Case Else
				If p(0).IndexOf(":")>-1 Then
					RequestHeader.Put(Row.SubString2(0, Row.IndexOf(":")),Row.SubString(Row.IndexOf(":")+1))
				End If
		End Select
	Next
	
	If RequestHOST="" Then
		' missing Host
		Close
	else if WebSocket Then 
		LgC(HandShake.Replace(Chr(13),""),0xFF0000FF)
		SendAcceptKeyWs
		BBB = ArrayInitialize
	Else if OtherData=False Then 
		extractParameterFromData
		CallEvent
	End If
End Sub

Private Sub ElaborateHandSkakeDigestMessage(Request As ServletRequest, SResponse As ServletResponse)
	Dim HeaderDigest As String = "Authorization"
	Dim HeaderMap, ParamtereMap As Map
	
	HeaderMap.Initialize
	ParamtereMap.Initialize
	Try
		LgP("Client: " & Request.RemoteAddress)
		LgP(Request.RequestURI)
'		Dim ServletRequestWrapper As Reflector
'		ServletRequestWrapper.Target=Request
'		
'		Dim req As JavaObject = ServletRequestWrapper.GetField("req")
'		Dim headerNames As JavaObject = req.RunMethod("getHeaderNames",Null)
'		Do While headerNames.RunMethod("hasMoreElements",Null)
'			Dim S As String= headerNames.RunMethod("nextElement",Null)
'			If S.Contains("Authenticate") Or S.Contains("Authorization") Then HeaderDigest=S
'			HeaderMap.Put(S,Request.GetHeader(S))
		''			Log($"header-${s}:${Request.GetHeader(S)}-"$)
'		Loop
'		Dim ParameterNames As JavaObject = req.RunMethod("getParameterNames",Null)
'		Do While ParameterNames.RunMethod("hasMoreElements",Null)
'			Dim S As String= ParameterNames.RunMethod("nextElement",Null)
'			ParamtereMap.Put(S,Request.GetParameter(S))
'			'Log($"Param-${s}:${Request.GetHeader(S)}-"$)
'		Loop
'		HeaderParameter.Put(Request,ParamtereMap)
		
		If  Request.GetHeader(HeaderDigest)="" Then
			' Accesso NON SICURO - RIFIUTA E MANDA nonce
			If SubExists2(mCallBack,mEventName & "_RefusedNoCredential",1) And LogFirstRefuse Then CallSub2(mCallBack,mEventName & "_RefusedNoCredential", Request.RemoteAddress)
			SendRefuse(SResponse,NewUser(Request.RemoteAddress))
		Else
			' protetto
			Dim Fields As Map
			Fields.Initialize

			For Each fl As String In Regex.Split(",",Request.GetHeader(HeaderDigest))
				If fl.IndexOf("=")>-1 Then
					Dim Key As String = fl.SubString2(0,fl.IndexOf("=")).Trim
					Dim Value As String = fl.SubString(fl.IndexOf("=")+1).Replace("""","").Trim
					Fields.Put(Key,Value)
					LgP($"** -${Key}- -${Value}-"$)
				Else
					Log("Error on Header: " & fl)
				End If
			Next
		
			'** Digest username "Salvo"
			Dim User As tUser = FindUser(Request.RemoteAddress,Fields.get("opaque"))
			FindCredential(Fields.Get("Digest username"))
			User.realm=CallServer.realm
			
			Dim HA1 As String = MD5($"${UserName}:${Fields.Get("realm")}:${Password}"$)
			Dim HA2 As String = MD5($"${Request.Method.ToUpperCase}:${Request.RequestURI}"$)
			If Fields.ContainsKey("qop") Then
				Dim SS As String = $"${HA1}:${Fields.Get("nonce")}:${Fields.Get("nc")}:${Fields.Get("cnonce")}:${Fields.Get("qop")}:${HA2}"$
			Else
				Dim SS As String = $"${HA1}:${Fields.Get("nonce")}:${HA2}"$
			End If
			Dim MD5response As String = MD5(SS)

			LgP($"HA1: ${HA1}"$)
			LgP($"HA2: ${HA2}"$)
			LgP(SS)
			LgP($"MD5 send: ${Fields.get("response")}"$)
			LgP($"MD5 Calc : ${MD5response}"$)
			
			Dim L As Int = HexToDec(Fields.Get("nc"))
			
			LgC("opaque:" & User.opaque & " clnc." & L & " nc." & User.nc ,0xFF000F04)
			If MD5response=Fields.get("response") Then
				LgC("Digest: " & MD5response,0xFF0000FF)
				If  L>User.nc Or CallServer.IgnoreNC Then
					' autorizzato
					User.nonce=bc.HexFromBytes(bc.LongsToBytes(Array As Long(DateTime.Now)))
					User.nc=Max(l,User.nc)
					If SubExists2(mCallBack,mEventName & "_LogIn",2) Then CallSub3(mCallBack,mEventName & "_LogIn",UserName, Request.RemoteAddress)
					If SubExists2(mCallBack,mEventName & "_Handle",2) Then
						SResponse.SetHeader("WWW-Authenticate",$"Digest realm="${User.realm}", qop="auth,auth-int", nonce="${User.nonce}", opaque="${User.opaque}""$)
						SResponse.ContentType="text/html"
						CallSub3(mCallBack,mEventName & "_Handle",Request,SResponse)
					End If
				Else
					' nc non sequenziale
					User.nc=Max(l,User.nc)
					SendRefuse(SResponse,User)
					If SubExists2(mCallBack,mEventName & "_RefusedWrongNC",2) Then CallSub3(mCallBack,mEventName & "_RefusedWrongNC",UserName, Request.RemoteAddress)
				End If
			Else
				' non autorizzato
				LgC("Wrong<> " & MD5response & CRLF & "Digest<> " & Fields.get("response"),0xFFFF0000)
				User.nc=Max(l,User.nc)
				SendRefuse(SResponse,User)
				If SubExists2(mCallBack,mEventName & "_RefusedWrongCredential",2) Then CallSub3(mCallBack,mEventName & "_RefusedWrongCredential",UserName, Request.RemoteAddress)
			End If
		End If
	Catch
		SResponse.Status = 500
		Log("Error serving request: " & LastException)
		SResponse.SendString("Error serving request: " & LastException)
	End Try
End Sub

Private Sub extractParameterFromData
	'Content-Disposition: form-data; name="file2"; filename="up12.JPG"
	If BBB.Length>0 Then
		If ContentType.ToLowerCase="multipart/form-data" Then
			Dim Index As Int = ArrayIndexOf(BBB,Array As Byte(13,10,13,10))
			MultipartFilename.Clear
			If Index>-1 Then
				Dim Cnt As String = BytesToString(BBB,0,Index,CharacterEncoding)
				BBB=ArrayRemove(BBB,0,Index+4)

				Index=ArrayIndexOf(BBB,Array As Byte(13,10,13,10))
				Dim M() As Byte = SubArray(BBB,Index)'ignore
				BBB=ArrayRemove(BBB,Index,BBB.Length)
				
				For Each Rw As String  In Regex.Split(CRLF,Cnt)
					If Rw.IndexOf("filename=")>-1 Then
						Dim Name As String = Rw.SubString(Rw.IndexOf("filename=")+9).Replace(Chr(34),"").Trim
						If Name<>"" Then 
							MultipartFilename.Put(Name,DateTime.Now & ".tmp")
						End If
					End If
				Next
			End If

			#If B4I or B4J
			File.WriteBytes(File.DirTemp,MultipartFilename.Get(Name),SubArray2(BBB,0,BBB.Length-89))
			#Else IF B4A
			File.WriteBytes(File.DirInternalCache,MultipartFilename.Get(Name),SubArray2(BBB,0,BBB.Length-89))
			#end if
			If SubExists2(mCallBack,mEventName & "_UploadedFile",2) Then CallSub3(mCallBack,mEventName & "_UploadedFile",Me,Response)
			BBB=ArrayInitialize
			CallEvent
		Else
			If method.ToUpperCase="POST" Then
				'multipart/form-data;
				Dim rows() As String = Regex.Split(CRLF,BytesToString(BBB,0,BBB.Length,CharacterEncoding))
				For Each Rw As String In rows
					Dim Params() As String = Regex.Split("&",Rw)
					For Each parm As String In Params
						parm=decode(parm)
						If parm.IndexOf("=")>-1 Then
							RequestParameter.Put(parm.SubString2(0,parm.IndexOf("=")),parm.SubString(parm.IndexOf("=")+1))
						Else
							RequestPostDataRow.Add(parm)
						End If
					Next
				Next
				'If Data.Length>0 Then File.WriteBytes(File.DirTemp,"temp.tmp",BBB.ToArray)
			End If
		End If
	End If
	If Cache.Length>0 Then ExtractHandShake
End Sub

Private Sub HexToDec(Hex As String) As Int
	Return bc.IntsFromBytes(bc.HexToBytes(Hex))(0)
End Sub

Private Sub DecToHex(Dec As Int) As String 'ignore
	Return bc.HexFromBytes(bc.IntsToBytes(Array As Int (Dec)))
End Sub
Private Sub MD5(stringToConvert As String) As String
	Dim MD As MessageDigest
	Dim Data() As Byte =  MD.GetMessageDigest(stringToConvert.GetBytes("UTF8"),"MD5")
	Return bc.HexFromBytes(Data).ToLowerCase
End Sub

Private Sub NewUser(nAddress As String) As tUser
	Dim nUser As tUser
	
	nUser.Initialize
	nUser.Address=nAddress
	nUser.LastRequest=DateTime.Now
	nUser.opaque=nAddress.SubString2(0,3) & bc.HexFromBytes(bc.LongsToBytes(Array As Long(DateTime.Now)))
	nUser.nonce=bc.HexFromBytes(bc.LongsToBytes(Array As Long(DateTime.Now)))
	nUser.nc=1
	nUser.realm=CallServer.realm

	Users.Put(nUser.opaque,nUser)
	
	Return nUser
End Sub

Private Sub FindUser(nAddress As String,opaque As String) As tUser
	Dim nUser As tUser
	
	If Users.ContainsKey(opaque) Then
		nUser=Users.Get(opaque)
	Else
		nUser=NewUser(nAddress)
		Users.Put(nUser.opaque,nUser)
	End If
	
	Return nUser
End Sub

Private Sub FindCredential(Un As String)
	UserName  = ""
	'realm= ""
	Password = ""
	
	For Each S As String In CallServer.htdigest
		If S.StartsWith($"${Un}:"$) Then
			Dim Cz() As String = Regex.Split(":",s)
			If Cz.Length=3 Then
				UserName=Cz(0)
				CallServer.realm=Cz(1)
				Password=Cz(2)
			End If
		End If
	Next
End Sub

private Sub SendRefuse (sResponse As ServletResponse, User As tUser)
'	Header = $"HTTP/1.0 401 Unauthorized
'	Server: HTTPd/0.9
'	Date: Sun, 10 Apr 2014 20:26:47 GMT
'	WWW-Authenticate: Digest realm="testrealm@host.com",
'	                        qop="auth,auth-int",
'	                        nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
'	                        opaque="5ccc069c403ebaf9f0171e9517f40e41"
'	Content-Type: text/html
'	Content-Length: $1.0{Body.Length}
'	"$
	Dim Body As String = $"<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Error</title>
  </head>
  <body>
    <h1>401 Unauthorized.</h1>
  </body>
</html>"$
	Dim HandShake As String = $"HTTP/1.0 401 Unauthorized
Server: HttpServer (B4X)
Date: ${CompleteDate(DateTime.Now)}
"$
	HandShake=$"${HandShake}WWW-Authenticate: Digest realm="${User.realm}", qop="auth,auth-int", nonce="${User.nonce}", opaque="${User.opaque}"
Content-Type: text/html
Content-Length: ${Body.Length}

${Body}"$
	sResponse.Write(HandShake)
	sResponse.Close
End Sub

Private Sub CompleteDate(DT As Long) As String
	Return $"${DDay(DateTime.GetDayOfWeek(DT))}, $2.0{DateTime.GetDayOfMonth(DT)} ${MMmonth(DateTime.GetMonth(DT)).SubString2(0,3)} ${DateTime.GetYear(DT)} ${DateTime.Time(DT)} GMT"$
	'Return DateTime.Date(DT) & " GMT"
End Sub

Private Sub BrowserTimeOut
	Sleep(Timeout)
	If LastBrowserComunicate+Timeout=0 Then
		Log($"TimeOut: ${ID}"$)
		Dim Body As String = $"<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Error</title>
  </head>
  <body>
    <h1>401 Unauthorized.</h1>
  </body>
</html>"$
		Dim HandShake As String = $"HTTP/1.0 408 Request Time-out
Date: ${CompleteDate(DateTime.Now)}
Content-Type: text/html
Content-Length: ${Body.Length}

${Body}"$
		Response.Write(HandShake)
		Close
	End If
End Sub

Private Sub CallEvent
	If CallServer.DigestAuthentication And RequestURI.StartsWith(CallServer.DigestPath) Then
		ElaborateHandSkakeDigestMessage(Me,Response)
	Else
		If SubExists2(mCallBack,mEventName & "_Handle",2) Then 
			CallSub3(mCallBack,mEventName & "_Handle",Me,Response)
		End If
		If ConnectionAlive=False Then Close
	End If
End Sub

Private Sub encode(S As String) As String 'ignore
	Dim Addr As String = ""
	For i=0 To S.Length-1
		'!"#$%&'()*+,-./0,
		If Asc(S.CharAt(i))>=44 And Asc(S.CharAt(i))<=122 Or Asc(S.CharAt(i))=38 Then
			Addr=Addr & S.CharAt(i)
		Else
			Addr=Addr & "%" &  DecToHex(Asc(S.CharAt(i)))
		End If
	Next
	
	Return Addr
End Sub

Private Sub decode(S As String) As String
	Dim dec As String = ""
	For i=0 To S.Length-1
		'!"#$%&'()*+,-./0,
		If Asc(S.CharAt(i))=37 Then
			Dim Ascii As Int = ((Asc(S.CharAt(i+1))-48)*16) + (Asc(S.CharAt(i+2))-48)
			dec= dec & Chr(Ascii)
			i=i+3
		Else
			dec=dec & S.charat(i)
		End If
	Next
	
	Return dec
End Sub

Private Sub webSocketKey(Key As String) As String
	Dim sha As MessageDigest
	Log("HandShake WebSocket ID: " & ID)
	LgC(Key,0xFF000F00)

	Key=Key & "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
	Dim data() As Byte = sha.GetMessageDigest(Key.GetBytes(CharacterEncoding), "SHA-1")
	LgC(su.EncodeBase64(data),0xFF000F00)
	Return su.EncodeBase64(data)
End Sub

Private Sub SendAcceptKeyWs
	Dim AcpDef As String = "" '"Sec-WebSocket-Extensions: permessage-deflate"  & CRLF
	Dim Sw As String = $"HTTP/1.1 101 Switching Protocols
Date: ${CompleteDate(DateTime.Now)}
Connection: Upgrade
Upgrade: WebSocket
Sec-WebSocket-Accept: ${webSocketKey(keyWebSocket)}
Server: HttpServer (B4X)
${AcpDef}
"$
	'no deflate cmpress
	'Sec-WebSocket-Extensions: permessage-deflate
	'Sec-WebSocket-Version:
	'Sec-WebSocket-Protocol: chat
	Response.Write(Sw)

	If SubExists2(mCallBack,mEventName & "_SwitchToWebSocket",2) Then CallSub3(mCallBack,mEventName & "_SwitchToWebSocket",Me,Response)
End Sub

Private Sub ElaborateWebSocket(Data() As Byte)
' 0                   1                   2                   3
' 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
'+-+-+-+-+-------+-+-------------+-------------------------------+
'|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
'|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
'|N|V|V|V|       |S|             |   (If payload len==126/127)   |
'| |1|2|3|       |K|             |                               |
'+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
'|     Extended payload length continued, If payload len == 127  |
'+ - - - - - - - - - - - - - - - +-------------------------------+
'|                               |Masking-key, If MASK set To 1  |
'+-------------------------------+-------------------------------+
'| Masking-key (continued)       |          Payload Data         |
'+-------------------------------- - - - - - - - - - - - - - - - +
':                     Payload Data continued ...                :
'+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
'|                     Payload Data continued ...                |
'+---------------------------------------------------------------+
	If Data.Length>=8 Then ' is frame
		Dim Fin As Boolean = (Bit.And(Data(0),128)>0)
		Dim DeflateCompress As Boolean  = (Bit.And(Data(0),64)>0)
		Dim OpCode As Byte = Bit.And(Data(0),15)
		Dim Masked As Boolean = (Bit.And(Data(1),128)>0)
		Dim Maskingkey(4) As Byte
		Dim Payload As Byte = Bit.And(Data(1),127)
		Dim Startdate As Int = 0
		Dim Lng As Long
		
		If OpCode>0 Then LastOpCode=OpCode ' opcode continue
		WebSocketString=""
		
		LgC("____________________________________",0xFFC0C0C0)
		LgC("Fin: " & Fin,0xFFC0C0C0)
		LgC("OpCode: " & OpCode,0xFFC0C0C0)
		LgC("Mask: " & Masked,0xFFC0C0C0)
		LgC("Payload: " & Payload,0xFFC0C0C0)
		
		If Payload<126 Then 
			' length 0-125
			Lng=Payload
			Startdate=2
		else if Payload=126 Then 
			' lenght
			Lng = Data(2) *256 + Data(3)
			Startdate=4
		Else if Payload = 127 Then
			Dim bc As ByteConverter
			Lng =  bc.IntsFromBytes(Array As Byte(Data(2),Data(3),Data(4),Data(5)))(0)
			Startdate=6
		End If
		If Masked Then 
			Bit.ArrayCopy(Data,Startdate,Maskingkey,0,4)
			Startdate=Startdate+4
		End If
		BBB=ArrayAppend(BBB,DeMasked(Data,Maskingkey,Lng,Startdate,Masked))
		
		If Fin Then
			If DeflateCompress Then  ' deflate header
				BBB=ArrayInsert(BBB,0,Array As Byte(120,156)) '78,9C
				Dim DataClear() As Byte = InflateDate(ArrayCopy(BBB))
			Else
				Dim DataClear() As Byte = ArrayCopy(BBB)
			End If
			'opcode; 0-continue; 1-text; 2-bin; 8-close; 9-ping; 10-pong;
			Select LastOpCode
				Case 0 'continue
				
				Case 1 ' text
					Try
						WebSocketString=BytesToString(DataClear,0,DataClear.Length,"UTF8")
						If SubExists2(mCallBack,mEventName & "_HandleWebSocket",2) Then CallSub3(mCallBack,mEventName & "_HandleWebSocket",Me,Response)
					Catch
						WebSocketString=""
						Log(LastException)
					End Try
				Case 2 ' bin
					Try
						WebSocketString=BytesToString(DataClear,0,DataClear.Length,"UTF8")
						If SubExists2(mCallBack,mEventName & "_HandleWebSocket",2) Then CallSub3(mCallBack,mEventName & "_HandleWebSocket",Me,Response)
					Catch
						WebSocketString=""
						Log(LastException)
					End Try
				Case 8 ' close
					Dim CloseCode As Int = DataClear(0)*256+DataClear(1)
					Dim CloseMessage As String =""
					' 1000-Normal; 1001-uscito pagina; 1002-chiuso conenssione;1003-accetta endpoint
					Try
						CloseMessage=BytesToString(DataClear,2,DataClear.Length-2,"UTF8")
					Catch
						Log(LastException)
					End Try
					If SubExists2(mCallBack,mEventName & "_WebSocketClose",2) Then CallSub3(mCallBack,mEventName & "_WebSocketClose",CloseCode,CloseMessage)
				Case 9 'Ping
					Response.SendWebSocketPong
				Case 10 'Pong
				
				Case Else
					Log("OpCode unknown "& LastOpCode)
			End Select
			BBB=ArrayInitialize
		End If
		
	End If
End Sub

Private Sub DeMasked(MaskData() As Byte, Maskingkey() As Byte, Length As Long, Spacement As Long,Masked As Boolean) As Byte()
	Dim DataFree(Length) As Byte
	If Masked Then
		'j = i Mod 4
		'transformed-octet-i = original-octet-i XOR masking-key-octet-j
		For i=0 To Length-1
			Dim Smask As Int = Bit.And(0xFF, MaskData(i+Spacement))
			DataFree(i)=Bit.Xor(Smask,Bit.And(0xFF,Maskingkey(i Mod 4)))
		Next
	Else
		Bit.ArrayCopy(MaskData,Spacement,DataFree,0,Length)
	End If
	Return DataFree
End Sub

Private Sub InflateDate(data() As Byte) As Byte()
	#IF B4I
'		Dim Cd As CompressedStreams
'		Return Cd.DecompressBytes(data,"zlib")
		Dim NativeMe As NativeObject = Me
		Dim infd As Object = NativeMe.RunMethod("gzipInflate:", Array (NativeMe.ArrayToNSData(data)))
		Return NativeMe.ArrayToNSData(infd)
	#Else
		Try
			Dim Inflater As JavaObject
			Inflater.InitializeNewInstance("java.util.zip.Inflater", Null)
			Inflater.RunMethod("setInput", Array(data))
			Dim raw(1024) As Byte
			Dim Read As Int = Inflater.RunMethod("inflate", Array(raw))
			'Log(BytesToString(raw,0,READ,"utf8"))
			Dim Inft(Read) As Byte
			Bit.ArrayCopy(raw,0,Inft,0,Read)
			Return Inft
		Catch
			Log("_________________________" & LastException.Message)
			Return data
		End Try
	#End If
End Sub

Private Sub LgC(Message As String, Color As Int) 'ignore
	#If B4J 
	If LogActive Then Log(Message)
	#Else
	If LogActive Then LogColor(Message,Color)
	#End If
End Sub

Private Sub LgP(Message As String)
	If LogPrivate Then Log(Message)
End Sub

#Region Buffer Operation

Private Sub SubExists2(CallObject As Object, EventName As String, Param As Int) As Boolean 'ignore
	#IF B4i
	Return SubExists(CallObject,EventName, Param)
	#Else
	Return SubExists(CallObject,EventName)
	#END IF
End Sub

Private Sub ArrayInitialize() As Byte()
	Return Array As Byte()
End Sub

Private Sub ArrayAppend(Data1() As Byte,Data2() As Byte) As Byte()
	Dim Fn(Data1.Length+Data2.Length) As Byte
	Bit.ArrayCopy(Data1,0,Fn,0,Data1.Length)
	Bit.ArrayCopy(Data2,0,Fn,Data1.Length,Data2.Length)
	Return Fn
End Sub

Private Sub ArrayCopy(Data() As Byte) As Byte()
	Dim Fn(Data.Length) As Byte
	Bit.ArrayCopy(Data,0,Fn,0,Data.Length)
	Return Fn
End Sub

Private Sub ArrayIndexOf(Data() As Byte,SearchData() As Byte) As Int
	For i = 0 To Data.Length - SearchData.Length
		If SearchData(0) = Data(i) Then
			For sindex = 0 To SearchData.Length - 1
				If SearchData(sindex) <> Data(i + sindex) Then
					Exit
				End If
			Next
			If sindex = SearchData.Length Then
				Return i
			End If
		End If
	Next
	Return -1
End Sub

Public Sub ArrayInsert(DataSource() As Byte,Index As Int, DataInsert() As Byte) As Byte()
	Dim Fn(DataSource.Length+DataInsert.Length) As Byte
	
	If Index>0 Then Bit.ArrayCopy(DataSource,0,Fn,0,Index-1)
	Bit.ArrayCopy(DataInsert,0,Fn,Index,DataInsert.Length)
	If DataSource.Length-Index>0 Then Bit.ArrayCopy(DataSource,Index,Fn,DataInsert.Length+Index,DataSource.Length-Index)
	
	Return Fn
End Sub

Public Sub ArrayRemove(Data() As Byte,Start As Int, Last As Int) As Byte()
	Dim EraseQ As Int = Last-Start
	Dim Fn(Data.Length-EraseQ) As Byte
	
	If Data.Length-EraseQ=0 Then Return Array As Byte()
	
	If Start>0 Then 
		Bit.ArrayCopy(Data,0,Fn,0,Start)
	End If
	If Last<Data.Length Then 
		Bit.ArrayCopy(Data,Last,Fn,Start,Data.Length-Last)
	End If

	Return Fn
End Sub

Private Sub SubArray(Data() As Byte, Pos As Int) As Byte()
	Dim Fn(Data.Length-Pos) As Byte
	
	Bit.ArrayCopy(Data,Pos,Fn,0,Data.Length-Pos)
	Return Fn
End Sub

Public Sub SubArray2(Data() As Byte,Start As Int, Last As Int) As Byte()
	Dim fn(Last - Start) As Byte
	Bit.ArrayCopy(Data, Start, fn, 0, fn.Length)
	Return fn
End Sub

#End Region