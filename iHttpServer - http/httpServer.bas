﻿B4i=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6.8
@EndOfDesignText@
#Event: NewConection (req As ServletRequest)
#Event: Handle (req As ServletRequest, resp As ServletResponse)
'#Event: UploadProgress (resp As ServletResponse, Progress As float) ' Progress = 0-1
#Event: UploadedFile (req As ServletRequest, resp As ServletResponse)
#Event: SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
#Event: HandleWebSocket (req As ServletRequest, resp As ServletResponse)
#Event: WebSocketClose(CloseCode As Int, CloseMessage As String)

Sub Class_Globals
	Private Users As Map
	Private Serv As ServerSocket
	
	Private Work As Boolean = False
	Private mCallBack As Object
	Private mEventName As String
	
	'Public DigestAuthentication As Boolean = True
	Public Const DigestAuthentication As Boolean = False
	Public DigestPath As String = "/"
	Public realm As String = ""
	Public htdigest As List
	Public IgnoreNC As Boolean = False
	Public Timeout As Int = 5000
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize(CallBack As Object, EventName As String)
	Users.Initialize
	mCallBack=CallBack
	mEventName=EventName
	
	htdigest.Initialize
End Sub

' eg. Start(51051)
Public Sub Start(Port As Int)
	Serv.Initialize(Port,"Serv")

	Work=True
	Do While Work
		Serv.Listen
		Wait For Serv_NewConnection (Successful As Boolean, NewSocket As Socket)

		If Successful Then
			Dim SR As ServletRequest
			SR.Timeout=Timeout
			SR.Initialize(Me,"Data",NewSocket)
			#IF B4I
			If SubExists(mCallBack,mEventName & "_NewConection",1) Then CallSub2(mCallBack,mEventName & "_NewConection",SR)
			#Else
			If SubExists(mCallBack,mEventName & "_NewConection") Then CallSub2(mCallBack,mEventName & "_NewConection",SR)
			#END IF
		End If
	Loop
End Sub

Public Sub Stop
	Work=False
End Sub

Public Sub GetMyIP As String
	Return Serv.GetMyIP
End Sub

Public Sub GetMyWifiIp As String
	#If B4J
	Return Serv.GetMyIP
	#Else
	Return Serv.GetMyWifiIp
	#End if
End Sub

Public Sub getTempPath As String
	#If B4A
	Return File.DirInternalCache
	#Else IF B4i or B4J
	Return File.DirTemp
	#End If
End Sub

Private Sub Data_Handle(Req As ServletRequest, Res As ServletResponse)
	If SubExists2(mCallBack,mEventName & "_Handle",2) Then CallSub3(mCallBack,mEventName & "_Handle",Req,Res)
End Sub

'Private Sub Data_UploadProgress(resp As ServletResponse, Progress As Float)
'	If SubExists2(mCallBack,mEventName & "_UploadProgress",2) Then CallSub3(mCallBack,mEventName & "_UploadProgress",resp,Progress)
'End Sub

Private Sub Data_UploadedFile(req As ServletRequest, resp As ServletResponse)
	If SubExists2(mCallBack,mEventName & "_UploadedFile",2) Then CallSub3(mCallBack,mEventName & "_UploadedFile",req,resp)
End Sub

Private Sub Data_SwitchToWebSocket (req As ServletRequest, resp As ServletResponse)
	If SubExists2(mCallBack,mEventName & "_SwitchToWebSocket",2) Then CallSub3(mCallBack,mEventName & "_SwitchToWebSocket",req,resp)
End Sub

Private Sub Data_HandleWebSocket (req As ServletRequest, resp As ServletResponse)
	If SubExists2(mCallBack,mEventName & "_HandleWebSocket",2) Then CallSub3(mCallBack,mEventName & "_HandleWebSocket",req,resp)
End Sub

Private Sub Data_WebSocketClose(CloseCode As Int, CloseMessage As String)
	If SubExists2(mCallBack,mEventName & "_WebSocketClose",2) Then CallSub3(mCallBack,mEventName & "_WebSocketClose",CloseCode,CloseMessage)
End Sub

Private Sub SubExists2(CallObject As Object, EventName As String, Param As Int) As Boolean 'ignore
	#IF B4i
	Return SubExists(CallObject,EventName, Param)
	#Else
	Return SubExists(CallObject,EventName)
	#END IF
End Sub