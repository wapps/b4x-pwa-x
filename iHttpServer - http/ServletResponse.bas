﻿B4i=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6.8
@EndOfDesignText@
Sub Class_Globals
	Private Request As ServletRequest
	Private astream As AsyncStreams  
	
	Private ResponseHeader As Map
	Private ResponseParameter As Map
	Private ResponseCookies As Map
	Private Code As Map 
	Private MIME As Map
	Private Client As Socket
	
	Public Status As Int = 200
	Public ContentType As String = ""
	Public CharacterEncoding As String = "UTF-8" '"ISO-8859-1,UTF-8;"
	Public ContentLenght As Int = 0

	Private DDay() As String = Array As String("","Sun","Mon","Tue","Wed","Thu","Fri","Sat")
	Private MMmonth() As String = Array As String("","January","February","March","April","May","June","July","August","September","October","November","December")
	
	Private BC As ByteConverter
	Private myQuery As QueryElement
	Private Final() As Byte
	
	'-----------RESPONSE
	'ContentLenght
	'SendRedirect, SendError, Write, 
	'OutputStrem
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize(Req As ServletRequest, ast As AsyncStreams, Sck As Socket)
	Request=Req
	astream=ast
	Client=Sck
	
	ResponseHeader.Initialize
	ResponseParameter.Initialize
	ResponseCookies.Initialize
	
	ResponseHeader.Put("Accept-Ranges","bytes")
	ResponseHeader.Put("Server","b4x (0.0.1)")
	ResponseHeader.Put("Accept-Charset","utf-8")
	ResponseHeader.Put("Cache-control","no-cache")
	'ResponseHeader.Put("Connection","close") 'Keep-Alive
	
	myQuery.Initialize(Me)
	
	'1xx-Info, 2xx-Success, 3xx-Redirect, 4xx-Client error, 5xx-Server error
	Code=CreateMap(100:"Continue", 101:"Switching Protocols" ,200:"OK",201: "Created", 202:"Accepted", 203:"Non-Authoritative Informatio", _
	204:"No Content", 205:"Reset Content", 206:"Partial Content",300:"Multiple Choices", 301:"Moved Permanently", _
	302:"Found", 303:"See Other", 304:"Not Modified", 305:"Use Proxy", 307:"Temporary Redirect", 400:"Bad Request", _
	401:"Unauthorized", 402:"Payment Required", 403:"Forbidden", 404:"Not Found", 405:"	Method Not Allowed", 406:"Not Acceptable", _
	407:"Proxy Authentication Required", 408:"Request Time-out", 409:"Conflict", 410:"Gone", 411:"Length Required", _
	412:"Precondition Failed", 413:"Request Entity Too Large", 414:"Request-URI Too Large", 415:"Unsupported Media Type", _
	416:"Requested range not satisfiable", 417:"Expectation Failed", 500:"Internal Server Error", 502:"Bad Gateway", _
	503:"Service Unavailable", 504:"Gateway Time-out", 505:"HTTP Version Not Supported.")
	
	MIME=CreateMap("htm":"text/html","html":"text/html","xml":"text/xml","txt":"text/plain","pdf":"application/pdf", _ 
	"css":"text/css", "csv":"text/csv", "js":"text/javascript", "json":"application/json", _
	"doc":"application/msword", "docx":"application/vnd.openxmlformats-officedocument.wordprocessingml.document", _
	"xls":"application/vnd.ms-excel", "xlsx":"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", _
	"odt":"application/vnd.oasis.opendocument.text", "ods":"application/vnd.oasis.opendocument.spreadsheet", _
	"epub":"application/epub+zip", "jar":"application/java-archive", "rar":"application/vnd.rar", "zip":"application/zip", _
	"jpeg":"image/jpeg","jpg":"image/jpeg","bmp":"image/bmp","png":"image/png", "gif":"image/gif", _
	"ico":"image/vnd.microsoft.icon", _
	"mp3":"audio/mpeg", _
	"avi":"video/x-msvideo", "mpeg":"video/mpeg")
End Sub

Public Sub SetHeader(Name As String, Value As String)
	ResponseHeader.Put(Name,Value)
End Sub

' Set Cokies values on Browser
Public Sub SetCookies(Name As String,Value As String)
	ResponseCookies.Put(Name,Value)
End Sub

Public Sub ResetCookies
	ResponseCookies.Clear
End Sub

Public Sub Close
	Try
		Request.Close
	Catch
		Log("Server error:" & LastException.Message)
	End Try
End Sub

Public Sub getQuery As QueryElement
	Return myQuery
End Sub

Public Sub Connected As Boolean
	Return Client.Connected
End Sub

Public Sub getOutputStream As OutputStream
	Return Client.OutputStream
End Sub

' Sending text without header to dynamically send more text after the SendString
Public Sub Write(Text As String)
	astream_write(Text.GetBytes(CharacterEncoding))
End Sub

' sending text with Header
Public Sub SendString(Text As String)
	If ContentType="" Then ContentType=$"text/html; charset=${CharacterEncoding};"$
	Dim B() As Byte = Text.GetBytes(CharacterEncoding)
	ContentLenght=B.Length
	Dim S As String = HandShakeString & Text
	astream_write(s.GetBytes(CharacterEncoding))
End Sub

Public Sub SendRaw(Data() As Byte)
	astream_write(Data)
End Sub

' don't use DirAssets
Public Sub SendFile(Dir As String, fileName As String)
	If File.Exists(Dir, fileName) Then 
		Dim FileSize As Long = File.Size(Dir, fileName)
		Dim lastModified As String = CompleteDate(File.LastModified(Dir, fileName))
		
		ContentType=SearchMime(fileName)
		astream_write(HandShakeFile(lastModified,FileSize).GetBytes(CharacterEncoding))
		astream_write(File.ReadBytes(Dir, fileName))
	Else
		SendNotFound(fileName)
	End If
End Sub

Public Sub SendFile2(Dir As String, fileName As String, Content_Type As String)
	If File.Exists(Dir, fileName) Then
		Dim FileSize As Long = File.Size(Dir, fileName)
		Dim lastModified As String = CompleteDate(File.LastModified(Dir, fileName))
		
		ContentType=Content_Type
		astream_write(HandShakeFile(lastModified,FileSize).GetBytes(CharacterEncoding))

		astream_write(File.ReadBytes(Dir, fileName))
	Else
		SendNotFound(fileName)
	End If
End Sub

Public Sub SendRedirect(Address As String)
	Dim HandShake As String = ""
	
	'Header
	For Each K As String In ResponseHeader.Keys
		HandShake=$"${HandShake}${k}: ${ResponseHeader.Get(K)}${CRLF}"$
	Next
	'Cookies
	Dim Scks As String = ""
	For Each K As String In ResponseCookies.Keys
		Scks=$"${Scks}${k}=${ResponseCookies.Get(K)}; "$
	Next
	If Scks<>"" Then Scks=$"Set-Cookie: ${Scks}${CRLF}Expires=${CompleteDate(DateTime.Now+(DateTime.TicksPerDay*365))}${CRLF}"$

	
	Dim S As String = $"HTTP/1.1 302 Found
Date: ${CompleteDate(DateTime.Now)}
Location: ${Address}
Content-Length: 0
Server: HttpServer (B4X)
${HandShake}${Scks}
"$
	astream_write(S.GetBytes(CharacterEncoding))
End Sub

Public Sub SendNotFound(filenameNotFound As String)
	Dim S As String = $"HTTP/1.1 404 Not Found
Date: ${CompleteDate(DateTime.Now)}
Cache-Control: must-revalidate,no-cache,no-store
Content-Type: text/html; charset=${CharacterEncoding};
Content-Length: 320
Server: HttpServer (B4X)

<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<title>Error 404 Not Found</title>
</head>
<body><h2>HTTP ERROR 404</h2>
<p>Problem accessing ${filenameNotFound}. Reason:
<pre>    Not Found</pre></p>
</body>
</html>"$
	astream_write(S.GetBytes(CharacterEncoding))
End Sub

' -------------------------------------- Private -----------------------------------------------------

Private Sub CompleteDate(DT As Long) As String
	Return $"${DDay(DateTime.GetDayOfWeek(DT))}, $2.0{DateTime.GetDayOfMonth(DT)} ${MMmonth(DateTime.GetMonth(DT)).SubString2(0,3)} ${DateTime.GetYear(DT)} ${DateTime.Time(DT)} GMT"$
End Sub

Private Sub StatusCode(Sts As Int) As String
	If Code.ContainsKey(Sts) Then 
		Return Code.Get(Sts)
	Else
		Return ""
	End If
End Sub

Private Sub HandShakeString As String
	Dim HandShake As String = ""
	
	'Header
	For Each K As String In ResponseHeader.Keys
		HandShake=$"${HandShake}${k}: ${ResponseHeader.Get(K)}${CRLF}"$
	Next
	'Cookies
	Dim Scks As String = ""
	For Each K As String In ResponseCookies.Keys
		Scks=$"${Scks}${k}=${ResponseCookies.Get(K)}; "$
	Next
	If Scks<>"" Then Scks=$"Set-Cookie: ${Scks}${CRLF}Expires=${CompleteDate(DateTime.Now+(DateTime.TicksPerDay*365))}${CRLF}"$
	
	' Complete Header
	HandShake=$"HTTP/1.1 ${Status} ${StatusCode(Status)}
Server: HttpServer (B4X)
Date: ${CompleteDate(DateTime.Now)}
Last-Modified: ${CompleteDate(DateTime.Now)}
Content-Type: ${ContentType}; charset=${CharacterEncoding}
${HandShake}${Scks}Content-Length: ${ContentLenght}

"$
	Return HandShake
End Sub

Private Sub HandShakeFile(lastModified As String, FileSize As Long) As String
	Dim HandShake As String = ""
	
	'Header
	For Each K As String In ResponseHeader.Keys
		HandShake=$"${HandShake}${k}: ${ResponseHeader.Get(K)}${CRLF}"$
	Next
	'Cookies
	Dim Scks As String = ""
	For Each K As String In ResponseCookies.Keys
		Scks=$"${Scks}${k}=${ResponseCookies.Get(K)}; "$
	Next
	If Scks<>"" Then Scks=$"Set-Cookie: ${Scks}${CRLF}Expires=${CompleteDate(DateTime.Now+(DateTime.TicksPerDay*365))}${CRLF}"$
	
	HandShake=$"HTTP/1.1 200 OK
Date: ${CompleteDate(DateTime.Now)}
Last-Modified: ${lastModified}
Content-Type: ${ContentType}
Accept-Ranges: bytes
Content-Length: ${FileSize}
Server: HttpServer (B4X)
${HandShake}${Scks}
"$
	Return HandShake.Replace(CRLF,Chr(13) & Chr(10))
End Sub

private Sub SearchMime(Nm As String) As String
	If Nm.IndexOf(".")>-1 Then 
		Nm=Nm.ToLowerCase.SubString(Nm.IndexOf(".")+1)
		If MIME.ContainsKey(Nm) Then 
			Return MIME.Get(Nm)
		End If
	End If
	
	Return "application/*.*"
End Sub

Private Sub astream_write(Data() As Byte)
	Try
		astream.Write(Data)
	Catch
		Log(LastException)
	End Try
End Sub


'--------------------------------------------- WEB SOCKET ------------------------------------------

' 0                   1                   2                   3
' 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
'+-+-+-+-+-------+-+-------------+-------------------------------+
'|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
'|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
'|N|V|V|V|       |S|             |   (If payload len==126/127)   |
'| |1|2|3|       |K|             |                               |
'+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
'|     Extended payload length continued, If payload len == 127  |
'+ - - - - - - - - - - - - - - - +-------------------------------+
'|                               |Masking-key, If MASK set To 1  |
'+-------------------------------+-------------------------------+
'| Masking-key (continued)       |          Payload Data         |
'+-------------------------------- - - - - - - - - - - - - - - - +
':                     Payload Data continued ...                :
'+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
'|                     Payload Data continued ...                |
'+---------------------------------------------------------------+
'opcode 0-continue; 1-text; 2-bin; 8-close; 9-ping; 10-pong;

'Cmpressed as Deflate=zlib, gzip, none - (set always none)
Public Sub SendWebSocketString(Text As String, Masked As Boolean, Compressed As String)
	Compressed=""
	Dim PayLoad() As Byte 
	If Compressed<>"none" And Compressed<>"" Then 
		PayLoad=DeflateDate(Text.GetBytes("UTF8"))
	Else
		PayLoad= Text.GetBytes("UTF8")
	End If

	Final= Array As Byte()
	If PayLoad.Length<126 Then
		Dim Head(2) As Byte
		Head(0)=129 ' Final+ Compressed (64-100) + Text 
		Head(1)=PayLoad.Length
	else if PayLoad.Length<65536 Then 
		Dim Head(4) As Byte
		Head(0)=129 ' Final+ reserved (64-100) + Text
		Head(1)=126 ' 2 Byte
		Head(2)=BC.IntsToBytes(Array As Int(PayLoad.Length))(2)
		Head(3)=BC.IntsToBytes(Array As Int(PayLoad.Length))(3)
	Else
		Dim Head(10) As Byte
		Head(0)=129 ' Final+ reserved (64-100) + Text
		Head(1)=127 ' 4 byte
		Head(2)=BC.IntsToBytes(Array As Int(PayLoad.Length))(0)
		Head(3)=BC.IntsToBytes(Array As Int(PayLoad.Length))(1)
		Head(4)=BC.IntsToBytes(Array As Int(PayLoad.Length))(2)
		Head(5)=BC.IntsToBytes(Array As Int(PayLoad.Length))(3)
		Head(6)=0
		Head(7)=0
		Head(8)=0
		Head(9)=0
	End If
	If Compressed<>"" And Compressed<>"none" Then Head(0)=Bit.Or(129,64)
	
	If Masked Then 
		Head(1)=Bit.Or(128,Head(1))
		Dim MaskKey(4) As Byte
		For i=0 To 3
			MaskKey(0)=Rnd(0,256)
		Next
		AppenFinal(MaskKey)
		AppenFinal(DataMask(PayLoad,MaskKey))
		'Final.Append(MaskKey)
		'Final.Append(DataMask(PayLoad,MaskKey))
	Else
		AppenFinal(Head)
		AppenFinal(PayLoad)
	End If
	
	astream_write(Final)
End Sub

Public Sub SendWebSocketBinary(Data() As Byte, Masked As Boolean)
	
End Sub

Public Sub SendWebSocketClose
	Dim B(2) As Byte
	B(0)=Bit.Or(128,8)
	B(1)=0
	
	astream_write(B)
End Sub

Public Sub SendWebSocketPing
	Dim B(2) As Byte
	B(0)=Bit.Or(128,9)
	B(1)=0
	
	astream_write(B)
End Sub

Public Sub SendWebSocketPong
	Dim B(2) As Byte
	B(0)=Bit.Or(128,10)
	B(1)=0
	
	astream_write(B)
End Sub

Private Sub DataMask(Data() As Byte, Maskingkey() As Byte) As Byte()
	'j = i Mod 4
	'transformed-octet-i = original-octet-i XOR masking-key-octet-j
	Dim DataFree(Data.Length) As Byte
	For i=0 To Data.Length-1
		Dim Smask As Int = Bit.And(0xFF, Data(i))
		DataFree(i)=Bit.Xor(Smask,Bit.And(0xFF,Maskingkey(i Mod 4)))
	Next
	
	Return DataFree
End Sub

Private Sub DeflateDate(data() As Byte) As Byte()
	#IF B4I
		'Dim Deflate As CompressedStreams
		'PayLoad=Deflate.CompressBytes(Text.GetBytes("UTF8"),Compressed)
		Dim NativeMe As NativeObject = Me
		Dim infObject As Object = NativeMe.RunMethod("gzipInflate:", Array (NativeMe.ArrayToNSData(data)))
		Dim raw() As Byte= NativeMe.NSDataToArray(infObject)
		Dim Inft(raw.Length-2) As Byte
		Bit.ArrayCopy(raw,2,Inft,0,raw.Length-2)
	Return Inft
	#else
		Try
			Dim Deflater As JavaObject
			Deflater.InitializeNewInstance("java.util.zip.Deflater", Null)
			Deflater.RunMethod("setInput", Array(data))
			Dim raw(1024) As Byte
			Dim Read As Int = Deflater.RunMethod("deflate", Array(raw))
			'Log(BytesToString(raw,0,READ,"utf8"))
			Dim Inft(Read-2) As Byte
			Bit.ArrayCopy(raw,2,Inft,0,Read-2)
			Return Inft
		Catch
			Log("_________________________" & LastException.Message)
			Return data
		End Try
	#End If
End Sub

Private Sub AppenFinal(B() As Byte)
	Dim Fn(Final.Length+b.Length) As Byte
	Bit.ArrayCopy(Final,0,Fn,0,Final.Length)
	Bit.ArrayCopy(B,0,Fn,Final.Length,B.Length)
	Final=Fn
End Sub

