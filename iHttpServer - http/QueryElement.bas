﻿B4i=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=7.2
@EndOfDesignText@
#Event: click (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: change (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: dblclick (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: focus (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: focusin (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: focusout (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: keyup (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: mousedown (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: mouseenter (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: mouseleave (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: mousemove (Resp As ServletResponse,Params As Map) ' QueryElement Event Click
#Event: mouseup (Resp As ServletResponse,Params As Map) ' QueryElement Event Click

Sub Class_Globals
	Public Const Event_change As String = "change"
	Public Const Event_click As String = "click"
	Public Const Event_dblclick As String = "dblclick"
	Public Const Event_focus As String = "focus"
	Public Const Event_focusin As String = "focusin"
	Public Const Event_focusout As String = "focusout"
	Public Const Event_keyup As String = "keyup"
	Public Const Event_mousedown As String = "mousedown"
	Public Const Event_mouseenter As String = "mouseenter"
	Public Const Event_mouseleave As String = "mouseleave"
	Public Const Event_mousemove As String = "mousemove"
	Public Const Event_mouseup As String = "mouseup"
	
	Public const NoEvent() As Map=Array As Map()
	
	Private Resp As ServletResponse
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize(Response As ServletResponse)
	Resp=Response
End Sub

Public Sub SetCommand (etype As String,Method As String,property As String, ID As String, Params As List, Arg As List) As String
	Dim J As JSONGenerator
	Dim M As Map
	M.Initialize
	M.Put("etype",etype)
	If Method<>"" Then M.Put("method",Method)
	If ID<>"" Then M.Put("id",ID)
	If property<>"" Then M.Put("prop",property)
	If Params.IsInitialized Then M.Put("params",Params)
	If Arg.IsInitialized Then M.Put("value",Arg)
	J.Initialize(M)
	'Log(J.ToString)
	Return J.ToString
End Sub

Public Sub setAutomaticEvents(Arg() As Map)
	Dim J As JSONGenerator
	Dim M As Map
	M.Initialize
	M.Put("etype","setAutomaticEvents")
	M.Put("data",Arg)
	J.Initialize(M)
	'Log(J.ToString)
	Resp.SendWebSocketString(J.ToString,False,"")
End Sub

' Param = list or array: array as Map or String (array as Object is wrong)
Public Sub RunFunction (function As String, ID As String, Params As List)
	Resp.SendWebSocketString(SetCommand("runFunction","",function,ID,Null,Params),False,"")
End Sub

Public Sub RunFunctionWithResult (function As String, ID As String, Params As List)
	Resp.SendWebSocketString(SetCommand("runFunctionWithResult","",function,ID,Null,Params),False,"")
End Sub

' Param = list or array: array as Object is wrong - array as Map is correct
Public Sub RunMethod (Method As String, ID As String, Params As List)
	Resp.SendWebSocketString(SetCommand("runmethod",Method,"",ID,Params,Null),False,"")
End Sub

' Param = list or array: array as Object is wrong - array as Map is correct
Public Sub RunMethodWithResult (Method As String, ID As String, Params As List)
	Resp.SendWebSocketString(SetCommand("runmethodWithResult",Method,"",ID,Params,Null),False,"")
End Sub

Public Sub Eval(Script As String, Params As List)
	Resp.SendWebSocketString(SetCommand("eval","",Script,"",Null,Params),False,"")
End Sub

Public Sub EvalWithResult(Script As String, Params As List)
	Resp.SendWebSocketString(SetCommand("evalWithResult","",Script,"",Null,Params),False,"")
End Sub

Public Sub CreateEvent(ObjectName As String,Event As String, OtherEvent() As Map) As Map()
	If isnull(OtherEvent)=False Then
		Dim Ev(OtherEvent.Length+1) As Map
		For i=0 To OtherEvent.Length-1
			Ev(i)=OtherEvent(i)
		Next
		Ev(OtherEvent.Length).Initialize
		Ev(OtherEvent.Length).Put("id",ObjectName)
		Ev(OtherEvent.Length).Put("event",Event)
	Else
		Dim Ev(1) As Map
		Ev(0).Initialize
		Ev(0).Put("id",ObjectName)
		Ev(0).Put("event",Event)
	End If
	
	Return Ev
End Sub

Private Sub isnull(O As Object) As Boolean
	Return (o = Null)
End Sub

' not active
Private Sub setAutomaticEventsFromPage(Html As String) 'ignore
	Log(Html)
End Sub

'Public Sub  GetCSS (Property As String) As Object
'	
'End Sub
'	
'Public Sub GetHeight As Object
'	
'End Sub
'
'Public Sub GetHtml As Object
'	
'End Sub
'
'Public Sub GetProp (Property As String) As Object
'	
'End Sub
'
'Public Sub GetText As Object
'	
'End Sub

Public Sub GetVal (ID As String, ValueList As List)
	RunMethodWithResult("val",ID,ValueList)
End Sub

'Public Sub GetWidth As Object
'	
'End Sub
'
Public Sub SetDialog (id As String, Params As List)
	Resp.Query.RunMethod("dialog",id,Params)
End Sub

Public Sub SetCSS (id As String, Params As List)
	RunMethod("css",id,Params)
End Sub
'
'Public Sub SetHeight (Value As String)
'	
'End Sub
'
Public Sub SetHtml (id As String,  Params As List)
	RunMethod("html",id,Params)
End Sub
'	
'
Public Sub SetPropriety (Property As String, Value As List)
	RunMethod("prop",Property,Value)
End Sub

Public Sub GetPropriety (Property As String, Value As List)
	RunMethodWithResult("prop",Property,Value)
End Sub

Public Sub SetText (ID As String, TextList As List)
	RunMethod("text",ID,TextList)
End Sub

Public Sub SetVal (ID As String, ValueList As List)
	RunMethod("val",ID,ValueList)
End Sub

'Public Sub SetWidth (Value As String)
'	
'End Sub

Public Sub SelectElement (ID As String)
	Resp.SendWebSocketString(SetCommand("runmethod","select","",ID,Null,Null),False,"")
End Sub

Public Sub EscapeHtml(Raw As String) As String
	Return Raw.Replace(QUOTE,"&quot;").Replace("'","&#39;").Replace("<","&lt;").Replace("<","&gt;").Replace("&","&amp;")
End Sub